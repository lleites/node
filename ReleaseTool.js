#!/usr/bin/env node

var program = require('commander');
var util = require('util');
var mp = require("./ReleaseToolModule.js");

program.version('1.0.0')
    .option('-r, --dates [command]', 'Date period to fetch. Ex: {2013-03-13}:{2013-04-01}', '{2013-03-13}:{2013-04-01}')
    .parse(process.argv);

var command = "svn log -v http://subversion.k12.com/svn/appdev/k12_systems/learning_delivery/trunk -r " + program.dates;
util.log("Fetching SVN data....");
mp.exec(command);



