#!/usr/bin/env node

var fs = require('fs');
var util = require('util');
var mp = require("./ReleaseToolModule.js");
var assert = require('assert');
var _ = require("underscore");

var actual;
var expected;


var ReleaseToolTest = {};
 
ReleaseToolTest.assert = function (file) {
    util.log("Asserting "+file+" file....");
    actual = JSON.parse( fs.readFileSync("Test/actual/"+file) );
    expected = JSON.parse( fs.readFileSync("Test/expected/"+file) );
    assert.ok(_.isEqual(actual,expected)) ;
    util.log("OK: "+file+" file.");
};


mp.path = "Test/actual";
mp.commandCallback(undefined, fs.readFileSync("Test/source.txt").toString());

util.log("Starting assertions....");

ReleaseToolTest.assert("modifiedProjects.json");
ReleaseToolTest.assert("dependentProjects.json");
ReleaseToolTest.assert("projectsWithoutConfiguration.json");
ReleaseToolTest.assert("actions.json");

util.log("Asserting prettyPrint file....");
actual = fs.readFileSync("Test/actual/prettyPrint.txt").toString() ;
expected = fs.readFileSync("Test/expected/prettyPrint.txt").toString() ;
assert.ok(_.isEqual(actual,expected)) ;
util.log("OK: prettyPrint.txt file.");

