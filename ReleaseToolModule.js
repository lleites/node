#!/usr/bin/env node

var fs = require('fs');
var exec = require('child_process').exec;
var util = require('util');
var _ = require("underscore");



var ReleaseTool = {};


ReleaseTool.dependentProjects ={};
ReleaseTool.projectsConfigs = {};
ReleaseTool.projectsWithoutConfiguration = [];
ReleaseTool.path = module.exports.path = "results";

ReleaseTool.actions = ["Modify properties", "Release"];

ReleaseTool.causes = ["Modified Project",
                            "Modified Project with no configuration",
                            "Dependent Project",
                            "Dependent Project with no configuration"];

ReleaseTool.commandCallback = module.exports.commandCallback = function(error, stdout) {

    util.log("Loading config....");
    ReleaseTool.projectsConfigs = ReleaseTool.loadConfig();

    util.log("Finding modified projects files...");
    var modifiedProj = ReleaseTool.findModifiedProjects(stdout);


    util.log("Finding Dependent Projects....");

    for (var project in modifiedProj) {
        ReleaseTool.processProject(project, true);

    }

    util.log("Creating action List....");
    var actionList = ReleaseTool.createActionList();

    util.log("De-duplicating action list...");
    for (var actionLevel in actionList) {
        if(actionList[actionLevel]){
            actionList[actionLevel] = _.unique(actionList[actionLevel].sort(), true);
        }
    }

    util.log("Creating pretty print file list...");
    var prettyPrint = ReleaseTool.prettyPrint(actionList);

    util.log("Writing Files....");
    fs.writeFileSync(module.exports.path + '/modifiedProjects.json', JSON.stringify(modifiedProj, undefined, 5), 'utf8');
    fs.writeFileSync(module.exports.path + '/dependentProjects.json', JSON.stringify(ReleaseTool.dependentProjects, undefined, 5), 'utf8');
    fs.writeFileSync(module.exports.path + '/projectsWithoutConfiguration.json', JSON.stringify(ReleaseTool.projectsWithoutConfiguration, undefined, 5), 'utf8');
    fs.writeFileSync(module.exports.path + '/actions.json', JSON.stringify(actionList, undefined, 5), 'utf8');
    fs.writeFileSync(module.exports.path + '/prettyPrint.txt', prettyPrint, 'utf8');
    util.log('Process finished OK');
};


ReleaseTool.findModifiedProjects = function(stdout) {

    var stdoutLines = [];
    stdout.split("\n").forEach(function(line) {
        if (line.indexOf('/k12_systems/') !== -1) {
            stdoutLines.push(line);
        }
    });


    var modifiedProjects = {};

    for (var currentLineIndex = 0; currentLineIndex < stdoutLines.length; currentLineIndex++) {
        var splittedLine = stdoutLines[currentLineIndex].split("/");
        var indexOfTrunk = splittedLine.indexOf("trunk");
        if (indexOfTrunk == -1) {
            continue;
        }

        var projectName = splittedLine[indexOfTrunk + 1].split(" ")[0];

        if (projectName === undefined) {
            continue;
        }

        //if the second dir level after trunk word starts with the project name use it instead, also checks to see that its not a file
        if (splittedLine[indexOfTrunk + 2] && splittedLine[indexOfTrunk + 2].indexOf(projectName) !== -1 && (indexOfTrunk + 2) !== (splittedLine.length - 1)) {
            projectName = splittedLine[indexOfTrunk + 2];
        }


        var filePath = stdoutLines[currentLineIndex].slice(stdoutLines[currentLineIndex].indexOf("k12_systems") - 1);
        if (projectName in modifiedProjects) {
            modifiedProjects[projectName].files.push(filePath);

        }
        else {
            modifiedProjects[projectName] = {
                total: 0,
                files: [filePath]
            };
        }

    }
    
    for (var project in modifiedProjects) {
        modifiedProjects[project].files = _.unique(modifiedProjects[project].files);
        modifiedProjects[project].total = modifiedProjects[project].files.length;
    }

    return modifiedProjects;

};



ReleaseTool.prettyPrint = function(actionList) {
    var printFile = "";
    var projectCounter = 1;

    for (var i = actionList.length; i > 0; i--) {
        if (actionList[i]) {
            printFile += "\nLEVEL " + i + " ACTIONS. \n";
            var project = "";
            for (var j = 0; j < actionList[i].length; j++) {
                if (project !== actionList[i][j].split(":")[0]) {
                    project = actionList[i][j].split(":")[0];
                    printFile += "\n";
                    printFile += projectCounter + ") " + project + "\n";
                    projectCounter++;
                }
                printFile += "    " + actionList[i][j].substring(actionList[i][j].indexOf("action:") + "action:".length, actionList[i][j].indexOf(";"));
                printFile += " (" + actionList[i][j].substring(actionList[i][j].indexOf("Causes:") + "Causes:".length) + " )\n";
            }
        }
    }

    return printFile;

};

ReleaseTool.createActionList = function() {
    
    function buildAction(project, action, causes) {
        return project + ": action: " + action + "; Causes: " + causes;
    }
    
    function getCauseName(cause){
        return ReleaseTool.causes[cause];
    }
    
    var actionList = [
        [0]
    ];
    
    var actualLevel;
    var causes;
       
    
    for (var project in ReleaseTool.dependentProjects) {
        if (ReleaseTool.dependentProjects[project]) {
            /*Add him self*/
            util.log("Adding " + project + " to action list.");
            actualLevel = ReleaseTool.dependentProjects[project].level;
            if (!actionList[actualLevel]) {
                actionList[actualLevel] = [];
            }
            causes = _.unique(ReleaseTool.dependentProjects[project].causes.sort()).map(
                                                                                                getCauseName
                                                                                            ).toString();
            /*Add RELEASE*/                                                                                
            actionList[actualLevel].push(buildAction(project, ReleaseTool.actions[1], causes) );
            

            /*Add instructions*/
            if(ReleaseTool.dependentProjects[project].instructions){
                actionList[actualLevel].push(buildAction(project, ReleaseTool.dependentProjects[project].instructions, causes));
            }
            
            /*Add actions*/
            if (ReleaseTool.dependentProjects[project].actions) {
                util.log("  Adding actions from: " + project);
                for (var affectedProject in ReleaseTool.dependentProjects[project].actions) {
                    /*Using now the level of the affected project*/
                    try {
                        actualLevel = ReleaseTool.dependentProjects[affectedProject].level;
                    }
                    catch (err) {
                        util.error("Error trying to access level from : " + affectedProject + ". Check config file and add this project to the root.");
                        throw err;
                    }
                    
                    util.log("      Adding actions affecting " + affectedProject);    
                    for (var i = 0; i < ReleaseTool.dependentProjects[project].actions[affectedProject].length; i++) {

                        if (!actionList[actualLevel]) {
                            actionList[actualLevel] = [];
                        }
                        if (ReleaseTool.dependentProjects[project].actions[affectedProject][i]!==1){
                            actionList[actualLevel].push( buildAction(affectedProject, ReleaseTool.actions[ReleaseTool.dependentProjects[project].actions[affectedProject][i]], ReleaseTool.causes[2]) );
                        }
                    }
                }

            }

        }

    }
    return actionList;
    
};




ReleaseTool.processProject = function(project, modifiedProject) {
    util.log("Processing:" + project);
    ReleaseTool.dependentProjects[project] = ReleaseTool.projectsConfigs[project];
    if (ReleaseTool.projectsConfigs[project]) {
        if (!ReleaseTool.dependentProjects[project].causes) {
            ReleaseTool.dependentProjects[project].causes = [];
        }

        if (modifiedProject) {
            ReleaseTool.dependentProjects[project].causes.push(0);
        }
        else {
            ReleaseTool.dependentProjects[project].causes.push(2);
        }

        if (ReleaseTool.projectsConfigs[project].actions) {
            for (var childproject in ReleaseTool.projectsConfigs[project].actions) {
                    util.log("  Child:" + childproject);
                    ReleaseTool.processProject(childproject);
            }

        }
    }
    else {
        util.log("WARNING " + project + " is not included on config file!!!!!");
        ReleaseTool.projectsWithoutConfiguration.push(project);
        
        ReleaseTool.dependentProjects[project] = {
                                                        "level": 1
                                                        };
        if (!ReleaseTool.dependentProjects[project].causes) {
            ReleaseTool.dependentProjects[project].causes = [];
        }

        if (modifiedProject) {
            ReleaseTool.dependentProjects[project].causes.push(1);
        }
        else {
            ReleaseTool.dependentProjects[project].causes.push(3);
        }
        
    }
};


ReleaseTool.loadConfig = function() {
    var artifactsConfig = JSON.parse(fs.readFileSync("artifacts/config.json"));
    //util.log("Files:" + JSON.stringify(artifactsConfig));
    return artifactsConfig;
};


ReleaseTool.exec = module.exports.exec = function(comm, path) {
    if (path) {
        ReleaseTool.path = path;
    }
    exec(comm, ReleaseTool.commandCallback);

};
