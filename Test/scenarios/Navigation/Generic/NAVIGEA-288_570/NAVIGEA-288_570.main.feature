Feature: NAVIGEA-570:GENERAL: Rename Portfolio Types (names to be displayed)
         NAVIGEA-469: GENERAL - Show customer and advisor information at every page
  
   Scenario: @NAVIGEA-570-@NAVIGEA-469-V1
    Given I am logged in with organization "TetralogSystemsQA" and CrmKey "SeleniumTestPortfolio_QAPortfolio"
    And I navigate to the "Advisory" module
    When I select the "Analysis" page
    And The app should show "Internal active" portfolio type in "Active PTF" internal portfolio in "Analysis" page
    And The app should show "Internal passive" portfolio type in "Passive PTF" internal portfolio in "Analysis" page
    And The app should show "Internal insurance" portfolio type in "Insurance PTF" internal portfolio in "Analysis" page
    And The app should show "External" portfolio type in "External PTF" external portfolio in "Analysis" page
    And The app should show the following customer and advisor information
      | information | value                            |
      | Customer    | Heine Mustermann                 |
      | Personal ID | 05094944807                      |
      | Address     | Musterstra?e 22  1000 Musterdorf |
      | Advisor     | ISVC Columbus1(Asle Berg)        |
  
  Scenario: @NAVIGEA-570-@NAVIGEA-469-V2 Portfolio overview: Check the portfolios that are shown in in Portfolio Overview page.
    Given I am logged in with organization "TetralogSystemsQA" and CrmKey "SeleniumTestPortfolio_QAPortfolio"
    And I navigate to the "Portfolio" module
    And The app should show "Internal active" portfolio type in "Active PTF" internal portfolio in "Portfolio Overview" page
    And The app should show "Internal passive" portfolio type in "Passive PTF" internal portfolio in "Portfolio Overview" page
    And The app should show "Internal insurance" portfolio type in "Insurance PTF" internal portfolio in "Portfolio Overview" page
    And The app should show "External" portfolio type in "External PTF" external portfolio in "Portfolio Overview" page
    And The app should show the following customer and advisor information
      | information | value                            |
      | Customer    | Heine Mustermann                 |
      | Personal ID | 05094944807                      |
      | Address     | Musterstra?e 22  1000 Musterdorf |
      | Advisor     | ISVC Columbus1(Asle Berg)        |
   
   
    Scenario: @NAVIGEA-570- @NAVIGEA-469-V3  Parameters page: Check the portfolios that are shown  in Portfolio Overview page.
    Given I am logged in with organization "TetralogSystemsQA" and CrmKey "SeleniumTestPortfolio_QAPortfolio"
    And I navigate to the "Advisory" module
    And I select the "Analysis" page
    And In the Basic advisory setup area, I select the following checkboxes:
      | name                               | state |
      | Internal active - Active PTF       | false |
      | Internal passive - Passive PTF     | true  |
      | Internal insurance - Insurance PTF | false |
      | External - External PTF            | true  |
    When I select the "Setup" page
    And I click on the "Show Parameters" button
    And The app should show "Internal active" portfolio type in "Active PTF" internal portfolio in "Parameters" page
    And The app should show "Internal passive" portfolio type in "Passive PTF" internal portfolio in "Parameters" page
    And The app should show "Internal insurance" portfolio type in "Insurance PTF" internal portfolio in "Parameters" page
    And The app should show "External" portfolio type in "External PTF" external portfolio in "Parameters" page
    And The app should show the following customer and advisor information
      | information | value                            |
      | Customer    | Heine Mustermann                 |
      | Personal ID | 05094944807                      |
      | Address     | Musterstra?e 22  1000 Musterdorf |
      | Advisor     | ISVC Columbus1(Asle Berg)        |  
      
      
  @dev @NAVIGEA-570 @NAVIGEA-469-V4 @Advisory_module @Result-page
  Scenario: @NAVIGEA-570_NAVIGEA-469-V4--Results page: Check the portfolios that are shown in Transaction page.
    Given I am logged in the application
    When I navigate to the "Advisory" module
    And I select the "Results" page
    Then The app should show "Internal active" portfolio type in "Internal portfolio 1" internal portfolio in "Results" page
    And The app should show "Internal passive" portfolio type in "Internal portfolio 2" internal portfolio in "Results" page
    And The app should show "External" portfolio type in "External portfolio" external portfolio in "Results" page
    And The app should show the following customer and advisor information
      | information | value                            |
      | Customer    | Heine Mustermann                 |
      | Personal ID | 05094944807                      |
      | Address     | Musterstra?e 22  1000 Musterdorf |
      | Advisor     | ISVC Columbus1(Asle Berg)        |

  @dev @NAVIGEA-570 @NAVIGEA-469-V5 @Advisory_module @Transaction-page
  Scenario: @NAVIGEA-570_@NAVIGEA-469-v5- -Transaction page: Check the portfolios that are shown in Transaction page.
    Given I am logged in the application
    And I navigate to the "Advisory" Module
    And I select the "Results" page
    And I click on the "Show transactions" button
    Then The app should show "Internal active" portfolio type in "Internal portfolio 1" internal portfolio in "Transaction" page
    And The app should show "Internal passive" portfolio type in "Internal portfolio 2" internal portfolio in "Transaction" page
    And The app should show "External" portfolio type in "External portfolio" external portfolio in "Transaction" page
    And The app should show the following customer and advisor information
      | information | value                            |
      | Customer    | Heine Mustermann                 |
      | Personal ID | 05094944807                      |
      | Address     | Musterstra?e 22  1000 Musterdorf |
      | Advisor     | ISVC Columbus1(Asle Berg)        |
