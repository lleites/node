Feature: PO AV: SEARCH: Deactivate Categorial Dropdowns
  Search will be avialble for ISIN, Name and substituts filter (this already works)

  @devAna @NAVIGEA-413 @NAVIGEA-413-v1 @Portfolio_module @Edit_page
  Scenario: @NAVIGEA-413-v1-- Search a security by isin in Portfolio-Editpage.
    Given I am logged in the application
    And I navigate to the "Portfolio" module
    And I click on the edit portfolio button in "External portfolio" portfolio
    When I search by "isin" the security "SE0000577389"
    Then The app should show the following values in the Search Results table:
      | order | name         | securityClass                    | securityExposure | volatilityRiskClass | complexityRiskClass | liquidityRiskClass | datafeedStatus | price | currency |
      | 1     | SE0000577389 | # SEB Sverigefond Småbolag (364) | UCITS fund       | Sweden              | H                   | L                  | L              | VALID | 51.74    |
    And The app should show the following search-filters disabled:
      | Search-filters    |
      | Security Class    |
      | Security Exposure |
      | Currency          |
      | Volaitlity        |
      | Complexity        |
      | Liquidity         |

  @devAna @NAVIGEA-413 @NAVIGEA-413-v2 @Portfolio_module @Edit_page
  Scenario: @NAVIGEA-413-v2-- Search a security by name in Portfolio-Editpage.
    Given I am logged in the application
    And I navigate to the "Portfolio" module
    And I click on the edit portfolio button in "External portfolio" portfolio
    When I search by "name" the security "# SEB Östeuropafond"
    Then The app should show the following values in the Search Results table:
      | order | name         | securityClass       | securityExposure | volatilityRiskClass | complexityRiskClass | liquidityRiskClass | datafeedStatus | price | currency |
      | 1     | SE0000433203 | # SEB Östeuropafond | UCITS fund       | Eastern Europe      | H                   | L                  | L              | VALID | 18.45    |
    And The app should show the following search-filters disabled:
      | Search-filters    |
      | Security Class    |
      | Security Exposure |
      | Currency          |
      | Volaitlity        |
      | Complexity        |
      | Liquidity         |

  @devAna @NAVIGEA-413 @NAVIGEA-413-v3 @Advisory_module @Favorites_page
  Scenario: @NAVIGEA-413-v3-- Search a security by isin in Advisory-Favorites page.
    Given I am logged in the application
    And I navigate to the "Advisory" module
    And I select the "Setup" page
    And I click on the "Show Favorites" button
    When I search by "isin" the security "LU0057025933"
    Then The app should show the following values in the Search Results table:
      | order | name         | securityClass                     | securityExposure | volatilityRiskClass | complexityRiskClass | liquidityRiskClass | datafeedStatus | price | currency |
      | 1     | LU0057025933 | AB Global Growth Trends A USD Acc | UCITS fund       | Global              | H                   | L                  | L              | VALID | 33.55    |
    And The app should show the following search-filters disabled:
      | Search-filters    |
      | Security Class    |
      | Security Exposure |
      | Currency          |
      | Volaitlity        |
      | Complexity        |
      | Liquidity         |

  @devAna @NAVIGEA-413 @NAVIGEA-413-v4 @Advisory_module @Favorites_page
  Scenario: @NAVIGEA-413-v4-- Search a security by name in Advisory-Favorites page.
    Given I am logged in the application
    And I navigate to the "Advisory" module
    And I select the "Setup" page
    And I click on the "Show Favorites" button
    When I search by "name" the security "Aberdeen Global - Asian Smaller Companies Fund"
    Then The app should show the following values in the Search Results table:
      | order | name         | securityClass                                  | securityExposure | volatilityRiskClass | complexityRiskClass | liquidityRiskClass | datafeedStatus | price | currency |
      | 1     | LU0231459107 | Aberdeen Global - Asian Smaller Companies Fund | UCITS fund       | Asia                | H                   | L                  | L              | VALID | 62.00    |
    And The app should show the following search-filters disabled:
      | Search-filters    |
      | Security Class    |
      | Security Exposure |
      | Currency          |
      | Volaitlity        |
      | Complexity        |
      | Liquidity         |
