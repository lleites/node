Feature: Portfolio overview module

  @active @Portfolio_module @Portfolio_Overview
  Scenario: Overview of all portfolios
    Given I am logged in with organization "TetralogSystemsQA" and CrmKey "SeleniumTestPortfolio_Default"
    When I navigate to the "Portfolio" module
    Then I have an internal portfolio
    And I have an external portfolio
    And I display the portfolio summary information
    And I should see a total of "219,229" "NOK" in internal summary
    And I should see a total of "8,803" "NOK" in external summary
    And I should see a total of "228,032" "NOK" in overall summary
    And The "internal" portfolio 1 should have "Internal portfolio 1" as name and "Internal active" as type
    And I should see internal portfolio 1 as
      """
      {
      rowData : [
      {
      	'@class' : 'com.tetralog.webtest.table.row.impl.NavigeaPortfolioRow',
      	rowIndex : 1,
      	order : 1,
      	name : 'AB Global Growth Trends A USD Acc',
      	nameAndIsinTooltip : {
      		isin : 'LU0057025933',
      		name : 'AB Global Growth Trends A USD Acc' 
      	},
      	securityExposure : 'UCITS Funds - International',
      	volatilityRiskClass : 'H',
      	complexityRiskClass : 'L',
      	liquidityRiskClass : 'L',
      	datafeedStatus : 'VALID',
      	dontSell : '-',
      	dontBuy : '-',
      	price : '33.55',
      	currency : 'USD',
      	numberOfShares : '120',
      	value : '22,493'
      }, {
      	'@class' : 'com.tetralog.webtest.table.row.impl.NavigeaPortfolioRow',
      	rowIndex : 2,
      	order : 2,
      	name : 'Aberdeen Global - Asian Smaller Companies Fund',
      	nameAndIsinTooltip : {
      		isin : 'LU0231459107',
      		name : 'Aberdeen Global - Asian Smaller Companies Fund'
      	},
      	securityExposure : 'UCITS Funds - International',
      	volatilityRiskClass : 'H',
      	complexityRiskClass : 'L',
      	liquidityRiskClass : 'L',
      	datafeedStatus : 'VALID',
      	dontSell : '-',
      	dontBuy : '-',
      	price : '162.00',
      	currency : 'USD',
      	numberOfShares : '120',
      	value : '108,613'
      }
      ]
      }
      """
    And The "internal" portfolio 2 should have "Internal portfolio 2" as name and "Internal passive" as type
    And I should see internal portfolio 2 as
      """
      {
      rowData : [
      {
      	'@class' : 'com.tetralog.webtest.table.row.impl.NavigeaPortfolioRow',
      	rowIndex : 1,
      	order : 1,
      	name : 'Aberdeen Global - Asian Smaller Companies Fund',
      	nameAndIsinTooltip : {
      		isin : 'LU0231459107',
      		name : 'Aberdeen Global - Asian Smaller Companies Fund'
      	},
      	securityExposure : 'UCITS Funds - International',
      	volatilityRiskClass : 'H',
      	complexityRiskClass : 'L',
      	liquidityRiskClass : 'L',
      	datafeedStatus : 'VALID',
      	dontSell : '-',
      	dontBuy : '-',
      	price : '162.00',
      	currency : 'USD',
      	numberOfShares : '86',
      	value : '78,473'
      }, {
      	'@class' : 'com.tetralog.webtest.table.row.impl.NavigeaPortfolioRow',
      	rowIndex : 2,
      	order : 2,
      	name : 'Aberdeen Global - Emerging Markets Equity Fund',
      	nameAndIsinTooltip : {
      		isin : 'LU0132412106',
      		name : 'Aberdeen Global - Emerging Markets Equity Fund'
      	},
      	securityExposure : 'UCITS Funds - Emerging Markets',
      	volatilityRiskClass : 'H',
      	complexityRiskClass : 'L',
      	liquidityRiskClass : 'L',
      	datafeedStatus : 'VALID',
      	dontSell : '-',
      	dontBuy : '-',
      	price : '157.00',
      	currency : 'USD',
      	numberOfShares : '11',
      	value : '9,648'
      }
      ]
      }
      """
    And The "external" portfolio 1 should have "External portfolio" as name and "External" as type
    And I should see external portfolio 1 as
      """
      {
      rowData : [
      {
      	'@class' : 'com.tetralog.webtest.table.row.impl.NavigeaPortfolioRow',
      	rowIndex : 1,
      	order : 1,
      	name : 'Bankinvest Asien',
      	nameAndIsinTooltip : {
      		isin : 'DK0015939359',
      		name : 'Bankinvest Asien'
      	},
      	securityExposure : 'UCITS Funds - Emerging Markets',
      	volatilityRiskClass : 'H',
      	complexityRiskClass : 'L',
      	liquidityRiskClass : 'L',
      	datafeedStatus : 'VALID',
      	dontSell : '-',
      	dontBuy : '✓',
      	price : '426.00',
      	currency : 'DKK',
      	numberOfShares : '15',
      	value : '6,324'
      }, {
      	'@class' : 'com.tetralog.webtest.table.row.impl.NavigeaPortfolioRow',
      	rowIndex : 2,
      	order : 2,
      	name : 'Bankinvest Basis Aktier Udb',
      	nameAndIsinTooltip : {
      		isin : 'DK0015773873',
      		name : 'Bankinvest Basis Aktier Udb'
      	},
      	securityExposure : 'UCITS Funds - International',
      	volatilityRiskClass : 'H',
      	complexityRiskClass : 'L',
      	liquidityRiskClass : 'L',
      	datafeedStatus : 'VALID',
      	dontSell : '-',
      	dontBuy : '✓',
      	price : '129.00',
      	currency : 'DKK',
      	numberOfShares : '8',
      	value : '1,110'
      }, {
      	'@class' : 'com.tetralog.webtest.table.row.impl.NavigeaPortfolioRow',
      	rowIndex : 3,
      	order : 3,
      	name : 'Storebrand Barnespar',
      	nameAndIsinTooltip : {
      		isin : 'NO0008000981',
      		name : 'Storebrand Barnespar'
      	},
      	securityExposure : 'UCITS Funds - International',
      	volatilityRiskClass : 'H',
      	complexityRiskClass : 'L',
      	liquidityRiskClass : 'L',
      	datafeedStatus : 'VALID',
      	dontSell : '-',
      	dontBuy : '✓',
      	price : '48.81',
      	currency : 'NOK',
      	numberOfShares : '28',
      	value : '1,366'
      }
      ]
      }
      """

  
