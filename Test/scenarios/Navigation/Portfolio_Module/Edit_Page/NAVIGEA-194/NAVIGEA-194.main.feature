Feature: Add ISIN and Security Class to the search results table

 @dev 
  Scenario: Navigea-194 part 1: Check that "ISIN" value is merged with the "Name" column
   	Given I am logged in with organization "TetralogSystemsQA" and CrmKey "SeleniumTestPortfolio_Default"
   	And I navigate to the "Portfolio" module
   	# DO NOT IMPLEMENT THIS STEPS, THERE ARE ALREADY IMPLEMENTED
  	And I expand the details of the external portfolio number 1
  	And Click on Edit Portfolio
  	When Click on the Search button in the Search Box.
  	Then I should see a column containing "ISIN" and "Name" on the search results
  	
  @dev @Brian
  Scenario: Navigea-194 part 2: Check that "Security Exposure" value is merged with the "Security Class" column
   	Given I am logged in with organization "TetralogSystemsQA" and CrmKey "SeleniumTestPortfolio_Default"
   	And I navigate to the "Portfolio" module
    # DO NOT IMPLEMENT THIS STEPS, THERE ARE ALREADY IMPLEMENTED
    And I expand the details of the external portfolio number 1
  	And Click on Edit Portfolio
  	When Click on the Search button in the Search Box.
  	Then I should see a column containing "Security Exposure" and "Security Class" on the search results
