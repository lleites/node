Feature: PO - Edit page - Exchange a security marked with the "substitution" symbol" by cklicking at the subtitution symbol when in "Edit deposit" page

  #the feature to pre populate the search boxes has been removed or posponed.
  @Navigea-75 @Navigea-75-v1 @Portfolio_module @Edit_page @Obsolete
  Scenario: Navigea-75-v1: Verify predefined search criteria
    Given I am logged in with organization "TetralogSystemsQA" and CrmKey "QaTestData10"
    When I navigate to the "Portfolio" module
    And I edit the portfolio number 1
    And I click substitution icon for security "Equities Norway HLL"
    Then I should see the following prepopulated search options:
      | securityClass | securityExposure | currency | riskVolatility | riskComplexity | riskLiquidity |
      | Listed Shares | Equities Norway  | NOK      | High           | Low            | Low           |
    And I should see the following text "Specification for "Equities Norway HLL", "TS00000ENHLL", Value 200,000.00 NOK"

  @active @Navigea-75 @Navigea-75-v2 @Portfolio_module @Edit_page
  Scenario: Navigea-75-v2: Check that is not possible to select more than one instrument
    Given I am logged in with organization "TetralogSystemsQA" and CrmKey "QaTestData10"
    When I navigate to the "Portfolio" module
    And I edit the portfolio number 1
    And I click substitution icon for security "Equities Norway HLL"
    And I search for instrument "Storebrand"
    And I select instrument "Storebrand Aksjespar" from substitution search results
    And I select instrument "Storebrand Norden" from substitution search results
    And I apply the changes
    Then I should see a popup warning
    And I click on the popup ok button

  @active @Navigea-75 @Navigea-75-v3 @Portfolio_module @Edit_page
  Scenario: Navigea-75-v3 : Exchange a security
    Given I am logged in with organization "TetralogSystemsQA" and CrmKey "QaTestData10"
    When I navigate to the "Portfolio" module
    And I edit the portfolio number 1
    And I click substitution icon for security "Equities Norway HLL"
    And I search for instrument "Copy of LU0048578792 Fidelity Europ Growth"
    And I select instrument "Copy of LU0048578792 Fidelity Europ Growth" from substitution search results
    And I apply the changes
    Then I should see the editable portfolio as
      | order | name                                       | securityExposure              | volatilityRiskClass | complexityRiskClass | liquidityRiskClass | datafeedStatus | dontSell | dontBuy  | price | currency | nominalShare | value      |
      | 1     | Client Account                             | Money Market NOK              | L                   | L                   | L                  | REMPLACEMENT   | false    | true     | n/a   | NOK      | 400,000.000  | 400,000.00 |
      | 2     | Equities International HLL                 | Equities International        | H                   | L                   | L                  | REMPLACEMENT   | false    | true     | n/a   | NOK      | 100,000.000  | 100,000.00 |
      | 3     | Copy of LU0048578792 Fidelity Europ Growth | UCITS Funds - Sector oriented | H                   | L                   | L                  | VALID          | false    | true     | 10.09 | EUR      | 2,684.218    | 200,000.00 |
      | 4     | Structured_HMM                             |                               | H                   | M                   | M                  | INVALID        | false    | false    | 0.00  | NOK      |              | 300,000.00 |


  @active @Navigea-75 @Navigea-75-v4 @Portfolio_module @Edit_page
  Scenario: Navigea-75 : Substitute a security for another that is already on the portfolio
    Given I am logged in with organization "TetralogSystemsQA" and CrmKey "QaTestData11"
    When I navigate to the "Portfolio" module
    And I edit the portfolio number 1
    And I click substitution icon for security "Equities Norway HLL"
    And I search for instrument "Equities International HLL"
    And I select instrument "Equities International HLL" from substitution search results
    And I apply the changes
    Then I should see the editable portfolio as
      | order | name                           | securityExposure           | volatilityRiskClass | complexityRiskClass | liquidityRiskClass | datafeedStatus | dontSell | dontBuy | price | currency | nominalShare | value      |
      | 1     | Client Account                 | Money Market NOK           | L                   | L                   | L                  | REMPLACEMENT   | false    | true    | n/a   | NOK      | 400,000.000  | 400,000.00 |
      | 2     | Equities International HLL     | Equities International     | H                   | L                   | L                  | REMPLACEMENT   | false    | true    | n/a   | NOK      | 300,000.000  | 300,000.00 |
      | 3     | Alternatives - Unspecified MMH | Alternatives - Unspecified | M                   | M                   | H                  | REMPLACEMENT   | true     | true    | n/a   | NOK      | 300,000.000  | 300,000.00 |
