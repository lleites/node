Feature: NAVIGEA-160: PO - Edit page - Adding a security which is already part of the portfolio: addition of number of shares
         NAVIGEA-335: PO-Edit page--The additions in Nominal/No. of shares and in VAlue NOK are not always correct.

  @active @Navigea-160 @Portfolio_module @Edit_page
  Scenario: Navigea-160: the total number of shares should be added when adding an existing instrument
    Given I am logged in with organization "TetralogSystemsQA" and CrmKey "SeleniumTestPortfolio_Default"
    When I navigate to the "Portfolio" module
    And I have an external portfolio
    And I edit the portfolio number 1
    And I add "50" shares of position name "Bankinvest Asien" and ISIN "DK0015939359"
    And I add "30" shares of position name "Bankinvest Asien" and ISIN "DK0015939359"
    Then I should see "95.000" "shares" of position name "Bankinvest Asien" and ISIN "DK0015939359"

  @active @Navigea-335 @Navigea-335-v1 @Portfolio_module @Edit_page
  Scenario: Navigea-335-v1: the total number of shares should be added when adding an existing instrument
    Given I am logged in with organization "TetralogSystemsQA" and CrmKey "SeleniumTestPortfolio_Default"
    And I navigate to the "Portfolio" module
    And I click on the edit portfolio button in "External portfolio" portfolio
    When I add in the external portfolio the following securities:
      | ISIN         | Name                 | Share |
      | QAEEUR122001 | Copy of Daimler N.A. | 1000  |
    And I add "1000" shares of position name "Copy of Daimler N.A." and ISIN "QAEEUR122001"
    Then I should see "2,000.000" "shares" of position name "Copy of Daimler N.A." and ISIN "QAEEUR122001"

  @active @Navigea-335 @Navigea-335-v2 @Portfolio_module @Edit_page
  Scenario: Navigea-335-v2: the total number of shares should be added when adding an existing instrument
    Given I am logged in with organization "TetralogSystemsQA" and CrmKey "SeleniumTestPortfolio_Default"
    And I navigate to the "Portfolio" module
    And I click on the edit portfolio button in "External portfolio" portfolio
    When I add in the external portfolio the following securities:
      | ISIN         | Name                 | Value |
      | QAEEUR122001 | Copy of Daimler N.A. | 1000  |
    And I add "1500" value of position name "Copy of Daimler N.A." and ISIN "QAEEUR122001"
    Then I should see "2,140.29" "value NOK" of position name "Copy of Daimler N.A." and ISIN "QAEEUR122001"
