Feature: PO - Edit page - Behavior of entry fields 1) using wrong characters 2) try to key in both number of shares and values

@active @Navigea-165-v1 @Portfolio_module @Edit_page
Scenario: Navigea-165 part 1 Display visual clues when typing entries for both number of shares and values
  	Given I am logged in with organization "TetralogSystemsQA" and CrmKey "SeleniumTestPortfolio_Default"
    When I navigate to the "Portfolio" module
  	And I edit the portfolio number 1
  	And Click on the Search button in the Search Box.
  	And I add "50" shares of position name "Bankinvest Asien" and ISIN "DK0015939359" without saving
  	And I add a value of "1000" for position name "Bankinvest Asien" and ISIN "DK0015939359"
  	Then I should see a visual clue next to the "Nominal" entry field for position "Bankinvest Asien" and ISIN "DK0015939359"
  	And I should see a visual clue next to the "Value" entry field for position "Bankinvest Asien" and ISIN "DK0015939359"
  	And when I click on the visual clue in position name "Bankinvest Asien" and ISIN "DK0015939359" I should see the following message: "Please use either number of shares or values. If you want to correct your entry, please delete the first one you made"

@active @Navigea-165-v2 @Portfolio_module @Edit_page
Scenario: Navigea-165 part 2 Display visual clues when typing non numerical characters into the n° of shares case in the external portfolio edit table.
	Given I am logged in with organization "TetralogSystemsQA" and CrmKey "SeleniumTestPortfolio_Default"
    When I navigate to the "Portfolio" module
	And I edit the portfolio number 1
	And I add "123abc" shares of position name "Bankinvest Asien" and ISIN "DK0015939359" without saving
	And I save the portfolio
	Then I should see a visual clue next to the "Nominal" entry field for position "Bankinvest Asien" and ISIN "DK0015939359"
	
@active @Navigea-165-v3 @Portfolio_module @Edit_page
Scenario: Navigea-165 part 3 Display visual clues when non numeric characters are entered into the n°of shares and value fields
	Given I am logged in with organization "TetralogSystemsQA" and CrmKey "SeleniumTestPortfolio_Default"
    When I navigate to the "Portfolio" module
	And I edit the portfolio number 1
	And Click on the Search button in the Search Box.
	And I add "abc" shares of position name "Bankinvest Asien" and ISIN "DK0015939359" without saving
	And I add a value of "123" for position name "Bankinvest Asien" and ISIN "DK0015939359"
	Then I should see a visual clue next to the "Nominal" entry field for position "Bankinvest Asien" and ISIN "DK0015939359"
	And when I click on the visual clue in position name "Bankinvest Asien" and ISIN "DK0015939359" I should see the following message: "You have entered invalid characters. Please use numeric values only!"
  	
@active @Navigea-165-v4  @Portfolio_module @Edit_page
Scenario: Navigea-165 part 4 Display visual clues when non numeric characters are entered into the n°of shares and value fields
	Given I am logged in with organization "TetralogSystemsQA" and CrmKey "SeleniumTestPortfolio_Default"
    When I navigate to the "Portfolio" module
	And I edit the portfolio number 1
	And Click on the Search button in the Search Box.
	And I add "56tr" shares of position name "Bankinvest Asien" and ISIN "DK0015939359" without saving
	And I add a value of "123" for position name "Bankinvest Asien" and ISIN "DK0015939359"
	Then I should see a visual clue next to the "Nominal" entry field for position "Bankinvest Asien" and ISIN "DK0015939359"
	And when I click on the visual clue in position name "Bankinvest Asien" and ISIN "DK0015939359" I should see the following message: "You have entered invalid characters. Please use numeric values only!"
  	
@active @Navigea-165-v5 @Portfolio_module @Edit_page
Scenario: Navigea-165 part 5 Display visual clues when typing non numerical characters into the n° of shares case in the external portfolio edit table [and the portfolio is not saved].
	Given I am logged in with organization "TetralogSystemsQA" and CrmKey "SeleniumTestPortfolio_Default"
    When I navigate to the "Portfolio" module
	And I edit the portfolio number 1
	And I add "123abc" shares of position name "Bankinvest Asien" and ISIN "DK0015939359" without saving
	And I save the portfolio
	And I navigate back to the portfolio overview screen
    Then The external portfolio "External portfolio" should have the following positions :
      | order | name                        | securityExposure               | volatilityRiskClass | complexityRiskClass | liquidityRiskClass | datafeedStatus | dontSell | dontBuy | price  | currency | numberOfShares | value |
      | 1     | Bankinvest Asien            | UCITS Funds - Emerging Markets | H                   | L                   | L                  | VALID          | -        | ✓       | 426.00 | DKK      | 15             | 6,324 |
      | 2     | Bankinvest Basis Aktier Udb | UCITS Funds - International    | H                   | L                   | L                  | VALID          | -        | ✓       | 129.00 | DKK      | 8              | 1,110 |
      | 3     | Storebrand Barnespar        | UCITS Funds - International    | H                   | L                   | L                  | VALID          | -        | ✓       | 48.81  | NOK      | 28             | 1,366 |
