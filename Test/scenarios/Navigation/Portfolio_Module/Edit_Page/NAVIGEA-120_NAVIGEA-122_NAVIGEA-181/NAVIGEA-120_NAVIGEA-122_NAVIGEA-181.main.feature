Feature: Navigea-120 PO - General - Popup warning when leaving the Portfolio area with unsaved changes
   Navigea-122 PO - General -Add footnotes and copyright to the PO - overview and the PO - edit pages
   Navigea-181 PO - General -Cut names which are too long for the width of a cell within portfolio tables

  @dev @Navigea-181 @Portfolio_module @Edit_page
  Scenario: Navigea-181 Cut names which are too long for the width of a cell within portfolio tables
    Given I am logged in with organization "TetralogSystemsQA" and CrmKey "SeleniumTestPortfolio_Default"
    When I navigate to the "Portfolio" module
    And I expand the details of the internal portfolio number 1
    Then I should see internal portfolio 1 as
      """
      {
      rowData : [
      {
      	'@class' : 'com.tetralog.webtest.table.row.impl.NavigeaPortfolioTable',
      	rowIndex : 1,
      	order : 1,
      	name : 'AB Global Growth Trends ...',
      	nameAndIsinTooltip : {
      		isin : 'LU0057025933',
      		name : 'AB Global Growth Trends A USD Acc' 
      	}  	
      }
      ]
      }
      """

  @active @Navigea-120 @Portfolio_module @Edit_page
  Scenario: Navigea-120: Popup warning when leaving the Portfolio area with unsaved changes
    Given I am logged in with organization "TetralogSystemsQA" and CrmKey "SeleniumTestPortfolio_Default"
    And I navigate to the "Portfolio" module
    And I have an external portfolio
    When I click on the edit portfolio button in "External portfolio" portfolio
    And I change in the external portfolio the following securities:
      | ISIN         | Name             | Share |
      | DK0015939359 | Bankinvest Asien | 5.9   |
    And I navigate to the "Portfolio" module
    Then The message that ask me: "If you leave this page, the changes you have made and didn't save yet will be lost. Do you really want to save the changes?" should be shown
    And I click on the popup no button    
    And I should be in the "Portfolio" module "Portfolio" page

  @active @Navigea-122 @Navigea-122-v1 @Portfolio_module @Edit_page
  Scenario: Navigea-122-v1-- Add footnotes and copyright to the overview page
    Given I am logged in with organization "TetralogSystemsQA" and CrmKey "SeleniumTestPortfolio_Default"
    When I navigate to the "Portfolio" module
    Then I should see on the top of the footnote the first line started with text "Risk Class : Volatility (V), complexity (C) and liquidity (L) risk according to standard categorization: Low (L), medium (M), high (H)."

  @active @Navigea-122 @Navigea-122-v2 @Portfolio_module @Edit_page
  Scenario: Navigea-122-v2-- Test footnotes and copyright info in the overview page
    Given I am logged in with organization "TetralogSystemsQA" and CrmKey "SeleniumTestPortfolio_Default"
    When I navigate to the "Portfolio" module
	Then I should see a list of footnotes for organization "TetralogSystemsQA" and language "en" according to the resources 
		| Risk Class : Volatility (V), complexity (C) and liquidity (L) risk according to standard categorization: Low (L), medium (M), high (H).						|
		| Risk p.a. %: Annualized standard deviation of quote/price in given period.                     																|
		| Perf. p.a. %: Annualized expected performance based on the Future Adjusted Markowitz Estimator (F.A.M.E.®) or on evaluation by the product department.	|
		| Datafeed: Status of price histories. Securities with insufficient data are not included in analysis and recommendation section except being automatically substituted by using the T.E.D. feature.	|
		| Don't sell / Don't buy: Security set to "don't buy" and "don't sell" automatically according to liquidity state for internal securities or manually according to customers wish for external securities. |
		| Price: Last available closing price of the security in instrument currency. Prices for bonds are clean prices.           |
		| Cur: Instrument currency of the security.        |
		| Nominal / Number of Shares: Number of shares according to portfolio system for securities of internal portfolios or according to the information given by the customer for securities of external portfolios.             |
		| Value: Value in base currency, based on the last available closing price(s).                    |
	And I should see the following footnote for datafeed status for organization "TetralogSystemsQA" and language "en" according to images and resources
		| imageId       | resource                            |
		| imgOk         | Price history of the security is sufficient. Security can be included in analysis and recommendation section.    |
		| imgTet        | Price history of the security was automatically completed using the T.E.D. feature. Security can be included in analysis and recommendation section. |
		| imgSubstitute | Price history of an index or categorial substitute. Security can be included in analysis and recommendation section.    |
		| imgInvalid    | Missing or insufficient price history. Security cannot be included in in analysis and recommendation section.       |
    And I should see a copyright text for organization "TetralogSystemsQA" and language "en" according to the resource "© 2002 - 2013 tetralog systems AG | All rights reserved"

  @active @Navigea-122 @Navigea-122-v3 @Portfolio_module @Edit_page
  Scenario: Navigea-122-v3-- Test footnotes and copyright info in the edit page
    Given I am logged in with organization "TetralogSystemsQA" and CrmKey "SeleniumTestPortfolio_Default"
    And I navigate to the "Portfolio" module
    When I click on the edit portfolio button in "External portfolio" portfolio
	Then I should see a list of footnotes for organization "TetralogSystemsQA" and language "en" according to the resources 
		| Risk Class : Volatility (V), complexity (C) and liquidity (L) risk according to standard categorization: Low (L), medium (M), high (H).						|
		| Risk p.a. %: Annualized standard deviation of quote/price in given period.                     																|
		| Perf. p.a. %: Annualized expected performance based on the Future Adjusted Markowitz Estimator (F.A.M.E.®) or on evaluation by the product department.	|
		| Datafeed: Status of price histories. Securities with insufficient data are not included in analysis and recommendation section except being automatically substituted by using the T.E.D. feature.	|
		| Don't sell / Don't buy: Security set to "don't buy" and "don't sell" automatically according to liquidity state for internal securities or manually according to customers wish for external securities. |
		| Price: Last available closing price of the security in instrument currency. Prices for bonds are clean prices.           |
		| Cur: Instrument currency of the security.        |
		| Nominal / Number of Shares: Number of shares according to portfolio system for securities of internal portfolios or according to the information given by the customer for securities of external portfolios.             |
		| Value: Value in base currency, based on the last available closing price(s).                    |    
	And I should see the following footnote for datafeed status for organization "TetralogSystemsQA" and language "en" according to images and resources
		| imageId       | resource                            |
		| imgOk         | Price history of the security is sufficient. Security can be included in analysis and recommendation section.    |
		| imgTet        | Price history of the security was automatically completed using the T.E.D. feature. Security can be included in analysis and recommendation section. |
		| imgSubstitute | Price history of an index or categorial substitute. Security can be included in analysis and recommendation section.    |
		| imgInvalid    | Missing or insufficient price history. Security cannot be included in in analysis and recommendation section.       |
    And I should see a copyright text for organization "TetralogSystemsQA" and language "en" according to the resource "© 2002 - 2013 tetralog systems AG | All rights reserved"
