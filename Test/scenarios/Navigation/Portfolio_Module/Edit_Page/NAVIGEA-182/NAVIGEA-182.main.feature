Feature: NAVIGEA-182:PO - Edit page: Automatically create a security "Bank account", when a new external portfolio is created
		NAVIGEA-655:PO - Edit: Initial Cash account is not set to don't buy
			
  @active @NAVIGEA-182 @NAVIGEA-182-v1 @NAVIGEA-655 @Portfolio_module @Edit_page
  Scenario: NAVIGEA-182 NAVIGEA-182-NAVIGEA-655-v1 -- Create a new external portfolio
    check: automatically create a security "Bank account"  when a new external portfolio is created

    Given I am logged in with organization "TetralogSystemsQA" and CrmKey "SeleniumTestPortfolio_Default"
    And I navigate to the "Portfolio" module
    When I click on the new external portfolio button
    Then I should be in Edit page
    And I should see the editable portfolio as
      | order | name           | securityExposure | volatilityRiskClass | complexityRiskClass | liquidityRiskClass | dontSell | dontBuy | price | currency | numberOfShares | value |
      | 1     | Client Account | Money Market NOK | L                   | L                   | L                  | false    | true    | n/a   | NOK      | 0.000          | 0.00  |

  @active @NAVIGEA-182 @NAVIGEA-182-v2 @NAVIGEA-655 @Portfolio_module @Edit_page
  Scenario: @NAVIGEA-182 NAVIGEA-182-NAVIGEA-655-v2 -- Create a new external portfolio
    check: automatically create a security "Bank account"  when a new external portfolio is created

    Given I am logged in with organization "TetralogSystemsQA" and CrmKey "SeleniumTestPortfolio_Default"
    And I navigate to the "Portfolio" module
    When I click on the new external portfolio button
    And I enter the value "Test Ext. Porfolio 1" on the portfolio name
    And I add in the external portfolio the following securities:
      | ISIN         | Name                | Share |
      | SE0000433203 | # SEB Östeuropafond | 100   |
    And I change in the external portfolio the following securities:
      | ISIN         | Name                | Share |
      | 			 | Client Account      | 150   |
    And I save the portfolio
    And I navigate back to the portfolio overview screen
    Then The external portfolio "Test Ext. Porfolio 1" should have the following positions :
      | order | name                | securityExposure               | volatilityRiskClass | complexityRiskClass | liquidityRiskClass | dontSell | dontBuy | price | currency | numberOfShares | value |
      | 1     | Client Account      | Money Market NOK               | L                   | L                   | L                  | -        | ✓       | n/a   | NOK      | 150            | 150   |
      | 2     | # SEB Östeuropafond | UCITS Funds - Frontier Markets | H                   | L                   | L                  | -        | ✓       | 18.45 | SEK      | 100            | 1,581 |
