Feature: PO - Edit page: Search - Checkbox for selection of "Substitutes" only

 @dev @Navigea-214 @Portfolio_module @Edit_page
  Scenario: Navigea-214: Check that the search for substitutes/replacements only returns correct results  
  	Given I am logged in with organization "TetralogSystemsQA" and CrmKey "SeleniumTestPortfolio_Default"
   	And I navigate to the "Portfolio" module
  	When I edit the portfolio number 1
  	And I select to search for substitutes/replacements instruments only 
  	And Click on the Search button in the Search Box.
  	Then I should see the following search results:
  		| ISIN			|
  		| LU0057025933	|
  		| LU0132412106	|
  		| LU0231459107	|