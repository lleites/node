Feature:PO - Overview - Don't show decimals for number of shares and value in portfolio tables

 @active @Navigea-93 @Navigea-93-v1 @POrtfolio_module @Overview_page
  Scenario: Navigea-93 variation 1: Decimal values must be truncated for number of shares in portfolio overview but shown on the tooltip
  	Given I am logged in with organization "TetralogSystemsQA" and CrmKey "SeleniumTestPortfolio_Default"
    When I navigate to the "Portfolio" module
  	And I have an external portfolio
  	And I edit the portfolio number 1
  	And I add "5" shares of position name "Bankinvest Asien" and ISIN "DK0015939359"  
  	And I save the portfolio number 1
  	And I navigate back to the portfolio overview screen
  	And I expand the details of the external portfolio number 1
  	And I hover the mouse over the "shares" column in portfolio number 1 and position name "Bankinvest Asien"
  	Then I should see a tooltip with text
  	  """
  	  20.000
  	  """
  	#And I should see "20" "shares" of position name "Bankinvest Asien"
  	
  @active @Navigea-93 @Navigea-93-v2 @POrtfolio_module @Overview_page
  Scenario: Navigea-93 variation 2: Decimal values must be truncated for values in portfolio overview but shown on the tooltip
  	Given I am logged in with organization "TetralogSystemsQA" and CrmKey "SeleniumTestPortfolio_Default"
    When I navigate to the "Portfolio" module
  	And I have an external portfolio
  	And I edit the portfolio number 1
  	And I add "5" shares of position name "Bankinvest Asien" and ISIN "DK0015939359"
  	And I save the portfolio number 1
  	And I navigate back to the portfolio overview screen
  	And I expand the details of the external portfolio number 1
  	And I hover the mouse over the "value" column in portfolio number 1 and position name "Bankinvest Asien"
  	Then I should see a tooltip with text
  	  """
  	  8,433.32
  	  """
  	#And I should see "8,433" "value" of position name "Bankinvest Asien"
  