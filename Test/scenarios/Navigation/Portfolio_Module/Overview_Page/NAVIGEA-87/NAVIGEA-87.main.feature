Feature: PO - Overview - Additional Button within deposit detail table to delete external deposit
		
# This test will not be implemented (GUI)
@dev @GUI @Navigea-87 @Navigea-87-v1 @Portfolio_module @Overview_page
Scenario: Navigea-87 - Test table columns width
	Given I am logged in with organization "TetralogSystemsQA" and CrmKey "SeleniumTestPortfolio_Default"
    And I navigate to the "Portfolio" module
  	When I expand the details of the internal portfolio number 1
  	Then the element "logoID" should have a "font-family" of "Calibri,Arial,Verdana"
  	
# This test will not be implemented (GUI)
@dev @GUI-experimental @Navigea-87 @Navigea-87-v2 @Portfolio_module @Overview_page
Scenario: Navigea-87 - Test table layout (white line separator between columns, blue frane around content area, 
#This is normally done with manual testing, but i added this to try an image comparison tool
	Given I am logged in with organization "TetralogSystemsQA" and CrmKey "SeleniumTestPortfolio_Default"
    And I navigate to the "Portfolio" module
	And I load "portfolio-complex" data
  	When I expand the details of the internal portfolio number 1
	And I expand the details of the internat porfolio number 2
	And I expand the details of the external portfolio number 1
	Then I should see the "Page body" look the same as the reference image "Mockup 2013 05 02 Mockup PO Design"