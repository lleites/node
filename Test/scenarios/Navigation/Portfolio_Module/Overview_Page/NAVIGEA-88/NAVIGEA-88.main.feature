Feature: Navigea-88 PO - Overview and Edit page - Don't show the ISIN and the security class in the deposit tables at overview page; provide mousovers to show additional information
	
  @active @Navigea-88 @Portfolio_module @Overview_page
  Scenario: Navigea-88: Dont show the ISIN and the security class in the deposit tables at overview page; provide mousovers to show additional information
  #in order to test the "removing" of the column "class", we can implement a test for the whole table structure, and check the correct headers, etc.. 
  	Given I am logged in with organization "TetralogSystemsQA" and CrmKey "SeleniumTestPortfolio_Default"
    And I navigate to the "Portfolio" module
  	And I have an external portfolio
  	And I edit the portfolio number 1
  	And I add "50" shares of position name "Bankinvest Asien" and ISIN "DK0015939359"
  	And I save the portfolio
  	When I navigate back to the portfolio overview screen
  	And I expand the details of the external portfolio number 1
  	And I hover the mouse over the "isinAndName" column in portfolio number 1 and position name "Bankinvest Asien"
  	Then I should see a tooltip with text
  	  """
  	  DK0015939359
  	  Bankinvest Asien
  	  """
  	