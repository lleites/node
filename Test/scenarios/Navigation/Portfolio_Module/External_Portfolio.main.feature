Feature: External portfolio

  @active @Portfolio_module @Edit_page @CreatePortfolio
  Scenario: Create basic new external portfolio
    Given I am logged in with organization "TetralogSystemsQA" and CrmKey "SeleniumTestPortfolio_Default"
    When I navigate to the "Portfolio" module
    And I click on the new external portfolio button
    And I enter the value "Test Ext. Porfolio 1" on the portfolio name
    And I enter the value "General" on the portfolio type
    And I add "50" shares of position name "# SEB Europafond Småbolag (518)" and ISIN "SE0000433252"
    And I save the portfolio
    And I navigate back to the portfolio overview screen
    Then The "external" portfolio 2 should have "Test Ext. Porfolio 1" as name and "External" as type
    And I should see external portfolio 2 as
      """
          {
          rowData : [
          {
          	'@class' : 'com.tetralog.webtest.table.row.impl.NavigeaPortfolioRow',
          	rowIndex : 1,
          	order : 1,
          	name : 'Client Account',
          	nameAndIsinTooltip : {
          		name : 'Client Account' 
          	},
          	securityExposure : 'Money Market NOK',
          	volatilityRiskClass : 'L',
          	complexityRiskClass : 'L',
          	liquidityRiskClass : 'L',
          	datafeedStatus : 'VALID',
          	dontSell : '-',
          	dontBuy : '✓',
          	price : 'n/a',
          	currency : 'NOK',
          	numberOfShares : '0',
          	value : '0'
          },     
          {
         	'@class' : 'com.tetralog.webtest.table.row.impl.NavigeaPortfolioRow',
            rowIndex : 2,
          	order : 2,
           	name : '# SEB Europafond Småbolag (518)',
           	nameAndIsinTooltip : {
           		isin : 'SE0000433252',
           		name : '# SEB Europafond Småbolag (518)'
           	},
           	securityExposure : 'UCITS Funds - Developed Markets',
           	volatilityRiskClass : 'H',
           	complexityRiskClass : 'L',
           	liquidityRiskClass : 'L',
           	datafeedStatus : 'VALID',
           	dontSell : '-',
           	dontBuy : '✓',
           	price : '37.46',
           	currency : 'SEK',
           	numberOfShares : '50',
           	value : '1,605'
           }
          	]
          }
      """

  @active @Portfolio_module @Edit_page @Change_external_portfolio
  Scenario: Do a change in one deposit in external portfolio.
    Given I am logged in with organization "TetralogSystemsQA" and CrmKey "SeleniumTestPortfolio_Default"
    And I navigate to the "Portfolio" module
    And I click on the edit portfolio button in "External portfolio" portfolio
    When I add in the external portfolio the following securities:
      | ISIN         | Name                | Share |
      | SE0000433203 | # SEB Östeuropafond | 100   |
    And I save the portfolio
    And I change in the external portfolio the following securities:
      | ISIN         | Name                | Share |
      | SE0000433203 | # SEB Östeuropafond | 150   |
    And I save the portfolio
    And I navigate back to the portfolio overview screen
    Then The external portfolio "External portfolio" should have the following positions :
      | order | name                        | securityExposure               | volatilityRiskClass | complexityRiskClass | liquidityRiskClass | datafeedStatus | dontSell | dontBuy | price  | currency | numberOfShares | value |
      | 1     | Bankinvest Asien            | UCITS Funds - Emerging Markets | H                   | L                   | L                  | VALID          | -        | ✓       | 426.00 | DKK      | 15             | 6,324 |
      | 2     | Bankinvest Basis Aktier Udb | UCITS Funds - International    | H                   | L                   | L                  | VALID          | -        | ✓       | 129.00 | DKK      | 8              | 1,110 |
      | 3     | Storebrand Barnespar        | UCITS Funds - International    | H                   | L                   | L                  | VALID          | -        | ✓       | 48.81  | NOK      | 28             | 1,366 |
      | 4     | # SEB Östeuropafond         | UCITS Funds - Frontier Markets | H                   | L                   | L                  | VALID          | -        | ✓       | 18.45  | SEK      | 150            | 2,372 |


  @active @Portfolio_module @Edit_page @Add_position_to_external
  Scenario: Add a deposit in external portfolio.
    Given I am logged in with organization "TetralogSystemsQA" and CrmKey "SeleniumTestPortfolio_Default"
    And I navigate to the "Portfolio" module
    And I click on the edit portfolio button in "External portfolio" portfolio
    When I add in the external portfolio the following securities:
      | ISIN         | Name                | Share |
      | SE0000433203 | # SEB Östeuropafond | 100   |
    And I save the portfolio
    And I navigate back to the portfolio overview screen
    Then The external portfolio "External portfolio" should have the following positions :
      | order | name                        | securityExposure               | volatilityRiskClass | complexityRiskClass | liquidityRiskClass | datafeedStatus | dontSell | dontBuy | price  | currency | numberOfShares | value |
      | 1     | Bankinvest Asien            | UCITS Funds - Emerging Markets | H                   | L                   | L                  | VALID          | -        | ✓       | 426.00 | DKK      | 15             | 6,324 |
      | 2     | Bankinvest Basis Aktier Udb | UCITS Funds - International    | H                   | L                   | L                  | VALID          | -        | ✓       | 129.00 | DKK      | 8              | 1,110 |
      | 3     | Storebrand Barnespar        | UCITS Funds - International    | H                   | L                   | L                  | VALID          | -        | ✓       | 48.81  | NOK      | 28             | 1,366 |
      | 4     | # SEB Östeuropafond         | UCITS Funds - Frontier Markets | H                   | L                   | L                  | VALID          | -        | ✓       | 18.45  | SEK      | 100            | 1,581 |

 
  @active @Portfolio_module @Edit_page @Delete_position
  Scenario: Delete a deposit in external portfolio.
    Given I am logged in with organization "TetralogSystemsQA" and CrmKey "SeleniumTestPortfolio_Default"
    And I navigate to the "Portfolio" module
    And I click on the edit portfolio button in "External portfolio" portfolio
    And I delete the "Storebrand Barnespar" deposit
    And The message that ask me: "You are going to remove position 'NO0008000981', Are you sure?" should be shown
    And I click on the save button in the message
    And I save the portfolio
    And I navigate back to the portfolio overview screen
    Then The external portfolio "External portfolio" should have the following positions :
      | order | name                        | securityExposure               | volatilityRiskClass | complexityRiskClass | liquidityRiskClass | datafeedStatus | dontSell | dontBuy | price  | currency | numberOfShares | value |
      | 1     | Bankinvest Asien            | UCITS Funds - Emerging Markets | H                   | L                   | L                  | VALID          | -        | ✓       | 426.00 | DKK      | 15             | 6,324 |
      | 2     | Bankinvest Basis Aktier Udb | UCITS Funds - International    | H                   | L                   | L                  | VALID          | -        | ✓       | 129.00 | DKK      | 8              | 1,110 |

  @active @Portfolio_module @Overview_page
  Scenario: Delete an external portfolio
    Given I am logged in with organization "TetralogSystemsQA" and CrmKey "QaTestData10"
    When I navigate to the "Portfolio" module
    And I expand the details of the external portfolio number 1
    And I delete the portfolio number 1
    And I see a popup with a delete button
    And I click on the popup delete button
    And I click on the popup ok button
    Then I should not see the external portfolio number 1
