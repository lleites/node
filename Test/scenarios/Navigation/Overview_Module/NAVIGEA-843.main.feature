Feature:OM - ERROR Handling: M.A.P. behavior when receiving missing Data from the CRM

 @active @Navigea-843
  Scenario: Navigea-843: Filter for blocking MAP usage when CRM does not deliver realization year(s) for goals
   Given I am trying to login with organization "TetralogSystemsQA" and CrmKey "CrmTestData24"
   Then The message that ask me: "Missing data from CRM. Advisory in M.A.P. is not possible. Missing Data: Goals" should be shown