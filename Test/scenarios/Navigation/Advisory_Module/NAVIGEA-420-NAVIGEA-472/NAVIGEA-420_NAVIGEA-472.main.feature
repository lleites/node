Feature: NAVIGEA-420: AV - START: Target portfolio selection and fixations
         NAVIGEA-472: AV - SETUP: Show Use Case Label
   5 tests( 5 dev)

  @active @NAVIGEA-420 @NAVIGEA-420-v1-- @Advisory_module @Analysis-page
  Scenario: NAVIGEA-420--v1--Analysis page
     - select portfolios in the Advisory basic setup area. 
     - that selected portfolios are expanded and the overlay/Not select text are removed

    Given I am logged in with organization "TetralogSystemsQA" and CrmKey "QaTestData06"
    And I navigate to the "Advisory" module
    And In the Basic advisory setup area, I select the following checkboxes:
      | name                         | state |
      | Internal active - Active PTF | true  |
    Then I should see a text "Selected" in "Active PTF" internal portfolio in Analysis page
    And I should see a text "Not selected" in "Passive PTF" internal portfolio in Analysis page
    And I should see a text "Not selected" in "Insurance PTF" internal portfolio in Analysis page
    And I should see a text "Not selected" in "External PTF" external portfolio in Analysis page
    And I should see the "Active PTF" internal portfolio table "expanded" in Analysis page
    And I should see the "Passive PTF" internal portfolio table "collapsed" in Analysis page
    And I should see the "Insurance PTF" internal portfolio table "collapsed" in Analysis page
    And I should see the "External PTF" external portfolio table "collapsed" in Analysis page

  @active @NAVIGEA-420 @NAVIGEA-420-v2-- @Advisory_module @Analysis-page
  Scenario: NAVIGEA-420--v2--Default values in Analysis page.
    - that all portfolio tables are collapsed, when I enter the advisory module (start page)
    - that all portfolio tables of unselected portfolios contain an overlay and the text "Not selected" to indicate, that these portfolios won't be touched within the advisory session

    Given I am logged in with organization "TetralogSystemsQA" and CrmKey "SeleniumTestPortfolio_Default"
    And I navigate to the "Advisory" module
    Then I should see a text "Not selected" in "Internal portfolio 1" internal portfolio in Analysis page
    And I should see a text "Not selected" in "Internal portfolio 2" internal portfolio in Analysis page
    And I should see a text "Not selected" in "External portfolio" external portfolio in Analysis page
    And I should see the "Internal portfolio 1" internal portfolio table "disabled"
    And I should see the "Internal portfolio 2" internal portfolio table "disabled"
    And I should see the "External portfolio" external portfolio table "disabled"
    And I should see the "Internal portfolio 1" internal portfolio table "collapsed" in Analysis page
    And I should see the "Internal portfolio 2" internal portfolio table "collapsed" in Analysis page
    And I should see the "External portfolio" external portfolio table "collapsed" in Analysis page

  @active @NAVIGEA-420 @NAVIGEA-420-v3-- @Advisory_module @Analysis-page
  Scenario: NAVIGEA-420--v3--Use case "Manual setup"
    - select portfolios in the Advisory basic setup area. 
    - that selected portfolios are expanded and the overlay/Not select text are removed

    Given I am logged in with organization "TetralogSystemsQA" and CrmKey "QaTestData06"
    And I navigate to the "Advisory" module
    And In the Basic advisory setup area,I select the "Manual setup" use case in the dropdown
    And In the Basic advisory setup area, I select the following checkboxes:
      | name                         | state |
      | Internal active - Active PTF | true  |
      | External - External PTF      | true  |
    And I press the Apply/Proceed button
    And I click on the "Show Parameters" button
    Then I should see a text "Selected" in "Active PTF" internal portfolio in Parameters page
    And I should see a text "Not selected" in "Passive PTF" internal portfolio in Parameters page
    And I should see a text "Not selected" in "Insurance PTF" internal portfolio in Parameters page
    And I should see a text "Selected" in "External PTF" external portfolio in Parameters page
    And I should see the "Passive PTF" internal portfolio table "disabled" in Parameters page
    And I should see the "Insurance PTF" internal portfolio table "disabled" in Parameters page
    And I should see the "Active PTF" internal portfolio table "enabled" in Parameters page
    And I should see the "External PTF" external portfolio table "enabled" in Parameters page
    And I should see "Use Case: Manual setup" in the use case label in Setup page

  @active @NAVIGEA-420 @NAVIGEA-420-v4-- @Advisory_module @Analysis-page
  Scenario: NAVIGEA-420--v4--Use case "optimize-rebalancing"
     - select portfolios in the Advisory basic setup area. 
     - that selected portfolios are expanded and the overlay/Not select text are removed

    Given I am logged in with organization "TetralogSystemsQA" and CrmKey "SeleniumTestPortfolio_Default"
    When I navigate to the "Advisory" module
    And I select the "Analysis" page
    And In the Basic advisory setup area,I select the "Optimize - rebalancing" use case in the dropdown
    And In the Basic advisory setup area, I select the following checkboxes:
      | name                                    | state |
      | Internal passive - Internal portfolio 2 | true  |
    And I press the Apply/Proceed button
    And I select the "Setup" page
    And I click on the "Show Parameters" button
    Then I should see a text "Not selected" in "Internal portfolio 1" internal portfolio in Parameters page
    And I should see a text "Selected" in "Internal portfolio 2" internal portfolio in Parameters page
    And I should see a text "Not selected" in "External portfolio" external portfolio in Parameters page
    And I should see the "Internal portfolio 2" internal portfolio table "enabled" in Parameters page
    And I should see the "Internal portfolio 1" internal portfolio table "disabled" in Parameters page
    And I should see the "External portfolio" external portfolio table "disabled" in Parameters page
    And I should see "Use Case: Optimize - rebalancing" in the use case label in Setup page

  @active @NAVIGEA-472 @NAVIGEA-472-3 @Advisory_module
  Scenario: @NAVIGEA-472-3: Use case "optimize-rebalancing"
    1) at the setup page
      Use case: Optimize - rebelacing - adapted to specific customer requirements.
    2) at the results page
    Use case: Optimize - rebelacing - adapted.

    Given I am logged in with organization "TetralogSystemsQA" and CrmKey "SeleniumTestPortfolio_Default"
    When I navigate to the "Advisory" module
    And I select the "Analysis" page
    And In the Basic advisory setup area,I select the "Optimize - rebalancing" use case in the dropdown
    And In the Basic advisory setup area, I select the following checkboxes:
      | name                                   | state |
      | Internal active - Internal portfolio 1 | true  |
      | External - External portfolio          | true  |
    And I select the "Setup" page
    And I click on the "Show Parameters" button
    And I enter the following values in "Internal portfolio 1" table in Parameters page
      | oder | name                              | minSellPercentage | minSellValue | maxBuyPercentage | maxBuyValue |
      | 1    | AB Global Growth Trends A USD Acc |                   | 10000.00     |                  | 20000.00    |
    Then I should see "Use Case: Optimize - rebalancing - adapted to specific customer requirements" in the use case label in Setup page
    And I select the "Result" page
    And I should see "Use Case: Optimize - rebalancing - adapted" in the use case label in Result page
