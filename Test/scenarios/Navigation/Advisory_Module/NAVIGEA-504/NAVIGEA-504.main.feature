Feature: NAVIGEA-504: AV - General: Detecting of manual changes and storing within the AV session

  @active @NAVIGEA-504 @NAVIGEA-504-v1-- @Advisory_module @Analysis-page
  Scenario: NAVIGEA-504--v1--Analysis page
    Given I am logged in with organization "TetralogSystemsQA" and CrmKey "SeleniumTestPortfolio_QAPortfolio"
    When I navigate to the "Advisory" module
    And I enter a "200000" in Withdrawal or additional field
    And I select the "Moderate" option in investment strategy dropdown
    And I click on the popup ok button
    And In the Basic advisory setup area,I select the "Manual setup" use case in the dropdown
    And In the Basic advisory setup area, I select the following checkboxes:
      | name                         | state |
      | Internal active - Active PTF | true  |
    And I press the Apply/Proceed button
    And I select the "Analysis" page
    Then In the Basic advisory setup area, I should see the "200,000" in Withdrawal or additional field
    And In the Basic advisory setup area, I should see the "Moderate" selected in investment strategy dropdown
    And In the Basic advisory setup area, I should see the "Manual setup" selected in use case dropdown
    And In the Basic advisory setup area, I should see the following checkboxes selected:
      | name                         | state |
      | Internal active - Active PTF | true  |

  @dev @fbecaria @NAVIGEA-504 @NAVIGEA-504-v2 @Advisory_module @Parameters_page
  Scenario: Navigea-504--v2: Parameters page
    Given I am logged in the application
    And I navigate to the "Advisory" module
    And In the Basic advisory setup area, I select the following checkboxes:
      | name                                 | state |
      | Internal active-Internal portfolio 1 | true  |
    And I select the "Setup" page
    And I click on the "Show Parameters" button
    When I enter the following values in "Internal portfolio 1" table in Parameters page
      | order | name                                           | dontSell | dontBuy | minSellPercentage | minSellValue | maxBuyPercentage | maxBuyValue |
      | 1     | AB Global Growth Trends A USD Acc              | false    | true    | 0                 | 100.00       |                  |             |
      | 2     | Aberdeen Global - Asian Smaller Companies Fund | true     | false   |                   |              | 20               | 20,764.00   |
    And I navigate to the "Portfolio" module
    And I navigate to the "Advisory" module
    And I select the "Setup" page
    And I click on the "Show Parameters" button
    Then I should see the table "Internal portfolio 1" in Parameters page as
      | order | name                                           | securityExposure | volatilityRiskClass | complexityRiskClass | liquidityRiskClass | datafeedStatus | dontSell | minSellPercentage | minSellValue | dontBuy | maxBuyPercentage | maxBuyValue | currentWeight | value  |
      | 1     | AB Global Growth Trends A USD Acc              | Global           | H                   | L                   | L                  | VALID          | false    | 0                 | 100.00       | true    |                  |             | 21.67         | 22,493 |
      | 2     | Aberdeen Global - Asian Smaller Companies Fund | Asia             | H                   | L                   | L                  | VALID          | true     |                   |              | false   | 20               | 20,764.00   | 40.04         | 41,568 |

  @dev @NAVIGEA-504 @NAVIGEA-504-v3 @Advisory_module @Favorites_page
  Scenario: Navigea-504--v3:Favorites page
    Given I am logged in the portfolio overview screen
    And I navigate to the "Advisory" Module
    And I select the "Setup" page
    And I click on the "Show Favorites" button
    When I enter the following values in "Dummy Favorite List 2" table in "Favorites" page
      | oder | isin         | name                       | Min% | Min value | Max% | Max value |
      | 1    | QAEEUR122001 | Copy of Daimler N.A.       | 10   |           | 20   |           |
      | 2    | QAEDKK122003 | Copy A.P. Møller - Mærsk A | 20   |           | 40   |           |
    And I navigate to the "Portfolio" page
    And I navigate to the "Advisory" Module
    And I select the "Setup" page
    And I click on the "Show Favorites" button
    Then I should see the table "Dummy Favorite List 2" in "Favorites" page as
      | oder | isin         | name                       | Sec. class                | Sec. Exposure | Risk_class_V | Risk_class_C | Risk_class_L | Datafeed | Min % | Min Value | Max % | Max Value | Include |
      | 1    | QAEEUR122001 | Copy of Daimler N.A.       | Capital protected product | Norge         | L            | H            | M            | VALID    | 10    | 10,382.00 | 20    | 20,764.00 | checked |
      | 2    | QAEDKK122003 | Copy A.P. Møller - Mærsk A | Capital protected product | Norge         | L            | H            | M            | VALID    | 20    | 20,764.00 | 40    | 41,528.00 | checked |

  @dev @NAVIGEA-504 @NAVIGEA-504-v4 @Advisory_module @Transactions_page
  Scenario: @NAVIGEA-504-v4-- Transactions page
    Given I am logged in the application as "SeleniumTestPortfolio_QAPortfolio"
    And I navigate to the "Advisory" Module
    And In the Basic advisory setup area, I select the following checkboxes:
      | name                       | state |
      | Internal active-Active PTF | true  |
    And I select the "Results" page
    And I click on the "Show transactions" button
    When I enter the following values for the internal portfolio "Active PTF" in Transaction page.
      | order | name                                       | buy/sell(modify) |
      | 1     | Copy of LU0048578792 Fidelity Europ Growth | 400000           |
    And I select the "Documentation" page
    And I select the "Results" page
    And I click on the "Show transactions" button
    Then I should see the following values for the internal portfolio "Active PTF" in Transaction page:
      | order | name                                       | buy/sell(modify) |
      | 1     | Copy of LU0048578792 Fidelity Europ Growth | 400000           |
