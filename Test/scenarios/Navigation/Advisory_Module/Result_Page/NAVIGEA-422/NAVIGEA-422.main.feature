Feature: NAVIGEA-422: AV - Results: Results and Transactions tables summaries: Additional row /change of row contents
   NAVIGEA-426: AV AV - START: Basic advisory setup area 1: Entry field "Withdrawal or addiditional"
   NAVIGEA-569: AV - General: Rename "Loan based additional investment" to "Additional investment" everywhere

  @active @NAVIGEA-422 @NAVIGEA-422-v1 @NAVIGEA-426-v1 @NAVIGEA-569 @Advisory_module @Result_page
  Scenario: @NAVIGEA-422-v1-NAVIGEA-426-v1-- Check the values in the dropdown.
    Given I am logged in with organization "TetralogSystemsQA" and CrmKey "QaTestData06"
    And I navigate to the "Advisory" module
    And I enter a "200000" in Withdrawal or additional field
    And In the Basic advisory setup area,I select the "Optimize - rebalancing" use case in the dropdown
    And In the Basic advisory setup area, I select the following checkboxes:
      | name                           | state |
      | Internal active - Active PTF   | true  |
      | Internal passive - Passive PTF | true  |
      | External - External PTF        | true  |
    And I press the Apply/Proceed button
    Then I should see the Results summary table expanded in the following way
      | Summary                | Weight% | Value NOK |
      | Internal portfolios    |         |           |
      | Current                | 62.59   | 1,087,465 |
      | Additional investments | 11.51   | 200,000   |
      | Withdrawals            | 0.00    | 0         |
      | Recommended            | 86.06   | 1,495,329 |
      | External portfolios    |         |           |
      | Current                | 25.90   | 450,092   |
      | Recommended            | 13.94   | 242,229   |
      | Current portfolios     | 88.49   | 1,537,558 |
      | Additional investments | 11.51   | 200,000   |
      | Withdrawals            | 0.00    | 0         |
      | Recommended portfolios | 100.00  | 1,737,558 |
      | Balance                | 0.00    | 0         |
    And I should see the Results summary table reduced in the following way
      | Summary                | Weight%| Value NOK |
      | Current portfolios     | 88.49  | 1,537,558 |
      | Additional investments | 11.51  | 200,000   |
      | Withdrawals            | 0.00   | 0         |
      | Recommended portfolios | 100.00 | 1,737,558 |
      | Balance                | 0.00   | 0         |

  @active @NAVIGEA-422 @NAVIGEA-422-v2 @NAVIGEA-426-v2 @NAVIGEA-569 @Advisory_module @Transactionen_page
  Scenario: @NAVIGEA-422-v2-NAVIGEA-426-v2-- Check the values in the dropdown.
    Given I am logged in with organization "TetralogSystemsQA" and CrmKey "QaTestData06"
    And I navigate to the "Advisory" module
    And I enter a "200000" in Withdrawal or additional field
    And In the Basic advisory setup area,I select the "Optimize - rebalancing" use case in the dropdown
    And In the Basic advisory setup area, I select the following checkboxes:
      | name                           | state |
      | Internal active - Active PTF   | true  |
      | Internal passive - Passive PTF | true  |
      | External - External PTF        | true  |
    And I press the Apply/Proceed button
    And I press the Show Transactions button
    Then I should see the Transactions summary table expanded in the following way
      | Summary                | Value NOK |
      | Internal portfolios    |           |
      | Additional investments | 200,000   |
      | Withdrawals            | 0         |
      | Buy recommendations    | 652,824   |
      | Sell recommendations   | 244,960   |
      | External portfolios    |           |
      | Sell recommendations   | 207,864   |
      | Additional investments | 200,000   |
      | Withdrawals            | 0         |
      | Buy recommendations    | 652,824   |
      | Sell recommendations   | 452,824   |
      | Balance                | 0         |
    And I should see the Transactions summary table reduced in the following way
      | Summary                | Value NOK |
      | Additional investments | 200,000   |
      | Withdrawals            | 0         |
      | Buy recommendations    | 652,824   |
      | Sell recommendations   | 452,824   |
      | Balance                | 0         |

  @active @NAVIGEA-422 @NAVIGEA-422-v3 @NAVIGEA-426-v3 @NAVIGEA-569 @Advisory_module @Result_page
  Scenario: @NAVIGEA-422-v3-NAVIGEA-426-v3-- Check the values in the dropdown.
    Given I am logged in with organization "TetralogSystemsQA" and CrmKey "QaTestData06"
    And I navigate to the "Advisory" module
    And I enter a "-100000" in Withdrawal or additional field
    And In the Basic advisory setup area,I select the "Optimize - rebalancing" use case in the dropdown
    And In the Basic advisory setup area, I select the following checkboxes:
      | name                           | state |
      | Internal active - Active PTF   | true  |
      | Internal passive - Passive PTF | true  |
      | External - External PTF        | true  |
    And I press the Apply/Proceed button
    Then I should see the Results summary table expanded in the following way
      | Summary                | Weight% | Value NOK |
      | Internal portfolios    |         |           |
      | Current                | 75.65   | 1,087,465 |
      | Additional investments | 0.00    | 0         |
      | Withdrawals            | -6.96   | -100,000  |
      | Recommended            | 83.15   | 1,195,329 |
      | External portfolios    |         |           |
      | Current                | 31.31   | 450,092   |
      | Recommended            | 16.85   | 242,229   |
      | Current portfolios     | 106.96  | 1,537,558 |
      | Additional investments | 0.00    | 0         |
      | Withdrawals            | -6.96   | -100,000  |
      | Recommended portfolios | 100.00  | 1,437,558 |
      | Balance                | 0.00    | 0         |
    And I should see the Results summary table reduced in the following way
      | Summary                | Weight% | Value NOK |
      | Current portfolios     | 106.96  | 1,537,558 |
      | Additional investments | 0.00    | 0         |
      | Withdrawals            | -6.96   | -100,000  |
      | Recommended portfolios | 100.00  | 1,437,558 |
      | Balance                | 0.00    | 0         |

  @active @NAVIGEA-422 @NAVIGEA-422-v4 @NAVIGEA-426-v4 @NAVIGEA-569 @Advisory_module @Transactionen_page
  Scenario: @NAVIGEA-422-v2-NAVIGEA-426-v4-- Check the values in the dropdown.
    Given I am logged in with organization "TetralogSystemsQA" and CrmKey "QaTestData06"
    And I navigate to the "Advisory" module
    And I enter a "-100000" in Withdrawal or additional field
    And In the Basic advisory setup area,I select the "Optimize - rebalancing" use case in the dropdown
    And In the Basic advisory setup area, I select the following checkboxes:
      | name                           | state |
      | Internal active - Active PTF   | true  |
      | Internal passive - Passive PTF | true  |
      | External - External PTF        | true  |
    And I press the Apply/Proceed button
    And I press the Show Transactions button
    Then I should see the Transactions summary table expanded in the following way
      | Summary                | Value NOK |
      | Internal portfolios    |           |
      | Additional investments | 0         |
      | Withdrawals            | -100,000  |
      | Buy recommendations    | 403,304   |
      | Sell recommendations   | 295,441   |
      | External portfolios    |           |
      | Sell recommendations   | 207,864   |
      | Additional investments | 0         |
      | Withdrawals            | -100,000  |
      | Buy recommendations    | 403,304   |
      | Sell recommendations   | 503,304   |
      | Balance                | 0         |
    And I should see the Transactions summary table reduced in the following way
      | Summary                | Value NOK |
      | Additional investments | 0         |
      | Withdrawals            | -100,000  |
      | Buy recommendations    | 403,304   |
      | Sell recommendations   | 503,304   |
      | Balance                | 0         |
