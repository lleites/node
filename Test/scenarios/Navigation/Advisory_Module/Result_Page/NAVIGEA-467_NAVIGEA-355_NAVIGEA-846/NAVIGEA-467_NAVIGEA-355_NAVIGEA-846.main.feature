Feature: NAVIGEA-355:AV - Results: T-TABLE Button "Update transactions"
         NAVIGEA-467:AV - Results: T-TABLE Changing number of shares (entry fields): Behavior
         NAVIGEA-568-AV -  Results: Sharpe Ratio is not calculated properly
		 NAVIGEA-846: AV - The tooltip in value<currency> is not shown with 2 decimals
		 	
  @dev @NAVIGEA-467 @Advisory_module @Result_page
  Scenario: @NAVIGEA-467-v1-- entries are made and the button "Update trasaction" was not yet clicked, and the advisor tries to leave the transaction table of the Result page.
    Click in "Apply updates" button in the popup

    Given I am logged in the application
    And I navigate to the "Advisory" module
    When I select the "Result" page
    And I click on the "Show transactions" button
    And I enter the following values in "Buy recommendation" table for the "Internal portfolio 2" in Transaction page.
      | order | name                       | buy_sellmodify |
      | 2     | Copy A.P. Møller - Mærsk A | 20             |
    And I click on the "Show results" button
    And the app shows the following message: "You changed the transactions suggested by the initial optimization... Do you want to update or clear the changes you made here?"
    And I click in "Apply updates" button in the popup
    Then The app should show the "Internal portfolio 2" table in "Results" page as
      | order | name                                           | securityExposure | volatilityRiskClass | complexityRiskClass | liquidityRiskClass | currentShare | currentWeight% | currentValueNOK | recommendedShare | recommendedWeight% | recommendedValueNOK |
      | 1    | Aberdeen Global - Asian Smaller Companies Fund | Asia             | H                   | L                   | L                  | 86           | 28.93          | 30,032          | 62               | 2.27               | 21,548              |
      | 2    | Aberdeen Global - Emerging Markets Equity Fund | Emerging markets | H                   | L                   | L                  | 11           | 0.89           | 921             | 213              | 1.89               | 17,910              |
      | 3    | Copy A.P. Møller - Mærsk A                     | Norge            | L                   | H                   | M                  | 0            | 0.00           | 0               | 20               | 89.28              | 847,792             |
      | 4    | Copy of Daimler N.A.                           | Norge            | L                   | H                   | M                  | 0            | 0.00           | 0               | 7                | 0.23               | 2,205               |

  @dev @NAVIGEA-467 @Advisory_module @Result_page
  Scenario: @NAVIGEA-467--v2 entries are made and the button "Update trasaction" was not yet clicked, and the advisor tries to leave the transaction table of the Result page.
    Click in "Discard updates" button in the popup

    Given I am logged in the application
    And I navigate to the "Advisory" module
    When I select the "Result" page
    And I click on the "Show transactions" button
    And I enter the following values in "Buy recommendation" table for the "Internal portfolio 2" in Transaction page.
      | order | name                       | buy_sellmodify |
      | 2     | Copy A.P. Møller - Mærsk A | 20             |
    And I click on the "Show results" button
    And the app show the following message:"You changed the transactions suggested by the initial optimization... Do you want to update or clear the changes you made here?"
    And I click in "Discard updates" button in the popup
    Then The app should show the "Internal portfolio 2" table in "Results" page as
      | oder | name                                           | securityExposure | volatilityRiskClass | complexityRiskClass | liquidityRiskClass | currentShare | currentWeight% | currentValueNOK | recommendedShare | recommendedWeight% | recommendedValueNOK |
      | 1    | Aberdeen Global - Asian Smaller Companies Fund | Asia             | H                   | L                   | L                  | 86           | 28.93          | 30,032          | 62               | 20.61              | 21,548              |
      | 2    | Aberdeen Global - Emerging Markets Equity Fund | Emerging markets | H                   | L                   | L                  | 11           | 0.89           | 921             | 213              | 17.13              | 17,910              |
      | 3    | Copy A.P. Møller - Mærsk A                     | Norge            | L                   | H                   | M                  | 0            | 0.00           | 0               | 0                | 2.60               | 2,715               |
      | 4    | Copy of Daimler N.A.                           | Norge            | L                   | H                   | M                  | 0            | 0.00           | 0               | 7                | 2.11               | 2,205               |

  @dev @NAVIGEA-355 @NAVIGEA-472 @NAVIGEA-568-v3 @Advisory_module @Result_page
  Scenario: @NAVIGEA-355--@NAVIGEA-472--@NAVIGEA-568-v3 --Change in "buy/sell modify" field a value.
    Check that this relocate updates the results table and the analysis charts and tables.

    Given I am logged in the application as "SeleniumTestPortfolio_QAPortfolio"
    And I navigate to the "Advisory" module
    When I select the "Analysis" page
    And In the Basic advisory setup area, I select the following checkboxes:
      | name                 | state |
      | ACTIVE Active PTF    | true  |
      | PASSIVE Passive PTF  | true  |
      | GENERAL External PTF | true  |
    And I select the "Result" page
    And I click on the "Show transactions" button
    And I enter the following values in "Buy recommendation" table for the "Internal portfolio 2" in Transaction page.
      | order | name                 | buy_sellmodify |
      | 1     | Copy of Daimler N.A. | 1000,000       |
    And I click on the "Update transactions" button
    And I click on the "Show results" button
    Then The app should show the following securities in "Internal portfolio 2" in Results page
      | oder | name                 | securityExposure       | volatilityRiskClass | complexityRiskClass | liquidityRiskClass | currentShare | currentWeight % | currentValueNOK | recommendedShare | recommendedWeight % | recommendedValueNOK |
      | 1    | Copy of Microsoft    | Equities International | M                   | M                   | H                  | 86           | 0,35            | 12 860          | 85               | 0,32                | 12 711              |
      | 2    | Copy of Daimler N.A. | Equities International | L                   | H                   | M                  | 0            | 0,00            | 0               | 1000             | 7,58                | 305 755             |
    And The app should show the following values in the "Efficiency-Risk/Return" table
      | description            | risk  | performance | sharpeRatio |
      | Current portfolios     | 41,01 | 8,90        | 0,17        |
      | Recommended portfolios | 39,63 | 9,00        | 0,19        |
      | Result                 | -1,38 | 0,10        | 0,02        |
    And I should see the following fields in the Results summary table expanded:
      | Summary                | Weight% | Value NOK |
      | Internal portfolios    |         |           |
      | Current                | 99,97   | 3 724 731 |
      | Additional investments | 0,00    | 0         |
      | Withdrawals            | 0.00    | 0         |
      | Recommended            | 100,00  | 3 725 849 |
      | External portfolios    |         |           |
      | Current                | 0,03    | 1 118     |
      | Recommended            | 0,00    | 0         |
      | Current portfolios     | 100,00  | 3 725 849 |
      | Additional investments | 0,00    | 0         |
      | Withdrawals            | 0,00    | 0         |
      | Recommended portfolios | 100,00  | 3 725 849 |
      | Balance                |         |           |
      
  @active @NAVIGEA-846 @Advisory_module @Result_page
  Scenario: @NAVIGEA-846 --Change in "buy/sell modify" field a value.
    Check the tooltip is ok

    Given I am logged in with organization "TetralogSystemsQA" and CrmKey "QaTestData06"
    And I navigate to the "Advisory" module
    When I select the "Analysis" page
    And In the Basic advisory setup area, I select the following checkboxes:
      | name                           | state |
      | Internal active - Active PTF   | true  |
      | Internal passive - Passive PTF | true  |
      | External - External PTF        | true  |
    And I select the "Result" page
    And I press the Show Transactions button
    And I enter the following values in Buy recommendation table for the "Active PTF" in Transaction page.
      | order | name                  | buySell        |
      | 1     | Navigea Synthetic HLM | 45,000         |
    And I hover the mouse over the "value" column in portfolio name "Active PTF", buy recommendation section and position number 1 in Transaction page$
  	Then I should see a tooltip with text
  	  """
  	  31,468.55
  	  """
