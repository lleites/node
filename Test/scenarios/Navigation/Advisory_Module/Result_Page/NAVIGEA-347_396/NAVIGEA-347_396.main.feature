Feature: NAVIGEA-347: AV - Results: T-TABLE Transaction table: insert currency columns/switch columns price and price limits
         NAVIGEA-396: AV - Results: T-TABLE Transaction table: sensitivity of price limits entry fields

  @active @NAVIGEA-347 @NAVIGEA-396-v1 @Advisory_module @Transactions_page
  Scenario: NAVIGEA-347_@NAVIGEA-396-v1 -Transaction page: Checks the headers of the table. Checks that the price limit upper/lower are enabled only for products which got the Liquidity Risks "medium" or "high".
    Given I am logged in with organization "TetralogSystemsQA" and CrmKey "QaTestData07"
    And I navigate to the "Advisory" module
    And In the Basic advisory setup area, I select the following checkboxes:
      | name                                    | state |
      | Internal passive - Internal passive PTF | true  |
    And I select the "Setup" page
    And I click on the "Show Parameters" button
    And I enter the following values in "Internal passive PTF" table in Parameters page
      | order | name                                                     | dontSell | dontBuy |
      | 1     | Copy of Daimler N.A.                                     | false    | false   |
      | 2     | Copy of Microsoft                                        | false    | false   |
      | 3     | Copy A.P. Møller - Mærsk A                               | false    | false   |
      | 4     | Copy of LU0048578792 Fidelity Europ Growth               | false    | false   |
      | 5     | Garantum Kreditobligation High Yield Europa 5 år nr 1408 | false    | false   |
    When I select the "Result" page
    And I click on the "Show transactions" button in the Result page
    Then I should see the following values in the Buy recommendation table for the internal portfolio "Internal passive PTF" in Transaction page.
      | order | name                                                     | volatilityRiskClass | complexityRiskClass | liquidityRiskClass | limitLower | limitUpper |
      | 1     | Copy of Microsoft                                        | M                   | M                   | H                  | enabled    | enabled    |
      | 2     | Copy of LU0048578792 Fidelity Europ Growth               | H                   | L                   | L                  | disabled   | disabled   |
      | 3     | Garantum Kreditobligation High Yield Europa 5 år nr 1408 | H                   | L                   | M                  | enabled    | enabled    |
    And I should see the following values in the Sell recommendation table for the internal portfolio "Internal passive PTF" in Transaction page.
      | order | name                       | volatilityRiskClass | complexityRiskClass | liquidityRiskClass | limitLower | limitUpper |
      | 1     | Copy of Daimler N.A.       | L                   | H                   | M                  | enabled    | enabled    |
      | 2     | Copy A.P. Møller - Mærsk A | L                   | H                   | M                  | enabled    | enabled    |

  @active @NAVIGEA-396-v2 @Advisory_module @Transactions_page
  Scenario: @NAVIGEA-396-v2 -Transaction page: Checks the invalid characters message is shown.
    Given I am logged in with organization "TetralogSystemsQA" and CrmKey "QaTestData03"
    And I navigate to the "Advisory" module
    And In the Basic advisory setup area, I select the following checkboxes:
      | name                               | state |
      | Internal insurance - Insurance PTF | true  |
    And I select the "Setup" page
    And I click on the "Show Parameters" button
    And I enter the following values in "Insurance PTF" table in Parameters page
      | order | name                                                     | dontSell | dontBuy |
      | 1     | Jyske Invest Globale Aktier Udb                          | false    | false   |
      | 2     | Garantum Kreditobligation High Yield Europa 5 år nr 1408 | false    | false   |
      | 3     | Næringsbygg Holding 1 AS                                 | false    | false   |
    And I select the "Result" page
    And I click on the "Show transactions" button in the Result page
    When I enter the following values in Buy recommendation table for the "Insurance PTF" in Transaction page.
      | order | name                     | volatilityRiskClass | complexityRiskClass | liquidityRiskClass | limitLower | limitUpper |
      | 3     | Navigea Synthetic MMM    | M                   | M                   | M                  | mm.00      |            |
    Then I should see in Buy recommendation table for the "Insurance PTF" a visual clue next to the "limitLower" entry field for position "Navigea Synthetic MMM" 
    And when I click in Buy recommendation table "Insurance PTF" on the visual clue next to the "limitLower" entry field in position name "Navigea Synthetic MMM" I should see the following message: "You have entered invalid characters. Please use numeric values only!"

  @active @NAVIGEA-396-v3 @Advisory_module @Transactions_page
  Scenario: @NAVIGEA-396-v3 -Transaction page: Checks that the upper limit has to be higher than lower limit.
    Given I am logged in with organization "TetralogSystemsQA" and CrmKey "QaTestData03"
    And I navigate to the "Advisory" module
    And In the Basic advisory setup area, I select the following checkboxes:
      | name                               | state |
      | Internal insurance - Insurance PTF | true  |
    And I select the "Result" page
    And I click on the "Show transactions" button in the Result page
    When I enter the following values in Buy recommendation table for the "Insurance PTF" in Transaction page.
      | order | name                       | volatilityRiskClass | complexityRiskClass | liquidityRiskClass | limitLower | limitUpper |
      | 3     | Navigea Synthetic MMM      | L                   | H                   | M                  | 3000.00    | 2000.00    |
    Then I should see in Buy recommendation table for the "Insurance PTF" a visual clue next to the "limitUpper" entry field for position "Navigea Synthetic MMM"
	And when I click in Buy recommendation table "Insurance PTF" on the visual clue next to the "limitUpper" entry field in position name "Navigea Synthetic MMM" I should see the following message: "The minimum value cannot be greater than the maximum"

  @active @NAVIGEA-396-v4 @Advisory_module @Transactions_page
  Scenario: @NAVIGEA-396-v4--Transaction page: Checks that, for products which got the Liquidity Risks "medium" or "high", entering a value in price limits [lower and upper] is allowed.
    Given I am logged in with organization "TetralogSystemsQA" and CrmKey "QaTestData07"
    When I navigate to the "Advisory" module
    And In the Basic advisory setup area, I select the following checkboxes:
      | name                                    | state |
      | Internal passive - Internal passive PTF | true  |
    And I select the "Setup" page
    And I click on the "Show Parameters" button
    And I enter the following values in "Internal passive PTF" table in Parameters page
      | order | name                                                     	| dontSell | dontBuy |
      | 1     | Copy of Daimler N.A.                          				| false    | false   |
      | 2     | Copy of Microsoft 											| false    | false   |
      | 3     | Copy A.P. Møller - Mærsk A                                 	| false    | false   |
      | 5     | Garantum Kreditobligation High Yield Europa 5 år nr 1408	| false    | false   |
    And I select the "Result" page
    And I click on the "Show transactions" button in the Result page
	Then I should see the following values in the Buy recommendation table for the internal portfolio "Internal passive PTF" in Transaction page.
      | order | name                                                     | volatilityRiskClass | complexityRiskClass | liquidityRiskClass | limitLower | limitUpper |
      | 1     | Copy of Microsoft                                        | M                   | M                   | H                  | enabled    | enabled    |
      | 2     | Copy of LU0048578792 Fidelity Europ Growth               | H                   | L                   | L                  | disabled   | disabled   |
      | 3     | Garantum Kreditobligation High Yield Europa 5 år nr 1408 | H                   | L                   | M                  | enabled    | enabled    |