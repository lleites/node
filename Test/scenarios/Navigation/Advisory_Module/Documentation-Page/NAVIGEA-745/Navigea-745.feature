Feature: NAVIGEA-745: Documentation Page - General

@active @Advisory_module @Documentation_page @Chapters
  Scenario: Navigea-745--TetralogSystemsQA---- Check default selected chapters in the Documentation page.
    Given I am logged in with organization "TetralogSystemsQA" and CrmKey "QaTestData13"
    When I navigate to the "Advisory" module
    And I select the "Analysis" page
    And In the Basic advisory setup area, I select the following checkboxes:
      | name                               | state |
      | Internal passive - IntPASS         | true  |
      | Internal active - IntACT           | true  |
      | External - External_SEB HIEH YIELD | true  |
      | External - External Underspecified | true  |
    And I select the "Result" page
    And I select the "Documentation" page
    Then The following chapters have to be active
      | name                                                 | state |
      | Basic document                                       | true  |
      | Recommendation/Analysis details                      | false  |
      | Efficiency analysis                                  | false  |
      | Portfolio structure analysis: Security Class         | false  | 
      | Portfolio structure analysis: Security Exposure      | false  |
      | Portfolio structure analysis: Volatility Class       | false  |
      | Portfolio structure analysis: Complexity Class       | false  |
      | Portfolio structure analysis: Liquidity Class        | false  |
      | Forecast analysis                                    | false  |
      | Historical analysis                                  | false  |
      | Methodical background                                | false  |
      | Glossary                                             | false  |
      | KIIDs/Products information                           | true  |