Feature: NAVIGEA-850: Create a new External Portfolio and Generate AV-DOC

//THIS TEST HAS TO BE REIMPLEMENTED BECAUSE WE CAN NOT CHECK IF WE DOWNLOAD A FILE SUCCESSFULLY IN IE USING SELENIUM
//MAYBE WE CAN USE SOMETHING LIKE http://goo.gl/ftA4UF

@dev @Advisory_module @Documentation_page @AV-DOC
  Scenario: Navigea-850--TetralogSystemsQA---- Create a new External Portfolio and Generate AV-DOC
    Given I am logged in with organization "TetralogSystemsQA" and CrmKey "QaTestData13"
    When I navigate to the "Portfolio" module
    And I click on the new external portfolio button
    And I enter the value "Test Ext. Porfolio 1" on the portfolio name
    And I enter the value "General" on the portfolio type
    And I add "50" shares of position name "# SEB Europafond Småbolag (518)" and ISIN "SE0000433252"
    And I save the portfolio
    And I navigate back to the portfolio overview screen
    And I navigate to the "Advisory" module
    And I select the "Analysis" page
    And In the Basic advisory setup area, I select the following checkboxes:
      | name                               | state |
      | Internal passive - IntPASS         | true  |
      | Internal active - IntACT           | true  |
      | External - External_SEB HIEH YIELD | true  |
      | External - External Underspecified | true  |
      | External - Test Ext. Porfolio 1    | true  |
    And I select the "Result" page
    And I select the "Documentation" page
    And I generate the AV-DOC document
    Then I should see the AV-DOC document 