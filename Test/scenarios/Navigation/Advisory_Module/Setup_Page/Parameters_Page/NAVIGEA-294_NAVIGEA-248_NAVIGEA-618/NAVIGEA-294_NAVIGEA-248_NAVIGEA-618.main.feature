Feature:  NAVIGEA-294--Advisory module--AV-- Setup 3: Parameters
  NAVIGEA-555: AV - SETUP: Parameters: Don't buy/sell have to limit manual Min%/Value resp Max%/Value settings
  NAVIGEA-618: BA/SA: Product universe configuration: Implementation Max% parameter
  	
  @active @NAVIGEA-294 @NAVIGEA-294-v1 @Advisory_module @Parameters_page @Brian2 @error
  Scenario: Navigea-294--v1: Check parameters table data
    Within the portfolio tables, basically all instruments within the current portfolio are shown.

    Given I am logged in with organization "TetralogSystemsQA" and CrmKey "SeleniumTestPortfolio_Default"
    When I navigate to the "Advisory" module
    And I select the "Analysis" page
    And In the Basic advisory setup area, I select the following checkboxes:
      | name                                    | state |
      | Internal active - Internal portfolio 1  | true  |
      | Internal passive - Internal portfolio 2 | true  |
      | External - External portfolio           | true  |
    And I select the "Setup" page
    And I click on the "Show Parameters" button
    Then I should see the parameters table for the "internal" portfolio 1 as
      | order | name                                           | securityExposure            | volatilityRiskClass | complexityRiskClass | liquidityRiskClass | datafeedStatus | dontSell | minSellPercentage | minSellValue | dontBuy | maxBuyPercentage | maxBuyValue | currentWeight | value   |
      | 1     | AB Global Growth Trends A USD Acc              | UCITS Funds - International | H                   | L                   | L                  | VALID          | false    |                   |              | false   |                  |             | 9.86          | 22,493  |
      | 2     | Aberdeen Global - Asian Smaller Companies Fund | UCITS Funds - International | H                   | L                   | L                  | VALID          | false    |                   |              | false   |                  |             | 47.63         | 108,613 |
    And I should see the parameters table for the "internal" portfolio 2 as
      | order | name                                           | securityExposure               | volatilityRiskClass | complexityRiskClass | liquidityRiskClass | datafeedStatus | dontSell | minSellPercentage | minSellValue | dontBuy | maxBuyPercentage | maxBuyValue | currentWeight | value  |
      | 1     | Aberdeen Global - Asian Smaller Companies Fund | UCITS Funds - International    | H                   | L                   | L                  | VALID          | false    |                   |              | false   |                  |             | 34.41         | 78,473 |
      | 2     | Aberdeen Global - Emerging Markets Equity Fund | UCITS Funds - Emerging Markets | H                   | L                   | L                  | VALID          | false    |                   |              | false   |                  |             | 4.23          | 9,648  |
    And I should see the parameters table for the "external" portfolio 1 as
      | order | name                        | securityExposure               | volatilityRiskClass | complexityRiskClass | liquidityRiskClass | datafeedStatus | dontSell | minSellPercentage | minSellValue | dontBuy | maxBuyPercentage | maxBuyValue | currentWeight | value |
      | 1     | Bankinvest Asien            | UCITS Funds - Emerging Markets | H                   | L                   | L                  | VALID          | false    |                   |              | true    |                  |             | 2.77          | 6,324 |
      | 2     | Bankinvest Basis Aktier Udb | UCITS Funds - International    | H                   | L                   | L                  | VALID          | false    |                   |              | true    |                  |             | 0.49          | 1,110 |
      | 3     | Storebrand Barnespar        | UCITS Funds - International    | H                   | L                   | L                  | VALID          | false    |                   |              | true    |                  |             | 0.60          | 1,366 |

  @dev @NAVIGEA-294 @NAVIGEA-294-v2 @Advisory_module @Parameters_page
  Scenario: Navigea-294--v2: Check parameters table data
    I also want to be able to define min-% or min-value and max-% or max-value for single products within the portfolios
    => id general limits are broken here for min-% for cahs accounts, a message should be provided here

    Given I am logged in with organization "TetralogSystemsQA" and CrmKey "SeleniumTestPortfolio_Default"
    And I navigate to the "Advisory" module
    And I select the "Analysis" page
    And In the Basic advisory setup area, I select the following checkboxes:
      | name                                    | state |
      | Internal active - Internal portfolio 1  | true  |
      | Internal passive - Internal portfolio 2 | true  |
      | External - External portfolio           | true  |
    And I select the "Setup" page
    And I click on the "Show Parameters" button
    When I enter the following values in "Internal portfolio 1" table in Parameters page
      | oder | name                                           | minSellPercentage | minSellValue | maxBuyPercentage | maxBuyValue |
      | 1    | AB Global Growth Trends A USD Acc              | 10                |              | 5                |             |
      | 2    | Aberdeen Global - Asian Smaller Companies Fund |                   | 200000       |                  | 150000      |
    Then I should see the table "Internal portfolio 1" in Parameters page as
      | order | name                                           | securityExposure            | volatilityRiskClass | complexityRiskClass | liquidityRiskClass | datafeedStatus | dontSell | minSellPercentage | minSellValue | dontBuy | maxBuyPercentage | maxBuyValue | currentWeight | value   |
      | 1     | AB Global Growth Trends A USD Acc              | UCITS Funds - International | H                   | L                   | L                  | VALID          | false    | 10                | 22,803.20    | false   | 5                |             | 9.86          | 22,493  |
      | 2     | Aberdeen Global - Asian Smaller Companies Fund | UCITS Funds - International | H                   | L                   | L                  | VALID          | false    | 87.71             | 200,000.00   | false   | 100.00           | 150,000.00  | 47.63         | 108,613 |
    And I should see a visual clue next to the "maxBuyValue" entry field for position "Aberdeen Global - Asian Smaller Companies Fund" and ISIN "LU0231459107"
    And I should see a visual clue next to the "maxBuyPercentage" entry field for position "AB Global Growth Trends A USD Acc" and ISIN "LU0057025933"
    And when I click on the visual clue next to the "maxBuyValue" entry field for position "Aberdeen Global - Asian Smaller Companies Fund" and ISIN "LU0231459107" I should see the following message: "The minimun value cannot be greater than the maximum"
    And when I click on the visual clue next to the "maxBuyPercentage" entry field for position "AB Global Growth Trends A USD Acc" and ISIN "LU0057025933" I should see the following message: "The minimun value cannot be greater than the maximum"

  @dev @NAVIGEA-294 @NAVIGEA-294-v3 @Advisory_module @Parameters_page
  Scenario: Navigea-294--v3: Status can be changed manually by selecting or deselecting the checkboxes ("Don't sell" and "Don't buy")
    "Selected" deactivates corresponding "Min% / Min Value" AND "Min% / Min Value" entry fields
    (like it is implemented in invest suite):
    => "Don't sell" = selected => "Min% / Min Value" disabled
    => "Don't buy" = selected => "Max% / Max Value" disabled

    Given I am logged in with organization "TetralogSystemsQA" and CrmKey "SeleniumTestPortfolio_Default"
    And I navigate to the "Advisory" module
    And I select the "Analysis" page
    And In the Basic advisory setup area, I select the following checkboxes:
      | name                                    | state |
      | Internal active - Internal portfolio 1  | true  |
      | Internal passive - Internal portfolio 2 | true  |
      | External - External portfolio           | true  |
    And I select the "Setup" page
    And I click on the "Show Parameters" button
    When I enter the following values in "Internal portfolio 1" table in Parameters page
      | oder | name                                           | dontSell | dontBuy |
      | 1    | AB Global Growth Trends A USD Acc              | false    | true    |
      | 2    | Aberdeen Global - Asian Smaller Companies Fund | true     | false   |
    Then I should see the table "Internal portfolio 1" in Parameters page as
      | order | name                                           | securityExposure            | volatilityRiskClass | complexityRiskClass | liquidityRiskClass | datafeedStatus | dontSell | minSellPercentage | minSellValue | dontBuy | maxBuyPercentage | maxBuyValue | currentWeight | value   |
      | 1     | AB Global Growth Trends A USD Acc              | UCITS Funds - International | H                   | L                   | L                  | VALID          | false    |                   |              | true    |                  |             | 9.86          | 22,493  |
      | 2     | Aberdeen Global - Asian Smaller Companies Fund | UCITS Funds - International | H                   | L                   | L                  | VALID          | true     |                   |              | false   |                  |             | 47.63         | 108,613 |
    And I should see the following status for the "Internal portfolio 1" in Parameters page as
      | oder | name                                           | minSellPercentage | minSellValue | maxBuyPercentage | maxBuyValue |
      | 1    | AB Global Growth Trends A USD Acc              | enabled           | enabled      | disabled         | disabled    |
      | 2    | Aberdeen Global - Asian Smaller Companies Fund | disabled          | disabled     | enabled          | enabled     |

  @dev @NAVIGEA-294 @NAVIGEA-294-v4 @Advisory_module @Parameters_page
  Scenario: Navigea-294--v4: Status can be changed manually by selecting or deselecting the checkboxes ("Don't sell" and "Don't buy")
    "Selected" deactivates corresponding "Min% / Min Value" AND "Min% / Min Value" entry fields
    (like it is implemented in invest suite):
    => "Don't sell" = selected => "Min% / Min Value" disabled
    => "Don't buy" = selected => "Max% / Max Value" disabled

    Given I am logged in with organization "TetralogSystemsQA" and CrmKey "SeleniumTestPortfolio_Default"
    And I navigate to the "Advisory" module
    And I select the "Analysis" page
    And In the Basic advisory setup area, I select the following checkboxes:
      | name                                    | state |
      | Internal active - Internal portfolio 1  | true  |
      | Internal passive - Internal portfolio 2 | true  |
      | External - External portfolio           | true  |
    And I select the "Setup" page
    And I click on the "Show Parameters" button
    When I enter the following values in "Internal portfolio 1" table in Parameters page
      | oder | name                                           | dontSell | dontBuy |
      | 1    | AB Global Growth Trends A USD Acc              | false    | true    |
      | 2    | Aberdeen Global - Asian Smaller Companies Fund | true     | true    |
    And I enter the following values in "Internal portfolio 1" table in Parameters page
      | oder | name                                           | dontSell | dontBuy |
      | 1    | AB Global Growth Trends A USD Acc              | false    | false   |
      | 2    | Aberdeen Global - Asian Smaller Companies Fund | false    | false   |
    Then I should see the table "Internal portfolio 1" in Parameters page as
      | order | name                                           | securityExposure            | volatilityRiskClass | complexityRiskClass | liquidityRiskClass | datafeedStatus | dontSell | minSellPercentage | minSellValue | dontBuy | maxBuyPercentage | maxBuyValue | currentWeight | value   |
      | 1     | AB Global Growth Trends A USD Acc              | UCITS Funds - International | H                   | L                   | L                  | VALID          | false    |                   |              | false   |                  |             | 9.86          | 22,493  |
      | 2     | Aberdeen Global - Asian Smaller Companies Fund | UCITS Funds - International | H                   | L                   | L                  | VALID          | false    |                   |              | false   |                  |             | 47.63         | 108,613 |
    And I should see the following status for the "Internal portfolio 1" in Parameters page as
      | oder | name                                           | minSellPercentage | minSellValue | maxBuyPercentage | maxBuyValue |
      | 1    | AB Global Growth Trends A USD Acc              | enabled           | enabled      | enabled          | enabled     |
      | 2    | Aberdeen Global - Asian Smaller Companies Fund | enabled           | enabled      | enabled          | enabled     |

  @dev @NAVIGEA-294 @NAVIGEA-294-v5 @Advisory_module @Parameters_page
  Scenario: Navigea-294--v5: "Min% / Min Value" AND "Min% / Min Value".Either % or value can be set.
    if % is set, then the Value field is disabled but the resulting value 
    (in relation to the aggregated value of the overall portfolio) is displayed

    Given I am logged in with organization "TetralogSystemsQA" and CrmKey "SeleniumTestPortfolio_Default"
    And I navigate to the "Advisory" module
    And I select the "Analysis" page
    And In the Basic advisory setup area, I select the following checkboxes:
      | name                                    | state |
      | Internal active - Internal portfolio 1  | true  |
      | Internal passive - Internal portfolio 2 | true  |
      | External - External portfolio           | true  |
    And I select the "Setup" page
    And I click on the "Show Parameters" button
    When I enter the following values in "Internal portfolio 1" table in Parameters page
      | oder | name                              | minSellPercentage | maxBuyPercentage |
      | 1    | AB Global Growth Trends A USD Acc | 10                | 20               |
    And I enter the following values in "External Portfolio" table in Parameters page
      | oder | name                        | minSellPercentage | maxBuyPercentage |
      | 2    | Bankinvest Basis Aktier Udb | 0.20              |                  |
    Then I should see the table "Internal portfolio 1" in Parameters page as
      | order | name                                           | securityExposure            | volatilityRiskClass | complexityRiskClass | liquidityRiskClass | datafeedStatus | dontSell | minSellPercentage | minSellValue | dontBuy | maxBuyPercentage | maxBuyValue | currentWeight | value   |
      | 1     | AB Global Growth Trends A USD Acc              | UCITS Funds - International | H                   | L                   | L                  | VALID          | false    | 10                | 22,803.20    | false   | 20               | 45,606.39   | 9.86          | 22,493  |
      | 2     | Aberdeen Global - Asian Smaller Companies Fund | UCITS Funds - International | H                   | L                   | L                  | VALID          | false    |                   |              | false   |                  |             | 47.63         | 108,613 |
    And I should see the table "External Portfolio" in Parameters page as
      | order | name                        | securityExposure               | volatilityRiskClass | complexityRiskClass | liquidityRiskClass | datafeedStatus | dontSell | minSellPercentage | minSellValue | dontBuy | maxBuyPercentage | maxBuyValue | currentWeight | value |
      | 1     | Bankinvest Asien            | UCITS Funds - Emerging Markets | H                   | L                   | L                  | VALID          | false    |                   |              | false   |                  |             | 2.77          | 6,324 |
      | 2     | Bankinvest Basis Aktier Udb | UCITS Funds - International    | H                   | L                   | L                  | VALID          | false    | 0.20              | 456.06       | false   |                  |             | 0.49          | 1,110 |
      | 3     | Storebrand Barnespar        | UCITS Funds - International    | H                   | L                   | L                  | VALID          | false    |                   |              | false   |                  |             | 0.60          | 1,366 |

  @dev @NAVIGEA-294 @NAVIGEA-294-v6 @Advisory_module @Parameters_page
  Scenario: Navigea-294--v6: "Min% / Min Value" AND "Min% / Min Value".Either % or value can be set.
     if value is set, then the % field is disabled but the resulting % 
    (in relation to the aggregated value of the overall portfolio) is displayed

    Given I am logged in with organization "TetralogSystemsQA" and CrmKey "SeleniumTestPortfolio_Default"
    And I navigate to the "Advisory" module
    And I select the "Analysis" page
    And In the Basic advisory setup area, I select the following checkboxes:
      | name                                    | state |
      | Internal active - Internal portfolio 1  | true  |
      | Internal passive - Internal portfolio 2 | true  |
      | External - External portfolio           | true  |
    And I select the "Setup" page
    And I click on the "Show Parameters" button
    When I enter the following values in "Internal portfolio 1" table in Parameters page
      | oder | name                              | minSellPercentage | minSellValue | maxBuyPercentage | maxBuyValue |
      | 1    | AB Global Growth Trends A USD Acc |                   | 100.00       |                  | 10000.00    |
    And I enter the following values in "External Portfolio" table in Parameters page
      | oder | name                        | minSellPercentage | minSellValue | maxBuyPercentage | maxBuyValue |
      | 2    | Bankinvest Basis Aktier Udb |                   | 200.00       |                  |             |
    Then I should see the table "Internal portfolio 1" in Parameters page as
      | order | name                                           | securityExposure            | volatilityRiskClass | complexityRiskClass | liquidityRiskClass | datafeedStatus | dontSell | minSellPercentage | minSellValue | dontBuy | maxBuyPercentage | maxBuyValue | currentWeight | value   |
      | 1     | AB Global Growth Trends A USD Acc              | UCITS Funds - International | H                   | L                   | L                  | VALID          | false    | 0.04              | 100.00       | false   | 4.39             | 10000.00    | 9.86          | 22,493  |
      | 2     | Aberdeen Global - Asian Smaller Companies Fund | UCITS Funds - International | H                   | L                   | L                  | VALID          | false    |                   |              | false   |                  |             | 47.63         | 108,613 |
    And I should see the table "External Portfolio" in Parameters page as
      | order | name                        | securityExposure               | volatilityRiskClass | complexityRiskClass | liquidityRiskClass | datafeedStatus | dontSell | minSellPercentage | minSellValue | dontBuy | maxBuyPercentage | maxBuyValue | currentWeight | value |
      | 1     | Bankinvest Asien            | UCITS Funds - Emerging Markets | H                   | L                   | L                  | VALID          | false    |                   |              | false   |                  |             | 2.77          | 6,324 |
      | 2     | Bankinvest Basis Aktier Udb | UCITS Funds - International    | H                   | L                   | L                  | VALID          | false    | 0.09              | 200.00       | false   |                  |             | 0.49          | 1,110 |
      | 3     | Storebrand Barnespar        | UCITS Funds - International    | H                   | L                   | L                  | VALID          | false    |                   |              | false   |                  |             | 0.60          | 1,366 |

  @dev @NAVIGEA-294 @NAVIGEA-294-v7 @Advisory_module @Parameters_page
  Scenario: Navigea-294--v7: "Min% / Min Value" AND "Min% / Min Value".Either % or value can be set.
     To clear a % or value that has to been set, the field has to be cleraed manually
    then both fields are activated again).-

    Given I am logged in with organization "TetralogSystemsQA" and CrmKey "SeleniumTestPortfolio_Default"
    And I navigate to the "Advisory" module
    And I select the "Analysis" page
    And In the Basic advisory setup area, I select the following checkboxes:
      | name                                    | state |
      | Internal active - Internal portfolio 1  | true  |
      | Internal passive - Internal portfolio 2 | true  |
      | External - External portfolio           | true  |
    And I select the "Setup" page
    And I click on the "Show Parameters" button
    When I enter the following values in "Internal portfolio 1" table in Parameters page
      | oder | name                                           | minSellPercentage | minSellValue | maxBuyPercentage | maxBuyValue |
      | 1    | AB Global Growth Trends A USD Acc              |                   | 100.00       |                  | 10000.00    |
      | 2    | Aberdeen Global - Asian Smaller Companies Fund | 10                |              | 20               |             |
    And I enter the following values in "Internal portfolio 1" table in Parameters page
      | oder | name                              | minSellPercentage | minSellValue | maxBuyPercentage | maxBuyValue |
      | 1    | AB Global Growth Trends A USD Acc |                   |              |                  |             |
    Then I should see the table "Internal portfolio 1" in Parameters page as
      | order | name                                           | securityExposure            | volatilityRiskClass | complexityRiskClass | liquidityRiskClass | datafeedStatus | dontSell | minSellPercentage | minSellValue | dontBuy | maxBuyPercentage | maxBuyValue | currentWeight | value   |
      | 1     | AB Global Growth Trends A USD Acc              | UCITS Funds - International | H                   | L                   | L                  | VALID          | false    |                   |              | false   |                  |             | 9.86          | 22,493  |
      | 2     | Aberdeen Global - Asian Smaller Companies Fund | UCITS Funds - International | H                   | L                   | L                  | VALID          | false    | 10                | 22,803.20    | false   | 20               | 45,606.39   | 47.63         | 108,613 |

  @dev @NAVIGEA-555 @NAVIGEA-555-v1 @Advisory_module @Parameters_page
  Scenario: Navigea-555: AV -- current Max %/Value of the investment product is higher than the manually set Max%/MaxValue,
    and the current Min%/Value of the investment product is lower than the manually set Min%/MinValue

    Given I am logged in with organization "TetralogSystemsQA" and CrmKey "SeleniumTestPortfolio_Default"
    And I navigate to the "Advisory" module
    And I select the "Analysis" page
    And In the Basic advisory setup area, I select the following checkboxes:
      | name                                    | state |
      | Internal active - Internal portfolio 1  | true  |
      | Internal passive - Internal portfolio 2 | true  |
      | External - External portfolio           | true  |
    And I select the "Setup" page
    And I click on the "Show Parameters" button
    When I enter the following values in "Internal portfolio 1" table in Parameters page
      | order | name                                           | dontSell | dontBuy | minSellPercentage | minSellValue | maxBuyPercentage | maxBuyValue |
      | 1     | AB Global Growth Trends A USD Acc              | true     | false   |                   |              |                  | 35,000.00   |
      | 2     | Aberdeen Global - Asian Smaller Companies Fund | false    | true    |                   | 20,000.00    |                  |             |
    Then I should see the table "Internal portfolio 1" in Parameters page as
      | order | name                                           | securityExposure            | volatilityRiskClass | complexityRiskClass | liquidityRiskClass | datafeedStatus | dontSell | minSellPercentage | minSellValue | dontBuy | maxBuyPercentage | maxBuyValue | currentWeight | value   |
      | 1     | AB Global Growth Trends A USD Acc              | UCITS Funds - International | H                   | L                   | L                  | VALID          | true     |                   |              | false   | 15.35            | 35,000.00   | 9.86          | 22,493  |
      | 2     | Aberdeen Global - Asian Smaller Companies Fund | UCITS Funds - International | H                   | L                   | L                  | VALID          | false    | 8.77              | 20,000.00    | true    |                  |             | 47.63         | 108,613 |

  @dev @NAVIGEA-555 @NAVIGEA-555-v2 @Advisory_module @Parameters_page
  Scenario: Navigea-555: AV -- The min % and max% should be betwenn 0 and 100%
    and the current Min%/Value of the investment product is lower than the manually set Min%/MinValue

    Given I am logged in with organization "TetralogSystemsQA" and CrmKey "SeleniumTestPortfolio_Default"
    And I navigate to the "Advisory" module
    And I select the "Analysis" page
    And In the Basic advisory setup area, I select the following checkboxes:
      | name                                    | state |
      | Internal active - Internal portfolio 1  | true  |
      | Internal passive - Internal portfolio 2 | true  |
      | External - External portfolio           | true  |
    And I select the "Setup" page
    And I click on the "Show Parameters" button
    When I enter the following values in "Internal portfolio 1" table in Parameters page
      | order | name                                           | dontSell | dontBuy | minSellPercentage | minSellValue | maxBuyPercentage | maxBuyValue |
      | 1     | AB Global Growth Trends A USD Acc              | false    | true    | 10                |              |                  |             |
      | 2     | Aberdeen Global - Asian Smaller Companies Fund | true     | false   |                   |              | 45               |             |
     And I enter the following values in "Internal portfolio 2" table in Parameters page
      | order | name                                           | dontSell | dontBuy | minSellPercentage | minSellValue | maxBuyPercentage | maxBuyValue |
      | 2     |Aberdeen Global - Emerging Markets Equity Fund | true    | false    |                 |              |                  |      8,000.00       |
     And I enter the following values in "External Portfolio" table in Parameters page
      | oder | name                        | minSellPercentage | minSellValue | maxBuyPercentage | maxBuyValue |
      | 2    | Bankinvest Basis Aktier Udb |                   | 3,000.00       |                  |             |
    Then I should see a visual clue next to the "minSellPercentage" entry field for position "AB Global Growth Trends A USD Acc" and ISIN "LU0057025933"
    And I should see a visual clue next to the "maxBuyPercentage" entry field for position "Aberdeen Global - Asian Smaller Companies Fund" and ISIN "LU0231459107"
    And I should see a visual clue next to the "minSellValue" entry field for position "Bankinvest Basis Aktier Udb" and ISIN "DK0015773873"
    And I should see a visual clue next to the "maxBuyValue" entry field for position "Aberdeen Global - Emerging Markets Equity Fund" and ISIN "LU0132412106"
    And when I click on the visual clue next to the "minSellPercentage" entry field for position "AB Global Growth Trends A USD Acc" and ISIN "LU0057025933" I should see the following message: "The minimun percentage cannot be greater than the current weight"
    And when I click on the visual clue next to the "maxBuyPercentage" entry field for position "Aberdeen Global - Asian Smaller Companies Fund" and ISIN "LU0231459107" I should see the following message: "The maximum percentage cannot be lower than the current weight"
    And when I click on the visual clue next to the "minSellValue" entry field for position "Bankinvest Basis Aktier Udb" and ISIN "DK0015773873" I should see the following message: "The minimun value cannot be greater than the current value"
	And when I click on the visual clue next to the "maxBuyValue" entry field for position "Aberdeen Global - Emerging Markets Equity Fund" and ISIN "LU0132412106" I should see the following message: "The maximum value cannot be lower than the current value"
    
  @active @NAVIGEA-618 @Advisory_module @Parameters_page
  Scenario: Navigea-618--v1: Set Max buy Percentage using Product Universe
    
    Given I am logged in with organization "TetralogSystemsQA" and CrmKey "SeleniumTestPortfolio_Default"
    When I navigate to the "Portfolio" module
	And I edit the portfolio number 1
	And I add "50" shares of position name "US Recovery AS" and ISIN "NO0010472277"
	And I save the portfolio
	And I navigate back to the portfolio overview screen
    And I navigate to the "Advisory" module
    And I select the "Analysis" page
    And In the Basic advisory setup area, I select the following checkboxes:
      | name                                    | state |
      | Internal active - Internal portfolio 1  | true  |
      | Internal passive - Internal portfolio 2 | true  |
      | External - External portfolio           | true  |
    And I select the "Setup" page
    And I click on the "Show Parameters" button
    Then I should see the parameters table for the "external" portfolio 1 as
      | order | name                        | securityExposure               | volatilityRiskClass | complexityRiskClass | liquidityRiskClass | datafeedStatus | dontSell | minSellPercentage | minSellValue | dontBuy | maxBuyPercentage | maxBuyValue | currentWeight | value |
      | 1     | Bankinvest Asien            | UCITS Funds - Emerging Markets | H                   | L                   | L                  | VALID          | false    |                   |              | true    |                  |             | 2.70          | 6,324 |
      | 2     | Bankinvest Basis Aktier Udb | UCITS Funds - International    | H                   | L                   | L                  | VALID          | false    |                   |              | true    |                  |             | 0.48          | 1,110 |
      | 3     | Storebrand Barnespar        | UCITS Funds - International    | H                   | L                   | L                  | VALID          | false    |                   |              | true    |                  |             | 0.58          | 1,366 |
      | 4     | US Recovery AS              | Alternatives - Real Estate     | H                   | M                   | H                  | VALID          | false    |                   |              | true    |     25.00        |             | 2.48          | 5,800 |
