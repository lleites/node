 Feature: NAVIGEA-473: AV - Setup: Implement function of "results" button

 @dev @NAVIGEA-473  @NAVIGEA-473-v1 @Advisory_module @Parameters_page 
  Scenario: Navigea-473--v1: Check the result button in Setup page.
    Given I am logged in with organization "TetralogSystemsQA" and CrmKey "QaTestData06"
    And I navigate to the "Advisory" module
    And In the Basic advisory setup area,I select the "Manual setup" use case in the dropdown
    And In the Basic advisory setup area, I select the following checkboxes:
      | name                         | state |
      | Internal active - Active PTF | true  |
      | External - External PTF      | true  |
    And I press the Apply/Proceed button
    When I press the "Result" button in "Setup" page
    Then I should be in the "Result" page
	
