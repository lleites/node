Feature: Mapping
  NAVIGEA-564: DF - General: Implementation of Treatment of "Underspecified Products", "Cash" and "Bonds"
  NAVIGEA-660:GENERAL: Improve display of values/prices/No of Shares as described at NAVIGEA-564
  NAVIGEA-690:PO/AV Cash accounts are not calculated properly - Unifed treatment of cash accounts, indices and underspecified instruments required
  NAVIGEA-723:PO - General / AV: Add tooltip to Datafeed symbol "invald" in Portfolio tables
  NAVIGEA-724:PO - General /AV: Display of invalid instruments
  NAVIGEA-725:PO - GENERAL / AV: Display attention symbol at value sums/totals in portfolio tables
  NAVIGEA-740:PO-Edit page: Consitent display of underspecified/replacement indices in Searchresults and when taken into the portfolio
  
  BUG Tickets:
  NAVIGEA-710: PO - Loading of Invalid "underpecified" is not done properly
  NAVIGEA-706: CLONE - PO - Edit: Adding a Cash account to an external portfolio: Nominal 1 should be value 1 in Base Currency
  NAVIGEA-691: PO-Edit: Instrument from search is not taken into the portfolio
  NAVIGEA-738:PO - Overview / AV: Underspecified Instruments: Display and calculation
  NAVIGEA-739: PO - Edit page / AV - Tansactions: Underspecified Instruments: Display and calculation
  NAVIGEA-889:PO - GENERAL / AV: The "attention" symbol at portfolio summary sum rows is not displayed
  (12 scenarios)

  @dev @Mapping @Undespecified_Products @NAVIGEA-564 @NAVIGEA-660 @NAVIGEA-690 @NAVIGEA-710
  Scenario: Invalid and Cash and Undespecified instruments in Portfolio overview
    Check that: 
    - Invalid Isisn--Available values are displayed, but don’t contribute to the total values of the portfolio(s).

    Given I am logged in with organization "TetralogSystemsQA" and CrmKey "QaTestData10"
    When I navigate to the "Portfolio" module
    And I expand the details of the internal portfolio number 1
    And I should see internal portfolio 1 as Table
      | order | name                      | securityExposure           | volatilityRiskClass | complexityRiskClass | liquidityRiskClass | risk  | performance | datafeedStatus | dontSell | dontBuy | price  | currency | numberOfShares | value      |
      | 1     | Boligutleie Holding II AS | Alternatives - Real Estate | M                   | M                   | H                  | 13.82 | 5.78        | VALID          | ?        | ?       | 102.00 | NOK      | 248,441        | 25,340,982 |
      | 2     | Deliveien 4 Holding AS    | Alternatives - Real Estate | M                   | M                   | H                  | 25.46 | 7.32        | VALID          | ?        | ?       | 120.00 | NOK      | 350,000        | 42,000,000 |
      | 3     | Client Account            | Money Market NOK           | L                   | L                   | L                  | 1.52  | 1.72        | VALID          | -        | -       | n/a    | NOK      | 700,00         | 700,000    |
    And The Internal portfolio 1 total value should be "68,040,982" in Portfolio Overview page
    And I should see internal portfolio 2 as Table
      | order | name                           | securityExposure                | volatilityRiskClass | complexityRiskClass | liquidityRiskClass | risk  | performance | datafeedStatus | dontSell | dontBuy | price  | currency | numberOfShares | value      |
      | 1     | JPMF German Equity Fund A      | UCITS Funds - Developed Markets | H                   | L                   | L                  | 41.81 | 11.08       | VALID          | -        | -       | 155.00 | EUR      | 1,304          | 1,492,555  |
      | 2     | Templeton Global Bond Fund NOK | UCITS Funds - Fixed Income      | L                   | L                   | L                  | 32.33 | 11.30       | VALID          | -        | -       | 170.00 | NOK      | 7,810          | 1,327,700  |
      | 3     | SEB High Yield NOK             | UCITS Funds - Fixed Income      | L                   | L                   | L                  | 29.47 | 6.63        | VALID          | -        | -       | 169.00 | EUR      | 11,350         | 14,165,243 |
      | 4     | Client Account                 | Money Market NOK                | L                   | L                   | L                  | 1.52  | 1.72        | VALID          | -        | -       | n/a    | NOK      | 800,000        | 800,000    |
    And The Internal portfolio 2 total value should be "17,785,498" in Portfolio Overview page
    And I should see external portfolio 1 as Table
      | order | name                       | securityExposure       | volatilityRiskClass | complexityRiskClass | liquidityRiskClass | risk  | performance | datafeedStatus | dontSell | dontBuy | price | currency | numberOfShares | value   |
      | 1     | Client Account             | Money Market NOK       | L                   | L                   | L                  | 10.46 | 4.22        | VALID          | -        | ?       | n/a   | NOK      | 400,000        | 400,000 |
      | 2     | Equities International HLL | Equities International | H                   | L                   | L                  | 13.59 | 7.20        | REMPLACEMENT   | -        | ?       | n/a   | NOK      | 100,000        | 100,000 |
      | 3     | Equities Norway HLL        | Equities Norway        | H                   | L                   | L                  | 11.73 | 7.61        | REMPLACEMENT   | -        | ?       | n/a   | NOK      | 200,000        | 200,000 |
      | 4     | Structured_HMM             |                        | H                   | M                   | M                  | 11.73 | 7.61        | INVALID        | -        | -       | 0.00  | NOK      | 0              | 300,000 |
    And The External portfolio 1 total value should be "700,000" in Portfolio Overview page
    And The Portfolio summary should have the following values in Portfolio Overview page:
      | Portfolio summary | Value NOK  |
      | Internal          | 85,826,480 |
      | External          | 700,000    |
      | Total             | 86,526,480 |

  @dev @Mapping @InvalidIsins @NAVIGEA-723 @NAVIGEA-724 @NAVIGEA-725 @NAVIGEA-889
  Scenario: Invalid Intruments--Portfolio overview
    Check that:
    -The tooltip in the datafeed symbol is displayed
    -The "attention" symbol at portfolio table sum rows is displayed, if an invalid instrument is part of the porfolio table
    -The "attention" symbol at portfolio summary sum rows is displayed, if an invalid instrument is part of the sum of internal or sum of external portfolios
    -The tooltip in the "attention" symbol is displayed

    Given I am logged in with organization "TetralogSystemsQA" and CrmKey "QaTestData13"
    When I navigate to the "Portfolio" module
    And I put the mouse over the datafeed symbol in the position "Listed TS00000EIHLL" in the External portfolio table
    And I should see the following message: "This investment instrument is unknown, has an insufficient price history or has unknown/insufficient classifications. Available values are displayed, but don’t contribute to the total values of the portfolio(s). Invalid investment instruments are not available for the advisory!"
    And when I click on the visual clue in the portfolio table "External" sum row I should see the following message: "Invalid instruments don't contribute to the calculated sum"
    And when I click on the visual clue in the external summary sum row I should see the following message: "Invalid instruments don't contribute to the calculated sum"

  @dev @Mapping @InvalidIsins @NAVIGEA-723 @NAVIGEA-724 @NAVIGEA-725 @NAVIGEA-710
  Scenario: Invalid Intruments-Edit page
    Check that:
    -The tooltip in the datafeed symbol is displayed
    -Entry fields for nominal/number of shares at the PO-Edit page should be deactivated for invalid instruments
    -The "attention" symbol at portfolio table sum rows is displayed, if an invalid instrument is part of the porfolio table

    Given I am logged in with organization "TetralogSystemsQA" and CrmKey "QaTestData13"
    When I navigate to the "Portfolio" module
    And I click on the edit portfolio button in "External Underspecified" portfolio
    And I put the mouse over the datafeed symbol in the position "UCITS TS0000UFAMLM" in Edit page
    Then I should see the following message: "This investment instrument is unknown, has an insufficient price history or has unknown/insufficient classifications. Available values are displayed, but don’t contribute to the total values of the portfolio(s). Invalid investment instruments are not available for the advisory!"
    And The following instruments should NOT have an entry field for nominal/number of shares:
      | isin                |
      | UCITS TS0000UFAMLM  |
      | Listed TS00000EIHLL |

  #  And when I click on the visual clue in the sum row I should see the following message: "Invalid instruments don't contribute to the calculated sum"
  @dev @Mapping @InvalidIsins @NAVIGEA-564 @NAVIGEA-724
  Scenario: Invalid, cash and undespecified isntruments-- Start page
    Check that:
     - Invalid investment instruments are not available for the advisory module

    Given I am logged in with organization "TetralogSystemsQA" and CrmKey "QaTestData15"
    When I navigate to the "Advisory" module
    And I select the "Analysis" page
    And I expand the details of the "internal" portfolio number 1
    Then The app should show the following securities in "internal" portfolio 1 in Analysis page
      | order | name                      | securityExposure           | volatilityRiskClass | complexityRiskClass | liquidityRiskClass | risk  | performance | datafeedStatus | dontSell | dontBuy | price  | currency | numberOfShares | weight | value      |
      | 1     | Boligutleie Holding II AS | Alternatives - Real Estate | M                   | M                   | H                  | 13.82 | 5.78        | VALID          | ?        | ?       | 102.00 | NOK      | 248,441        | 37.24  | 25,340,982 |
      | 2     | Deliveien 4 Holding AS    | Alternatives - Real Estate | M                   | M                   | H                  | 25.46 | 7.32        | VALID          | ?        | ?       | 120.00 | NOK      | 350,000        | 61.72  | 42,000,000 |
      | 3     | Client Account            | Money Market NOK           | L                   | L                   | L                  | 1.52  | 1.72        | VALID          | -        | -       | n/a    | NOK      | 700,000        | 1.03   | 700,000    |
    And The "internal" portfolio 1 total value should be "68,040,982" and "99.99" % in Analysis page
    And I expand the details of the "external" portfolio number 1
    And The app should show the following securities in "external" portfolio 1 in Analysis page
      | order | name                                  | securityExposure                  | volatilityRiskClass | complexityRiskClass | liquidityRiskClass | risk  | performance | datafeedStatus | dontSell | dontBuy | price | currency | numberOfShares | weight | value  |
      | 1     | Structured Products - Unspecified MHM | Structured Products - Unspecified | M                   | H                   | H                  | 15.67 | 7.46        | REMPLACEMENT   | ?        | ?       | n/a   | NOK      | 10,000         | 0.01   | 10,000 |
    And The "external" portfolio 1 total value should be "10,000" and "0.01" % in Analysis page
    And The Portfolio summary should have the following values in Analysis page:
      | Portfolio summary | Weight % | Value NOK  |
      | Internal          | 99.99    | 68,040,982 |
      | External          | 0.01     | 10,000     |
      | Total             | 100.00   | 68,050,982 |

  @dev @Mapping @InvalidIsins @NAVIGEA-564 @NAVIGEA-724
  Scenario: Invalid, cash and undespecified isntruments-- Parameters page
    Check that:
     - Invalid investment instruments are not available for the advisory module

    Given I am logged in with organization "TetralogSystemsQA" and CrmKey "QaTestData15"
    When I navigate to the "Advisory" module
    And I select the "Analysis" page
    And In the Basic advisory setup area, I select the following checkboxes:
      | name                               | state |
      | Internal passive - IntPASS         | true  |
      | External - External Underspecified | true  |
    And I select the "Setup" page
    And I click on the "Show Parameters" button
    Then I should see the parameters table for the "internal" portfolio 1 as
      | order | name                      | securityExposure           | volatilityRiskClass | complexityRiskClass | liquidityRiskClass | risk  | performance | datafeedStatus | dontSell | minSellPercentage | minSellValue | dontBuy | maxBuyPercentage | maxBuyValue | currentWeight | value      |
      | 1     | Boligutleie Holding II AS | Alternatives - Real Estate | M                   | M                   | H                  | 13.82 | 5.78        | VALID          | true     | 0.00              |              | true    | 100.00           |             | 37.24         | 25,340,982 |
      | 2     | Deliveien 4 Holding AS    | Alternatives - Real Estate | M                   | M                   | H                  | 25.46 | 7.32        | VALID          | true     | 0.00              |              | true    | 100.00           |             | 61.72         | 42,000,000 |
      | 3     | Client Account            | Money Market NOK           | L                   | L                   | L                  | 1.52  | 1.72        | VALID          | false    | 0.00              |              | false   | 100.00           |             | 1.03          | 700,000    |
    And The "internal" portfolio 1 total value should be "68,040,982" and "99.99" % in Parameters page
    And I should see the parameters table for the "external" portfolio 1 as
      | order | name                                  | securityExposure                  | volatilityRiskClass | complexityRiskClass | liquidityRiskClass | risk  | performance | datafeedStatus | dontSell | minSellPercentage | minSellValue | dontBuy | maxBuyPercentage | maxBuyValue | currentWeight | value  |
      | 1     | Structured Products - Unspecified MHM | Structured Products - Unspecified | M                   | H                   | H                  | 15.67 | 7.46        | REMPLACEMENT   | true     | 0.00              |              | true    | 100.00           |             | 0.01          | 10,000 |
    And The "external" portfolio 1 total value should be "10,000" and "0.01" % in Parameters page

  @dev @Mapping @InvalidIsins @NAVIGEA-564 @NAVIGEA-724 @NAVIGEA-739
  Scenario: Invalid, cash and undespecified isntruments-- Result page
    Check that:
     - Invalid investment instruments are not available for the advisory module

    Given I am logged in with organization "TetralogSystemsQA" and CrmKey "QaTestData15"
    When I navigate to the "Advisory" module
    And I select the "Analysis" page
    And In the Basic advisory setup area, I select the following checkboxes:
      | name                               | state |
      | Internal passive - IntPASS         | true  |
      | External - External Underspecified | true  |
    And I select the "Result" page
    Then The app should show the "IntPASS" table in Results page as
      | oder | name                      | securityExposure           | volatilityRiskClass | complexityRiskClass | liquidityRiskClass | risk  | performance | currentShare | currentWeight% | currentValueNOK | recommendedShare | recommendedWeight% | recommendedValueNOK |
      | 1    | Boligutleie Holding II AS | Alternatives - Real Estate | M                   | M                   | H                  | 13.82 | 5.78        | 248,441      | 37.24          | 25,340,982      | 248,441          | 37.24              | 25,340,982          |
      | 2    | Deliveien 4 Holding AS    | Alternatives - Real Estate | M                   | M                   | H                  | 25.46 | 7.32        | 350,000      | 61.72          | 42,000,000      | 350,000          | 61.72              | 42,000,000          |
      | 3    | Client Account            | Money Market NOK           | L                   | L                   | L                  | 1.52  | 1.72        | 700,000      | 1.03           | 700,000         | 700,000          | 1.03               | 700,000             |
    And The "IntPASS" total value should be "68,040,982" and "99.99" % in Results page
    And The app should show the "External Underspecified" table in Results page as
      | oder | name                                  | securityExposure                  | volatilityRiskClass | complexityRiskClass | liquidityRiskClass | risk  | performance | currentShare | currentWeight% | currentValueNOK | recommendedShare | recommendedWeight% | recommendedValueNOK |
      | 1    | Structured Products - Unspecified MHM | Structured Products - Unspecified | M                   | H                   | H                  | 15.67 | 7.46        |              | 15             | 0.01            | 10,000           | 0.01               | 10,000              |
    And The "External Underspecified" total value should be "10,000" and "0.01" % in Results page
    Then I should see the Results summary table expanded in the following way
      | Summary                | Weight% | Value NOK  |
      | Internal portfolios    |         |            |
      | Current                | 99.99   | 68,040,982 |
      | Additional investments | 0.00    | 0          |
      | Withdrawals            | 0.00    | 0          |
      | Recommended            | 99.99   | 68,040,982 |
      | External portfolios    |         |            |
      | Current                | 0.01    | 10,000     |
      | Recommended            | 0.01    | 10,000     |
      | Current portfolios     | 100.00  | 68,050,982 |
      | Additional investments | 0.00    | 0          |
      | Withdrawals            | 0.00    | 0          |
      | Recommended portfolios | 100.00  | 68,050,982 |
      | Balance                | 0.00    | 0          |

  @dev @Mapping @InvalidIsins @NAVIGEA-564 @NAVIGEA-724 @NAVIGEA-739
  Scenario: Invalid, cash and undespecified isntruments-- Result page
    Check that:
     - Invalid investment instruments are not available for the advisory module
    - change the numberofshare in transaction page and check the calculations for the undespecified, bonds and cash in AV module

    Given I am logged in with organization "TetralogSystemsQA" and CrmKey "QaTestData15"
    And I navigate to the "Advisory" module
    And I select the "Analysis" page
    And In the Basic advisory setup area, I select the following checkboxes:
      | name                               | state |
      | Internal passive - IntPASS         | true  |
      | External - External Underspecified | true  |
    When I enter the following values in "IntPASS" table in Parameters page
      | order | name                      | dontSell | dontBuy |
      | 1     | Boligutleie Holding II AS | false    | false   |
      | 2     | Deliveien 4 Holding AS    | false    | false   |
      | 3     | Client Account            | false    | false   |
    And I enter the following values in "External Underspecified" table in Parameters page
      | 1 | Structured Products - Unspecified MHM | false | false |
    And I select the "Result" page
    And I click on the "Show transactions" button
    And I enter the following values in "Buy recommendation" table for the "External Underspecified" in Transaction page.
      | order | name                                  | buy_sellmodify |
      | 1     | Structured Products - Unspecified MHM | 1000           |
    And I click on the "Update transactions" button
    And I click on the "Show results" button
    Then The app should show the "IntPASS" table in Results page as
      | oder | name                      | securityExposure           | volatilityRiskClass | complexityRiskClass | liquidityRiskClass | risk  | performance | currentShare | currentWeight% | currentValueNOK | recommendedShare | recommendedWeight% | recommendedValueNOK |
      | 1    | Boligutleie Holding II AS | Alternatives - Real Estate | M                   | M                   | H                  | 13.82 | 5.78        | 248,441      | 37.24          | 25,340,982      | 59,947           | 9.98               | 6,114,594           |
      | 2    | Deliveien 4 Holding AS    | Alternatives - Real Estate | M                   | M                   | H                  | 25.46 | 7.32        | 350,000      | 61.72          | 42,000,000      | 16,958           | 3.32               | 2,034,960           |
      | 3    | Client Account            | Money Market NOK           | L                   | L                   | L                  | 1.52  | 1.72        | 700,000      | 1.03           | 700,000         | 53,135,941       | 86.69              | 53,135,941          |
    And The "IntPASS" total value should be "61,285,496" and "99.98" % in Results page
    And The app should show the "External Underspecified" table in Results page as
      | oder | name                                  | securityExposure                  | volatilityRiskClass | complexityRiskClass | liquidityRiskClass | risk  | performance | currentShare | currentWeight% | currentValueNOK | recommendedShare | recommendedWeight% | recommendedValueNOK |
      | 1    | Structured Products - Unspecified MHM | Structured Products - Unspecified | M                   | H                   | H                  | 15.67 | 7.46        |              | 15             | 0.02            | 10,000           | 0.01               | 11,000              |
    And The "External Underspecified" total value should be "11,000" and "0.02" % in Results page
    Then I should see the Results summary table expanded in the following way
      | Summary                | Weight% | Value NOK  |
      | Internal portfolios    |         |            |
      | Current                | 111.00  | 68,040,982 |
      | Additional investments | 0.00    | 0          |
      | Withdrawals            | 0.00    | 0          |
      | Recommended            | 99.98   | 61,285,496 |
      | External portfolios    |         |            |
      | Current                | 0.01    | 10,000     |
      | Recommended            | 0.02    | 11,000     |
      | Current portfolios     | 111.02  | 68,050,982 |
      | Additional investments | 0.00    | 0          |
      | Withdrawals            | 0.00    | 0          |
      | Recommended portfolios | 100.00  | 61,296,496 |
      | Balance                | 11.02   | 6,754,486  |

  @dev @Mapping @InvalidIsins @NAVIGEA-564 @NAVIGEA-724 @NAVIGEA-739
  Scenario: Invalid, cash and undespecified isntruments-- Transactions
    Check that:
     - Invalid investment instruments are not available for the advisory module
     - change the numberofshare in transaction page and check the calculations for the undespecified, bonds and cash in AV module

    Given I am logged in with organization "TetralogSystemsQA" and CrmKey "QaTestData13"
    And I navigate to the "Advisory" module
    And I select the "Analysis" page
    And In the Basic advisory setup area, I select the following checkboxes:
      | name                               | state |
      | Internal passive - IntPASS         | true  |
      | Internal active - IntACT           | true  |
      | External - External_SEB HIEH YIELD | true  |
      | External - External Underspecified | true  |
    And I select the "Result" page
    And I click on the "Show transactions" button
    And I enter the following values in "Sell recommendation" table for the "External Underspecified" in Transaction page.
      | order | name                    | buy_sellmodify |
      | 2     | Client Account          | 1000           |
      | 3     | Bonds International LLL | 2000           |
    And I click on the "Update transactions" button
    And I click on the "Show results" button
    Then The app should show the "External Underspecified" table in Results page as
      | oder | name                                  | securityExposure                   | volatilityRiskClass | complexityRiskClass | liquidityRiskClass | currentShare | currentWeight% | currentValueNOK | recommendedShare | recommendedWeight% | recommendedValueNOK |
      | 1    | Client Account                        | Money Market NOK                   | L                   | L                   | L                  | 400,000      | 0.46           | 400,000         | 0                | 0.00               | 0                   |
      | 2    | Client Account                        | Money Market International         | L                   | L                   | L                  | 54,167       | 0.46           | 400,000         | 53,167           | 0.45               | 392,615             |
      | 3    | Alternatives - Unspecified MMH        | Alternatives - Unspecified         | M                   | M                   | H                  | 100,001      | 0.12           | 100,001         | 100,001          | 0.11               | 100,001             |
      | 4    | Bonds International LLL               | Bonds International                | L                   | L                   | L                  | 100,002      | 0.12           | 100,002         | 98,002           | 0.11               | 98,002              |
      | 5    | Structured Products - Unspecified MHM | SStructured Products - Unspecified | M                   | H                   | H                  | 100,004      | 0.12           | 100,004         | 100,004          | 0.11               | 100,004             |
    And The "External portfolio" total value should be "690,623" and "0.79" % in Results page
    Then I should see the Results summary table expanded in the following way
      | Summary                | Weight% | Value NOK  |
      | Internal portfolios    |         |            |
      | Current                | 81.98   | 71,661,237 |
      | Additional investments | 0.00    | 0          |
      | Withdrawals            | 0.00    | 0          |
      | Recommended            | 99.21   | 86,726,483 |
      | External portfolios    |         |            |
      | Current                | 17.46   | 15,265,250 |
      | Recommended            | 0.79    | 690,623    |
      | Current portfolios     | 99.44   | 86,926,487 |
      | Additional investments | 0.00    | 0          |
      | Withdrawals            | 0.00    | 0          |
      | Recommended portfolios | 100.00  | 87,417,105 |
      | Balance                | -0.56   | -490,618   |

  @dev @Mapping @InvalidIsins @NAVIGEA-740 @NAVIGEA-564 @NAVIGEA-710 @NAVIGEA-738
  Scenario: Undespecified,Cash and Bonds instruments-Edit page
    Check that:
    when showing replacements/underlyings in the results table of the PO-Edit page, Replacements/underlyings should
    -be displayed with the substitute/replacement symbol
    -be displayed with the price information n/a
    -be calculated by using the virtual price "1" (if underlying is not in base currency, then the exchange rate should be taken into account additionally)

    Given I am logged in with organization "TetralogSystemsQA" and CrmKey "QaTestData13"
    And I navigate to the "Portfolio" module
    And I click on the edit portfolio button in "External Underspecified" portfolio
    When I search all the securities
    And I go to the "5" page in the search result pagination bar
    Then The search result should include the following securities:
      | order | nameAndIsin                                        | securityClassAndExposure                                  | volatilityRiskClass | complexityRiskClass | liquidityRiskClass | datafeedIcon | price | currency |
      | 42    | TS00000BIMLL  @ Bonds International MLL            | Bonds  @ Bonds International                              | M                   | L                   | L                  | REMPLACEMENT | n/a   | NOK      |
      | 47    | TS000AIREHMH @ Alternatives - Renewable Energy HMH | Alternative Investments @ Alternatives - Renewable Energy | H                   | M                   | H                  | REMPLACEMENT | n/a   | NOK      |
      | 50    | TS00000BNLLM  @ Bonds Norway LLM                   | Bonds @ Bonds Norway                                      | L                   | L                   | M                  | REMPLACEMENT | n/a   | NOK      |
    And I should see the editable portfolio as
      | order | name                                  | securityExposure                  | volatilityRiskClass | complexityRiskClass | liquidityRiskClass | Datafeed     | dontSell | dontBuy | price | currency | numberOfShares | value       |
      | 1     | Client Account                        | Money Market NOK                  | L                   | L                   | L                  | VALID        | false    | true    | n/a   | NOK      | 400,000.000    | 400,000.000 |
      | 2     | Client Account                        | Money Market International        | L                   | L                   | L                  | VALID        | false    | true    | n/a   | EUR      | 54,167.513     | 400,000.000 |
      | 3     | Alternatives - Unspecified MMH        | Alternatives - Unspecified        | M                   | M                   | H                  | REMPLACEMENT | true     | true    | n/a   | NOK      | 100,001.000    | 100,001.000 |
      | 4     | Bonds International LLL               | Bonds International               | L                   | L                   | L                  | REMPLACEMENT | false    | true    | n/a   | NOK      | 100,002.000    | 100,002.000 |
      | 5     | Listed TS00000EIHLL                   |                                   | H                   | L                   | L                  | INVALID      | false    | false   | 0.00  | NOK      |                | 100,003.00  |
      | 6     | Structured Products - Unspecified MHM | Structured Products - Unspecified | M                   | H                   | M                  | REMPLACEMENT | true     | true    | n/a   | NOK      | 100,004.000    | 100,004.000 |
      | 7     | UCITS TS0000UFAMLM                    |                                   | M                   | M                   | L                  | INVALID      | false    | false   | 0.00  | NOK      |                | 100,005.00  |

  @dev @Mapping @InvalidIsins @NAVIGEA-740 @NAVIGEA-564 @NAVIGEA-738
  Scenario: Undespecified instruments and Cash -Edit page
    Check that:
    when showing replacements/underlyings in the results table of the PO-Edit page, Replacements/underlyings should
    -be displayed with the substitute/replacement symbol
    -be displayed with the price information n/a
    -be calculated by using the virtual price "1" (if underlying is not in base currency, then the exchange rate should be taken into account additionally)

    Given I am logged in with organization "TetralogSystemsQA" and CrmKey "QaTestData16"
    And I navigate to the "Portfolio" module
    And I click on the edit portfolio button in "ExtUnderSpec" portfolio
    When I search all the securities
    And I go to the "2" page in the search result pagination bar
    Then The search result should include the following securities:
      | order | nameAndIsin                         | securityClassAndExposure           | volatilityRiskClass | complexityRiskClass | liquidityRiskClass | datafeedIcon | price | currency |
      | 11    | Bank Account EUR   @ Client Account | Cash  @ Money Market International | L                   | L                   | L                  | VALID        | n/a   | EUR      |
      | 12    | Bank Account GBP  @ Client Account  | Cash  @ Money Market International | L                   | L                   | L                  | VALID        | n/a   | GBP      |
      | 14    | Bank Account JPY  @ Client Account  | Cash  @ Money Market International | L                   | L                   | L                  | REMPLACEMENT | n/a   | JPY      |
      | 16    | Bank Account USD   @ Client Account | Cash  @ Money Market International | M                   | L                   | L                  | VALID        | n/a   | USD      |
      | 18    | Bank Account SEK  @ Client Account  | Cash  @ Money Market International | M                   | L                   | L                  | REMPLACEMENT | n/a   | SEK      |
    And I should see the editable portfolio as
      | order | name                           | securityExposure           | volatilityRiskClass | complexityRiskClass | liquidityRiskClass | Datafeed     | dontSell | dontBuy | price | currency | numberOfShares | value       |
      | 1     | Client Account                 | Money Market NOK           | L                   | L                   | L                  | VALID        | false    | true    | n/a   | NOK      | 400,000.000    | 400,000.000 |
      | 2     | Equities International HLL     | Equities International     | H                   | L                   | L                  | REMPLACEMENT | false    | true    | n/a   | NOK      | 100,000.000    | 100,000.000 |
      | 3     | Equities Norway HLL            | Equities Norway            | H                   | L                   | L                  | REMPLACEMENT | false    | true    | n/a   | NOK      | 200,000.000    | 200,000.000 |
      | 4     | Alternatives - Unspecified MMH | Alternatives - Unspecified | M                   | M                   | H                  | REMPLACEMENT | true     | true    | n/a   | NOK      | 300,000.000    | 300,000.000 |

  @dev @Mapping @InvalidIsins @NAVIGEA-740 @NAVIGEA-564 @NAVIGEA-706 @NAVIGEA-691 @NAVIGEA-738
  Scenario: Undespecified instruments and Cash -Edit page- change the numberOfShares in an undespecified instrument and add a cash account in base currency(NOK).
    Check that:
    when showing replacements/underlyings in the results table of the PO-Edit page, Replacements/underlyings should
    -be displayed with the substitute/replacement symbol
    -be displayed with the price information n/a
    -be calculated by using the virtual price "1" (if underlying is not in base currency, then the exchange rate should be taken into account additionally)

    Given I am logged in with organization "TetralogSystemsQA" and CrmKey "QaTestData16"
    And I navigate to the "Portfolio" module
    And I click on the edit portfolio button in "ExtUnderSpec" portfolio
    When I search all the securities
    When I add in the external portfolio the following securities:
      | ISIN | Name             | Share |
      |      | Bank Account NOK | 1000  |
    And I save the portfolio
    And I change in the external portfolio the following securities:
      | ISIN         | Name                       | Share |
      | TS00000EIHLL | Equities International HLL | 150   |
    And I save the portfolio
    And I navigate back to the portfolio overview screen
    Then The external portfolio "External portfolio" should have the following positions :
      | order | name                           | securityExposure           | volatilityRiskClass | complexityRiskClass | liquidityRiskClass | Datafeed     | dontSell | dontBuy | price | currency | numberOfShares | value   |
      | 1     | Client Account                 | Money Market NOK           | L                   | L                   | L                  | VALID        | false    | true    | n/a   | NOK      | 400,000        | 400,000 |
      | 2     | Equities International HLL     | Equities International     | H                   | L                   | L                  | REMPLACEMENT | false    | true    | n/a   | NOK      | 150            | 150     |
      | 3     | Equities Norway HLL            | Equities Norway            | H                   | L                   | L                  | REMPLACEMENT | false    | true    | n/a   | NOK      | 200,000        | 200,000 |
      | 4     | Alternatives - Unspecified MMH | Alternatives - Unspecified | M                   | M                   | H                  | REMPLACEMENT | true     | true    | n/a   | NOK      | 300,000        | 300,000 |
      | 5     | Client Account                 | Money Market NOK           | M                   | L                   | L                  | VALID        | false    | true    | n/a   | NOK      | 1,000          | 1,000   |
    And The External portfolio 1 total value should be "901,150" in Portfolio Overview page
    And The Portfolio summary should have the following values in Portfolio Overview page:
      | Portfolio summary | Value NOK  |
      | Internal          | 90,050,177 |
      | External          | 901,150    |
      | Total             | 90,951,327 |

  @dev @Mapping @InvalidIsins @NAVIGEA-740 @NAVIGEA-564 @NAVIGEA-706 @NAVIGEA-691 @NAVIGEA-738
  Scenario: Undespecified instruments and Cash
    -Edit page- add some instruments in teh external portfolio different currency (SEK and EUR).
            -change the numberOfShares in the cash account EUR
    Check that:
    when showing replacements/underlyings in the results table of the PO-Edit page, Replacements/underlyings should
    -be displayed with the substitute/replacement symbol
    -be displayed with the price information n/a
    -be calculated by using the virtual price "1" (if underlying is not in base currency, then the exchange rate should be taken into account additionally)

    Given I am logged in with organization "TetralogSystemsQA" and CrmKey "QaTestData13"
    And I navigate to the "Portfolio" module
    And I click on the edit portfolio button in "External Underspecified" portfolio
    When I search all the securities
    When I add in the external portfolio the following securities:
      | ISIN             | Name                             | Share |
      | Bank Account SEK | Client Account                   | 1000  |
      | SE0000744195     | Stockholmsbørsen: OMXS All Share | 1000  |
      | TS00000ESHLL     | Equities Sweden HLL              | 1000  |
      | TS00000BSLLM     | Bonds Sweden LLM                 | 1000  |
    And I save the portfolio
    And I change in the external portfolio the following securities:
      | ISIN             | Name           | Share |
      | Bank Account EUR | Client Account | 150   |
    And I save the portfolio
    And I navigate back to the portfolio overview screen
    Then The external portfolio "External portfolio" should have the following positions :
      | order | name                                  | securityExposure                  | volatilityRiskClass | complexityRiskClass | liquidityRiskClass | Datafeed     | dontSell | dontBuy | price    | currency | numberOfShares | value   |
      | 1     | Client Account                        | Money Market NOK                  | L                   | L                   | L                  | VALID        | false    | true    | n/a      | NOK      | 400,000        | 400,000 |
      | 2     | Client Account                        | Money Market International        | L                   | L                   | L                  | VALID        | false    | true    | n/a      | EUR      | 150            | 1,107   |
      | 3     | Alternatives - Unspecified MMH        | Alternatives - Unspecified        | M                   | M                   | H                  | REMPLACEMENT | true     | true    | n/a      | NOK      | 100,001        | 100,001 |
      | 4     | Bonds International LLL               | Bonds International               | L                   | L                   | L                  | REMPLACEMENT | false    | true    | n/a      | NOK      | 100,002        | 100,002 |
      | 5     | Listed TS00000EIHLL                   |                                   | H                   | L                   | L                  | INVALID      | false    | false   | 0.00     | NOK      | 0              | 100,003 |
      | 6     | Structured Products - Unspecified MHM | Structured Products - Unspecified | M                   | H                   | M                  | REMPLACEMENT | true     | true    | n/a      | NOK      | 100,004        | 100,004 |
      | 7     | UCITS TS0000UFAMLM                    |                                   | M                   | M                   | L                  | INVALID      | false    | false   | 0.00     | NOK      | 0              | 100,005 |
      | 8     | Listed TS00000EIHLL                   | Money Market SEK                  | L                   | L                   | L                  | VALID        | false    | true    | n/a      | SEK      | 1,000          | 857     |
      | 9     | Stockholmsbørsen: OMXS All Share      | Index - Unspecified               | H                   | L                   | L                  | VALID        | false    | true    | 1,010.00 | SEK      | 1,000          | 865,870 |
      | 10    | Equities Sweden HLL                   | Equities Sweden                   | H                   | L                   | L                  | REMPLACEMENT | false    | false   | n/a      | SEK      | 1,000          | 857     |
      | 11    | Bonds Sweden LLM                      | Bonds Sweden                      | L                   | L                   | M                  | REMPLACEMENT | true     | true    | n/a      | SEK      | 1,000          | 857     |
    And The External portfolio 1 total value should be "1,569,557" in Portfolio Overview page
    And The Portfolio summary should have the following values in Portfolio Overview page:
      | Portfolio summary | Value NOK  |
      | Internal          | 71,661,237 |
      | External          | 15,734,800 |
      | Total             | 87,396,037 |
