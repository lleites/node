Feature: Constraints.
   NAVIGEA-288-- BA SA: CONSTRAINTS: Basic don't sell / don't buy constraints
  (@NAVIGEA-288-V1,@NAVIGEA-288-V2 and @NAVIGEA-288-V3 are in Regresion test in the Navigea-339 folder)

  @dev @NAVIGEA-288-V4 @Advisory_module @Analysis-page
  Scenario: @NAVIGEA-288-V4 -Analysis page: Check the portfolios that are shown in Analysis page.
    Given I am logged in with organization "TetralogSystemsQA" and CrmKey "SeleniumTestPortfolio_QAPortfolio"
    And I navigate to the "Advisory" module
    When I select the "Analysis" page
    Then The app should show the following securities in "Active PTF" in "Analysis" page
      | order | name                 | securityExposure       | volatilityRiskClass | complexityRiskClass | liquidityRiskClass | risk  | performance | datafeedStatus | dontSell | dontBuy | price | currency | numberOfShares | weight | value  |
      | 1     | Copy of Daimler N.A. | Equities International | L                   | H                   | M                  | 45.98 | 10.19       | VALID          | ?        | ?       | 41.41 | EUR      | 120            | 0.98   | 36,690 |
    And The app should show the following securities in "Passive PTF" in "Analysis" page
      | order | name              | securityExposure       | volatilityRiskClass | complexityRiskClass | liquidityRiskClass | risk  | performance | datafeedStatus | dontSell | dontBuy | price | currency | numberOfShares | weight | value  |
      | 1     | Copy of Microsoft | Equities International | M                   | M                   | H                  | 34.73 | 7.62        | VALID          | ?        | ?       | 26.55 | USD      | 86             | 0.35   | 12,860 |
    And The app should show the following securities in "Insurance PTF" in "Analysis" page
      | order | name                       | securityExposure       | volatilityRiskClass | complexityRiskClass | liquidityRiskClass | risk  | performance | datafeedStatus | dontSell | dontBuy | price     | currency | numberOfShares | weight | value     |
      | 1     | Copy A.P. Møller - Mærsk A | Equities International | L                   | H                   | M                  | 41.34 | 8.89        | VALID          | ?        | ?       | 42,825.31 | DKK      | 86             | 98.64  | 3,675,179 |
    And The app should show the following securities in "External PTF" in "Analysis" page
      | order | name                                       | securityExposure              | volatilityRiskClass | complexityRiskClass | liquidityRiskClass | risk  | performance | datafeedStatus | dontSell | dontBuy | price | currency | numberOfShares | weight | value |
      | 1     | Copy of LU0048578792 Fidelity Europ Growth | UCITS Funds - Sector oriented | H                   | L                   | L                  | 22.98 | 10.84       | VALID          | -        | ?       | 10.09 | EUR      | 15             | 0.03   | 1,117 |

  @dev @NAVIGEA-288-v5- @Portfolio_module
  Scenario: NAVIGEA-288-V5-- Portfolio overview: Check the portfolios that are shown in in Portfolio Overview page.
    Given I am logged in with organization "TetralogSystemsQA" and CrmKey "SeleniumTestPortfolio_QAPortfolio"
    And I navigate to the "Portfolio" module
    When I expand the details of the internal portfolio number 1
    Then The app should show the following securities in "Active PTF" in "Portfolio Overview" page
      | order | name                 | securityExposure       | volatilityRiskClass | complexityRiskClass | liquidityRiskClass | risk  | performance | datafeedStatus | dontSell | dontBuy | price | currency | numberOfShares | value  |
      | 1     | Copy of Daimler N.A. | Equities International | L                   | H                   | M                  | 45.98 | 10.19       | VALID          | ?        | ?       | 41.41 | EUR      | 120            | 36,690 |
    And The app should show the following securities in "Passive PTF" in "Portfolio Overview" page
      | order | name              | securityExposure       | volatilityRiskClass | complexityRiskClass | liquidityRiskClass | risk  | performance | datafeedStatus | dontSell | dontBuy | price | currency | numberOfShares | value  |
      | 1     | Copy of Microsoft | Equities International | M                   | M                   | H                  | 34.73 | 7.62        | VALID          | ?        | ?       | 26.55 | USD      | 86             | 12,860 |
    And The app should show the following securities in "Insurance PTF" in "Portfolio Overview" page
      | order | name                       | securityExposure       | volatilityRiskClass | complexityRiskClass | liquidityRiskClass | risk  | performance | datafeedStatus | dontSell | dontBuy | price     | currency | numberOfShares | value     |
      | 1     | Copy A.P. Møller - Mærsk A | Equities International | L                   | H                   | M                  | 41.34 | 8.89        | VALID          | ?        | ?       | 42,825.31 | DKK      | 86             | 3,675,179 |
    And The app should show the following securities in "External PTF" in "Portfolio Overview" page
      | order | name                                       | securityExposure              | volatilityRiskClass | complexityRiskClass | liquidityRiskClass | risk  | performance | datafeedStatus | dontSell | dontBuy | price | currency | nominalShare | value |
      | 1     | Copy of LU0048578792 Fidelity Europ Growth | UCITS Funds - Sector oriented | H                   | L                   | L                  | 22.98 | 10.84       | VALID          | -        | ?       | 10.09 | EUR      | 15           | 1,117 |

  @dev @NAVIGEA-288-V6 @Advisory_module @Parameters-page
  Scenario: @NAVIGEA-288-V6-- Parameters page: Check the portfolios that are shown  in Portfolio Overview page.
    Given I am logged in with organization "TetralogSystemsQA" and CrmKey "SeleniumTestPortfolio_QAPortfolio"
    And I navigate to the "Advisory" module
    And I select the "Analysis" page
    And In the Basic advisory setup area, I select the following checkboxes:
      | name                               | state |
      | Internal active - Active PTF       | true  |
      | Internal passive - Passive PTF     | true  |
      | Internal insurance - Insurance PTF | true  |
      | External - External PTF            | true  |
    When I select the "Setup" page
    And I click on the "Show Parameters" button
    Then I should see the table "Active PTF" in Parameters page as
      | order | name                 | securityExposure       | volatilityRiskClass | complexityRiskClass | liquidityRiskClass | risk  | performance | datafeedStatus | dontSell | minSellPercentage | minSellValue | dontBuy | maxBuyPercentage | maxBuyValue | currentWeight | value  |
      | 1     | Copy of Daimler N.A. | Equities International | L                   | H                   | M                  | 45.98 | 10.19       | VALID          | true     |                   |              | true    |                  |             | 0.98          | 36,690 |
    And I should see the table "Passive PTF" in Parameters page as
      | order | name              | securityExposure       | volatilityRiskClass | complexityRiskClass | liquidityRiskClass | risk  | performance | datafeedStatus | dontSell | minSellPercentage | minSellValue | dontBuy | maxBuyPercentage | maxBuyValue | currentWeight | value  |
      | 1     | Copy of Microsoft | Equities International | M                   | M                   | H                  | 34.73 | 7.62        | VALID          | true     |                   |              | true    |                  |             | 0.35          | 12,860 |
    And I should see the table "Insurance PTF" in Parameters page as
      | order | name                       | securityExposure       | volatilityRiskClass | complexityRiskClass | liquidityRiskClass | risk  | performance | datafeedStatus | dontSell | minSellPercentage | minSellValue | dontBuy | maxBuyPercentage | maxBuyValue | currentWeight | value     |
      | 1     | Copy A.P. Møller - Mærsk A | Equities International | L                   | H                   | M                  | 41.34 | 8.89        | VALID          | true     |                   |              | true    |                  |             | 98.64         | 3,675,179 |
    And I should see the table "External PTF" in Parameters page as
      | order | name                                       | securityExposure              | volatilityRiskClass | complexityRiskClass | risk | performance | liquidityRiskClass | datafeedStatus | dontSell | minSellPercentage | minSellValue | dontBuy | maxBuyPercentage | maxBuyValue | currentWeight | value |
      | 1     | Copy of LU0048578792 Fidelity Europ Growth | UCITS Funds - Sector oriented | H                   | L                   | L    | 22.98       | 10.84              | VALID          | false    |                   |              | true    |                  |             | 0.03          | 1,117 |

  @review @NAVIGEA-288-V7 @Advisory_module @Parameters-page
  Scenario: @NAVIGEA-288-V7-- Parameters page: Check 'Don't buy' constraint for all positions with liquidity risk = high or medium
    Given I am logged in with organization "TetralogSystemsQA" and CrmKey "QaTestData06"
    And I navigate to the "Advisory" module
    And I select the "Analysis" page
    And In the Basic advisory setup area, I select the following checkboxes:
      | name                               | state |
      | Internal active - Active PTF       | false |
      | Internal passive - Passive PTF     | true  |
      | Internal insurance - Insurance PTF | false |
      | External - External PTF            | true  |
    When I select the "Setup" page
    And I click on the "Show Parameters" button
    And I enter the following values in "Passive PTF" table in Parameters page
      | order | name                                                     | dontSell | dontBuy | minSellPercentage | minSellValue | maxBuyPercentage | maxBuyValue |
      | 1     | Garantum Kreditobligation High Yield Europa 5 år nr 1408 | false    | true    | 0.10              |              |                  |             |
      | 2     | Næringsbygg Holding 1 AS                                 | true     | false   |                   |              | 45               |             |
    And I enter the following values in "External PTF" table in Parameters page
      | order | name                                   | dontSell | dontBuy | minSellPercentage | minSellValue | maxBuyPercentage | maxBuyValue |
      | 2     | Dexia Invest Lange Danske Obligationer | false    | true    | 0.11              |              |                  |             |
      | 3     | Næringsbygg Holding 1 AS               | true     | false   |                   |              | 99.00            |             |
    Then I should see the table "Passive PTF" in Parameters page as
      | order | name                                                     | securityExposure           | volatilityRiskClass | complexityRiskClass | liquidityRiskClass | risk  | performance | datafeedStatus | dontSell | minSellPercentage | minSellValue | dontBuy | maxBuyPercentage | maxBuyValue | currentWeight | value   |
      | 1     | Garantum Kreditobligation High Yield Europa 5 år nr 1408 | Bonds International        | H                   | L                   | M                  | 17.62 | 10.12       | VALID          | false    | 0.10              | 1,537.56     | true    |                  |             | 0.14          | 2,228   |
      | 2     | Næringsbygg Holding 1 AS                                 | Alternatives - Real Estate | M                   | M                   | H                  | 23.65 | 4.88        | VALID          | true     |                   |              | false   | 45               | 691,900.90  | 15.61         | 240,000 |
    And I should see the table "External PTF" in Parameters page as
      | order | name                                                     | securityExposure           | volatilityRiskClass | complexityRiskClass | liquidityRiskClass | risk  | performance | datafeedStatus | dontSell | minSellPercentage | minSellValue | dontBuy | maxBuyPercentage | maxBuyValue  | currentWeight | value   |
      | 1     | Garantum Kreditobligation High Yield Europa 5 år nr 1408 | Bonds International        | H                   | L                   | M                  | 17.62 | 10.12       | VALID          | false    | 0.11              | 1,691.31     | true    |                  |              | 0.14          | 2,228   |
      | 2     | Dexia Invest Lange Danske Obligationer                   | UCITS Funds - Fixed Income | L                   | L                   | L                  | 23.13 | 8.95        | VALID          | false    |                   |              | true    |                  |              | 13.52         | 207,863 |
      | 3     | Næringsbygg Holding 1 AS                                 | Alternatives - Real Estate | M                   | M                   | H                  | 23.65 | 4.88        | VALID          | true     |                   |              | false   | 99               | 1,522,181.97 | 15.61         | 240,000 |
