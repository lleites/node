Feature: Historical Fluctuation
  NAVIGEA-386:AV - Results: HIST FLUCTUATION 1: Draw Chart
  NAVIGEA-387:AV - Results: HIST FLUCTUATION 2: Show value comparison in table (below Chart)
  NAVIGEA-388:AV - Results: HIST FLUCTUATION 3: Select analysis options

  @review @NAVIGEA-386 @NAVIGEA-387 @NAVIGEA-388 @NAVIGEA-388-v1 @Advisory_module @Result_page @HistoricalFluctuations
  Scenario: Historical fluctuations values
    -Check a portfolio with 2 isins with EUR currency in TetralogSystemsAG organization with EUR currency as base currency.
    -Add 2 BenchmarkS in the analysis with NOK currency.

    #review risk fluctuation--OBX Equities-- risk values in 1 year has 0.23 the diference in app 23.08 and in the excel sheet 23.31
    Given I am logged in with organization "TetralogSystemsAG" and CrmKey "QaTestData23"
    And I navigate to the "Advisory" module
    And In the Basic advisory setup area, I select the following checkboxes:
      | name                         | state |
      | Internal active - Active PTF | true  |
    When I select the "Result" page
    And I select the "Obx equities" in Comparison with dropdown 1
    And I select the "St1x fixed income" in Comparison with dropdown 2
    And I select the "Historical Fluctuation" option in the Select Analysis dropdown
    Then The app should show the following values in the "Historical Fluctuation" table
      | description            | oneYearHistoricalRisk | twoYearHistoricalRisk | threeYearHistoricalRisk | fourYearHistoricalRisk | fiveYearHistoricalRisk | oneYearHistoricalReturn | twoYearHistoricalReturn | threeYearHistoricalReturn | fourYearHistoricalReturn | fiveYearHistoricalReturn |
      | Current portfolios     |                       |                       |                         |                        |                        |                         |                         |                           |                          |                          |
      | Recommended portfolios |                       |                       |                         |                        |                        |                         |                         |                           |                          |                          |
      | OBX Equities           | 23.08                 | 23.38                 | 23.57                   | 24.07                  | 24.29                  | 0.85                    | -8.83                   | -8.61                     | -0.24                    | -51.38                   |
      | St1x fixed income      | 6.49                  | 7.94                  | 9.09                    | 9.21                   | 10.02                  | 2.00                    | -7.35                   | 2.80                      | 8.02                     | -6.76                    |

  @review @NAVIGEA-386 @NAVIGEA-387 @NAVIGEA-388 @NAVIGEA-388-v2 @Advisory_module @Result_page @HistoricalFluctuations
  Scenario: Historical fluctuations values
    -Check a portfolio with 2 isins with EUR currency in TetralogSystemsQA organization with NOK currency as base currency.
    -Add 2 BenchmarkS in the analysis with NOK currency.

    #review risk fluctuation--OBX Equities-- risk values in 1 year has 0.21 the diference in app 21.41 and in the excel sheet 21.62
    Given I am logged in with organization "TetralogSystemsQA" and CrmKey "QaTestData23"
    And I navigate to the "Advisory" module
    And In the Basic advisory setup area, I select the following checkboxes:
      | name                         | state |
      | Internal active - Active PTF | true  |
    When I select the "Result" page
    And I select the "Historical Fluctuation" option in the Select Analysis dropdown
    And I select the "Obx equities" in Comparison with dropdown 1
    And I select the "St1x fixed income" in Comparison with dropdown 2
    Then The app should show the following values in the "Historical Fluctuation" table
      | description            | oneYearHistoricalRisk | twoYearHistoricalRisk | threeYearHistoricalRisk | fourYearHistoricalRisk | fiveYearHistoricalRisk | oneYearHistoricalReturn | twoYearHistoricalReturn | threeYearHistoricalReturn | fourYearHistoricalReturn | fiveYearHistoricalReturn |
      | Current portfolios     |                       |                       |                         |                        |                        |                         |                         |                           |                          |                          |
      | Recommended portfolios |                       |                       |                         |                        |                        |                         |                         |                           |                          |                          |
      | OBX Equities           | 21.41                 | 22.09                 | 22.64                   | 22.76                  | 23.03                  | -2.95                   | -12.96                  | -18.69                    | -24.01                   | -54.35                   |
      | St1x fixed income      | 4.59                  | 4.87                  | 5.28                    | 5.16                   | 5.20                   | -1.84                   | -11.55                  | -8.54                     | -17.72                   | -12.45                   |

  @review @NAVIGEA-386 @NAVIGEA-387 @NAVIGEA-388 @NAVIGEA-388-v3 @Advisory_module @Result_page @HistoricalFluctuations
  Scenario: Historical fluctuations values
    Check a portfolio with 2 isins with EUR currency in Navexa organization with SEK currency as base currency.
    Add 1 Benchmarks in the analysis with SEK currency.

    Given I am logged in with organization "Navexa" and CrmKey "QaTestData23"
    And I navigate to the "Advisory" module
    And In the Basic advisory setup area, I select the following checkboxes:
      | name                             | state |
      | Portföljkonto Aktiv - Active PTF | true  |
    When I select the "Result" page
    And I select the "Historical Fluctuation" option in the Select Analysis dropdown
    And I select the "Omrx stadsskuldsindex sverige" in Comparison with dropdown 1
    Then The app should show the following values in the "Historical Fluctuation" table
      | description                   | oneYearHistoricalRisk | twoYearHistoricalRisk | threeYearHistoricalRisk | fourYearHistoricalRisk | fiveYearHistoricalRisk | oneYearHistoricalReturn | twoYearHistoricalReturn | threeYearHistoricalReturn | fourYearHistoricalReturn | fiveYearHistoricalReturn |
      | Nuvarande portföljer          |                       |                       |                         |                        |                        |                         |                         |                           |                          |                          |
      | Rekommenderade portföljer     |                       |                       |                         |                        |                        |                         |                         |                           |                          |                          |
      | Omrx stadsskuldsindex sverige | 5,23                  | 5,48                  | 5,22                    | 5,24                   | 5,22                   | 0,15                    | -3,04                   | -3,62                     | -13,95                   | -13,19                   |

  @review @NAVIGEA-386 @NAVIGEA-387 @NAVIGEA-388 @NAVIGEA-388-v4 @Advisory_module @Result_page @HistoricalFluctuations
  Scenario: Historical fluctuations values
    -Check a portfolio with 2 isins with NOK currency in TetralogSystemsQA organization with NOK currency as base currency.
    -Add 2 Benchmarks in the analysis with NOK and SEK currencies.

    #review risk fluctuation--OBX Equities-- risk values in 1 year has 0.21 the diference in app 21.41 and in the excel sheet 21.62
    Given I am logged in with organization "TetralogSystemsQA" and CrmKey "QaTestData24"
    And I navigate to the "Advisory" module
    And In the Basic advisory setup area, I select the following checkboxes:
      | name                         | state |
      | Internal active - Active PTF | true  |
    When I select the "Result" page
    And I select the "Historical Fluctuation" option in the Select Analysis dropdown
    And I select the "Obx equities" in Comparison with dropdown 1
    And I select the "Omrx fixed income" in Comparison with dropdown 2
    Then The app should show the following values in the "Historical Fluctuation" table
      | description            | oneYearHistoricalRisk | twoYearHistoricalRisk | threeYearHistoricalRisk | fourYearHistoricalRisk | fiveYearHistoricalRisk | oneYearHistoricalReturn | twoYearHistoricalReturn | threeYearHistoricalReturn | fourYearHistoricalReturn | fiveYearHistoricalReturn |
      | Current portfolios     |                       |                       |                         |                        |                        |                         |                         |                           |                          |                          |
      | Recommended portfolios |                       |                       |                         |                        |                        |                         |                         |                           |                          |                          |
      | OBX Equities           | 21.41                 | 22.09                 | 22.64                   | 22.76                  | 23.03                  | -2.95                   | -12.96                  | -18.69                    | -24.01                   | -54.35                   |
      | Omrx fixed income      | 7.71                  | 8.06                  | 7.97                    | 9.19                   | 9.42                   | -1.23                   | -3.79                   | 2.06                      | -17.50                   | -11.29                   |
