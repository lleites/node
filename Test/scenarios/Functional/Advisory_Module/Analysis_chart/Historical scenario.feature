Feature: Historical scenario
  NAVIGEA-389:AV - Results: HIST SCENARIO 1: Draw Chart
  NAVIGEA-390:AV - Results: HIST SCENARIO 2: Show value comparison in table (below Chart)
  NAVIGEA-391:AV - Results: HIST SCENARIO 3: Select analysis options

  @dev @NAVIGEA-389 @NAVIGEA-390 @NAVIGEA-391 @NAVIGEA-391-v1 @Advisory_module @Result_page @Historical_scenario
  Scenario: Historical scenario values
    -Check a portfolio with 2 isins with EUR currency in TetralogSystemsAG organization with EUR currency as base currency.
    Add 2 Benchmarks in the analysis with NOK currency.

    Given I am logged in with organization "TetralogSystemsAG" and CrmKey "QaTestData23"
    And I navigate to the "Advisory" module
    And In the Basic advisory setup area, I select the following checkboxes:
      | name                         | state |
      | Internal active - Active PTF | true  |
    When I select the "Result" page
    And I select the "Historical scenario" option in the Select Analysis dropdown
    And I select the "Obx equities" in Comparison with dropdown 1
    And I select the "St1x fixed income" in Comparison with dropdown 2
    And I select the option "Individual scenario" in the Select scenario dropdown
    And I enter a period of time to analyze
      | startDate  | endDate    |
      | 04/01/2008 | 28/12/2012 |
    And I apply the historical scenario
    Then The app should show the following values in the "Historical scenario" table
      | description            | totalStart | totalEnd | historicalPerformance | maxDrawdown |
      | Current portfolios     | 100.00     | 71.86    | -28.14                | -66.21      |
      | Recommended portfolios | 100.00     | 74.86    | -25.14                | -57.23      |
      | OBX Equities           | 100.00     | 48.62    | -51.38                | -54.04      |
      | ST1X Fixed income      | 100.00     | 93.24    | -6.76                 | -17.80      |

  @dev @NAVIGEA-389 @NAVIGEA-390 @NAVIGEA-391 @NAVIGEA-391-v2 @Advisory_module @Result_page @Historical_scenario
  Scenario: Historical scenario values
    Check a portfolio with 2 isins with EUR currency in TetralogSystemsQA organization with NOK currency as base currency.
    Add 2 Benchmarks in the analysis with NOK and SEK currencies.

    Given I am logged in with organization "TetralogSystemsQA" and CrmKey "QaTestData23"
    And I navigate to the "Advisory" module
    And In the Basic advisory setup area, I select the following checkboxes:
      | name                         | state |
      | Internal active - Active PTF | true  |
    When I select the "Result" page
    And I select the "Historical scenario" option in the Select Analysis dropdown
    And I select the "Obx equities" in Comparison with dropdown 1
    And I select the "Omrx fixed income" in Comparison with dropdown 2
    And I select the option "Individual scenario" in the Select scenario dropdown
    And I enter a period of time to analyze
      | startDate  | endDate    |
      | 04/01/2008 | 28/12/2012 |
    And I apply the historical scenario
    Then The app should show the following values in the "Historical scenario" table
      | description            | totalStart | totalEnd | historicalPerformance | maxDrawdown |
      | Current portfolios     | 100.00     | 67.47    | -32.53                | -61.54      |
      | Recommended portfolios | 100.00     | 70.47    | -29.53                | -51.82      |
      | OBX Equities           | 100.00     | 45.64    | -54.35                | -55.50      |
      | OMRX Fixed income      | 100.00     | 88.71    | -11.29                | -21.14      |

  @dev @NAVIGEA-389 @NAVIGEA-390 @NAVIGEA-391 @NAVIGEA-391-v3 @Advisory_module @Result_page @Historical_scenario
  Scenario: Historical scenario values
    Check a portfolio with 2 isins with EUR currency in Navexa organization with SEK currency as base currency.
    Add 1 Benchmark in the analysis with SEK currency.

    Given I am logged in with organization "Navexa" and CrmKey "QaTestData23"
    And I navigate to the "Advisory" module
    And In the Basic advisory setup area, I select the following checkboxes:
      | name                             | state |
      | Portföljkonto Aktiv - Active PTF | true  |
    When I select the "Result" page
    And I select the "Historical scenario" option in the Select Analysis dropdown
    And I select the "Omrx stadsskuldsindex sverige" in Comparison with dropdown 1
    And I select the option "Individual scenario" in the Select scenario dropdown
    And I enter a period of time to analyze
      | startDate  | endDate    |
      | 04/01/2008 | 28/12/2012 |
    And I apply the historical scenario
    Then The app should show the following values in the "Historical scenario" table
      | description                   | totalStart | totalEnd | historicalPerformance | maxDrawdown |
      | Nuvarande portföljer          | 100.00     | 66,02    | -33,98                | -58,87      |
      | Rekommenderade portföljer     | 100.00     | 67,72    | -32,28                | -49,23      |
      | OMRX Stadsskuldsindex Sverige | 100.00     | 86,81    | -13,19                | -16,47      |

  @dev @NAVIGEA-389 @NAVIGEA-390 @NAVIGEA-391 @NAVIGEA-391-v4 @Advisory_module @Result_page @Historical_scenario
  Scenario: Historical scenario values
    Check a portfolio with 2 isins with NOK currency in TetralogSystemsQA organization with NOK currency as base currency.
    Add 2 Benchmarks in the analysis with NOK currency.

    Given I am logged in with organization "TetralogSystemsQA" and CrmKey "QaTestData24"
    And I navigate to the "Advisory" module
    And In the Basic advisory setup area, I select the following checkboxes:
      | name                         | state |
      | Internal active - Active PTF | true  |
    When I select the "Result" page
    And I select the "Historical scenario" option in the Select Analysis dropdown
    And I select the "Obx equities" in Comparison with dropdown 1
    And I select the "St1x fixed income" in Comparison with dropdown 2
    And I select the option "Individual scenario" in the Select scenario dropdown
    And I enter a period of time to analyze
      | startDate  | endDate    |
      | 04/01/2008 | 28/12/2012 |
    And I apply the historical scenario
    Then The app should show the following values in the "Historical scenario" table
      | description            | totalStart | totalEnd | historicalPerformance | maxDrawdown |
      | Current portfolios     | 100.00     | 87.64    | -12.36                | -40.07      |
      | Recommended portfolios | 100.00     | 87.64    | -12.36                | -40.07      |
      | OBX Equities           | 100.00     | 45.64    | -54.35                | -55.50      |
      | ST1X Fixed income      | 100.00     | 93.24    | -6.76                 | -17.80      |

  @dev @NAVIGEA-389 @NAVIGEA-390 @NAVIGEA-391 @NAVIGEA-391-v5 @Advisory_module @Result_page @Historical_scenario
  Scenario: Historical scenario values
    Check a portfolio with 2 isins with NOK currency in TetralogSystemsAG organization with EUR currency as base currency.
    Add 2 Benchmarks in the analysis with NOK currency.

    Given I am logged in with organization "TetralogSystemsAG" and CrmKey "QaTestData24"
    And I navigate to the "Advisory" module
    And In the Basic advisory setup area, I select the following checkboxes:
      | name                         | state |
      | Internal active - Active PTF | true  |
    When I select the "Result" page
    And I select the "Historical scenario" option in the Select Analysis dropdown
    And I select the "Obx equities" in Comparison with dropdown 1
    And I select the "St1x fixed income" in Comparison with dropdown 2
    And I select the option "Individual scenario" in the Select scenario dropdown
    And I enter a period of time to analyze
      | startDate  | endDate    |
      | 04/01/2008 | 28/12/2012 |
    And I apply the historical scenario
    Then The app should show the following values in the "Historical scenario" table
      | description            | totalStart | totalEnd | historicalPerformance | maxDrawdown |
      | Current portfolios     | 100.00     | 93.35    | -6.65                 | -43.15      |
      | Recommended portfolios | 100.00     | 93.35    | -6.65                 | -43.15      |
      | OBX Equities           | 100.00     | 48.62    | -51.38                | -54.04      |
      | ST1X Fixed income      | 100.00     | 93.24    | -6.76                 | -17.80      |

  @dev @NAVIGEA-389 @NAVIGEA-390 @NAVIGEA-391 @NAVIGEA-391-v6 @Advisory_module @Result_page @Historical_scenario
  Scenario: Historical scenario values
    Check a portfolio with 2 isins with NOK currency in Navexa organization with SEK currency as base currency.
    Add 2 Benchmarks in the analysis with SEK currency.

    Given I am logged in with organization "Navexa" and CrmKey "QaTestData24"
    And I navigate to the "Advisory" module
    And In the Basic advisory setup area, I select the following checkboxes:
      | name                             | state |
      | Portföljkonto Aktiv - Active PTF | true  |
    When I select the "Result" page
    And I select the "Historical scenario" option in the Select Analysis dropdown
    And I select the "Omrx stadsskuldsindex sverige" in Comparison with dropdown 1
    And I select the option "Individual scenario" in the Select scenario dropdown
    And I enter a period of time to analyze
      | startDate  | endDate    |
      | 04/01/2008 | 28/12/2012 |
    And I apply the historical scenario
    Then The app should show the following values in the "Historical scenario" table
      | description                   | totalStart | totalEnd | historicalPerformance | maxDrawdown |
      | Nuvarande portföljer          | 100.00     | 85,76    | -14,24                | -43,04      |
      | Rekommenderade portföljer     | 100.00     | 85,76    | -14,24                | -43,04      |
      | OMRX Stadsskuldsindex Sverige | 100.00     | 86,81    | -13,19                | -16,47      |

  @dev @NAVIGEA-389 @NAVIGEA-390 @NAVIGEA-391 @NAVIGEA-391-v7 @Advisory_module @Result_page @Historical_scenario
  Scenario: Historical scenario values
    Check a portfolio with 5 isins with diferent currencies in TetralogSystemsAG organization with EUR currency as base currency.

    Given I am logged in with organization "TetralogSystemsAG" and CrmKey "QaTestData07"
    And I navigate to the "Advisory" module
    And In the Basic advisory setup area, I select the following checkboxes:
      | name                                    | state |
      | Internal passive - Internal passive PTF | true  |
    When I select the "Result" page
    And I select the "Historical scenario" option in the Select Analysis dropdown
    And I select the option "Individual scenario" in the Select scenario dropdown
    And I enter a period of time to analyze
      | startDate  | endDate    |
      | 05/09/2008 | 27/02/2009 |
    And I apply the historical scenario
    Then The app should show the following values in the "Historical scenario" table
      | description            | totalStart | totalEnd | historicalPerformance | maxDrawdown |
      | Current portfolios     | 100.00     | 54.85    | -45.15                | -49.08      |
      | Recommended portfolios | 100.00     | 54.85    | -45.15                | -49.08      |

  @dev @NAVIGEA-389 @NAVIGEA-390 @NAVIGEA-391 @NAVIGEA-391-v8 @Advisory_module @Result_page @Historical_scenario
  Scenario: Historical scenario values
    Check a portfolio with 5 isins with diferent currencies in TetralogSystemsQA organization with NOK currency as base currency.

    Given I am logged in with organization "TetralogSystemsQA" and CrmKey "QaTestData07"
    And I navigate to the "Advisory" module
    And In the Basic advisory setup area, I select the following checkboxes:
      | name                                    | state |
      | Internal passive - Internal passive PTF | true  |
    When I select the "Result" page
    And I select the "Historical scenario" option in the Select Analysis dropdown
    And I select the option "Individual scenario" in the Select scenario dropdown
    And I enter a period of time to analyze
      | startDate  | endDate    |
      | 05/09/2008 | 27/02/2009 |
    And I apply the historical scenario
    Then The app should show the following values in the "Historical scenario" table
      | description            | totalStart | totalEnd | historicalPerformance | maxDrawdown |
      | Current portfolios     | 100.00     | 60.87    | -39.13                | -43.23      |
      | Recommended portfolios | 100.00     | 60.87    | -39.13                | -43.23      |
