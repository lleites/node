Feature: Return fluctuations
  NAVIGEA-392:AV - Results: RET FLUCTUATION 1: Draw Chart
  NAVIGEA-393: AV - Results: RET FLUCTUATION 2: Show value comparison in table (below Chart)
  NAVIGEA-394:AV - Results: RET FLUCTUATION 3: Select analysis options

  @active @NAVIGEA-392 @NAVIGEA-393 @NAVIGEA-394 @NAVIGEA-393-v1 @Advisory_module @Result_page @ReturnFluctuations
  Scenario: Return fluctuations values
    -Check a portfolio with 2 isins with EUR currency in TetralogSystemsAG organization with EUR currency as base currency.

    Given I am logged in with organization "TetralogSystemsAG" and CrmKey "QaTestData23"
    And I navigate to the "Advisory" module
    And In the Basic advisory setup area, I select the following checkboxes:
      | name                         | state |
      | Internal active - Active PTF | true  |
    When I select the "Result" page
    And I select the "Return fluctuations" option in the Select Analysis dropdown
    Then The app should show the following values in the "Return fluctuations" table
      | description            | oneYearReturn  | twoYearReturn   | threeYearReturn | fourYearReturn | fiveYearReturn  |
      | Current portfolios     | 73.68 @ -63.40 | 153.09 @ -41.02 | 134.09 @ -19.53 | 69.94 @ -40.65 | -28.14 @ -28.14 |
      | Recommended portfolios | 62.99 @ -54.53 | 112.42 @ -35.12 | 95.58 @ -14.81  | 53.49 @ -36.17 | -25.14 @ -25.14 |

  @active @NAVIGEA-392 @NAVIGEA-393 @NAVIGEA-394 @NAVIGEA-393-v2 @Advisory_module @Result_page @ReturnFluctuations
  Scenario: Return fluctuations values
    Check a portfolio with 2 isins with EUR currency in TetralogSystemsQA organization with NOK currency as base currency.
    Add 2 Benchmarks in the analysis with NOK and SEK currencies.

    Given I am logged in with organization "TetralogSystemsQA" and CrmKey "QaTestData23"
    And I navigate to the "Advisory" module
    And In the Basic advisory setup area, I select the following checkboxes:
      | name                         | state |
      | Internal active - Active PTF | true  |
    When I select the "Result" page
    And I select the "Return fluctuations" option in the Select Analysis dropdown
    And I select the "Obx equities" in Comparison with dropdown 1
    And I select the "Omrx fixed income" in Comparison with dropdown 2
    Then The app should show the following values in the "Return fluctuations" table
      | description            | oneYearReturn  | twoYearReturn   | threeYearReturn | fourYearReturn  | fiveYearReturn  |
      | Current portfolios     | 62.17 @ -58.90 | 120.88 @ -40.06 | 97.49 @ -24.90  | 45.26 @ -41.55  | -32.53 @ -32.53 |
      | Recommended portfolios | 54.50 @ -49.12 | 86.01 @ -34.13  | 65.84 @ -20.49  | 35.20 @ -36.99  | -29.53 @ -29.53 |
      | OBX Equities           | 20.71 @ -41.19 | 16.67 @ -45.70  | 9.10 @ -47.55    | -12.10 @ -54.36 | -54.35 @ -54.35 |
      | OMRX Fixed income      | 9.71 @ -20.35  | 6.65 @ -15.61   | 4.06 @ -17.51   | -6.05 @ -18.58  | -11.29 @ -11.29 |

  @active @NAVIGEA-392 @NAVIGEA-393 @NAVIGEA-394 @NAVIGEA-393-v3 @Advisory_module @Result_page @ReturnFluctuations
  Scenario: Return fluctuations values
    Check a portfolio with 2 isins with EUR currency in Navexa organization with SEK currency as base currency.
    Add 1 Benchmark in the analysis with SEK currency.

    Given I am logged in with organization "Navexa" and CrmKey "QaTestData23"
    And I navigate to the "Advisory" module
    And In the Basic advisory setup area, I select the following checkboxes:
      | name                             | state |
      | Portföljkonto Aktiv - Active PTF | true  |
    When I select the "Result" page
    And I select the "Return fluctuations" option in the Select Analysis dropdown
    And I select the "Omrx stadsskuldsindex sverige" in Comparison with dropdown 1
    Then The app should show the following values in the "Return fluctuations" table
      | description                   | oneYearReturn  | twoYearReturn   | threeYearReturn | fourYearReturn | fiveYearReturn  |
      | Nuvarande portföljer          | 63,55 @ -55,38 | 105,23 @ -38,95 | 80,20 @ -24,45  | 47,23 @ -43,41 | -33,98 @ -33,98 |
      | Rekommenderade portföljer     | 41,20 @ -45,06 | 73,66 @ -34,34  | 49,01 @ -19,87  | 29,10 @ -38,81 | -32,28 @ -32,28 |
      | OMRX Stadsskuldsindex Sverige | 3,69 @ -11,69  | 0,67 @ -12,64   | -2,83 @ -15,16  | -8,37 @ -14,76 | -13,19 @ -13,19 |

  @active @NAVIGEA-392 @NAVIGEA-393 @NAVIGEA-394 @NAVIGEA-393-v4 @Advisory_module @Result_page @ReturnFluctuations
  Scenario: Return fluctuations values
    Check a portfolio with 2 isins with NOK currency in TetralogSystemsQA organization with NOK currency as base currency.
    Add 2 Benchmarks in the analysis with NOK currency.

    Given I am logged in with organization "TetralogSystemsQA" and CrmKey "QaTestData24"
    And I navigate to the "Advisory" module
    And In the Basic advisory setup area, I select the following checkboxes:
      | name                         | state |
      | Internal active - Active PTF | true  |
    When I select the "Result" page
    And I select the "Return fluctuations" option in the Select Analysis dropdown
    And I select the "Obx equities" in Comparison with dropdown 1
    And I select the "St1x fixed income" in Comparison with dropdown 2
    Then The app should show the following values in the "Return fluctuations" table
      | description            | oneYearReturn  | twoYearReturn  | threeYearReturn | fourYearReturn  | fiveYearReturn  |
      | Current portfolios     | 24.07 @ -34.11 | 32.03 @ -34.64 | 27.24 @ -34.74  | 6.68 @ -25.54   | -12.36 @ -12.36 |
      | Recommended portfolios | 24.07 @ -34.11 | 32.03 @ -34.64 | 27.24 @ -34.74  | 6.68 @ -25.54   | -12.36 @ -12.36 |
      | OBX Equities           | 20.71 @ -41.19 | 16.67 @ -45.70 | 9.10 @ -47.55    | -12.10 @ -54.36 | -54.35 @ -54.35 |
      | ST1X Fixed income      | 8.59 @ -12.16  | 0.97 @ -13.59  | -1.57 @ -18.75  | -7.81 @ -17.48  | -12.45 @ -12.45 |

  @active @NAVIGEA-392 @NAVIGEA-393 @NAVIGEA-394 @NAVIGEA-393-v5 @Advisory_module @Result_page @ReturnFluctuations
  Scenario: Return fluctuations values
    Check a portfolio with 2 isins with NOK currency in TetralogSystemsAG organization with EUR currency as base currency.
    Add 2 Benchmarks in the analysis with NOK currency.

    Given I am logged in with organization "TetralogSystemsAG" and CrmKey "QaTestData24"
    And I navigate to the "Advisory" module
    And In the Basic advisory setup area, I select the following checkboxes:
      | name                         | state |
      | Internal active - Active PTF | true  |
    When I select the "Result" page
    And I select the "Return fluctuations" option in the Select Analysis dropdown
    And I select the "Obx equities" in Comparison with dropdown 1
    And I select the "St1x fixed income" in Comparison with dropdown 2
    Then The app should show the following values in the "Return fluctuations" table
      | description            | oneYearReturn  | twoYearReturn  | threeYearReturn | fourYearReturn | fiveYearReturn  |
      | Current portfolios     | 26.26 @ -38.72 | 37.23 @ -35.06 | 50.51 @ -33.87  | 40.92 @ -22.06 | -6.65 @ -6.65   |
      | Recommended portfolios | 26.26 @ -38.72 | 37.23 @ -35.06 | 50.51 @ -33.87  | 40.92 @ -22.06 | -6.65 @ -6.65   |
      | OBX Equities           | 34.42 @ -51.58 | 37.22 @ -46.98 | 26.72 @ -46.84  | 0.08 @ -52.23  | -51.38 @ -51.38 |
      | ST1X Fixed income      | 10.93 @ -13.68 | 18.63 @ -9.68  | 7.09 @ -9.08    | 12.46 @ -9.80  | -6.76 @ -6.76   |

  @dev @NAVIGEA-392 @NAVIGEA-393 @NAVIGEA-394 @NAVIGEA-393-v6 @Advisory_module @Result_page @ReturnFluctuations
  Scenario: Return fluctuations values
    Check a portfolio with 2 isins with NOK currency in Navexa organization with SEK currency as base currency.
    Add 1 Benchmark in the analysis with SEK currency.

    Given I am logged in with organization "Navexa" and CrmKey "QaTestData24"
    And I navigate to the "Advisory" module
    And In the Basic advisory setup area, I select the following checkboxes:
      | name                             | state |
      | Portföljkonto Aktiv - Active PTF | true  |
    When I select the "Result" page
    And I select the "Return fluctuations" option in the Select Analysis dropdown
    And I select the "Omrx stadsskuldsindex sverige" in Comparison with dropdown 1
    Then The app should show the following values in the "Return fluctuations" table
      | description                   | oneYearReturn  | twoYearReturn  | threeYearReturn | fourYearReturn | fiveYearReturn  |
      | Nuvarande portföljer          | 19,50 @ -33,51 | 27,40 @ -31,85 | 21,31 @ -37,83  | 10,03 @ -26,49 | -14.24 @ -14.24 |
      | Rekommenderade portföljer     | 19,50 @ -33,51 | 27,40 @ -31,85 | 21,31 @ -37,83  | 10,03 @ -26,49 | -14.24 @ -14.24 |
      | OMRX Stadsskuldsindex Sverige | 3,69 @ -11,69  | 0,67 @ -12,64  | -2,83 @ -15,16  | -8,37 @ -14,76 | -13.19 @ -13.19 |

  @active @NAVIGEA-392 @NAVIGEA-393 @NAVIGEA-394 @NAVIGEA-393-v7 @Advisory_module @Result_page @ReturnFluctuations
  Scenario: Historical Fluctuation values
    Check a portfolio with 5 isins with diferent currencies in TetralogSystemsQA organization with NOK currency as base currency.

    Given I am logged in with organization "TetralogSystemsQA" and CrmKey "QaTestData07"
    And I navigate to the "Advisory" module
    And In the Basic advisory setup area, I select the following checkboxes:
      | name                                    | state |
      | Internal passive - Internal passive PTF | true  |
    When I select the "Result" page
    And I select the "Return fluctuations" option in the Select Analysis dropdown
    Then The app should show the following values in the "Return fluctuations" table
      | description            | oneYearReturn  | twoYearReturn  | threeYearReturn | fourYearReturn | fiveYearReturn  |
      | Current portfolios     | 69.85 @ -48.37 | 93.72 @ -32.04 | 72.29 @ -46.13  | 25.40 @ -41.22 | -23.62 @ -23.62 |
      | Recommended portfolios | 69.85 @ -48.37 | 93.72 @ -32.04 | 72.29 @ -46.13  | 25.40 @ -41.22 | -23.62 @ -23.62 |

  @active @NAVIGEA-392 @NAVIGEA-393 @NAVIGEA-394 @NAVIGEA-393-v8 @Advisory_module @Result_page @ReturnFluctuations
  Scenario: Return fluctuations values
    Check a portfolio with 5 isins with diferent currencies in TetralogSystemsAG organization with EUR currency as base currency.

    Given I am logged in with organization "TetralogSystemsAG" and CrmKey "QaTestData07"
    And I navigate to the "Advisory" module
    And In the Basic advisory setup area, I select the following checkboxes:
      | name                                    | state |
      | Internal passive - Internal passive PTF | true  |
    When I select the "Result" page
    And I select the "Return fluctuations" option in the Select Analysis dropdown
    Then The app should show the following values in the "Return fluctuations" table
      | description            | oneYearReturn  | twoYearReturn   | threeYearReturn | fourYearReturn | fiveYearReturn  |
      | Current portfolios     | 88.92 @ -54.36 | 122.77 @ -32.75 | 107.92 @ -44.98 | 53.30 @ -38.09 | -18.65 @ -18.65 |
      | Recommended portfolios | 88.92 @ -54.36 | 122.77 @ -32.75 | 107.92 @ -44.98 | 53.30 @ -38.09 | -18.65 @ -18.65 |
