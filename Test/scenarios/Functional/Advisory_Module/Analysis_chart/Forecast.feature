Feature: Forecast
  NAVIGEA-383: AV - Results: FORECAST 1: Draw Chart
  NAVIGEA-384: AV - Results: FORECAST 2: Show value comparison in table (below Chart)
  NAVIGEA-385: AV - Results: FORECAST 3: Select analysis options

  @dev @NAVIGEA-383 @NAVIGEA-384 @NAVIGEA-385 @NAVIGEA-385-v1 @Advisory_module @Result_page @Forecast
  Scenario: Forecast values--Period in Years=10
    -Check a portfolio with 2 isins with EUR currency in TetralogSystemsAG organization with EUR currency as base currency.

    Given I am logged in with organization "TetralogSystemsAG" and CrmKey "QaTestData23"
    And I navigate to the "Advisory" module
    And In the Basic advisory setup area, I select the following checkboxes:
      | name                         | state |
      | Internal active - Active PTF | true  |
    When I select the "Result" page
    And I select the "Forecast" option in the Select Analysis dropdown
    And I select as period to analyze "10" years
    Then The app should show the following values in the "Forecast" table
      | description            | forecastLower | forecastUpper | forecast   |
      | Current portfolios     | 45,399.88     | 4,764,881.93  | 940,957.67 |
      | Recommended portfolios | 61,511.16     | 4,087,995.78  | 889,413.90 |

  @dev @NAVIGEA-383 @NAVIGEA-384 @NAVIGEA-385 @NAVIGEA-385-v2 @Advisory_module @Result_page @Forecast
  Scenario: Forecast values--Period in Years=10
    -Check a portfolio with 2 isins with EUR currency in TetralogSystemsQA organization with NOK currency as base currency.
    Add 1 Benchmark in the analysis with NOK currency.

    Given I am logged in with organization "TetralogSystemsQA" and CrmKey "QaTestData23"
    And I navigate to the "Advisory" module
    And In the Basic advisory setup area, I select the following checkboxes:
      | name                         | state |
      | Internal active - Active PTF | true  |
    When I select the "Result" page
    And I select the "Forecast" option in the Select Analysis dropdown
    And I select the "Obx equities" in Comparison with dropdown 1
    And I select as period to analyze "10" years
    Then The app should show the following values in the "Forecast" table
      | description            | forecastLower | forecastUpper | forecast     |
      | Current portfolios     | 447,901.16    | 40,135,671.80 | 8,183,453.47 |
      | Recommended portfolios | 586,138.22    | 34,563,985.89 | 7,730,414.91 |
      | Obx equities           | 1,412,935.49  | 20,384,994.62 | 6,766,926.17 |

  @dev @NAVIGEA-383 @NAVIGEA-384 @NAVIGEA-385 @NAVIGEA-385-v3 @Advisory_module @Result_page @Forecast
  Scenario: Forecast values--Period in Years=10
    -Check a portfolio with 2 isins with EUR currency in Navexa organization with SEK currency as base currency.
    Add 1 Benchmark in the analysis with SEK currency.

    Given I am logged in with organization "Navexa" and CrmKey "QaTestData23"
    And I navigate to the "Advisory" module
    And In the Basic advisory setup area, I select the following checkboxes:
      | name                             | state |
      | Portföljkonto Aktiv - Active PTF | true  |
    When I select the "Result" page
    And I select the "Prognos" option in the Select Analysis dropdown
    And I select the "Omrx stadsskuldsindex sverige" in Comparison with dropdown 1
    And I select as period to analyze "10" years
    Then The app should show the following values in the "Forecast" table
      | description                   | forecastLower | forecastUpper | forecast     |
      | Nuvarande portföljer          | 399 541,27    | 36 104 382,22 | 7 348 688,22 |
      | Rekommenderade portföljer     | 596 025,49    | 33 651 192,51 | 7 603 942,91 |
      | OMRX Stadsskuldsindex Sverige | 4 012 583,60  | 7 556 715,05  | 5 578 800,74 |

  @dev @NAVIGEA-383 @NAVIGEA-384 @NAVIGEA-385 @NAVIGEA-385-v4 @Advisory_module @Result_page @Forecast
  Scenario: Forecast values--Period in Years=10
    Check a portfolio with 5 isins with diferent currencies in TetralogSystemsAG organization with EUR currency as base currency.
    Add 1 Benchmark in the analysis with NOK currency.

    Given I am logged in with organization "TetralogSystemsAG" and CrmKey "QaTestData07"
    And I navigate to the "Advisory" module
    And In the Basic advisory setup area, I select the following checkboxes:
      | name                                    | state |
      | Internal passive - Internal passive PTF | true  |
    When I select the "Result" page
    And I select the "Forecast" option in the Select Analysis dropdown
    And I select the "Obx equities" in Comparison with dropdown 1
    And I select as period to analyze "10" years
    Then The app should show the following values in the "Forecast" table
      | description            | forecastLower | forecastUpper | forecast   |
      | Current portfolios     | 43,379.92     | 5,057,395.41  | 978,593.40 |
      | Recommended portfolios | 43,379.92     | 5,057,395.41  | 978,593.40 |
      | Obx equities           | 139,592.72    | 2,431,974.03  | 760,003.86 |

  @dev @NAVIGEA-383 @NAVIGEA-384 @NAVIGEA-385 @NAVIGEA-385-v5 @Advisory_module @Result_page @Forecast
  Scenario: Forecast values--Period in Years=5
    Check a portfolio with 5 isins with diferent currencies in TetralogSystemsQA organization with NOK currency as base currency.
    Add 1 Benchmark in the analysis with NOK currency.

    Given I am logged in with organization "TetralogSystemsQA" and CrmKey "QaTestData07"
    And I navigate to the "Advisory" module
    And In the Basic advisory setup area, I select the following checkboxes:
      | name                                    | state |
      | Internal passive - Internal passive PTF | true  |
    When I select the "Result" page
    And I select the "Forecast" option in the Select Analysis dropdown
    And I select the "Obx equities" in Comparison with dropdown 1
    And I select as period to analyze "5" years
    Then The app should show the following values in the "Forecast" table
      | description            | forecastLower | forecastUpper | forecast     |
      | Current portfolios     | 818,239.10    | 20,035,285.48 | 5,647,682.06 |
      | Recommended portfolios | 818,239.10    | 20,035,285.48 | 5,647,682.06 |
      | Obx equities           | 1,722,786.31  | 11,373,687.68 | 4,970,543.07 |

  @dev @NAVIGEA-383 @NAVIGEA-384 @NAVIGEA-385 @NAVIGEA-385-v6 @Advisory_module @Result_page @Forecast
  Scenario: Forecast values--Period in Years=1
    Check a portfolio with 2 isins with NOK currency in TetralogSystemsQA organization with NOK currency as base currency.
    Add 2 Benchmarks in the analysis with NOK currency.

    Given I am logged in with organization "TetralogSystemsQA" and CrmKey "QaTestData24"
    And I navigate to the "Advisory" module
    And In the Basic advisory setup area, I select the following checkboxes:
      | name                         | state |
      | Internal active - Active PTF | true  |
    When I select the "Result" page
    And I select the "Forecast" option in the Select Analysis dropdown
    And I select the "Obx equities" in Comparison with dropdown 1
    And I select the "St1x fixed income" in Comparison with dropdown 2
    And I select as period to analyze "5" years
    Then The app should show the following values in the "Forecast" table
      | description            | forecastLower | forecastUpper | forecast     |
      | Current portfolios     | 3,844,590.21  | 9,084,718.25  | 6,053,833.72 |
      | Recommended portfolios | 3,844,590.21  | 9,084,718.25  | 6,053,833.72 |
      | Obx equities           | 3,829,738.15  | 8,907,105.54  | 5,977,510.35 |
      | St1x fixed income      | 5,247,023.42  | 6,399,966.48  | 5,802,336.29 |

  @dev @NAVIGEA-383 @NAVIGEA-384 @NAVIGEA-385 @NAVIGEA-385-v7 @Advisory_module @Result_page @Forecast
  Scenario: Forecast values--Period in Years=8
    Check a portfolio with 2 isins with NOK currency in TetralogSystemsAG organization with EUR currency as base currency.
	Add 1 Benchmark in the analysis with NOK currency.
	
    Given I am logged in with organization "TetralogSystemsAG" and CrmKey "QaTestData24"
    And I navigate to the "Advisory" module
    And In the Basic advisory setup area, I select the following checkboxes:
      | name                         | state |
      | Internal active - Active PTF | true  |
    When I select the "Result" page
    And I select the "Forecast" option in the Select Analysis dropdown
    And I select the "St1x fixed income" in Comparison with dropdown 2
    And I select as period to analyze "5" years
    Then The app should show the following values in the "Forecast" table
      | description            | forecastLower | forecastUpper | forecast     |
      | Current portfolios     | 244,060.65    | 3,542,783.44  | 1,173,703.71 |
      | Recommended portfolios | 244,060.65    | 3,542,783.44  | 1,173,703.71 |
      | St1x fixed income      | 464,300.54    | 1,391,343.14  | 835,868.07   |
