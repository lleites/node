Feature: NAVIGEA-377--AV - Results: STRUCTURE 2: Show weight and values comparison in table (below Chart)
         NAVIGEA-378--AV - Results: STRUCTURE 3: Select analysis options
  	     NAVIGEA-424--AV - Results: Portfolio structure: Please remove some analysis options from dropdown
  			 Structure charts:
  		 Analysis options
  		 Please remove the following entries from the dropdown (these will be part of a later release)
  			 Asset Class
             Security exposure
             Portfolio type
         NAVIGEA-563--the Values NOK that the app shows in the Portfolio Structure table are not correct
         NAVIGEA-809: AV - Results: STRUCTURE: Tables for Breakdown Analysis "Security Class"
         NAVIGEA-769: AV - Results: STRUCTURE: Tables for Breakdown Analysis "Security Exposure"
   NAVIGEA-516-- AV - Results page: New chart for Security Exposure

  @active @NAVIGEA-377 @NAVIGEA-378-v1 @NAVIGEA-563 @NAVIEA-809 @Advisory_module @Result_page @Portfolio_structure
  Scenario: @NAVIGEA-377_@NAVIGEA-378_@NAVIGEA-563-v1 --Select the security class option.
    Given I am logged in with organization "TetralogSystemsQA" and CrmKey "QaTestData07"
    And I navigate to the "Advisory" module
    And In the Basic advisory setup area, I select the following checkboxes:
      | name                                    | state |
      | Internal passive - Internal passive PTF | true  |
    And I select the "Setup" page
    And I click on the "Show Parameters" button
    When I enter the following values in "Internal passive PTF" table in Parameters page
      | order | name                                                     | dontSell | dontBuy |
      | 1     | Copy of Daimler N.A.                                     | false    | false   |
      | 2     | Copy of Microsoft                                        | false    | false   |
      | 3     | Copy A.P. Møller - Mærsk A                               | false    | false   |
      | 4     | Copy of LU0048578792 Fidelity Europ Growth               | false    | false   |
      | 5     | Garantum Kreditobligation High Yield Europa 5 år nr 1408 | false    | false   |
    When I select the "Result" page
    And I select the "Portfolio structure" option in the Select Analysis dropdown
    And I select the "Security class" in Show structure by dropdown
    Then The app should show the following values in the "Portfolio Structure" table
      | description   | currentWeight | currentValue | recommendedWeight | recommendedValue |
      | Bonds         | 0.01          | 222.90       | 63.06             | 2,349,737.51     |
      | Listed Shares | 99.96         | 3,724,731.10 | 2.04              | 75,949.00        |
      | UCITS Funds   | 0.03          | 1,117.64     | 34.90             | 1,300,385.14     |

  @active @NAVIGEA-377 @NAVIGEA-378-v2 @Advisory_module @Result_page @Portfolio_structure
  Scenario: @NAVIGEA-377_@NAVIGEA-378_@NAVIGEA-563-v2 --Select the Liquidity risk option.
    Given I am logged in with organization "TetralogSystemsQA" and CrmKey "QaTestData07"
    And I navigate to the "Advisory" module
    And In the Basic advisory setup area, I select the following checkboxes:
      | name                                    | state |
      | Internal passive - Internal passive PTF | true  |
    And I select the "Setup" page
    And I click on the "Show Parameters" button
    When I enter the following values in "Internal passive PTF" table in Parameters page
      | order | name                                                     | dontSell | dontBuy |
      | 1     | Copy of Daimler N.A.                                     | false    | false   |
      | 2     | Copy of Microsoft                                        | false    | false   |
      | 3     | Copy A.P. Møller - Mærsk A                               | false    | false   |
      | 4     | Copy of LU0048578792 Fidelity Europ Growth               | false    | false   |
      | 5     | Garantum Kreditobligation High Yield Europa 5 år nr 1408 | false    | false   |
    When I select the "Result" page
    And I select the "Portfolio structure" option in the Select Analysis dropdown
    And I select the "Liquidity risk" in Show structure by dropdown
    Then The app should show the following values in the "Portfolio Structure" table
      | description | currentWeight | currentValue | recommendedWeight | recommendedValue |
      | Low         | 0.03          | 1,117.64     | 34.90             | 1,300,385.14     |
      | Medium      | 99.62         | 3,712,093.11 | 63.06             | 2,349,737.51     |
      | High        | 0.35          | 12,860.89    | 2.04              | 75,949.00        |

  @active @NAVIGEA-377 @NAVIGEA-378 @Advisory_module @Result_page @Portfolio_structure
  Scenario: @NAVIGEA-377_@NAVIGEA-378_@NAVIGEA-563-v3 --Select the Volatility risk option.
    Given I am logged in with organization "TetralogSystemsQA" and CrmKey "QaTestData07"
    And I navigate to the "Advisory" module
    And In the Basic advisory setup area, I select the following checkboxes:
      | name                                    | state |
      | Internal passive - Internal passive PTF | true  |
    And I select the "Setup" page
    And I click on the "Show Parameters" button
    When I enter the following values in "Internal passive PTF" table in Parameters page
      | order | name                                                     | dontSell | dontBuy |
      | 1     | Copy of Daimler N.A.                                     | false    | false   |
      | 2     | Copy of Microsoft                                        | false    | false   |
      | 3     | Copy A.P. Møller - Mærsk A                               | false    | false   |
      | 4     | Copy of LU0048578792 Fidelity Europ Growth               | false    | false   |
      | 5     | Garantum Kreditobligation High Yield Europa 5 år nr 1408 | false    | false   |
    When I select the "Result" page
    And I select the "Portfolio structure" option in the Select Analysis dropdown
    And I select the "Volatility risk" in Show structure by dropdown
    Then The app should show the following values in the "Portfolio Structure" table
      | description | currentWeight | currentValue | recommendedWeight | recommendedValue |
      | Low         | 99.62         | 3,711,870.21 | 0.00              | 0.00             |
      | Medium      | 0.35          | 12,860.89    | 2.04              | 75,949.00        |
      | High        | 0.04          | 1,340.54     | 97.96             | 3,650,122.65     |

  @active @NAVIGEA-377 @NAVIGEA-378 @Advisory_module @Result_page @Portfolio_structure
  Scenario: @NAVIGEA-377_@NAVIGEA-378_@NAVIGEA-563-v4 --Select the Complexity risk option.
    Given I am logged in with organization "TetralogSystemsQA" and CrmKey "QaTestData07"
    And I navigate to the "Advisory" module
    And In the Basic advisory setup area, I select the following checkboxes:
      | name                                    | state |
      | Internal passive - Internal passive PTF | true  |
    And I select the "Setup" page
    And I click on the "Show Parameters" button
    When I enter the following values in "Internal passive PTF" table in Parameters page
      | order | name                                                     | dontSell | dontBuy |
      | 1     | Copy of Daimler N.A.                                     | false    | false   |
      | 2     | Copy of Microsoft                                        | false    | false   |
      | 3     | Copy A.P. Møller - Mærsk A                               | false    | false   |
      | 4     | Copy of LU0048578792 Fidelity Europ Growth               | false    | false   |
      | 5     | Garantum Kreditobligation High Yield Europa 5 år nr 1408 | false    | false   |
    When I select the "Result" page
    And I select the "Portfolio structure" option in the Select Analysis dropdown
    And I select the "Complexity risk" in Show structure by dropdown
    Then The app should show the following values in the "Portfolio Structure" table
      | description | currentWeight | currentValue | recommendedWeight | recommendedValue |
      | Low         | 0.04          | 1,340.54     | 97.96             | 3,650,122.65     |
      | Medium      | 0.35          | 12,860.89    | 2.04              | 75,949.00        |
      | High        | 99.62         | 3,711,870.21 | 0.00              | 0.00             |

  @active @NAVIGEA-377 @NAVIGEA-378-v1 @NAVIGEA-563 @NAVIEA-769 @Advisory_module @Result_page @Portfolio_structure
  Scenario: @NAVIGEA-377_@NAVIGEA-378_@NAVIGEA-563-v1 --Select the security exposure option.
    Given I am logged in with organization "TetralogSystemsQA" and CrmKey "QaTestData07"
    And I navigate to the "Advisory" module
    And In the Basic advisory setup area, I select the following checkboxes:
      | name                                    | state |
      | Internal passive - Internal passive PTF | true  |
    And I select the "Setup" page
    And I click on the "Show Parameters" button
    When I enter the following values in "Internal passive PTF" table in Parameters page
      | order | name                                                     | dontSell | dontBuy |
      | 1     | Copy of Daimler N.A.                                     | false    | false   |
      | 2     | Copy of Microsoft                                        | false    | false   |
      | 3     | Copy A.P. Møller - Mærsk A                               | false    | false   |
      | 4     | Copy of LU0048578792 Fidelity Europ Growth               | false    | false   |
      | 5     | Garantum Kreditobligation High Yield Europa 5 år nr 1408 | false    | false   |
    When I select the "Result" page
    And I select the "Portfolio structure" option in the Select Analysis dropdown
    And I select the "Security exposure" in Show structure by dropdown
    Then The app should show the following values in the "Portfolio Structure" table
      | description                   | currentWeight | currentValue | recommendedWeight | recommendedValue |
      | Bonds International           | 0.01          | 222.90       | 63.06             | 2,349,737.51     |
      | Equities International        | 99.96         | 3,724,731.10 | 2.04              | 75,949.00        |
      | UCITS Funds - Sector oriented | 0.03          | 1,117.64     | 34.90             | 1,300,385.14     |

  @active @NAVIGEA-424 @NAVIGEA-769 @Advisory_module @Result_page @Portfolio_structure
  Scenario: @NAVIGEA-424-- Check the values in the dropdown.
    Given I am logged in with organization "TetralogSystemsQA" and CrmKey "SeleniumTestPortfolio_Default"
    And I navigate to the "Advisory" module
    And In the Basic advisory setup area, I select the following checkboxes:
      | name                                   | state |
      | Internal active - Internal portfolio 1 | true  |
    And I select the "Result" page
    When I select "Portfolio structure" in "Select Analysis" dropdown
    Then The app should show the following options in the "Analysis options" dropdown:
      | Security class    |
      | Security exposure |
      | Volatility risk   |
      | Complexity risk   |
      | Liquidity risk    |

  @active @NAVIGEA-516 @Advisory_module @Result-page @Security_exposure
  Scenario: NAVIGEA-516-- AV - Results page: New chart for Security Exposure
    Given I am logged in with organization "TetralogSystemsQA" and CrmKey "QaTestData01"
    When I navigate to the "Advisory" module
    And In the Basic advisory setup area, I select the following checkboxes:
      | name                         | state |
      | Internal active - Active PTF | true  |
    And I select the "Result" page
    And I select the "Portfolio structure" option in the Select Analysis dropdown
    And I select the "Security exposure" in Show structure by dropdown
    Then The app should show the following values in the "Portfolio Structure" table
      | description                    | currentWeight | currentValue | recommendedWeight | recommendedValue |
      | Bonds Norway                   | 0.00          | 0.00         | 8.22              | 33,279.22        |
      | UCITS Funds - Alternatives     | 48.68         | 197,178.33   | 13.38             | 54,186.14        |
      | UCITS Funds - Emerging Markets | 0.00          | 0.00         | 67.38             | 272,913.59       |
      | UCITS Funds - Fixed Income     | 51.32         | 207,863.52   | 11.03             | 44,662.91        |
