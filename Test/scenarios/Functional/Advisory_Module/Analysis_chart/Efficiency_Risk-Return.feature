Feature: NAVIGEA-367: AV - Results: RECOM. 3 Efficiency: Show Key Figures (Table below efficiency chart)
         NAVIGEA-368: AV - Results: RECOM. 4 Efficiency: Create Breakdown analysis area
         NAVIGEA-369: AV - Results: RECOM. 5 Efficiency: Include Benchmarks
         NAVIGEA-568: AV - Results: Sharpe Ratio is not calculated properly
         NAVIGEA-848: AV - Analysis should keep Benchmarks when Analysis type is changed

  @active @NAVIGEA-367-v1 @NAVIGEA-368-v1 @NAVIGEA-369-v1 @NAVIGEA-568-v1 @Advisory_module @Result_page @Efficiency_Risk-Return
  Scenario: Efficiency Risk and Return
    -Check that when I select the Result page the Efficiency chart is selected as default.
    -Check that no benchmarks is shown by default.
    -Check the Risk and Return values in a portfolio with 5 isins with different currencies.

    Given I am logged in with organization "TetralogSystemsQA" and CrmKey "QaTestData07"
    And I navigate to the "Advisory" module
    And In the Basic advisory setup area, I select the following checkboxes:
      | name                                    | state |
      | Internal passive - Internal passive PTF | true  |
    And I select the "Setup" page
    And I click on the "Show Parameters" button
    When I enter the following values in "Internal passive PTF" table in Parameters page
      | order | name                                                     | dontSell | dontBuy |
      | 1     | Copy of Daimler N.A.                                     | false    | false   |
      | 2     | Copy of Microsoft                                        | false    | false   |
      | 3     | Copy A.P. Møller - Mærsk A                               | false    | false   |
      | 4     | Copy of LU0048578792 Fidelity Europ Growth               | false    | false   |
      | 5     | Garantum Kreditobligation High Yield Europa 5 år nr 1408 | false    | false   |
    And I select the "Result" page
    Then The app should show the following values in the "Efficiency-Risk/Return" table
      | description            | risk   | performance | sharpeRatio |
      | Current portfolios     | 41.01  | 8.67        | 0.17        |
      | Recommended portfolios | 14.31  | 9.50        | 0.56        |
      | Result                 | -26.69 | 0.83        | 0.38        |

  @active @NAVIGEA-367-v2 @NAVIGEA-368-v2 @NAVIGEA-369-v2 @NAVIGEA-568-v2 @Advisory_module @Result_page @Efficiency_Risk-Return
  Scenario: Efficiency Risk and Return
    --Check the Risk and Return values in a portfolio with 5 isins with different currencies.
    -Check that the bechmarks' values are shown in the table.

    Given I am logged in with organization "TetralogSystemsQA" and CrmKey "QaTestData07"
    And I navigate to the "Advisory" module
    And In the Basic advisory setup area, I select the following checkboxes:
      | name                                    | state |
      | Internal passive - Internal passive PTF | true  |
    And I select the "Setup" page
    And I click on the "Show Parameters" button
    When I enter the following values in "Internal passive PTF" table in Parameters page
      | oder | name                                                     | dontSell | dontBuy |
      | 1    | Copy of Daimler N.A.                                     | false    | false   |
      | 2    | Copy of Microsoft                                        | false    | false   |
      | 3    | Copy A.P. Møller - Mærsk A                               | false    | false   |
      | 4    | Copy of LU0048578792 Fidelity Europ Growth               | false    | false   |
      | 5    | Garantum Kreditobligation High Yield Europa 5 år nr 1408 | false    | false   |
    And I select the "Result" page
    And I select the "Eficiency-Risk/Return" option in the Select Analysis dropdown
    And I select the "Obx equities" in Comparison with dropdown 1
    And I select the "St1x fixed income" in Comparison with dropdown 2
    Then The app should show the following values in the "Efficiency-Risk/Return" table
      | description            | risk   | performance | sharpeRatio |
      | Current portfolios     | 41.01  | 8.67        | 0.17        |
      | Recommended portfolios | 14.31  | 9.50        | 0.56        |
      | Result                 | -26.69 | 0.83        | 0.38        |
      |                        |        |             |             |
      | OBX Equities           | 23.08  | 5.93        | 0.19        |
      | ST1X Fixed income      | 5.21   | 2.83        | 0.25        |

  @review @NAVIGEA-367-v3 @NAVIGEA-368-v3 @NAVIGEA-369-v3 @NAVIGEA-568-v3 @Advisory_module @Result_page @Efficiency_Risk-Return
  Scenario: Efficiency Risk and Return
    -Check the Risk and Return in a Portfolio with 2 isins with EUR currency.
    -Check the bechmarks' values in the table.(Benchmarks in NOK currency)

    Given I am logged in with organization "TetralogSystemsAG" and CrmKey "QaTestData23"
    And I navigate to the "Advisory" module
    And In the Basic advisory setup area, I select the following checkboxes:
      | name                         | state |
      | Internal active - Active PTF | true  |
    And I select the "Result" page
    And I select the "Eficiency-Risk/Return" option in the Select Analysis dropdown
    And I select the "Obx equities" in Comparison with dropdown 1
    And I select the "St1x fixed income" in Comparison with dropdown 2
    Then The app should show the following values in the "Efficiency-Risk/Return" table
      | description        | risk  | performance | sharpeRatio |
      | Current portfolios | 41.32 | 6.21        | 0.14        |
      #  | Recommended portfolios | 36.81  | 5.62        |0.14        |
      | Result             | -4.51 | -0.60       | 0.00        |
      |                    |       |             |             |
    #  | OBX Equities       | 24.34 | 4.18        | 0.15        |
    #  | ST1X Fixed income  | 10.03 | 1.13        | 0.06        |

@review @NAVIGEA-367-v4 @NAVIGEA-368-v4 @NAVIGEA-369-v4 @NAVIGEA-568-v4 @Advisory_module @Result_page @Efficiency_Risk-Return
  Scenario: Efficiency Risk and Return
    -Check the Risk and Return in a Portfolio with 2 isins with EUR currency.
    -Check the bechmarks' values in the table.(Benchmarks in NOK currency)

    Given I am logged in with organization "TetralogSystemsQA" and CrmKey "QaTestData23"
    And I navigate to the "Advisory" module
    And In the Basic advisory setup area, I select the following checkboxes:
      | name                         | state |
      | Internal active - Active PTF | true  |
    And I select the "Result" page
    And I select the "Eficiency-Risk/Return" option in the Select Analysis dropdown
    And I select the "Obx equities" in Comparison with dropdown 1
    And I select the "St1x fixed income" in Comparison with dropdown 2
    Then The app should show the following values in the "Efficiency-Risk/Return" table
      | description        | risk  | performance | sharpeRatio |
      | Current portfolios | 41.32 | 6.21        | 0.14        |
      #  | Recommended portfolios | 36.81  | 5.62        |0.14        |
      | Result             | -4.51 | -0.60       | 0.00        |
      |                    |       |             |             |
    #  | OBX Equities       | 24.34 | 4.18        | 0.15        |
    #  | ST1X Fixed income  | 10.03 | 1.13        | 0.06        |

  @active @NAVIGEA-848 @Advisory_module @Result_page @Efficiency_Risk-Return
  Scenario: @NAVIGEA-848-- AV - Analysis should keep Benchmarks when Analysis type is changed.
    Given I am logged in with organization "TetralogSystemsQA" and CrmKey "QaTestData06"
    And I navigate to the "Advisory" module
    When I select the "Analysis" page
    And In the Basic advisory setup area,I select the "Optimize - rebalancing" use case in the dropdown
    And In the Basic advisory setup area, I select the following checkboxes:
      | name                           | state |
      | Internal active - Active PTF   | true  |
      | Internal passive - Passive PTF | true  |
      | External - External PTF        | true  |
    And I press the Apply/Proceed button
    And I select the "Eficiency-Risk/Return" option in the Select Analysis dropdown
    And I select the "Obx equities" in Comparison with dropdown 1
    And I select the "St1x fixed income" in Comparison with dropdown 2
    And I select the "Forecast" option in the Select Analysis dropdown
    Then I should see "Obx equities" in Comparison with dropdown 1
    And I should see "St1x fixed income" in Comparison with dropdown 2
