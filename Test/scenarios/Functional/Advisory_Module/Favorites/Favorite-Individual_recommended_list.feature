Feature: Favorites and Search functionality-- Individual recommendation list
  NAVIGEA-293: AV - Setup 2: Favorites and Search functionality
  NAVIGEA-823:BA/SA-UI: Favorite Lists: Usage of the search functionality to create and adapt
  NAVIGEA-834:BA/SA-UI: Favorite Lists: Editing
  NAVIGEA-521:Check the new favorites in both the result and transaction list tables
  NAVIGEA-921: AV-Setup module-Favorite list--Insurance Portfolio--The app has a problem when you want to use the Individual recommendation list for the recommendation
  
    
  BUG Tickets:
  NAVIGEA-681:AV - Setup: Advisroy module--Favorites page-- The isin's name is not shown in the Individual recommendation list
  NAVIGEA-592:AV - SETUP: Favorites percentage shows the in wrong place and format
  NAVIGEA-334:AV - General: Min Value NOK and Max Value NOK don't have the thousands separator
  NAVIGEA-696:AV - Results; Favorites are not taken into account
  NAVIGEA-572:AV - Results: Favorites in Results and Transaction tables
  NAVIGEA-917:AV-Setup: if individual list contains insuitabble products, a misleading berror messages is shown
  NAVIGEA-702:CLONE - Change in parameters dont effect result => RESP.: favorites are not moved into passive portfolioCLONE - Change in parameters dont effect result => RESP.: favorites are not moved into passive portfolio

  #*************************Individual recommendation List*********************************************
  @active @NAVIGEA-293 @NAVIGEA-293-v1 @NAVIGEA-823 @NAVIGEA-834 @NAVIGEA-681 @Advisory_module @Favorites_page
  Scenario: Navigea-293--v1: to create my own favorite list by using an additional search functionality
    -Create my own favorite list.
    -Check Individual favorite list values.
    -Remove instruments from list.

    Given I am logged in with organization "TetralogSystemsQA" and CrmKey "SeleniumTestPortfolio_Default"
    When I navigate to the "Advisory" module
    And In the Basic advisory setup area, I select the following checkboxes:
      | name                                   | state |
      | Internal active - Internal portfolio 1 | true  |
    And I select the "Setup" page
    And I click on the "Show Favorites" button
    And I select the following securities in Show Favorites page:
      | IsinOrName   |
      | LU0057025933 |
      | LU0132412106 |
      | LU0231459107 |
    Then The Individual recommendation list should have the following securities:
      """
      {
      rowData : [
      	{
      		'@class' : 'com.tetralog.webtest.table.row.impl.FavoritesIndividualRow',
      		nameAndIsin : {
      			isin : 'LU0057025933',
      			name : 'AB Global Growth Trends A USD Acc'
      		},
        include : {
        	checked : 'true'
        }
      	}, {
      		'@class' : 'com.tetralog.webtest.table.row.impl.FavoritesIndividualRow',
        nameAndIsin : {
        	isin : 'LU0132412106',
        	name : 'Aberdeen Global - Emerging Markets Equity Fund'
        },
        include : {
        	checked : 'true'
        }
      	}, {
        '@class' : 'com.tetralog.webtest.table.row.impl.FavoritesIndividualRow',
        nameAndIsin : {
        	isin : 'LU0231459107',
        	name : 'Aberdeen Global - Asian Smaller Companies Fund'
        },
        include : {
        	checked : 'true'
        }
      	}
      ]
      }
      """
    And I should be able to delete the following securities in the Individual recommendation list:
      | LU0057025933 |
      | LU0132412106 |
      | LU0231459107 |
    And The Individual recommendation list should be empty

  @dev @NAVIGEA-293 @NAVIGEA-293-v2 @NAVIGEA-823 @NAVIGEA-834 @NAVIGEA-592 @NAVIGEA-681 @NAVIGEA-334 @Advisory_module @Favorites_page
  Scenario: Navigea-293--v2: Individual recommendation lists.
    "Min% / Min Value" AND "Min% / Min Value".Either % or value can be set.
    - if % is set, the resulting value is displayed
    - if value is set, the resulting % is displayed
    - If modifications are made either at the favorites tables or at the parameters table, then "- modified" is additionally displayed after the name of the use case

    Given I am logged in with organization "TetralogSystemsQA" and CrmKey "QaTestData01"
    And I navigate to the "Advisory" module
    And In the Basic advisory setup area, I select the following checkboxes:
      | name                         | state |
      | Internal active - Active PTF | true  |
    And I select the "Setup" page
    And I click on the "Show Favorites" button
    And I select the following securities in Show Favorites page:
      | IsinOrName   |
      | LU0057025933 |
      | LU0132412106 |
      | LU0231459107 |
    When I enter the following values in "Individual recommendation list" table in Favorites page
      | order | isin         | name                                           | minSellPercentage | minSellValue | maxBuyPercentage | maxBuyValue |
      | 1     | LU0057025933 | AB Global Growth Trends A USD Acc              | 10                |              | 20               |             |
      | 3     | LU0132412106 | Aberdeen Global - Emerging Markets Equity Fund |                   | 50,000.00    |                  | 200,000.00  |
      | 5     | LU0231459107 | Aberdeen Global - Asian Smaller Companies Fund |                   | 250,000.00   | 100              |             |
    Then I should see the table "Individual recommendation list" in Favorites page as
      | order | isin         | name                                           | securityClass | securityExposure               | volatilityRiskClass | complexityRiskClass | liquidityRiskClass | datafeedStatus | minSellPercentage | minSellValue | maxBuyPercentage | maxBuyValue | include |
      | 1     | LU0057025933 | AB Global Growth Trends A USD Acc              | UCITS Funds   | UCITS Funds - International    | H                   | L                   | L                  | VALID          | 10                | 40,504.19    | 20               | 81,008.37   | true    |
      | 2     | LU0132412106 | Aberdeen Global - Emerging Markets Equity Fund | UCITS Funds   | UCITS Funds - Emerging Markets | H                   | L                   | L                  | VALID          | 12.34             | 50,000.00    | 49.38            | 200,000.00  | true    |
      | 3     | LU0231459107 | Aberdeen Global - Asian Smaller Companies Fund | UCITS Funds   | UCITS Funds - International    | H                   | L                   | L                  | VALID          | 100               | 250,000.00   | 100              | 405,041.85  | true    |
    And I should see "Use Case: Optimize - rebalancing - adapted to specific customer requirements" in the use case label in Setup page

  #**************************************Internal PASSIVE portfolio**************************************
  @dev @NAVIGEA-293 @NAVIGEA-521 @NAVIGEA-696 @NAVIGEA-572 @NAVIGEA-702 @Advisory_module @Favorites_page
  Scenario: Navigea-293--v2: Individual recommendation lists. Internal PASSIVE portfolio
    -Check the new favorites in both the result and transaction list tables
    -Create an indivisual list with instruments with Liquidity=M or H
    -The favorites should be shown in blue

    Given I am logged in with organization "TetralogSystemsQA" and CrmKey "QaTestData02"
    And I navigate to the "Advisory" module
    And In the Basic advisory setup area, I select the following checkboxes:
      | name                           | state |
      | Internal passive - Passive PTF | true  |
    And I select the "Setup" page
    And I click on the "Show Favorites" button
    And I select the following securities in Show Favorites page:
      | IsinOrName   |
      | NO0010382518 |
      | NO0010363971 |
      | QAFNOK122006 |
    And I click on the "Show Parameters" button
    And I enter the following values in "Passive PTF" table in Parameters page
      | order | name                                                     | dontSell | dontBuy |
      | 1     | Garantum Kreditobligation High Yield Europa 5 år nr 1408 | false    | false   |
      | 2     | Næringsbygg Holding 1 AS                                 | false    | false   |
    When I select the "Result" page
    Then The app should show the "Passive PTF" table in Results page as
      | order | name                                                     | securityExposure                   | volatilityRiskClass | complexityRiskClass | liquidityRiskClass | currentShare | currentWeight% | currentValueNOK | recommendedShare | recommendedWeight% | recommendedValueNOK | Colour |
      | 1     | Global Infrastruktur 1 AS                                | Alternatives - Infrastructure      | M                   | M                   | H                  | 0            | 0.00           | 0               | 171              | 7.91               | 19,152              | blue   |
      | 2     | Navigea Synthetic MMM                                    | UCITS Funds - Balanced/Mixed Funds | M                   | M                   | M                  | 0            | 0.00           | 0               | 1,111            | 57.01              | 138,104             | blue   |
      | 3     | Global Eiendom Vekst 2007 AS                             | Alternatives - Real Estate         | H                   | M                   | H                  | 0            | 0.00           | 0               | 322              | 14.62              | 35,420              | blue   |
      | 4     | Garantum Kreditobligation High Yield Europa 5 år nr 1408 | Bonds International                | H                   | L                   | M                  | 10,000       | 0.92           | 2,228           | 132,296          | 12.17              | 29,488              | noir   |
      | 5     | Næringsbygg Holding 1 AS                                 | Alternatives - Real Estate         | M                   | M                   | H                  | 10,000       | 99.08          | 240,000         | 836              | 8.28               | 20,064              | noir   |
    And I click on the "Show transactions" button in the Result page
    And I should see the following values in the Buy recommendation table for the internal portfolio "Passive PTF" in Transaction page.
      | order | name                                                     | securityExposure                   | volatilityRiskClass | complexityRiskClass | liquidityRiskClass | shareCurrent | shareRecommended | buySell(modify) | price  | currency | limitLower | limitUpper | valueBuySell | Colour |
      | 1     | Global Eiendom Vekst 2007 AS                             | Alternatives - Infrastructure      | H                   | M                   | H                  | 0            | 171              | 171.000         | 112.00 | NOK      |            |            | 19,152       | blue   |
      | 2     | Global Eiendom Utbetaling 2007 AS                        | UCITS Funds - Balanced/Mixed Funds | H                   | M                   | H                  | 0            | 1,111            | 1,111.909       | 124.20 | NOK      |            |            | 138,104      | blue   |
      | 3     | Garantum Kreditobligation High Yield Europa 5 år nr 1408 | Alternatives - Real Estate         | H                   | L                   | M                  | 0            | 322              | 322.000         | 110.00 | NOK      |            |            | 35,420       | blue   |
      | 4     | Garantum Kreditobligation High Yield Europa 5 år nr 1408 | Bonds International                | H                   | L                   | M                  | 10,000       | 132,296          | 122,296.000     | 0.26   | SEK      |            |            | 27,259       | noir   |
    And I should see the following values in the Sell recommendation table for the internal portfolio "Passive PTF" in Transaction page.
      | order | name                     | securityExposure           | volatilityRiskClass | complexityRiskClass | liquidityRiskClass | shareCurrent | shareRecommended | buySell(modify) | price | currency | limitLower | limitUpper | valueBuySell | Colour |
      | 1     | Næringsbygg Holding 1 AS | Alternatives - Real Estate | M                   | M                   | H                  | 10,000       | 836              | 9,164.000       | 24.00 | NOK      |            |            | 219,936      | noir   |

  @dev @NAVIGEA-293 @NAVIGEA-521 @NAVIGEA-917 @NAVIGEA-917-v1 @Advisory_module @Favorites_page
  Scenario: Individual recommendation lists. Internal PASSIVE portfolio
    -Create an individual list with instruments with Liquidity=L
    Liquidity Risk = L -> Not suitable for Passive portfolios. (except cash accounts).

    Given I am logged in with organization "TetralogSystemsQA" and CrmKey "QaTestData02"
    And I navigate to the "Advisory" module
    And In the Basic advisory setup area, I select the following checkboxes:
      | name                           | state |
      | Internal passive - Passive PTF | true  |
    And I select the "Setup" page
    And I click on the "Show Parameters" button
    And I enter the following values in "Passive PTF" table in Parameters page
      | order | name                                                     | dontSell | dontBuy |
      | 1     | Garantum Kreditobligation High Yield Europa 5 år nr 1408 | false    | false   |
      | 2     | Næringsbygg Holding 1 AS                                 | false    | false   |
    And I click on the "Show Favorites" button
    When I select the following securities in Show Favorites page:
      | IsinOrName   |
      | NO0010317282 |
      | NO0010297898 |
      | NO0010165145 |
    Then I should see the table "Individual recommendation list" in Favorites page as
      | order | isin         | name                                | securityClass | securityExposure            | volatilityRiskClass | complexityRiskClass | liquidityRiskClass | datafeedStatus | include |
      | 1     | NO0010317282 | Delphi Global                       | UCITS Funds   | UCITS Funds - International | M                   | L                   | L                  | VALID          | false   |
      | 2     | NO0010297898 | Pareto Verdi                        | UCITS Funds   | UCITS Funds - Fixed Income  | H                   | L                   | L                  | VALID          | false   |
      | 3     | NO0010165145 | Storebrand Aktiv Allokering direkte | UCITS Funds   | UCITS Funds - International | H                   | L                   | L                  | VALID          | false   |
    And I should see the "Individual recommendation list" list disabled
    And I select the "Result" page
    And The app should show the "Passive PTF " table in Results page as
      | order | name                                                     | securityExposure           | volatilityRiskClass | complexityRiskClass | liquidityRiskClass | currentShare | currentWeight% | currentValueNOK | recommendedShare | recommendedWeight% | recommendedValueNOK | Colour |
      | 1     | Garantum Kreditobligation High Yield Europa 5 år nr 1408 | Bonds International        | H                   | L                   | L                  | 10,000       | 0.92           | 2,228           | 690,427          | 63.53              | 153,894             | noir   |
      | 2     | Næringsbygg Holding 1 AS                                 | Alternatives - Real Estate | M                   | M                   | H                  | 10,000       | 99.08          | 240,000         | 3,681            | 36.47              | 88,344              | noir   |

  #**************************************Internal ACTIVE portfolio**************************************
  @dev @NAVIGEA-293 @NAVIGEA-521 @NAVIGEA-696 @NAVIGEA-572 @Advisory_module @Favorites_page
  Scenario: Navigea-293--v2: Individual recommendation lists. Internal ACTIVE portfolio
    -Check the new favorites in both the result and transaction list tables
    -Create an indivisual list with instruments with Liquidity=L
    -The favorites should be shown in blue

    Given I am logged in with organization "TetralogSystemsQA" and CrmKey "QaTestData01"
    And I navigate to the "Advisory" module
    And In the Basic advisory setup area, I select the following checkboxes:
      | name                         | state |
      | Internal active - Active PTF | true  |
    And I select the "Setup" page
    And I click on the "Show Favorites" button
    And I select the following securities in Show Favorites page:
      | IsinOrName   |
      | NO0010317282 |
      | NO0010297898 |
      | NO0010165145 |
    When I select the "Result" page
    Then The app should show the "Active PTF " table in Results page as
      | order | name                                   | securityExposure            | volatilityRiskClass | complexityRiskClass | liquidityRiskClass | currentShare | currentWeight% | currentValueNOK | recommendedShare | recommendedWeight% | recommendedValueNOK | Colour |
      | 1     | Delphi Global                          | UCITS Funds - International | M                   | L                   | L                  | 0            | 0.00           | 0               | 388              | 18.14              | 73,473              | blue   |
      | 2     | Pareto Verdi                           | UCITS Funds - Fixed Income  | H                   | L                   | L                  | 0            | 0.00           | 0               | 1,287            | 59.75              | 241,996             | blue   |
      | 3     | Dexia Invest Lange Danske Obligationer | UCITS Funds - Fixed Income  | L                   | L                   | L                  | 10,000       | 51.32          | 207,863         | 2,298            | 11.79              | 47,771              | noir   |
      | 4     | SEB Asset Selection SEK (990)          | UCITS Funds - Alternatives  | H                   | M                   | L                  | 10,000       | 48.68          | 197,178         | 2,119            | 10.32              | 41,800              | noir   |
    And I click on the "Show transactions" button in the Result page
    And I should see the following values in the Buy recommendation table for the internal portfolio "Active PTF" in Transaction page.
      | order | name          | securityExposure            | volatilityRiskClass | complexityRiskClass | liquidityRiskClass | shareCurrent | shareRecommended | buySell(modify) | price  | currency | limitLower | limitUpper | valueBuySell | Colour |
      | 1     | Delphi Global | UCITS Funds - International | M                   | L                   | L                  | 0            | 388              | 388.750         | 189.00 | NOK      |            |            | 73,473       | blue   |
      | 2     | Pareto Verdi  | UCITS Funds - Fixed Income  | H                   | L                   | L                  | 0            | 1,287            | 1,287.213       | 188.00 | NOK      |            |            | 241,996      | blue   |
    And I should see the following values in the Sell recommendation table for the internal portfolio "Active PTF" in Transaction page.
      | order | name                                   | securityExposure           | volatilityRiskClass | complexityRiskClass | liquidityRiskClass | shareCurrent | shareRecommended | buySell(modify) | price | currency | limitLower | limitUpper | valueBuySell | Colour |
      | 1     | Dexia Invest Lange Danske Obligationer | UCITS Funds - Fixed Income | L                   | L                   | L                  | 10,000       | 2,298            | 7,701.777       | 21.00 | DKK      |            |            | 160,091      | noir   |
      | 2     | SEB Asset Selection SEK (990)          | UCITS Funds - Alternatives | H                   | M                   | L                  | 10,000       | 2,119            | 7,880.079       | 23.00 | SEK      |            |            | 155,378      | noir   |

  @dev @NAVIGEA-293 @NAVIGEA-521 @NAVIGEA-917 @NAVIGEA-917-v2 @Advisory_module @Favorites_page
  Scenario: Individual recommendation lists. Internal ACTIVE portfolio
    -Create an indivisual list with instruments with Liquidity=M or H
    Liquidity Risk = M or H -> Not suitable for Active portfolios.

    Given I am logged in with organization "TetralogSystemsQA" and CrmKey "QaTestData01"
    And I navigate to the "Advisory" module
    And In the Basic advisory setup area, I select the following checkboxes:
      | name                         | state |
      | Internal active - Active PTF | true  |
    And I select the "Setup" page
    And I click on the "Show Favorites" button
    When I select the following securities in Show Favorites page:
      | IsinOrName   |
      | NO0010382518 |
      | NO0010363971 |
      | QAFNOK122006 |
    And I "deactivate" the "Fav-II" list
    And I "deactivate" the "Fav-IA-II-RiskFilter" list
    Then I should see the table "Individual recommendation list" in Favorites page as
      | order | isin         | name                         | securityClass           | securityExposure               | volatilityRiskClass | complexityRiskClass | liquidityRiskClass | datafeedStatus | include |
      | 1     | NO0010382518 | Global Infrastruktur 1 AS    | Alternative Investments | Alternatives - Infrastructure  | M                   | M                   | H                  | VALID          | false   |
      | 2     | NO0010363971 | Global Eiendom Vekst 2007 AS | Alternative Investments | Alternatives - Real Estate     | H                   | M                   | H                  | VALID          | false   |
      | 3     | QAFNOK122006 | Navigea Synthetic MMM        | UCITS Funds             | UCITS Funds - Balanced/Mixed F | M                   | M                   | M                  | VALID          | false   |
    And I should see the "Individual recommendation list" list disabled
    And I select the "Result" page
    And The app should show the "Active PTF" table in Results page as
      | order | name                                   | securityExposure           | volatilityRiskClass | complexityRiskClass | liquidityRiskClass | currentShare | currentWeight% | currentValueNOK | recommendedShare | recommendedWeight% | recommendedValueNOK | Colour |
      | 1     | Dexia Invest Lange Danske Obligationer | UCITS Funds - Fixed Income | L                   | L                   | L                  | 10,000       | 51.32          | 207,863         | 10,000           | 51.32              | 207,863             | noir   |
      | 2     | SEB Asset Selection SEK (990)          | UCITS Funds - Alternatives | H                   | M                   | L                  | 10,000       | 48.68          | 197,178         | 10,000           | 48.68              | 197,178             | noir   |

  #**************************************Internal INSURANCE portfolio**************************************
  @active @NAVIGEA-293 @NAVIGEA-521 @NAVIGEA-921 @NAVIGEA-696 @NAVIGEA-572 @Advisory_module @Favorites_page
  Scenario: Navigea-293--v2: Individual recommendation lists. Internal INSURANCE portfolio
    -Check the new favorites in both the result and transaction list tables
    -Create an indivisual list with instruments with Liquidity=L-M-H
    -The favorites should be shown in blue

    Given I am logged in with organization "TetralogSystemsQA" and CrmKey "QaTestData03"
    And I navigate to the "Advisory" module
    And In the Basic advisory setup area, I select the following checkboxes:
      | name                               | state |
      | Internal insurance - Insurance PTF | true  |
    And I select the "Setup" page
    And I select the following securities in Show Favorites page:
      | IsinOrName   |
      | NO0010382518 |
      | NO0010363971 |
      | LU0385327829 |
      | QAEDKK122003 |
      | QAEEUR122001 |
    And I "deactivate" the "Fav-II" list
    And I "deactivate" the "Fav-IA-II-RiskFilter" list
    And I click on the "Show Parameters" button
    And I enter the following values in "Insurance PTF" table in Parameters page
      | order | name                                                     | dontSell | dontBuy |
      | 1     | Jyske Invest Globale Aktier Udb                          | false    | false   |
      | 2     | Garantum Kreditobligation High Yield Europa 5 år nr 1408 | false    | false   |
      | 3     | Næringsbygg Holding 1 AS                                 | false    | false   |
    When I select the "Result" page
    Then The app should show the "Insurance PTF" table in Results page as
      | order | name                                                     | securityExposure              | volatilityRiskClass | complexityRiskClass | liquidityRiskClass | currentShare | currentWeight | currentValue | recommendedShare | recommendedWeight | recommendedValue | Colour |
      | 1     | Global Infrastruktur 1 AS                                | Alternatives - Infrastructure | M                   | M                   | H                  | 0            | 0.00          | 0            | 784              | 9.75              | 87,808           | blue   |
      | 2     | Copy A.P. Møller - Mærsk A                               | Equities International        | L                   | H                   | M                  | 0            | 0.00          | 0            | 1                | 4.71              | 42,389           | blue   |
      | 3     | SEB Asset Selection NOK                                  | UCITS Funds - Alternatives    | H                   | M                   | L                  | 0            | 0.00          | 0            | 90               | 15.09             | 135,905          | blue   |
      | 4     | Global Eiendom Vekst 2007 AS                             | Alternatives - Real Estate    | H                   | M                   | H                  | 0            | 0.00          | 0            | 2,144            | 26.18             | 235,840          | blue   |
      | 5     | Jyske Invest Globale Aktier Udb                          | UCITS Funds - International   | H                   | L                   | L                  | 10,000       | 21.98         | 197,965      | 10,000           | 21.97             | 197,965          | noir   |
      | 6     | Garantum Kreditobligation High Yield Europa 5 år nr 1408 | Bonds International           | H                   | L                   | M                  | 1,000,000    | 24.74         | 222,897      | 485,437          | 12.01             | 108,202          | noir   |
      | 7     | Næringsbygg Holding 1 AS                                 | Alternatives - Real Estate    | M                   | M                   | H                  | 20,000       | 53.28         | 480,000      | 3,865            | 10.30             | 92,760           | noir   |
    And I click on the "Show transactions" button in the Result page
    And I should see the following values in the Buy recommendation table for the internal portfolio "Insurance PTF" in Transaction page.
      | order | name                         | volatilityRiskClass | complexityRiskClass | liquidityRiskClass | buySell   | limitLower | limitUpper |
      | 1     | Global Infrastruktur 1 AS    | M                   | M                   | H                  | 784.000   | enabled    | enabled    |
      | 2     | Copy A.P. Møller - Mærsk A   | L                   | H                   | M                  | 1.000     | enabled    | enabled    |
      | 3     | SEB Asset Selection NOK      | H                   | M                   | L                  | 90.635    | disabled   | disabled   |
      | 4     | Global Eiendom Vekst 2007 AS | H                   | M                   | H                  | 2,144.000 | enabled    | enabled    |
    And I should see the following values in the Sell recommendation table for the internal portfolio "Insurance PTF" in Transaction page.
      | order | name                                                     | volatilityRiskClass | complexityRiskClass | liquidityRiskClass | buySell     | limitLower | limitUpper |
      | 1     | Garantum Kreditobligation High Yield Europa 5 år nr 1408 | H                   | L                   | M                  | 514,563.000 | enabled    | enabled    |
      | 2     | Næringsbygg Holding 1 AS                                 | M                   | M                   | H                  | 16,135.000  | enabled    | enabled    |

  #***************************************Internal PASSIVE and ACTIVE*************************
  @dev @NAVIGEA-293 @NAVIGEA-521 @NAVIGEA-696 @NAVIGEA-572 @NAVIGEA-917 @NAVIGEA-917-v3 @Advisory_module @Favorites_page
  Scenario: Individual recommendation lists. Internal ACTIVE and PASSIVE portfolio
    -Create an indivisual list with instruments with Liquidity=L,M y H 
    ACTIVE= favorites of the individual recommendation list (With Liquidity=L)
    PASSIVE= favorites of the individual recommendation list (With Liquidity=M y H)

    Given I am logged in with organization "TetralogSystemsQA" and CrmKey "QaTestData06"
    And I navigate to the "Advisory" module
    And In the Basic advisory setup area, I select the following checkboxes:
      | name                           | state |
      | Internal active - Active PTF   | true  |
      | Internal passive - Passive PTF | true  |
    And I select the "Setup" page
    When I select the following securities in Show Favorites page:
      | IsinOrName   |
      | LU0385327829 |
      | NO0010363971 |
      | NO0010349848 |
      | QAEDKK122003 |
      | QAEEUR122001 |
    Then I should see the table "Individual recommendation list" in Favorites page as
      | order | isin         | name                              | securityClass           | securityExposure           | volatilityRiskClass | complexityRiskClass | liquidityRiskClass | datafeedStatus | include |
      | 1     | LU0385327829 | SEB Asset Selection NOK           | UCITS Funds             | UCITS Funds - Alternatives | H                   | M                   | L                  | VALID          | true    |
      | 2     | NO0010363971 | Global Eiendom Vekst 2007 AS      | Alternative Investments | Alternatives - Real Estate | H                   | M                   | H                  | VALID          | true    |
      | 3     | NO0010349848 | Global Eiendom Utbetaling 2007 AS | Alternative Investments | Alternatives - Real Estate | H                   | M                   | H                  | VALID          | true    |
      | 4     | QAEDKK122003 | Copy A.P. Møller - Mærsk A        | Listed Shares           | Equities International     | L                   | H                   | M                  | VALID          | true    |
      | 5     | QAEEUR122001 | Copy of Daimler N.A.              | Listed Shares           | Equities International     | L                   | H                   | M                  | VALID          | true    |
    And I navigate to the "Advisory" module
    And In the Basic advisory setup area, I select the following checkboxes:
      | name                           | state |
      | Internal active - Active PTF   | true  |
      | Internal passive - Passive PTF | false |
    And I should see the table "Individual recommendation list" in Favorites page as
      | order | isin         | name                              | securityClass           | securityExposure           | volatilityRiskClass | complexityRiskClass | liquidityRiskClass | datafeedStatus | include |
      | 1     | LU0385327829 | SEB Asset Selection NOK           | UCITS Funds             | UCITS Funds - Alternatives | H                   | M                   | L                  | VALID          | true    |
      | 2     | NO0010363971 | Global Eiendom Vekst 2007 AS      | Alternative Investments | Alternatives - Real Estate | H                   | M                   | H                  | VALID          | false   |
      | 3     | NO0010349848 | Global Eiendom Utbetaling 2007 AS | Alternative Investments | Alternatives - Real Estate | H                   | M                   | H                  | VALID          | false   |
      | 4     | QAEDKK122003 | Copy A.P. Møller - Mærsk A        | Listed Shares           | Equities International     | L                   | H                   | M                  | VALID          | false   |
      | 5     | QAEEUR122001 | Copy of Daimler N.A.              | Listed Shares           | Equities International     | L                   | H                   | M                  | VALID          | false   |
    And I navigate to the "Advisory" module
    And In the Basic advisory setup area, I select the following checkboxes:
      | name                           | state |
      | Internal active - Active PTF   | false |
      | Internal passive - Passive PTF | true  |
    And I should see the table "Individual recommendation list" in Favorites page as
      | order | isin         | name                              | securityClass           | securityExposure           | volatilityRiskClass | complexityRiskClass | liquidityRiskClass | datafeedStatus | include |
      | 1     | LU0385327829 | SEB Asset Selection NOK           | UCITS Funds             | UCITS Funds - Alternatives | H                   | M                   | L                  | VALID          | false   |
      | 2     | NO0010363971 | Global Eiendom Vekst 2007 AS      | Alternative Investments | Alternatives - Real Estate | H                   | M                   | H                  | VALID          | true    |
      | 3     | NO0010349848 | Global Eiendom Utbetaling 2007 AS | Alternative Investments | Alternatives - Real Estate | H                   | M                   | H                  | VALID          | true    |
      | 4     | QAEDKK122003 | Copy A.P. Møller - Mærsk A        | Listed Shares           | Equities International     | L                   | H                   | M                  | VALID          | true    |
      | 5     | QAEEUR122001 | Copy of Daimler N.A.              | Listed Shares           | Equities International     | L                   | H                   | M                  | VALID          | true    |

  @active @NAVIGEA-293 @NAVIGEA-521 @NAVIGEA-696 @NAVIGEA-572 @NAVIGEA-921 @Advisory_module @Favorites_page
  Scenario: Individual recommendation lists. Internal ACTIVE and PASSIVE portfolio
    -Check the favorites in both the result and transaction list tables
    -Create an indivisual list with instruments with Liquidity=L,M y H 
    -Deactive other favorites lists (Only indicudual recommended list is active)
    -The favorites should be shown in blue
    ACTIVE= favorites of the individual recommendation list (With Liquidity=L)
    PASSIVE= favorites of the individual recommendation list (With Liquidity=M y H)

    Given I am logged in with organization "TetralogSystemsQA" and CrmKey "QaTestData06"
    And I navigate to the "Advisory" module
    And In the Basic advisory setup area, I select the following checkboxes:
      | name                           | state |
      | Internal active - Active PTF   | true  |
      | Internal passive - Passive PTF | true  |
    And I select the "Setup" page
    And I select the following securities in Show Favorites page:
      | IsinOrName   |
      | LU0385327829 |
      | NO0010363971 |
      | NO0010349848 |
      | QAEDKK122003 |
      | QAEEUR122001 |
    And I "deactivate" the "Fav-IA" list
    And I "deactivate" the "Fav-IA-II-RiskFilter" list
    When I select the "Result" page
    Then The app should show the "Active PTF" table in Results page as
      | order | name                                   | securityExposure           | volatilityRiskClass | complexityRiskClass | liquidityRiskClass | currentShare | currentWeight | currentValue | recommendedShare | recommendedWeight | recommendedValue | Colour |
      | 1     | SEB Asset Selection NOK                | UCITS Funds - Alternatives | H                   | M                   | L                  | 0            | 0.00          | 0            | 35               | 3.49              | 53,726           | blue   |
      | 2     | Dexia Invest Lange Danske Obligationer | UCITS Funds - Fixed Income | L                   | L                   | L                  | 10,000       | 13.52         | 207,863      | 0                | 0.00              | 0                | noir   |
      | 3     | SEB Asset Selection SEK (990)          | UCITS Funds - Alternatives | H                   | M                   | L                  | 10,000       | 12.82         | 197,178      | 0                | 0.00              | 0                | noir   |
    And The app should show the "Passive PTF" table in Results page as
      | order | name                                                     | securityExposure           | volatilityRiskClass | complexityRiskClass | liquidityRiskClass | currentShare | currentWeight | currentValue | recommendedShare | recommendedWeight | recommendedValue | Colour |
      | 1     | Global Eiendom Vekst 2007 AS                             | Alternatives - Real Estate | H                   | M                   | H                  | 0            | 0.00          | 0            | 293              | 2.10              | 32,230           | blue   |
      | 2     | Global Eiendom Utbetaling 2007 AS                        | Alternatives - Real Estate | H                   | M                   | H                  | 0            | 0.00          | 0            | 2,562            | 18.00             | 276,696          | blue   |
      | 3     | Copy A.P. Møller - Mærsk A                               | Equities International     | L                   | H                   | M                  | 0            | 0.00          | 0            | 1                | 2.76              | 42,389           | blue   |
      | 4     | Garantum Kreditobligation High Yield Europa 5 år nr 1408 | Bonds International        | H                   | L                   | M                  | 10,000       | 0.14          | 2,228        | 10,000           | 0.14              | 2,228            | noir   |
      | 5     | Næringsbygg Holding 1 AS                                 | Alternatives - Real Estate | M                   | M                   | H                  | 10,000       | 15.61         | 240,000      | 10,000           | 15.61             | 240,000          | noir   |
    And I click on the "Show transactions" button in the Result page
    And I should see the following values in the Buy recommendation table for the internal portfolio "Active PTF" in Transaction page.
      | order | name                    | volatilityRiskClass | complexityRiskClass | liquidityRiskClass | buySell | limitLower | limitUpper |
      | 1     | SEB Asset Selection NOK | H                   | M                   | L                  | 35.830  | disabled   | disabled   |
    And I should see the following values in the Sell recommendation table for the internal portfolio "Active PTF" in Transaction page.
      | order | name                                   | volatilityRiskClass | complexityRiskClass | liquidityRiskClass | buySell    | limitLower | limitUpper |
      | 1     | Dexia Invest Lange Danske Obligationer | L                   | L                   | L                  | 10,000.000 | disabled   | disabled   |
      | 2     | SEB Asset Selection SEK (990)          | H                   | M                   | L                  | 10,000.000 | disabled   | disabled   |
    And I should see the following values in the Buy recommendation table for the internal portfolio "Passive PTF" in Transaction page.
      | order | name                              | volatilityRiskClass | complexityRiskClass | liquidityRiskClass | buySell   | limitLower | limitUpper |
      | 1     | Global Eiendom Vekst 2007 AS      | H                   | M                   | H                  | 293.000   | enabled    | enabled    |
      | 2     | Global Eiendom Utbetaling 2007 AS | H                   | M                   | H                  | 2,562.000 | enabled    | enabled    |
      | 3     | Copy A.P. Møller - Mærsk A        | L                   | H                   | M                  | 1.000     | enabled    | enabled    |

  #***************************************Internal PASSIVE and EXTERNAL*************************
  @dev @NAVIGEA-293 @NAVIGEA-521 @Advisory_module @Favorites_page
  Scenario: Individual recommendation lists. Internal PASSIVE and EXTERNAL portfolio
    -Check that if you don't have individual recommendation list, you don't have favorites in result and transaction page.

    Given I am logged in with organization "TetralogSystemsQA" and CrmKey "QaTestData06"
    And I navigate to the "Advisory" module
    And In the Basic advisory setup area, I select the following checkboxes:
      | name                           | state |
      | Internal passive - Passive PTF | true  |
      | External - External PTF        | true  |
    And I select the "Setup" page
    And I click on the "Show Parameters" button
    And I enter the following values in "Passive PTF" table in Parameters page
      | order | name                                                     | dontSell | dontBuy |
      | 1     | Garantum Kreditobligation High Yield Europa 5 år nr 1408 | false    | false   |
      | 2     | Næringsbygg Holding 1 AS                                 | false    | false   |
    When I select the "Result" page
    Then The app should show the "Passive PTF" table in Results page as
      | order | name                                                     | securityExposure           | volatilityRiskClass | complexityRiskClass | liquidityRiskClass | currentShare | currentWeight% | currentValueNOK | recommendedShare | recommendedWeight% | recommendedValueNOK | Colour |
      | 1     | Garantum Kreditobligation High Yield Europa 5 år nr 1408 | Bonds International        | H                   | L                   | M                  | 10,000       | 0.14           | 2,228           | 2,019,282        | 29.27              | 450,092             | noir   |
      | 2     | Næringsbygg Holding 1 AS                                 | Alternatives - Real Estate | M                   | M                   | H                  | 10,000       | 15.61          | 240,000         | 0                | 0.00               | 0                   | noir   |
    And The app should show the "External PTF" table in Results page as
      | order | name                                                     | securityExposure           | volatilityRiskClass | complexityRiskClass | liquidityRiskClass | currentShare | currentWeight% | currentValueNOK | recommendedShare | recommendedWeight% | recommendedValueNOK | Colour |
      | 1     | Garantum Kreditobligation High Yield Europa 5 år nr 1408 | Bonds International        | H                   | L                   | M                  | 10,000       | 0.14           | 2,228           | 10,000           | 0.14               | 2,228               | noir   |
      | 2     | Dexia Invest Lange Danske Obligationer                   | UCITS Funds - Fixed Income | L                   | L                   | L                  | 10,000       | 13.52          | 207,863         | 0                | 0.00               | 0                   | noir   |
      | 3     | Global Eiendom Vekst 2007 AS                             | Alternatives - Real Estate | H                   | M                   | H                  | 10,000       | 15.61          | 240,000         | 10,000           | 15.61              | 240,000             | noir   |
    And I click on the "Show transactions" button in the Result page
    And I should see the following values in the Buy recommendation table for the internal portfolio "Passive PTF" in Transaction page.
      | order | name                                                     | securityExposure    | volatilityRiskClass | complexityRiskClass | liquidityRiskClass | shareCurrent | shareRecommended | buySell(modify) | price | currency | limitLower | limitUpper | valueBuySell | Colour |
      | 1     | Garantum Kreditobligation High Yield Europa 5 år nr 1408 | Bonds International | H                   | L                   | M                  | 10,000       | 2,019,282        | 2,009,282.000   | 0.26  | SEK      |            |            | 447,863      | noir   |
    And I should see the following values in the Sell recommendation table for the internal portfolio "Passive PTF" in Transaction page.
      | order | name                     | securityExposure           | volatilityRiskClass | complexityRiskClass | liquidityRiskClass | shareCurrent | shareRecommended | buySell(modify) | price | currency | limitLower | limitUpper | valueBuySell | Colour |
      | 1     | Næringsbygg Holding 1 AS | Alternatives - Real Estate | M                   | M                   | H                  | 10,000       | 0                | 10,000.000      | 24.00 | NOK      |            |            | 240,000      | noir   |
    And I should see the following values in the Sell recommendation table for the external portfolio "External PTF" in Transaction page.
      | order | name                                   | securityExposure           | volatilityRiskClass | complexityRiskClass | liquidityRiskClass | shareCurrent | shareRecommended | buySell(modify) | price | currency | limitLower | limitUpper | valueBuySell | Colour |
      | 1     | Dexia Invest Lange Danske Obligationer | UCITS Funds - Fixed Income | L                   | L                   | L                  | 10,000       | 0                | 9,999.995       | 21.00 | DKK      |            |            | 207,863      | noir   |

  @dev @NAVIGEA-293 @NAVIGEA-521 @NAVIGEA-696 @NAVIGEA-572 @Advisory_module @Favorites_page
  Scenario: Individual recommendation lists. Internal PASSIVE and EXTERNAL portfolio
    -Check the favorites in both the result and transaction list tables
    -Create an individual list with instruments with Liquidity=M y H 
    -The favorites should be shown in blue
    PASSIVE= favorites of the individual recommendation list

    Given I am logged in with organization "TetralogSystemsQA" and CrmKey "QaTestData06"
    And I navigate to the "Advisory" module
    And In the Basic advisory setup area, I select the following checkboxes:
      | name                           | state |
      | Internal passive - Passive PTF | true  |
      | External - External PTF        | true  |
    And I select the "Setup" page
    And I click on the "Show Favorites" button
    And I select the following securities in Show Favorites page:
      | IsinOrName   |
      | NO0010382518 |
      | NO0010363971 |
      | QAFNOK122006 |
    And I click on the "Show Parameters" button
    And I enter the following values in "Passive PTF" table in Parameters page
      | order | name                                                     | dontSell | dontBuy |
      | 1     | Garantum Kreditobligation High Yield Europa 5 år nr 1408 | false    | false   |
      | 2     | Næringsbygg Holding 1 AS                                 | false    | false   |
    When I select the "Result" page
    Then The app should show the "Passive PTF" table in Results page as
      | order | name                                                     | securityExposure                   | volatilityRiskClass | complexityRiskClass | liquidityRiskClass | currentShare | currentWeight% | currentValueNOK | recommendedShare | recommendedWeight% | recommendedValueNOK | Colour |
      | 1     | Global Infrastruktur 1 AS                                | Alternatives - Infrastructure      | M                   | M                   | H                  | 0            | 0.00           | 0               | 386              | 2.81               | 43,232              | blue   |
      | 2     | Navigea Synthetic MMM                                    | UCITS Funds - Balanced/Mixed Funds | M                   | M                   | M                  | 0            | 0.00           | 0               | 2,727            | 22.03              | 338,770             | blue   |
      | 3     | Global Eiendom Vekst 2007 AS                             | Alternatives - Real Estate         | H                   | M                   | H                  | 0            | 0.00           | 0               | 619              | 4.43               | 68,090              | blue   |
      | 4     | Garantum Kreditobligation High Yield Europa 5 år nr 1408 | Bonds International                | H                   | L                   | M                  | 10,000       | 0.14           | 2,228           | 0                | 0.00               | 0                   | noir   |
      | 5     | Næringsbygg Holding 1 AS                                 | Alternatives - Real Estate         | M                   | M                   | H                  | 10,000       | 15.61          | 240,000         | 0                | 0.00               | 0                   | noir   |
    And The app should show the "External PTF" table in Results page as
      | order | name                                                     | securityExposure           | volatilityRiskClass | complexityRiskClass | liquidityRiskClass | currentShare | currentWeight% | currentValueNOK | recommendedShare | recommendedWeight% | recommendedValueNOK | Colour |
      | 1     | Garantum Kreditobligation High Yield Europa 5 år nr 1408 | Bonds International        | H                   | L                   | M                  | 10,000       | 0.14           | 2,228           | 10,000           | 0.14               | 2,228               | noir   |
      | 2     | Dexia Invest Lange Danske Obligationer                   | UCITS Funds - Fixed Income | L                   | L                   | L                  | 10,000       | 13.52          | 207,863         | 0                | 0.00               | 0                   | noir   |
      | 3     | Global Eiendom Vekst 2007 AS                             | Alternatives - Real Estate | H                   | M                   | H                  | 10,000       | 15.61          | 240,000         | 10,000           | 15.61              | 240,000             | noir   |
    And I click on the "Show transactions" button in the Result page
    And I should see the following values in the Buy recommendation table for the internal portfolio "Passive PTF" in Transaction page.
      | order | name                         | securityExposure                   | volatilityRiskClass | complexityRiskClass | liquidityRiskClass | shareCurrent | shareRecommended | buySell(modify) | price  | currency | limitLower | limitUpper | valueBuySell | Colour |
      | 1     | Global Infrastruktur 1 AS    | Alternatives - Infrastructure      | M                   | M                   | H                  | 0            | 386              | 386.000         | 112.00 | NOK      |            |            | 43,232       | blue   |
      | 2     | Navigea Synthetic MMM        | UCITS Funds - Balanced/Mixed Funds | M                   | M                   | M                  | 0            | 2,727            | 2,727.513       | 124.20 | NOK      |            |            | 338,770      | blue   |
      | 3     | Global Eiendom Vekst 2007 AS | Alternatives - Real Estate         | H                   | M                   | H                  | 0            | 619              | 619.000         | 110.00 | NOK      |            |            | 68,090       | blue   |
    And I should see the following values in the Sell recommendation table for the internal portfolio "Passive PTF" in Transaction page.
      | order | name                                                     | securityExposure           | volatilityRiskClass | complexityRiskClass | liquidityRiskClass | shareCurrent | shareRecommended | buySell(modify) | price | currency | limitLower | limitUpper | valueBuySell | Colour |
      | 1     | Garantum Kreditobligation High Yield Europa 5 år nr 1408 | Bonds International        | H                   | L                   | M                  | 10,000       | 0                | 10,000.000      | 0.26  | SEK      |            |            | 2,228        | noir   |
      | 2     | Næringsbygg Holding 1 AS                                 | Alternatives - Real Estate | M                   | M                   | H                  | 10,000       | 0                | 10,000.000      | 24.00 | NOK      |            |            | 240,000      | noir   |
    And I should see the following values in the Sell recommendation table for the external portfolio "External PTF" in Transaction page.
      | order | name                                   | securityExposure           | volatilityRiskClass | complexityRiskClass | liquidityRiskClass | shareCurrent | shareRecommended | buySell(modify) | price | currency | limitLower | limitUpper | valueBuySell | Colour |
      | 1     | Dexia Invest Lange Danske Obligationer | UCITS Funds - Fixed Income | L                   | L                   | L                  | 10,000       | 0                | 10,000.000      | 21.00 | DKK      |            |            | 207,863      | noir   |
