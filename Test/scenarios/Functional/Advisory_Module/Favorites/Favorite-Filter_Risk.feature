Feature: Favorites-- Filtering according risk suitability
  NAVIGEA-407:BA SA: FAVORITE LIST: Filtering according risk suitability
   
  BUG Tickets:
  NAVIGEA-696:AV - Results; Favorites are not taken into account
  NAVIGEA-572:AV - Results: Favorites in Results and Transaction tables

  # *******************************Favorites Filter Risk*************************************************************
  @dev @NAVIGEA-407 @NAVIGEA-696 @NAVIGEA-572 @Advisory_module @Favorites_page
  Scenario: Customer Suitability: Volatility: Medium - Complexity: High - Liquidity: Low
    -Check the Risk Filter in Fav-IA-II-RiskFilter list
    -Check the favorites in both the result and transaction list tables
    -The favorites should be shown in blue

    Given I am logged in with organization "TetralogSystemsQA" and CrmKey "QaTestData12"
    And I navigate to the "Advisory" module
    And In the Basic advisory setup area, I select the following checkboxes:
      | name                         | state |
      | Internal active - Active PTF | true  |
    And I select the "Setup" page
    And I click on the "Show Favorites" button
    Then I should see the table "Fav-IA-II-RiskFilter" in Favorites page as
      | order | isin         | name                  | securityClass       | securityExposure               | volatilityRiskClass | complexityRiskClass | liquidityRiskClass | datafeedStatus | minSellPercentage | minSellValue | maxBuyPercentage | maxBuyValue | include |
      | 1     | QABNOK122005 | Navigea Synthetic LLL | Bonds               | UCITS Funds - International    | L                   | L                   | L                  | VALID          |                   |              |                  |             | true    |
      | 2     | QAFNOK122006 | Navigea Synthetic MMM | UCITS Funds         | UCITS Funds - Emerging Markets | M                   | M                   | M                  | VALID          |                   |              |                  |             | false   |
      | 3     | QAENOK122007 | Navigea Synthetic HHH | Listed Shares       | UCITS Funds - International    | H                   | H                   | H                  | VALID          |                   |              |                  |             | false   |
      | 4     | QAENOK122008 | Navigea Synthetic LMH | Listed Shares       | UCITS Funds - Emerging Markets | L                   | M                   | H                  | VALID          |                   |              |                  |             | false   |
      | 5     | QASNOK122009 | Navigea Synthetic MHL | Structured products | UCITS Funds - International    | M                   | H                   | L                  | VALID          |                   |              |                  |             | true    |
      | 6     | QABNOK122010 | Navigea Synthetic HLM | Bonds               | UCITS Funds - Emerging Markets | H                   | L                   | M                  | VALID          |                   |              |                  |             | false   |
    And The app should show the "Active PTF" table in Results page as
      | order | name                                   | securityExposure                  | volatilityRiskClass | complexityRiskClass | liquidityRiskClass | currentShare | currentWeight% | currentValueNOK | recommendedShare | recommendedWeight% | recommendedValueNOK | Colour |
      | 1     | Navigea Synthetic LLL                  | Bonds Norway                      | L                   | L                   | L                  | 0            | 0.00           | 0               | 454,309          | 14.31              | 220,031             | blue   |
      | 2     | Navigea Synthetic MHL                  | Structured Products - Unspecified | M                   | H                   | L                  | 0            | 0.00           | 0               | 32               | 8.13               | 124,978             | blue   |
      | 3     | Dexia Invest Lange Danske Obligationer | UCITS Funds - Fixed Income        | L                   | L                   | L                  | 10,000       | 13.52          | 207,863         | 0                | 0.00               | 0                   | noir   |
      | 4     | SEB Asset Selection SEK (990)          | UCITS Funds - Alternatives        | H                   | M                   | L                  | 10,000       | 12.82          | 197,178         | 3,044            | 3.90               | 60,031              | noir   |
    And I click on the "Show transactions" button in the Result page
    And I should see the following values in the Buy recommendation table for the internal portfolio "Active PTF" in Transaction page.
      | order | name                  | securityExposure                  | volatilityRiskClass | complexityRiskClass | liquidityRiskClass | shareCurrent | shareRecommended | buySell(modify) | price    | currency | limitLower | limitUpper | valueBuySell | Colour |
      | 1     | Navigea Synthetic LLL | Bonds Norway                      | L                   | L                   | L                  | 0            | 454,309          | 454,309.000     | 0.48     | NOK      |            |            | 220,031      | blue   |
      | 2     | Navigea Synthetic MHL | Structured Products - Unspecified | M                   | H                   | L                  | 0            | 32               | 32.000          | 3,905.59 | NOK      |            |            | 124,978      | blue   |
    And I should see the following values in the Sell recommendation table for the internal portfolio "Active PTF" in Transaction page.
      | order | name                                   | securityExposure           | volatilityRiskClass | complexityRiskClass | liquidityRiskClass | shareCurrent | shareRecommended | buySell(modify) | price | currency | limitLower | limitUpper | valueBuySell | Colour |
      | 1     | Dexia Invest Lange Danske Obligationer | UCITS Funds - Fixed Income | L                   | L                   | L                  | 10,000       | 0                | 10,000.000      | 21.00 | DKK      |            |            | 207,863      | noir   |
      | 2     | SEB Asset Selection SEK (990)          | UCITS Funds - Alternatives | H                   | M                   | L                  | 10,000       | 3,044            | 6,955.494       | 23.00 | SEK      |            |            | 137,147      | noir   |

  @dev @NAVIGEA-407 @NAVIGEA-696 @NAVIGEA-572 @Advisory_module @Favorites_page
  Scenario: Customer Suitability: Volatility: Medium - Complexity: High - Liquidity: Low
    -Check the Risk Filter in Fav-IA-II-RiskFilter list
    -Check the favorites in both the result and transaction list tables
    -The favorites should be shown in blue

    Given I am logged in with organization "TetralogSystemsQA" and CrmKey "QaTestData12"
    And I navigate to the "Advisory" module
    And In the Basic advisory setup area, I select the following checkboxes:
      | name                               | state |
      | Internal insurance - Insurance PTF | true  |
    And I select the "Setup" page
    And I click on the "Show Favorites" button
    Then I should see the table "Fav-IA-II-RiskFilter" in Favorites page as
      | order | isin         | name                  | securityClass       | securityExposure               | volatilityRiskClass | complexityRiskClass | liquidityRiskClass | datafeedStatus | minSellPercentage | minSellValue | maxBuyPercentage | maxBuyValue | include |
      | 1     | QABNOK122005 | Navigea Synthetic LLL | Bonds               | UCITS Funds - International    | L                   | L                   | L                  | VALID          |                   |              |                  |             | true    |
      | 2     | QAFNOK122006 | Navigea Synthetic MMM | UCITS Funds         | UCITS Funds - Emerging Markets | M                   | M                   | M                  | VALID          |                   |              |                  |             | false   |
      | 3     | QAENOK122007 | Navigea Synthetic HHH | Listed Shares       | UCITS Funds - International    | H                   | H                   | H                  | VALID          |                   |              |                  |             | false   |
      | 4     | QAENOK122008 | Navigea Synthetic LMH | Listed Shares       | UCITS Funds - Emerging Markets | L                   | M                   | H                  | VALID          |                   |              |                  |             | false   |
      | 5     | QASNOK122009 | Navigea Synthetic MHL | Structured products | UCITS Funds - International    | M                   | H                   | L                  | VALID          |                   |              |                  |             | true    |
      | 6     | QABNOK122010 | Navigea Synthetic HLM | Bonds               | UCITS Funds - Emerging Markets | H                   | L                   | M                  | VALID          |                   |              |                  |             | false   |
    And I  I should see the table "Fav-II" in Favorites page as
      | order | isin         | name                                                     | securityClass           | securityExposure                | volatilityRiskClass | complexityRiskClass | liquidityRiskClass | datafeedStatus | minSellPercentage | minSellValue | maxBuyPercentage | maxBuyValue | include |
      | 1     | IE00B6STVH45 | Heptagon Yacktman US Equity B                            | UCITS Funds             | UCITS Funds - Developed Markets | H                   | L                   | L                  | VALID          |                   |              |                  |             | false   |
      | 2     | LU0210536867 | JPM US Technology A (acc) USD                            | UCITS Funds             | UCITS Funds - Sector oriented   | H                   | L                   | L                  | VALID          |                   |              |                  |             | false   |
      | 3     | IE00B4M93X66 | Muzinich Short Duration High Yield                       | UCITS Funds             | UCITS Funds - Fixed Income      | L                   | L                   | L                  | VALID          |                   |              |                  |             | true    |
      | 4     | LU0385668222 | SEB Listed Private Equity (NOK)                          | UCITS Funds             | UCITS Funds - Sector oriented   | H                   | L                   | L                  | VALID          |                   |              |                  |             | false   |
      | 5     | LU0563142701 | Templeton Emerging Markets Bond Fund NOK                 | UCITS Funds             | UCITS Funds - Fixed Income      | L                   | L                   | L                  | VALID          |                   |              |                  |             | true    |
      | 6     | LU0476944185 | Templeton Global Bond Fund NOK                           | UCITS Funds             | UCITS Funds - Fixed Income      | L                   | L                   | L                  | VALID          |                   |              |                  |             | true    |
      | 7     | SE0004925469 | Garantum Kreditobligation High Yield Europa 5 år nr 1408 | Bonds                   | Bonds International             | H                   | L                   | M                  | VALID          |                   |              |                  |             | false   |
      | 6     | NO0010139363 | Næringsbygg Holding 1 ASM                                | Alternative Investments | Alternatives - Real Estate      | M                   | M                   | H                  | VALID          |                   |              |                  |             | false   |
    And The app should show the "Insurance PTF" table in Results page as
      | order | name                                                     | securityExposure            | volatilityRiskClass | complexityRiskClass | liquidityRiskClass | currentShare | currentWeight% | currentValueNOK | recommendedShare | recommendedWeight% | recommendedValueNOK | Colour |
      | 1     | Templeton Emerging Markets Bond Fund NOK                 | UCITS Funds - Fixed Income  | L                   | L                   | L                  | 0            | 0.00           | 0               | 628              | 6.99               | 107,506             | blue   |
      | 2     | Jyske Invest Globale Aktier Udb                          | UCITS Funds - International | H                   | L                   | L                  | 10,000       | 12.88          | 197,965         | 4,569            | 5.88               | 90,459              | noir   |
      | 3     | Garantum Kreditobligation High Yield Europa 5 år nr 1408 | Bonds International         | H                   | L                   | M                  | 10,000       | 0.14           | 2,228           | 10,000           | 0.14               | 2,228               | noir   |
      | 4     | Næringsbygg Holding 1 AS                                 | Alternatives - Real Estate  | M                   | M                   | H                  | 10,000       | 15.61          | 240,000         | 10,000           | 15.61              | 240,000             | noir   |
    And I click on the "Show transactions" button in the Result page
    And I should see the following values in the Buy recommendation table for the internal portfolio "Insurance PTF" in Transaction page.
      | order | name                                     | securityExposure           | volatilityRiskClass | complexityRiskClass | liquidityRiskClass | shareCurrent | shareRecommended | buySell(modify) | price  | currency | limitLower | limitUpper | valueBuySell | Colour |
      | 1     | Templeton Emerging Markets Bond Fund NOK | UCITS Funds - Fixed Income | L                   | L                   | L                  | 0            | 628              | 628.690         | 171.00 | NOK      |            |            | 107,506      | blue   |
    And I should see the following values in the Sell recommendation table for the internal portfolio "Insurance PTF" in Transaction page.
      | order | name                            | securityExposure            | volatilityRiskClass | complexityRiskClass | liquidityRiskClass | shareCurrent | shareRecommended | buySell(modify) | price | currency | limitLower | limitUpper | valueBuySell | Colour |
      | 1     | Jyske Invest Globale Aktier Udb | UCITS Funds - International | H                   | L                   | L                  | 10,000       | 4,569            | 5,430.550       | 20.00 | DKK      |            |            | 107,506      | noir   |

  @dev @NAVIGEA-407 @NAVIGEA-696 @NAVIGEA-572 @Advisory_module @Favorites_page
  Scenario: Customer Suitability: Volatility: Medium - Complexity: Medium - Liquidity: Medium
    -Check the Risk Filter in Fav-IA-II-RiskFilter list
    -Check the favorites in both the result and transaction list tables
    -The favorites should be shown in blue

    Given I am logged in with organization "TetralogSystemsQA" and CrmKey "QaTestData17"
    And I navigate to the "Advisory" module
    And In the Basic advisory setup area, I select the following checkboxes:
      | name                               | state |
      | Internal insurance - Insurance PTF | true  |
    And I select the "Moderate" option in investment strategy dropdown
    And I select the "Setup" page
    And I click on the "Show Favorites" button
    Then I should see the table "Fav-IA-II-RiskFilter" in Favorites page as
      | order | isin         | name                  | securityClass       | securityExposure               | volatilityRiskClass | complexityRiskClass | liquidityRiskClass | datafeedStatus | minSellPercentage | minSellValue | maxBuyPercentage | maxBuyValue | include |
      | 1     | QABNOK122005 | Navigea Synthetic LLL | Bonds               | UCITS Funds - International    | L                   | L                   | L                  | VALID          |                   |              |                  |             | true    |
      | 2     | QAFNOK122006 | Navigea Synthetic MMM | UCITS Funds         | UCITS Funds - Emerging Markets | M                   | M                   | M                  | VALID          |                   |              |                  |             | true    |
      | 3     | QAENOK122007 | Navigea Synthetic HHH | Listed Shares       | UCITS Funds - International    | H                   | H                   | H                  | VALID          |                   |              |                  |             | false   |
      | 4     | QAENOK122008 | Navigea Synthetic LMH | Listed Shares       | UCITS Funds - Emerging Markets | L                   | M                   | H                  | VALID          |                   |              |                  |             | false   |
      | 5     | QASNOK122009 | Navigea Synthetic MHL | Structured products | UCITS Funds - International    | M                   | H                   | L                  | VALID          |                   |              |                  |             | false   |
      | 6     | QABNOK122010 | Navigea Synthetic HLM | Bonds               | UCITS Funds - Emerging Markets | H                   | L                   | M                  | VALID          |                   |              |                  |             | false   |
    And I  I should see the table "Fav-II" in Favorites page as
      | order | isin         | name                                                     | securityClass           | securityExposure                | volatilityRiskClass | complexityRiskClass | liquidityRiskClass | datafeedStatus | minSellPercentage | minSellValue | maxBuyPercentage | maxBuyValue | include |
      | 1     | IE00B6STVH45 | Heptagon Yacktman US Equity B                            | UCITS Funds             | UCITS Funds - Developed Markets | H                   | L                   | L                  | VALID          |                   |              |                  |             | false   |
      | 2     | LU0210536867 | JPM US Technology A (acc) USD                            | UCITS Funds             | UCITS Funds - Sector oriented   | H                   | L                   | L                  | VALID          |                   |              |                  |             | false   |
      | 3     | IE00B4M93X66 | Muzinich Short Duration High Yield                       | UCITS Funds             | UCITS Funds - Fixed Income      | L                   | L                   | L                  | VALID          |                   |              |                  |             | true    |
      | 4     | LU0385668222 | SEB Listed Private Equity (NOK)                          | UCITS Funds             | UCITS Funds - Sector oriented   | H                   | L                   | L                  | VALID          |                   |              |                  |             | false   |
      | 5     | LU0563142701 | Templeton Emerging Markets Bond Fund NOK                 | UCITS Funds             | UCITS Funds - Fixed Income      | L                   | L                   | L                  | VALID          |                   |              |                  |             | true    |
      | 6     | LU0476944185 | Templeton Global Bond Fund NOK                           | UCITS Funds             | UCITS Funds - Fixed Income      | L                   | L                   | L                  | VALID          |                   |              |                  |             | true    |
      | 7     | SE0004925469 | Garantum Kreditobligation High Yield Europa 5 år nr 1408 | Bonds                   | Bonds International             | H                   | L                   | M                  | VALID          |                   |              |                  |             | false   |
      | 6     | NO0010139363 | Næringsbygg Holding 1 ASM                                | Alternative Investments | Alternatives - Real Estate      | M                   | M                   | H                  | VALID          |                   |              |                  |             | false   |
    And The app should show the "Insurance PTF" table in Results page as
      | order | name                                                     | securityExposure            | volatilityRiskClass | complexityRiskClass | liquidityRiskClass | currentShare | currentWeight% | currentValueNOK | recommendedShare | recommendedWeight% | recommendedValueNOK | Colour |
      | 1     | Templeton Emerging Markets Bond Fund NOK                 | UCITS Funds - Fixed Income  | L                   | L                   | L                  | 0            | 0.00           | 0               | 628              | 6.99               | 107,506             | blue   |
      | 2     | Jyske Invest Globale Aktier Udb                          | UCITS Funds - International | H                   | L                   | L                  | 10,000       | 12.88          | 197,965         | 4,569            | 5.88               | 90,459              | noir   |
      | 3     | Garantum Kreditobligation High Yield Europa 5 år nr 1408 | Bonds International         | H                   | L                   | M                  | 10,000       | 0.14           | 2,228           | 10,000           | 0.14               | 2,228               | noir   |
      | 4     | Næringsbygg Holding 1 AS                                 | Alternatives - Real Estate  | M                   | M                   | H                  | 10,000       | 15.61          | 240,000         | 10,000           | 15.61              | 240,000             | noir   |
    And I click on the "Show transactions" button in the Result page
    And I should see the following values in the Buy recommendation table for the internal portfolio "Insurance PTF" in Transaction page.
      | order | name                                     | securityExposure           | volatilityRiskClass | complexityRiskClass | liquidityRiskClass | shareCurrent | shareRecommended | buySell(modify) | price  | currency | limitLower | limitUpper | valueBuySell | Colour |
      | 1     | Templeton Emerging Markets Bond Fund NOK | UCITS Funds - Fixed Income | L                   | L                   | L                  | 0            | 628              | 628.690         | 171.00 | NOK      |            |            | 107,506      | blue   |
    And I should see the following values in the Sell recommendation table for the internal portfolio "Insurance PTF" in Transaction page.
      | order | name                            | securityExposure            | volatilityRiskClass | complexityRiskClass | liquidityRiskClass | shareCurrent | shareRecommended | buySell(modify) | price | currency | limitLower | limitUpper | valueBuySell | Colour |
      | 1     | Jyske Invest Globale Aktier Udb | UCITS Funds - International | H                   | L                   | L                  | 10,000       | 4,569            | 5,430.550       | 20.00 | DKK      |            |            | 107,506      | noir   |

  @dev @NAVIGEA-407 @NAVIGEA-696 @NAVIGEA-572 @Advisory_module @Favorites_page
  Scenario: Customer Suitability: Volatility: Low - Complexity: Medium - Liquidity: High
    -Check the Risk Filter in Fav-IA-II-RiskFilter list
    -Check the favorites in both the result and transaction list tables
    -The favorites should be shown in blue

    Given I am logged in with organization "TetralogSystemsQA" and CrmKey "QaTestData18"
    And I navigate to the "Advisory" module
    And In the Basic advisory setup area, I select the following checkboxes:
      | name                         | state |
      | Internal active - Active PTF | true  |
    And I select the "Setup" page
    And I click on the "Show Favorites" button
    Then I should see the table "Fav-IA-II-RiskFilter" in Favorites page as
      | order | isin         | name                  | securityClass       | securityExposure               | volatilityRiskClass | complexityRiskClass | liquidityRiskClass | datafeedStatus | minSellPercentage | minSellValue | maxBuyPercentage | maxBuyValue | include |
      | 1     | QABNOK122005 | Navigea Synthetic LLL | Bonds               | UCITS Funds - International    | L                   | L                   | L                  | VALID          |                   |              |                  |             | true    |
      | 2     | QAFNOK122006 | Navigea Synthetic MMM | UCITS Funds         | UCITS Funds - Emerging Markets | M                   | M                   | M                  | VALID          |                   |              |                  |             | false   |
      | 3     | QAENOK122007 | Navigea Synthetic HHH | Listed Shares       | UCITS Funds - International    | H                   | H                   | H                  | VALID          |                   |              |                  |             | false   |
      | 4     | QAENOK122008 | Navigea Synthetic LMH | Listed Shares       | UCITS Funds - Emerging Markets | L                   | M                   | H                  | VALID          |                   |              |                  |             | false    |
      | 5     | QASNOK122009 | Navigea Synthetic MHL | Structured products | UCITS Funds - International    | M                   | H                   | L                  | VALID          |                   |              |                  |             | false   |
      | 6     | QABNOK122010 | Navigea Synthetic HLM | Bonds               | UCITS Funds - Emerging Markets | H                   | L                   | M                  | VALID          |                   |              |                  |             | false   |
    And The app should show the "Active PTF" table in Results page as
      | order | name                                   | securityExposure                  | volatilityRiskClass | complexityRiskClass | liquidityRiskClass | currentShare | currentWeight% | currentValueNOK | recommendedShare | recommendedWeight% | recommendedValueNOK | Colour |
      | 1     | Navigea Synthetic LLL                  | Bonds Norway                      | L                   | L                   | L                  | 0            | 0.00           | 0               | 591,455          | 18.63              | 286,454             | blue   |
      | 2     | Navigea Synthetic MHL                  | Structured Products - Unspecified | M                   | H                   | L                  | 0            | 0.00           | 0               | 95               | 3.90               | 59,999              | blue   |
      | 3     | Dexia Invest Lange Danske Obligationer | UCITS Funds - Fixed Income        | L                   | L                   | L                  | 10,000       | 13.52          | 207,863         | 0                | 0.00               | 0                   | noir   |
      | 4     | SEB Asset Selection SEK (990)          | UCITS Funds - Alternatives        | H                   | M                   | L                  | 10,000       | 12.82          | 197,178         | 2,971            | 3.81               | 58,587              | noir   |
    And I click on the "Show transactions" button in the Result page
    And I should see the following values in the Buy recommendation table for the internal portfolio "Active PTF" in Transaction page.
      | order | name                  | securityExposure                  | volatilityRiskClass | complexityRiskClass | liquidityRiskClass | shareCurrent | shareRecommended | buySell(modify) | price  | currency | limitLower | limitUpper | valueBuySell | Colour |
      | 1     | Navigea Synthetic LLL | Bonds Norway                      | L                   | L                   | L                  | 0            | 591,455          | 591,455.000     | 0.48   | NOK      |            |            | 286,454      | blue   |
      | 2     | Navigea Synthetic MHL | Structured Products - Unspecified | M                   | H                   | L                  | 0            | 95               | 95.000          | 631.58 | NOK      |            |            | 59,999       | blue   |
    And I should see the following values in the Sell recommendation table for the internal portfolio "Active PTF" in Transaction page.
      | order | name                                   | securityExposure           | volatilityRiskClass | complexityRiskClass | liquidityRiskClass | shareCurrent | shareRecommended | buySell(modify) | price | currency | limitLower | limitUpper | valueBuySell | Colour |
      | 1     | Dexia Invest Lange Danske Obligationer | UCITS Funds - Fixed Income | L                   | L                   | L                  | 10,000       | 0                | 10,000.000      | 21.00 | DKK      |            |            | 207,863      | noir   |
      | 2     | SEB Asset Selection SEK (990)          | UCITS Funds - Alternatives | H                   | M                   | L                  | 10,000       | 2,971            | 7,028.713       | 23.00 | SEK      |            |            | 138,590      | noir   |

  @dev @NAVIGEA-407 @NAVIGEA-696 @NAVIGEA-572 @Advisory_module @Favorites_page
  Scenario: Customer Suitability: Volatility: High - Complexity: Low - Liquidity: Medium
    -Check the Risk Filter in Fav-IA-II-RiskFilter list
    -Check the favorites in both the result and transaction list tables
    -The favorites should be shown in blue

    Given I am logged in with organization "TetralogSystemsQA" and CrmKey "QaTestData19"
    And I navigate to the "Advisory" module
    And In the Basic advisory setup area, I select the following checkboxes:
      | name                               | state |
      | Internal insurance - Insurance PTF | true  |
    And I select the "Setup" page
    And I click on the "Show Favorites" button
    Then I should see the table "Fav-IA-II-RiskFilter" in Favorites page as
      | order | isin         | name                  | securityClass       | securityExposure               | volatilityRiskClass | complexityRiskClass | liquidityRiskClass | datafeedStatus | minSellPercentage | minSellValue | maxBuyPercentage | maxBuyValue | include |
      | 1     | QABNOK122005 | Navigea Synthetic LLL | Bonds               | UCITS Funds - International    | L                   | L                   | L                  | VALID          |                   |              |                  |             | true    |
      | 2     | QAFNOK122006 | Navigea Synthetic MMM | UCITS Funds         | UCITS Funds - Emerging Markets | M                   | M                   | M                  | VALID          |                   |              |                  |             | false   |
      | 3     | QAENOK122007 | Navigea Synthetic HHH | Listed Shares       | UCITS Funds - International    | H                   | H                   | H                  | VALID          |                   |              |                  |             | false   |
      | 4     | QAENOK122008 | Navigea Synthetic LMH | Listed Shares       | UCITS Funds - Emerging Markets | L                   | M                   | H                  | VALID          |                   |              |                  |             | false   |
      | 5     | QASNOK122009 | Navigea Synthetic MHL | Structured products | UCITS Funds - International    | M                   | H                   | L                  | VALID          |                   |              |                  |             | false   |
      | 6     | QABNOK122010 | Navigea Synthetic HLM | Bonds               | UCITS Funds - Emerging Markets | H                   | L                   | M                  | VALID          |                   |              |                  |             | true    |
    And I  I should see the table "Fav-II" in Favorites page as
      | order | isin         | name                                                     | securityClass           | securityExposure                | volatilityRiskClass | complexityRiskClass | liquidityRiskClass | datafeedStatus | minSellPercentage | minSellValue | maxBuyPercentage | maxBuyValue | include |
      | 1     | IE00B6STVH45 | Heptagon Yacktman US Equity B                            | UCITS Funds             | UCITS Funds - Developed Markets | H                   | L                   | L                  | VALID          |                   |              |                  |             | true    |
      | 2     | LU0210536867 | JPM US Technology A (acc) USD                            | UCITS Funds             | UCITS Funds - Sector oriented   | H                   | L                   | L                  | VALID          |                   |              |                  |             | true    |
      | 3     | IE00B4M93X66 | Muzinich Short Duration High Yield                       | UCITS Funds             | UCITS Funds - Fixed Income      | L                   | L                   | L                  | VALID          |                   |              |                  |             | true    |
      | 4     | LU0385668222 | SEB Listed Private Equity (NOK)                          | UCITS Funds             | UCITS Funds - Sector oriented   | H                   | L                   | L                  | VALID          |                   |              |                  |             | true    |
      | 5     | LU0563142701 | Templeton Emerging Markets Bond Fund NOK                 | UCITS Funds             | UCITS Funds - Fixed Income      | L                   | L                   | L                  | VALID          |                   |              |                  |             | true    |
      | 6     | LU0476944185 | Templeton Global Bond Fund NOK                           | UCITS Funds             | UCITS Funds - Fixed Income      | L                   | L                   | L                  | VALID          |                   |              |                  |             | true    |
      | 7     | SE0004925469 | Garantum Kreditobligation High Yield Europa 5 år nr 1408 | Bonds                   | Bonds International             | H                   | L                   | M                  | VALID          |                   |              |                  |             | true    |
      | 6     | NO0010139363 | Næringsbygg Holding 1 ASM                                | Alternative Investments | Alternatives - Real Estate      | M                   | M                   | H                  | VALID          |                   |              |                  |             | false   |
    And The app should show the "Insurance PTF" table in Results page as
      | order | name                                                     | securityExposure              | volatilityRiskClass | complexityRiskClass | liquidityRiskClass | currentShare | currentWeight% | currentValueNOK | recommendedShare | recommendedWeight% | recommendedValueNOK | Colour |
      | 1     | Templeton Emerging Markets Bond Fund NOK                 | UCITS Funds - Fixed Income    | L                   | L                   | L                  | 0            | 0.00           | 0               | 618              | 6.88               | 105,711             | blue   |
      | 2     | SEB Listed Private Equity (NOK)                          | UCITS Funds - Sector oriented | H                   | L                   | L                  | 0            | 0.00           | 0               | 74               | 6.00               | 92,253              | blue   |
      | 3     | Jyske Invest Globale Aktier Udb                          | UCITS Funds - International   | H                   | L                   | L                  | 10,000       | 12.88          | 197,965         | 0                | 0.00               | 0                   | noir   |
      | 4     | Garantum Kreditobligation High Yield Europa 5 år nr 1408 | Bonds International           | H                   | L                   | M                  | 10,000       | 0.14           | 2,228           | 10,000           | 0.14               | 2,228               | noir   |
      | 5     | Næringsbygg Holding 1 AS                                 | Alternatives - Real Estate    | M                   | M                   | H                  | 10,000       | 15.61          | 240,000         | 10,000           | 15.61              | 240,000             | noir   |
    And I click on the "Show transactions" button in the Result page
    And I should see the following values in the Buy recommendation table for the internal portfolio "Insurance PTF" in Transaction page.
      | order | name                                     | securityExposure              | volatilityRiskClass | complexityRiskClass | liquidityRiskClass | shareCurrent | shareRecommended | buySell(modify) | price  | currency | limitLower | limitUpper | valueBuySell | Colour |
      | 1     | Templeton Emerging Markets Bond Fund NOK | UCITS Funds - Fixed Income    | L                   | L                   | L                  | 0            | 618              | 618.198         | 171.00 | NOK      |            |            | 105,711      | blue   |
      | 2     | SEB Listed Private Equity (NOK)          | UCITS Funds - Sector oriented | H                   | L                   | L                  | 0            | 74               | 74.807          | 167.00 | EUR      |            |            | 92,252       | blue   |
    And I should see the following values in the Sell recommendation table for the internal portfolio "Insurance PTF" in Transaction page.
      | order | name                            | securityExposure            | volatilityRiskClass | complexityRiskClass | liquidityRiskClass | shareCurrent | shareRecommended | buySell(modify) | price | currency | limitLower | limitUpper | valueBuySell | Colour |
      | 1     | Jyske Invest Globale Aktier Udb | UCITS Funds - International | H                   | L                   | L                  | 10,000       | 0                | 10,000.000      | 20.00 | DKK      |            |            | 197,965      | noir   |

  @dev @NAVIGEA-407 @NAVIGEA-696 @NAVIGEA-572 @Advisory_module @Favorites_page
  Scenario: Customer Suitability: Volatility: High - Complexity: Low - Liquidity: Medium
    -Check the Risk Filter in Fav-IA-II-RiskFilter list
    -Check the favorites in both the result and transaction list tables
    -The favorites should be shown in blue

    Given I am logged in with organization "TetralogSystemsQA" and CrmKey "QaTestData19"
    And I navigate to the "Advisory" module
    And In the Basic advisory setup area, I select the following checkboxes:
      | name                         | state |
      | Internal active - Active PTF | true  |
    And I select the "Setup" page
    And I click on the "Show Favorites" button
    Then I should see the table "Fav-IA-II-RiskFilter" in Favorites page as
      | order | isin         | name                  | securityClass       | securityExposure               | volatilityRiskClass | complexityRiskClass | liquidityRiskClass | datafeedStatus | minSellPercentage | minSellValue | maxBuyPercentage | maxBuyValue | include |
      | 1     | QABNOK122005 | Navigea Synthetic LLL | Bonds               | UCITS Funds - International    | L                   | L                   | L                  | VALID          |                   |              |                  |             | true    |
      | 2     | QAFNOK122006 | Navigea Synthetic MMM | UCITS Funds         | UCITS Funds - Emerging Markets | M                   | M                   | M                  | VALID          |                   |              |                  |             | false   |
      | 3     | QAENOK122007 | Navigea Synthetic HHH | Listed Shares       | UCITS Funds - International    | H                   | H                   | H                  | VALID          |                   |              |                  |             | false   |
      | 4     | QAENOK122008 | Navigea Synthetic LMH | Listed Shares       | UCITS Funds - Emerging Markets | L                   | M                   | H                  | VALID          |                   |              |                  |             | false   |
      | 5     | QASNOK122009 | Navigea Synthetic MHL | Structured products | UCITS Funds - International    | M                   | H                   | L                  | VALID          |                   |              |                  |             | false   |
      | 6     | QABNOK122010 | Navigea Synthetic HLM | Bonds               | UCITS Funds - Emerging Markets | H                   | L                   | M                  | VALID          |                   |              |                  |             | false    |
    And I  I should see the table "Fav-IA" in Favorites page as
      | order | isin         | name                                           | securityClass | securityExposure               | volatilityRiskClass | complexityRiskClass | liquidityRiskClass | datafeedStatus | minSellPercentage | minSellValue | maxBuyPercentage | maxBuyValue | include |
      | 1     | LU0231459107 | Aberdeen Global - Asian Smaller Companies Fund | UCITS Funds   | UCITS Funds - International    | H                   | L                   | L                  | VALID          |                   |              |                  |             | true    |
      | 2     | LU0132412106 | Aberdeen Global - Emerging Markets Equity Fund | UCITS Funds   | UCITS Funds - Emerging Markets | H                   | L                   | L                  | VALID          |                   |              |                  |             | true    |
      | 3     | LU0231490524 | Aberdeen Global India Equity Fund              | UCITS Funds   | UCITS Funds - Emerging Markets | H                   | L                   | L                  | VALID          |                   |              |                  |             | true    |
      | 4     | LU0728928796 | Aberdeen Global - World Smaller Companies Fund | UCITS Funds   | UCITS Funds - International    | H                   | L                   | L                  | VALID          |                   |              |                  |             | true    |
      | 5     | LU0304976862 | BankInvest New Emerging Markets Equities       | UCITS Funds   | UCITS Funds - Emerging Markets | H                   | L                   | L                  | VALID          |                   |              |                  |             | true    |
      | 6     | SE0000888208 | East Capital Östeuropa Inc                     | UCITS Funds   | UCITS Funds - Emerging Markets | H                   | L                   | L                  | VALID          |                   |              |                  |             | true    |
      | 7     | SE0000777708 | East Capital Rysslandfonden                    | UCITS Funds   | UCITS Funds - Emerging Markets | H                   | L                   | L                  | VALID          |                   |              |                  |             | true    |
      | 8     | SE0001621327 | East Capital Turkiet                           | UCITS Funds   | UCITS Funds - Emerging Markets | H                   | L                   | L                  | VALID          |                   |              |                  |             | true    |
      | 9     | IE00B1W3WR42 | GAM Star China Equity USD Acc                  | UCITS Funds   | UCITS Funds - Emerging Markets | H                   | L                   | L                  | VALID          |                   |              |                  |             | true    |
    And The app should show the "Active PTF" table in Results page as
      | order | name                                   | securityExposure               | volatilityRiskClass | complexityRiskClass | liquidityRiskClass | currentShare | currentWeight% | currentValueNOK | recommendedShare | recommendedWeight% | recommendedValueNOK | Colour |
      | 1     | Aberdeen Global India Equity Fund      | UCITS Funds - Emerging Markets | H                   | L                   | L                  | 0            | 0.00           | 0               | 277              | 2.94               | 45,177              | blue   |
      | 2     | GAM Star China Equity USD Acc          | UCITS Funds - Emerging Markets | H                   | L                   | L                  | 0            | 0.00           | 0               | 2,570            | 23.40              | 359,864             | blue   |
      | 3     | Dexia Invest Lange Danske Obligationer | UCITS Funds - Fixed Income     | L                   | L                   | L                  | 10,000       | 13.52          | 207,863         | 0                | 0.00               | 0                   | noir   |
      | 4     | SEB Asset Selection SEK (990)          | UCITS Funds - Alternatives     | H                   | M                   | L                  | 10,000       | 12.82          | 197,178         | 0                | 0.00               | 0                   | noir   |
    And I click on the "Show transactions" button in the Result page
    And I should see the following values in the Buy recommendation table for the internal portfolio "Active PTF" in Transaction page.
      | order | name                              | securityExposure               | volatilityRiskClass | complexityRiskClass | liquidityRiskClass | shareCurrent | shareRecommended | buySell(modify) | price  | currency | limitLower | limitUpper | valueBuySell | Colour |
      | 1     | Aberdeen Global India Equity Fund | UCITS Funds - Emerging Markets | H                   | L                   | L                  | 0            | 277              | 277.160         | 163.00 | NOK      |            |            | 45,177       | blue   |
      | 2     | GAM Star China Equity USD Acc     | UCITS Funds - Emerging Markets | H                   | L                   | L                  | 0            | 2,570            | 2,570.463       | 140.00 | NOK      |            |            | 359,864      | blue   |
    And I should see the following values in the Sell recommendation table for the internal portfolio "Active PTF" in Transaction page.
      | order | name                                   | securityExposure           | volatilityRiskClass | complexityRiskClass | liquidityRiskClass | shareCurrent | shareRecommended | buySell(modify) | price | currency | limitLower | limitUpper | valueBuySell | Colour |
      | 1     | Dexia Invest Lange Danske Obligationer | UCITS Funds - Fixed Income | L                   | L                   | L                  | 10,000       | 0                | 10,000.000      | 21.00 | DKK      |            |            | 207,863      | noir   |
      | 1     | SEB Asset Selection SEK (990)          | UCITS Funds - Alternatives | H                   | M                   | L                  | 10,000       | 0                | 10,000.000      | 23.00 | DKK      |            |            | 197,178      | noir   |
