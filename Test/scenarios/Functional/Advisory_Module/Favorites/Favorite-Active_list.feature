Feature: Favorites and Search functionality--Active list
  NAVIGEA-293: AV - Setup 2: Favorites and Search functionality
  NAVIGEA-834:BA/SA-UI: Favorite Lists: Editing NAVIGEA-823:BA/SA-UI: Favorite Lists: Usage of the search functionality to create and adapt
  NAVIGEA-521:Check the new favorites in both the result and transaction list tables
 
  BUG Tickets:
  NAVIGEA-592:AV - SETUP: Favorites percentage shows the in wrong place and format
  NAVIGEA-334:AV - General: Min Value NOK and Max Value NOK don't have the thousands separator
  NAVIGEA-696:AV - Results; Favorites are not taken into account
  NAVIGEA-572:AV - Results: Favorites in Results and Transaction tables
  NAVIGEA-702:CLONE - Change in parameters dont effect result => RESP.: favorites are not moved into passive portfolioCLONE - Change in parameters dont effect result => RESP.: favorites are not moved into passive portfolio
  NAVIGEA-592:AV - SETUP: Favorites percentage shows the in wrong place and format 
  
   #************************************Favorites Active List*****************************
  @dev @NAVIGEA-293 @NAVIGEA-834 @NAVIGEA-592 @NAVIGEA-334 @Advisory_module @Favorites_page
  Scenario: Navigea-293--v2: Predefined favorite lists.
    "Min% / Min Value" AND "Min% / Min Value".Either % or value can be set.
    - if % is set, the resulting value is displayed
    - If modifications are made either at the favorites tables or at the parameters table, then "- modified" is additionally displayed after the name of the use case

    Given I am logged in with organization "TetralogSystemsQA" and CrmKey "QaTestData06"
    And I navigate to the "Advisory" module
    And In the Basic advisory setup area, I select the following checkboxes:
      | name                         | state |
      | Internal active - Active PTF | true  |
    And I select the "Setup" page
    And I click on the "Show Favorites" button
    When I enter the following values in "Fav-IA" table in Favorites page
      | oder | isin         | name                                           | minSellPercentage | minSellValue | maxBuyPercentage | maxBuyValue |
      | 1    | LU0231459107 | Aberdeen Global - Asian Smaller Companies Fund | 10                |              | 20               |             |
      | 3    | LU0231490524 | Aberdeen Global India Equity Fund              | 20                |              | 40               |             |
      | 5    | LU0304976862 | BankInvest New Emerging Markets Equities       | 100               |              | 100              |             |
    Then I should see the table "Fav-IA" in Favorites page as
      | order | isin         | name                                           | securityClass | securityExposure               | volatilityRiskClass | complexityRiskClass | liquidityRiskClass | datafeedStatus | minSellPercentage | minSellValue | maxBuyPercentage | maxBuyValue | include |
      | 1     | LU0231459107 | Aberdeen Global - Asian Smaller Companies Fund | UCITS Funds   | UCITS Funds - International    | H                   | L                   | L                  | VALID          | 10                | 40,504.19    | 20               | 81,008.37   | true    |
      | 3     | LU0231490524 | Aberdeen Global India Equity Fund              | UCITS Funds   | UCITS Funds - Emerging Markets | H                   | L                   | L                  | VALID          | 20                | 81,008.37    | 40               | 162,016.74  | true    |
      | 5     | LU0304976862 | BankInvest New Emerging Markets Equities       | UCITS Funds   | UCITS Funds - Emerging Markets | H                   | L                   | L                  | VALID          | 100               | 405,041.85   | 100              | 405,041.85  | true    |
    And I should see "Use Case: Optimize - rebalancing - adapted to specific customer requirements" in the use case label in Setup page

  @dev @NAVIGEA-293 @NAVIGEA-834 @NAVIGEA-592 @NAVIGEA-334 @Advisory_module @Favorites_page
  Scenario: Navigea-293--v3: Predefined favorite lists.
    "Min% / Min Value" AND "Min% / Min Value".Either % or value can be set.
     -if value is set, the resulting % is displayed
    - If modifications are made either at the favorites tables or at the parameters table, then "- modified" is additionally displayed after the name of the use case

    Given I am logged in with organization "TetralogSystemsQA" and CrmKey "QaTestData01"
    And I navigate to the "Advisory" module
    And In the Basic advisory setup area, I select the following checkboxes:
      | name                         | state |
      | Internal active - Active PTF | true  |
    And I select the "Setup" page
    And I click on the "Show Favorites" button
    When I enter the following values in "Fav-IA" table in Favorites page
      | oder | isin         | name                                           | minSellPercentage | minSellValue | maxBuyPercentage | maxBuyValue |
      | 1    | LU0231459107 | Aberdeen Global - Asian Smaller Companies Fund |                   | 100.00       |                  | 20,000.00   |
      | 3    | LU0231490524 | Aberdeen Global India Equity Fund              |                   | 50,000.00    |                  | 400,000.00  |
      | 5    | LU0304976862 | BankInvest New Emerging Markets Equities       |                   | 405,041.85   |                  | 405,041.85  |
    Then I should see the table "Fav-IA" in Favorites page as
      | order | isin         | name                                           | securityClass | securityExposure               | volatilityRiskClass | complexityRiskClass | liquidityRiskClass | datafeedStatus | minSellPercentage | minSellValue | maxBuyPercentage | maxBuyValue | include |
      | 1     | LU0231459107 | Aberdeen Global - Asian Smaller Companies Fund | UCITS Funds   | UCITS Funds - International    | H                   | L                   | L                  | VALID          | 0.02              | 100.00       | 4.94             | 20,000.00   | true    |
      | 3     | LU0231490524 | Aberdeen Global India Equity Fund              | UCITS Funds   | UCITS Funds - Emerging Markets | H                   | L                   | L                  | VALID          | 12.34             | 50,000.00    | 98.76            | 400,000.00  | true    |
      | 5     | LU0304976862 | BankInvest New Emerging Markets Equities       | UCITS Funds   | UCITS Funds - Emerging Markets | H                   | L                   | L                  | VALID          | 100.00            | 405,041.85   | 100.00           | 405,041.85  | true    |
    And I should see "Use Case: Optimize - rebalancing - adapted to specific customer requirements" in the use case label in Setup page

  @dev @NAVIGEA-293 @NAVIGEA-834 @NAVIGEA-592 @NAVIGEA-334 @Advisory_module @Favorites_page
  Scenario: Navigea-293--v4:Predefined favorite lists.
    "Min% / Min Value" AND "Min% / Min Value".Either % or value can be set.
    - To clear a % or value that has to been set, the field has to be cleared manually
    - If modifications are made either at the favorites tables or at the parameters table, then "- modified" is additionally displayed after the name of the use case

    Given I am logged in with organization "TetralogSystemsQA" and CrmKey "QaTestData01"
    And I navigate to the "Advisory" module
    And In the Basic advisory setup area, I select the following checkboxes:
      | name                         | state |
      | Internal active - Active PTF | true  |
    And I select the "Setup" page
    And I click on the "Show Favorite" button
    When I enter the following values in "Fav-IA" table in Favorites page
      | oder | isin         | name                                           | minSellPercentage | minSellValue | maxBuyPercentage | maxBuyValue |
      | 1    | LU0231459107 | Aberdeen Global - Asian Smaller Companies Fund |                   | 100.00       |                  | 20,000.00   |
      | 3    | LU0231490524 | Aberdeen Global India Equity Fund              | 50                |              | 80               |             |
    And I enter the following values in "Fav-IA" table in Favorites page
      | oder | isin         | name                                           | minSellPercentage | minSellValue | maxBuyPercentage | maxBuyValue |
      | 1    | LU0231459107 | Aberdeen Global - Asian Smaller Companies Fund |                   |              |                  |             |
      | 3    | LU0231490524 | Aberdeen Global India Equity Fund              |                   |              |                  |             |
    Then I should see the table "Fav-IA" in Favorites page as
      | order | isin         | name                                           | securityClass | securityExposure               | volatilityRiskClass | complexityRiskClass | liquidityRiskClass | datafeedStatus | minSellPercentage | minSellValue | maxBuyPercentage | maxBuyValue | include |
      | 1     | LU0231459107 | Aberdeen Global - Asian Smaller Companies Fund | UCITS Funds   | UCITS Funds - International    | H                   | L                   | L                  | VALID          |                   |              |                  |             | true    |
      | 3     | LU0231490524 | Aberdeen Global India Equity Fund              | UCITS Funds   | UCITS Funds - Emerging Markets | H                   | L                   | L                  | VALID          |                   |              |                  |             | true    |
    And I should see "Use Case: Optimize - rebalancing - adapted to specific customer requirements" in the use case label in Setup page
      
      
 #***************************************Internal INSURANCE*************************
@dev @NAVIGEA-293 @NAVIGEA-521 @NAVIGEA-696 @NAVIGEA-572 @NAVIEA-702 @Advisory_module @Favorites_page
  Scenario: Navigea-293--v2: Internal INSURANCE portfolio
    -Check the favorites in both the result and transaction list tables
    -The favorites should be shown in blue

    Given I am logged in with organization "TetralogSystemsQA" and CrmKey "QaTestData06"
    And I navigate to the "Advisory" module
    And In the Basic advisory setup area, I select the following checkboxes:
      | name                               | state |
      | Internal insurance - Insurance PTF | true  |
    When I select the "Result" page
    Then The app should show the "Insurance PTF" table in Results page as
      | order | name                                                     | securityExposure              | volatilityRiskClass | complexityRiskClass | liquidityRiskClass | currentShare | currentWeight% | currentValueNOK | recommendedShare | recommendedWeight% | recommendedValueNOK | Colour |
      | 1     | Templeton Global Bond Fund NOK                           | UCITS Funds - Fixed Income    | L                   | L                   | L                  | 0            | 0.00           | 0               | 350              | 3.88               | 59,585              | blue   |
      | 2     | Templeton Emerging Markets Bond Fund NOK                 | UCITS Funds - Fixed Income    | L                   | L                   | L                  | 0            | 0.00           | 0               | 449              | 5.00               | 76,877              | blue   |
      | 3     | SEB Listed Private Equity (NOK)                          | UCITS Funds - Sector oriented | H                   | L                   | L                  | 0            | 0.00           | 0               | 49               | 4.00               | 61,502              | blue   |
      | 4     | Jyske Invest Globale Aktier Udb                          | UCITS Funds - International   | H                   | L                   | L                  | 10,000       | 12.88          | 197,965         | 0                | 0.00               | 0                   | noir   |
      | 5     | Garantum Kreditobligation High Yield Europa 5 år nr 1408 | Bonds International           | H                   | L                   | M                  | 10,000       | 0.14           | 2,228           | 10,000           | 0.14               | 2,228               | noir   |
      | 6     | Næringsbygg Holding 1 AS                                 | Alternatives - Real Estate    | M                   | M                   | H                  | 10,000       | 15.61          | 240,000         | 10,000           | 15.61              | 240,000             | noir   |
    And I click on the "Show transactions" button in the Result page
    And I should see the following values in the Buy recommendation table for the internal portfolio "Insurance PTF" in Transaction page.
      | order | name                                     | securityExposure              | volatilityRiskClass | complexityRiskClass | liquidityRiskClass | shareCurrent | shareRecommended | buySell(modify) | price  | currency | limitLower | limitUpper | valueBuySell | Colour |
      | 1     | Templeton Global Bond Fund NOK           | UCITS Funds - Fixed Income    | L                   | L                   | L                  | 0            | 350              | 350.500         | 170.00 | NOK      |            |            | 59,585       | blue   |
      | 2     | Templeton Emerging Markets Bond Fund NOK | UCITS Funds - Fixed Income    | L                   | L                   | L                  | 0            | 449              | 449.578         | 171.00 | NOK      |            |            | 76,877       | blue   |
      | 3     | SEB Listed Private Equity (NOK)          | UCITS Funds - Sector oriented | H                   | L                   | L                  | 0            | 49               | 49.872          | 167.00 | EUR      |            |            | 61,502       | blue   |
    And I should see the following values in the Sell recommendation table for the internal portfolio "Insurance PTF" in Transaction page.
      | order | name                            | securityExposure            | volatilityRiskClass | complexityRiskClass | liquidityRiskClass | shareCurrent | shareRecommended | buySell(modify) | price | currency | limitLower | limitUpper | valueBuySell | Colour |
      | 1     | Jyske Invest Globale Aktier Udb | UCITS Funds - International | H                   | L                   | L                  | 10,000       | 0                | 10,000.000      | 20.00 | DKK      |            |            | 197,965      | noir   |

      
  #***************************************Internal ACTIVE and PASSIVE*************************
  @dev @NAVIGEA-293 @NAVIGEA-521 @NAVIGEA-696 @NAVIGEA-572 @Advisory_module @Favorites_page
  Scenario: Individual recommendation lists. Internal ACTIVE and PASSIVE portfolio
    -Check the favorites in both the result and transaction list tables
    -Create an indivisual list with instruments with Liquidity=M y H 
    -The favorites should be shown in blue
    ACTIVE= favorites of Fav-IA list
    PASSIVE= favorites of the individual recommendation list

    Given I am logged in with organization "TetralogSystemsQA" and CrmKey "QaTestData06"
    And I navigate to the "Advisory" module
    And In the Basic advisory setup area, I select the following checkboxes:
      | name                           | state |
      | Internal active - Active PTF   | true  |
      | Internal passive - Passive PTF | true  |
    And I select the "Setup" page
    And I click on the "Show Favorites" button
    And I select the following securities in Show Favorites page:
      | IsinOrName   |
      | NO0010382518 |
      | NO0010363971 |
      | QAFNOK122006 |
    When I select the "Result" page
    Then The app should show the "Active PTF" table in Results page as
      | order | name                                   | securityExposure               | volatilityRiskClass | complexityRiskClass | liquidityRiskClass | currentShare | currentWeight% | currentValueNOK | recommendedShare | recommendedWeight% | recommendedValueNOK | Colour |
      | 1     | Aberdeen Global India Equity Fund      | UCITS Funds - Emerging Markets | H                   | L                   | L                  | 0            | 0.00           | 0               | 193              | 2.05               | 31,585              | blue   |
      | 2     | GAM Star China Equity USD Acc          | UCITS Funds - Emerging Markets | H                   | L                   | L                  | 0            | 0.00           | 0               | 1,434            | 13.06              | 200,829             | blue   |
      | 3     | Dexia Invest Lange Danske Obligationer | UCITS Funds - Fixed Income     | L                   | L                   | L                  | 10,000       | 51.32          | 207,863         | 0                | 0.00               | 0                   | noir   |
      | 4     | SEB Asset Selection SEK (990)          | UCITS Funds - Alternatives     | H                   | M                   | L                  | 10,000       | 48.68          | 197,178         | 0                | 0.00               | 0                   | noir   |
    And The app should show the "Passive PTF" table in Results page as
      | order | name                                                     | securityExposure                   | volatilityRiskClass | complexityRiskClass | liquidityRiskClass | currentShare | currentWeight% | currentValueNOK | recommendedShare | recommendedWeight% | recommendedValueNOK | Colour |
      | 1     | Navigea Synthetic MMM                                    | UCITS Funds - Balanced/Mixed Funds | M                   | M                   | M                  | 0            | 0.00           | 0               | 1,389            | 11.23              | 172,626             | blue   |
      | 2     | Garantum Kreditobligation High Yield Europa 5 år nr 1408 | Bonds International                | H                   | L                   | L                  | 10,000       | 0.14           | 2,228           | 10,000           | 0.14               | 2,228               | noir   |
      | 3     | Næringsbygg Holding 1 AS                                 | Alternatives - Real Estate         | L                   | L                   | L                  | 10,000       | 15.61          | 240,000         | 10,000           | 15.61              | 240,000             | noir   |
    And I click on the "Show transactions" button in the Result page
    And I should see the following values in the Buy recommendation table for the internal portfolio "Active PTF" in Transaction page.
      | order | name                              | securityExposure               | volatilityRiskClass | complexityRiskClass | liquidityRiskClass | shareCurrent | shareRecommended | buySell(modify) | price  | currency | limitLower | limitUpper | valueBuySell | Colour |
      | 1     | Aberdeen Global India Equity Fund | UCITS Funds - Emerging Markets | H                   | L                   | L                  | 0            | 193              | 193.775         | 163.00 | NOK      |            |            | 31,585       | blue   |
      | 2     | GAM Star China Equity USD Acc     | UCITS Funds - Emerging Markets | H                   | L                   | L                  | 0            | 1,434            | 1,434.498       | 140.00 | NOK      |            |            | 200,829      | blue   |
    And I should see the following values in the Sell recommendation table for the internal portfolio "Active PTF" in Transaction page.
      | order | name                                   | securityExposure           | volatilityRiskClass | complexityRiskClass | liquidityRiskClass | shareCurrent | shareRecommended | buySell(modify) | price | currency | limitLower | limitUpper | valueBuySell | Colour |
      | 1     | Dexia Invest Lange Danske Obligationer | UCITS Funds - Fixed Income | L                   | L                   | L                  | 10,000       | 0                | 10,000.000      | 21.00 | DKK      |            |            | 207,863      | noir   |
      | 2     | SEB Asset Selection SEK (990)          | UCITS Funds - Alternatives | H                   | M                   | L                  | 10,000       | 0                | 10,000.000      | 23.00 | SEK      |            |            | 197,178      | noir   |
    And I should see the following values in the Buy recommendation table for the internal portfolio "Passive PTF" in Transaction page.
      | order | name                  | securityExposure                   | volatilityRiskClass | complexityRiskClass | liquidityRiskClass | shareCurrent | shareRecommended | buySell(modify) | price  | currency | limitLower | limitUpper | valueBuySell | Colour |
      | 1     | Navigea Synthetic MMM | UCITS Funds - Balanced/Mixed Funds | M                   | M                   | M                  | 0            | 1,389            | 1,389.854       | 124.20 | NOK      |            |            | 172,626      | blue   |

      