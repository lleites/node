 Feature: Favorites-- Connection to portfolio types
  NAVIGEA-293: AV - Setup 2: Favorites and Search functionality
  NAVIGEA-406: BA SA: FAVORITE LIST: Connection to portfolio types
   
 
 #**************************Internal PASSIVE *********************************************************
  @dev @NAVIGEA-293 @NAVIGEA-406 @Advisory_module @Favorites_page
  Scenario: PASSIVE Portfolio.
    -Check the lists enabled and disabled in favorites page

    Given I am logged in with organization "TetralogSystemsQA" and CrmKey "QaTestData02"
    And I navigate to the "Advisory" module
    And In the Basic advisory setup area, I select the following checkboxes:
      | name                           | state |
      | Internal passive - Passive PTF | true  |
    And I select the "Setup" page
    When I click on the "Show Favorites" button
    Then I sould see the "Fav-IA" list disabled
    And I should see the "Fav-II" list disabled
    And I should see the "Fav-IA-II-RiskFilter" list disabled
    
 #**************************Internal ACTIVE *********************************************************
  @dev @NAVIGEA-293 @NAVIGEA-406 @Advisory_module @Favorites_page
  Scenario: ACTIVE Portfolio.
    -Check the lists enabled and disabled in favorites page

    Given I am logged in with organization "TetralogSystemsQA" and CrmKey "QaTestData01"
    And I navigate to the "Advisory" module
    And In the Basic advisory setup area, I select the following checkboxes:
      | name                         | state |
      | Internal active - Active PTF | true  |
    And I select the "Setup" page
    When I click on the "Show Favorites" button
    Then I sould see the "Fav-IA" list active
    And I should see the "Fav-II" list disabled
    And I should see the "Fav-IA-II-RiskFilter" list active
    
  #**************************Internal INSURANCE *********************************************************
  @dev @NAVIGEA-293 @NAVIGEA-406 @Advisory_module @Favorites_page
  Scenario: INSURANCE Portfolio.
    -Check the lists enabled and disabled in favorites page

    Given I am logged in with organization "TetralogSystemsQA" and CrmKey "QaTestData03"
    And I navigate to the "Advisory" module
    And In the Basic advisory setup area, I select the following checkboxes:
      | name                               | state |
      | Internal insurance - Insurance PTF | true  |
    And I select the "Setup" page
    When I click on the "Show Favorites" button
    Then I sould see the "Fav-IA" list disabled
    And I should see the "Fav-II" list active
    And I should see the "Fav-IA-II-RiskFilter" list active
    
 #***************************************Internal PASSIVE and EXTERNAL*************************
  @dev @NAVIGEA-293  @NAVIGEA-406 @Advisory_module @Favorites_page
  Scenario: Internal PASSIVE and EXTERNAL portfolio
    -Check the lists enabled and disabled in favorites page

    Given I am logged in with organization "TetralogSystemsQA" and CrmKey "QaTestData02"
    And I navigate to the "Advisory" module
    And In the Basic advisory setup area, I select the following checkboxes:
      | name                           | state |
      | Internal passive - Passive PTF | true  |
      | External - External PTF        | true  |
    And I select the "Setup" page
    When I click on the "Show Favorites" button
    Then I sould see the "Fav-IA" list disabled
    And I should see the "Fav-II" list disabled
    And I should see the "Fav-IA-II-RiskFilter" list disabled