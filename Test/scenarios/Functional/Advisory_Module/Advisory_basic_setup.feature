Feature: Advisory basic setup 
  NAVIGEA-425: AV - START: Basic advisory setup area 0 : UI
  NAVIGEA-427: AV - START: Basic advisory setup area 2: Dropdown to select the appropriate target risk
  NAVIGEA-428: AV - START: Basic advisory setup area 3: Dropdown containing the available use cases
  NAVIGEA-429: AV - START: Basic advisory setup area 4: Checkboxes to select the portfolios to be included for treatment
  NAVIGEA-430: AV - START: Basic advisory setup area 5: Proceed Button
  NAVIGEA-472: AV - SETUP: Show Use Case Label
  7 tests(5 dev and 2 active)

  @active @Advisory_module @Analysis_page @NAVIGEA-425 @NAVIGEA-427 @NAVIGEA-428 
  Scenario: Test dropdown to select the appropriate target risk.
    Given I am logged in with organization "TetralogSystemsQA" and CrmKey "SeleniumTestPortfolio_Default"
    And I navigate to the "Advisory" module
    And In the Basic advisory setup area, the app should have an entry field "Withdrawal or addiditional"
    Then The app should show the following options in the "Investment Strategy" dropdown:
      | 			          |
      | Conservative          |
      | Conservative-moderate |
      | Moderate              |
      | Moderate-aggressive   |
      | Aggressive            |
    #And The corresponding Comfort zone in the risk return chart should be:
    #  | Comfort zone          | Min. Risk | Max. Risk | Target Risk |
    #  | Conservative          | 2.5       | 7.5       | 5.0         |
    #  | Conservative-moderate | 7.5       | 12.5      | 10.0        |
    #  | Moderate              | 12.5      | 17.5      | 15.0        |
    #  | Moderate-aggresive    | 17.5      | 22.5      | 20.0        |
    #  | Aggresive             | 22.5      | 27.5      | 25.0        |
    And The app should show the following options in the "Use Case" dropdown:
      | Optimize - rebalancing |
      | Manual setup           |
    And In the Basic advisory setup area, the checkboxes to select the portfolios should be:
      | name 			                        | state |
      | Internal active - Internal portfolio 1  | false |
      | Internal passive - Internal portfolio 2 | false |
      | External - External portfolio           | false |
    And In the Basic advisory setup area,the app should show the "Apply/Proceed" button

  @review @Advisory_module @Analysis_page @NAVIGEA-429 @NAVIGEA-429-v1 
  Scenario: Test Checkboxes to select the portfolios to be included for treatment.
  #External portfolio can only be selected together with active portfolio or passive portfolio.
    Given I am logged in with organization "TetralogSystemsQA" and CrmKey "SeleniumTestPortfolio_Default"
    And I navigate to the "Advisory" module
    Then In the Basic advisory setup area, the app should show the portfolio checkboxes with the following states:
    #state true = checkbox enabled, false = checkbox disabled
      | name                                    | state |
      | Internal active - Internal portfolio 1  | true  |
      | Internal passive - Internal portfolio 2 | true  |
      | External - External portfolio           | false |
    And In the Basic advisory setup area, I select the following checkboxes:
      | name                                    | state |
      | Internal active - Internal portfolio 1  | true  |
      | Internal passive - Internal portfolio 2 | false |
      | External - External portfolio           | true  |
    And In the Basic advisory setup area, I select the following checkboxes:
      | name                                    | state |
      | Internal active - Internal portfolio 1  | false |
      | Internal passive - Internal portfolio 2 | true  |
      | External - External portfolio           | true  |
    And In the Basic advisory setup area, I select the following checkboxes:
      | name                                    | state |
      | Internal active - Internal portfolio 1  | true  |
      | Internal passive - Internal portfolio 2 | true  |
      | External - External portfolio           | true  |
    And In the Basic advisory setup area, I select the following checkboxes:
      | name                                    | state |
      | Internal active - Internal portfolio 1  | true  |
      | Internal passive - Internal portfolio 2 | true  |
      | External - External portfolio           | false |

  @active @Advisory_module @Analysis_page @NAVIGEA-429 @NAVIGEA-429-v2 
  Scenario: Test Checkboxes to select the portfolios to be included for treatment.
  #Internal insurance and sub Internal insurance portfolio can only be selected alone.
    Given I am logged in with organization "TetralogSystemsQA" and CrmKey "SeleniumTestPortfolio_QAPortfolio"
    And I navigate to the "Advisory" module
    And In the Basic advisory setup area, I select the following checkboxes:
      | name                               | state |
      | Internal insurance - Insurance PTF | true  |      
    Then In the Basic advisory setup area, the app should show the portfolio checkboxes with the following states:
    #state true = checkbox enabled, false = checkbox disabled
      | name                           | state   |
      | Internal active - Active PTF   | false   |
      | Internal passive - Passive PTF | false   |
      | External - External PTF        | false   |
      | Internal insurance - Insurance PTF  | true |

  @active @Advisory_module @Analysis_page @NAVIGEA-429 @NAVIGEA-429-v3 
  Scenario: Test Checkboxes to select the portfolios to be included for treatment.
  #Internal insurance and sub Internal insurance portfolio can only be selected alone.
    Given I am logged in with organization "TetralogSystemsQA" and CrmKey "SeleniumTestPortfolio_QAPortfolio"
    And I navigate to the "Advisory" module
    And In the Basic advisory setup area, I select the following checkboxes:
      | name                         | state |
      | Internal active - Active PTF | true  |
    Then In the Basic advisory setup area, the app should show the portfolio checkboxes with the following states:
      | name                                | state |
      | Internal insurance - Insurance PTF  | false |
      | Internal active - Active PTF        | true  |
      | Internal passive - Passive PTF      | true  |
      | External - External PTF             | true  |

  @active @Advisory_module @Analysis_page @NAVIGEA-429 @NAVIGEA-429-v4 
  Scenario: Test Checkboxes to select the portfolios to be included for treatment.
  #Internal insurance and sub Internal insurance portfolio can only be selected alone.
    Given I am logged in with organization "TetralogSystemsQA" and CrmKey "SeleniumTestPortfolio_QAPortfolio"
    And I navigate to the "Advisory" module
    And In the Basic advisory setup area, I select the following checkboxes:
      | name                               | state |
      | Internal insurance - Insurance PTF | true  |
    And In the Basic advisory setup area, I select the following checkboxes:
      | name                                | state |
      | Internal insurance - Insurance PTF  | false |
    Then In the Basic advisory setup area, the app should show the portfolio checkboxes with the following states:
    #state true = checkbox enabled, false = checkbox disabled
      | name                               | state |
      | Internal active - Active PTF       | true  |
      | Internal passive - Passive PTF     | true  |
      | External - External PTF            | false  |
      | Internal insurance - Insurance PTF | true  |

  @active @Advisory_module @Analysis_page @NAVIGEA-430 @NAVIGEA-430-v1- @NAVIGEA-472-v1 
  Scenario: Test Proceed button going to result page
    Given I am logged in with organization "TetralogSystemsQA" and CrmKey "SeleniumTestPortfolio_Default"
    When I navigate to the "Advisory" module
    And I select the "Analysis" page
    And In the Basic advisory setup area,I select the "Optimize - rebalancing" use case in the dropdown
    And In the Basic advisory setup area, I select the following checkboxes:
      | name                        		   | state |
      | Internal active - Internal portfolio 1 | true  |
    And I press the Apply/Proceed button
    Then I should be in the "Result" page
  #And I should see "Use case: Optimize-rebalancing" in the use case label in "Result" page
  ##And I should see "Use case: Optimize-rebalancing" in the use case label in "Setup" page
  
  @active @Advisory_module @Analysis-page @NAVIGEA-430 @NAVIGEA-430-v2- @NAVIGEA-472-v2 
  Scenario: Test Proceed button going to manual setup page
    #And I should see "Use case: Manual setup" in the use case label in "Result" page
    ##And I should see "Use case: Manual setup" in the use case label in "Setup" page
    Given I am logged in with organization "TetralogSystemsQA" and CrmKey "SeleniumTestPortfolio_Default"
    When I navigate to the "Advisory" module
    And I select the "Analysis" page
    And In the Basic advisory setup area,I select the "Manual setup" use case in the dropdown
    And In the Basic advisory setup area, I select the following checkboxes:
      | name                        		   | state |
      | Internal active - Internal portfolio 1 | true  |
    And I press the Apply/Proceed button
    Then I should be in the "Setup" page
