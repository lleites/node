Feature: Financial Plausility checks in Documentation page 
  
  NAVIGEA-435:NAV-300: AV - DocPage: PLAUSIBILITY: Finance - Check basic variables calculation
  NAVIGEA-436:NAV-300: AV - DocPage: PLAUSIBILITY: Finance - Check definition and implementation
  NAVIGEA-300:BA SA: CONSTRAINTS Specification of Parameters and hierarchisation
  NAVIGEA-410:NAV-300: BA SA: CONSTRAINTS: min% of "Cash Accounts" within portfolios
  NAVIGEA-412:NAV-300: BA SA: CONSTRAINTS: min% of "liquid assets" within portfolios
  
  BUGS Tickets:
  NAVIGEA-484:AV - DocPage: 2a-financial suitability check
  NAVIGEA-665:AV - DocPage: Documentation page--The Plausibility checks in Documentation page are not correct
  
  LoanTotalAssets = SumDebts/SumAssets *100
  SumAssets = SUM <Assets> + PortfolioCash + PortfolioInvestments
  SumDebts = SUM <Debts> + AdditionalLoanBasedInvestment
  
  Liquid assets = LiquidityRiskClass = Low 

  @active @Advisory_module @Documentation_page @PlausilityChecks @NAVIGEA-410 @NAVIGEA-412 @NAVIGEA-300 @NAVIGEA-435 @NAVIGEA-436 @NAVIGEA-484 @NAVIGEA-665
  Scenario: VARIATION-1--Check suitability checks. All cases in "OK".
    Sum Assets=94.926.487
    Sum Debts=1.000.000
    LoanTotalAssets = 1.000.000/94.926.487*100=1.05
    so LoanTotalAssets <60% 
    Liquid assets=100% >20%
    Case with cash > 15% and cash > 150.000 NOK
    investment amount=0
    
    PortfolioInvestments >= NetAssets => show feedback
    Portfolio investments=84.626.487
    NetAssets = SumAssets - SumDebts
    NetAssets=94.926.487- 1.000.000=93.926.487
    so PortfolioInvestments < NetAssets

    Given I am logged in with organization "TetralogSystemsQA" and CrmKey "QaTestData13"
    When I navigate to the "Advisory" module
    And I select the "Analysis" page
    And In the Basic advisory setup area, I select the following checkboxes:
      | name                               | state |
      | Internal passive - IntPASS         | true  |
      | Internal active - IntACT           | true  |
      | External - External_SEB HIEH YIELD | true  |
      | External - External Underspecified | true  |
    And I select the "Result" page
    And I select the "Documentation" page
    Then the financial plausibility check should shown as
      | plausibility                        | message |
      | Control total debts:                | OK      |
      | Control cash accounts:              | OK      |
      | Control liquid assets:              | OK      |
      | Control new loan based investments: | OK      |
      | Control financial aptitude:         | OK      |
      | Office:                             |         |

  @active @Advisory_module @Documentation_page @PlausilityChecks @NAVIGEA-410 @NAVIGEA-412 @NAVIGEA-300 @NAVIGEA-435 @NAVIGEA-436 @NAVIGEA-484 @NAVIGEA-665
  Scenario: VARIATION-2--Check suitability checks.
    Check the messages in "Control liquid assets" and "Control new loan based investments" are shown.
    Check the message in "Control cash accounts" is NOT shown. Case with cash  < 15% but cash > 150.000 NOK
     
    Sum Assets=76.270.982
    Sum Debts=1.200.000
    LoanTotalAssets = 1.200.000/76.270.982*100=1.57
    so LoanTotalAssets <60% 
    
    Case with cash =1.57% < 15% but cash=900.000 > 150.000 NOK
    
    Liquidit asset=1.57% < 20% of the value
     
    Add investment amount 200.000 NOK
    
    PortfolioInvestments >= NetAssets => show feedback
    Portfolio investments=67.370.982
    NetAssets = SumAssets - SumDebts
    NetAssets=76.270.982- 1.200.000=75.070.982
    so PortfolioInvestments < NetAssets

    Given I am logged in with organization "TetralogSystemsQA" and CrmKey "QaTestData14"
    When I navigate to the "Advisory" module
    And I select the "Analysis" page
    And I enter a "200000" in Withdrawal or additional field
    And In the Basic advisory setup area, I select the following checkboxes:
      | name                       | state |
      | Internal passive - IntPASS | true  |
    And I select the "Result" page
    And I select the "Documentation" page
    Then the financial plausibility check should shown as
      | plausibility                        | message                                                                     |
      | Control total debts:                | OK                                                                          |
      | Control cash accounts:              | OK                                                                          |
      | Control liquid assets:              | Liquid assets should include min. 20% of the value of the overal portfolio. |
      | Control new loan based investments: | The recommendation contains new additional investments.                     |
      | Control financial aptitude:         | OK                                                                          |
      | Office:                             | The Investment Document has to be contersigned by the office manager.       |

  @active @Advisory_module @Documentation_page @PlausilityChecks @NAVIGEA-410 @NAVIGEA-412 @NAVIGEA-300 @NAVIGEA-435 @NAVIGEA-436 @NAVIGEA-484 @NAVIGEA-665
  Scenario: VARIATION-3-- Check suitability checks.
    Check the message in "Control cash accounts" is shown.
    Case with cash=0
    
    Sum Assets=8.405.042
    Sum Debts=1.000.000
    LoanTotalAssets = 1.000.000/8.405.042*100=11.89%
    so LoanTotalAssets <60%
     
    Liquid assets >20%
    
    investment amount=0
    
    PortfolioInvestments >= NetAssets => show feedback
    Portfolio investments=405,042
    NetAssets = SumAssets - SumDebts
    NetAssets=8.405.042- 1.000.000=7.405.42
    so PortfolioInvestments < NetAssets

    Given I am logged in with organization "TetralogSystemsQA" and CrmKey "QaTestData01"
    When I navigate to the "Advisory" module
    And I select the "Analysis" page
    And In the Basic advisory setup area, I select the following checkboxes:
      | name                         | state |
      | Internal active - Active PTF | true  |
    And I select the "Result" page
    And I select the "Documentation" page
    Then the financial plausibility check should shown as
      | plausibility                        | message                                                                                                   |
      | Control total debts:                | OK                                                                                                        |
      | Control cash accounts:              | Bank accounts should include min. 15% (up to 150.000 NOK) of the value of the overal portfolio. |
      | Control liquid assets:              | OK                                                                                                        |
      | Control new loan based investments: | OK                                                                                                        |
      | Control financial aptitude:         | OK                                                                                                        |
      | Office:                             | The Investment Document has to be contersigned by the office manager.                                     |

  @active @Advisory_module @Documentation_page @PlausilityChecks @NAVIGEA-410 @NAVIGEA-412 @NAVIGEA-300 @NAVIGEA-435 @NAVIGEA-436 @NAVIGEA-484 @NAVIGEA-665
  Scenario: VARIATION-4--Check suitability checks.
    Check the messages in "Control total debts" and "Control new loan based investments" are shown.
    
    Sum Assets=226.070.982
    Sum Debts=151.000.000
    LoanTotalAssets = 151.000.000/226.070.982*100=66.79%
    so LoanTotalAssets >60% 
    
    Add investment amount 150.000.000 NOK
    
    Liquidit asset=69.11% > 20% of the value 
    
    cash account=69.11% so >15% and >150.000 NOK
    
    PortfolioInvestments >= NetAssets => show feedback
    Portfolio investments=67.370.983
    NetAssets = SumAssets - SumDebts
    NetAssets=226.070.982- 151.000.000=75.070.982
    so PortfolioInvestments < NetAssets

    Given I am logged in with organization "TetralogSystemsQA" and CrmKey "QaTestData14"
    When I navigate to the "Advisory" module
    And I select the "Analysis" page
    And I enter a "150000000" in Withdrawal or additional field
    And In the Basic advisory setup area, I select the following checkboxes:
      | name                       | state |
      | Internal passive - IntPASS | true  |
    And I select the "Setup" page
    And I click on the "Show Parameters" button
    And I enter the following values in "IntPASS" table in Parameters page
      | order | name                                                     | dontSell | dontBuy |
      | 1     | Boligutleie Holding II AS                                | true     | true    |
      | 2     | Deliveien 4 Holding AS                                   | true     | true    |
      | 3     | Client Account                                           | false    | false   |
    And I select the "Result" page
    And I select the "Documentation" page
    Then the financial plausibility check should shown as
      | plausibility                        | message                                                               |
      | Control total debts:                | Loans are in excess of 60% of net assets.                             |
      | Control cash accounts:              | OK                                                                    |
      | Control liquid assets:              | OK                                                                    |
      | Control new loan based investments: | The recommendation contains new additional investments.               |
      | Control financial aptitude:         | OK                                                                    |
      | Office:                             | The Investment Document has to be contersigned by the office manager. |

  @active @Advisory_module @Documentation_page @PlausilityChecks @NAVIGEA-410 @NAVIGEA-412 @NAVIGEA-300 @NAVIGEA-435 @NAVIGEA-436 @NAVIGEA-484 @NAVIGEA-665
  Scenario: VARIATION-5--Check suitability checks.
    Check the message in "Control new loan based investments:" and "Control financial aptitude: " are shown.
    
    Sum Assets=174.926.487
    Sum Debts=81.000.000
    LoanTotalAssets = 81.000.000/174.926.487*100=46.30%
    so LoanTotalAssets <60% 
    
    Add investment amount 80.000.000 NOK
    
    Liquidit asset > 20% of the value 
    
    cash account=40.82% so >15% and >150.000 NOK
    
    PortfolioInvestments >= NetAssets => show feedback
    Portfolio investments=106.786.888
    NetAssets = SumAssets - SumDebts
    NetAssets=174.926.487- 81.000.000=93.926.487
    so PortfolioInvestments > NetAssets

    Given I am logged in with organization "TetralogSystemsQA" and CrmKey "QaTestData13"
    When I navigate to the "Advisory" module
    And I select the "Analysis" page
    And I enter a "80000000" in Withdrawal or additional field
    And In the Basic advisory setup area, I select the following checkboxes:
      | name                               | state |
      | Internal passive - IntPASS         | true  |
      | Internal active - IntACT           | true  |
      | External - External_SEB HIEH YIELD | true  |
      | External - External Underspecified | true  |
    And I select the "Result" page
    And I select the "Documentation" page
    Then the financial plausibility check should shown as
      | plausibility                        | message                                                               |
      | Control total debts:                | OK                                                                    |
      | Control cash accounts:              | OK                                                                    |
      | Control liquid assets:              | OK                                                                    |
      | Control new loan based investments: | The recommendation contains new additional investments.               |
      | Control financial aptitude:         | The recommendation contains new additional investments.               |
      | Office:                             | The Investment Document has to be contersigned by the office manager. |

  @active @Navigea-435-Navigea-436-v6 @Advisory_module @Documentation_page @PlausilityChecks @NAVIGEA-410 @NAVIGEA-412 @NAVIGEA-300 @NAVIGEA-435 @NAVIGEA-436 @NAVIGEA-484 @NAVIGEA-665
  Scenario: VARIATION-6--Check suitability checks.
    Check the message in "Control cash accounts" is NOT shown.Case with cash > 15% but cash < 150.000 NOK
    
    Case with cash=149.999 and 27.02%
     
    Sum Assets=8.555.041
    Sum Debts=1.000.000
    LoanTotalAssets = 1.000.000/8.555.041*100=11.68%
    so LoanTotalAssets <60%
     
    Liquid assets >20%
    
    investment amount=0
    
    PortfolioInvestments >= NetAssets => show feedback
    Portfolio investments=405.042
    NetAssets = SumAssets - SumDebts
    NetAssets=8.555.041- 1.000.000=7.555.041
    so PortfolioInvestments < NetAssets

    Given I am logged in with organization "TetralogSystemsQA" and CrmKey "QaTestData01"
    When I navigate to the "Portfolio" module
    And I click on the new external portfolio button
    And I enter the value "New External" on the portfolio name
    And I add in the external portfolio the following securities:
      | ISIN             | Name           | Share   | dontSell | dontBuy |
      | TS0000NOKLLL     | Client Account | 149999 | true     | true    |
    And I save the portfolio
    And I navigate to the "Advisory" module
    And In the Basic advisory setup area, I select the following checkboxes:
      | name                         | state |
      | Internal active - Active PTF | true  |
      | External - New External      | true  |
    And I select the "Result" page
    And I select the "Documentation" page
    Then the financial plausibility check should shown as
      | plausibility                        | message |
      | Control total debts:                | OK      |
      | Control cash accounts:              | OK      |
      | Control liquid assets:              | OK      |
      | Control new loan based investments: | OK      |
      | Control financial aptitude:         | OK      |
      | Office:                             |         |

  @active @Advisory_module @Documentation_page @PlausilityChecks @NAVIGEA-410 @NAVIGEA-412 @NAVIGEA-300 @NAVIGEA-435 @NAVIGEA-436 @NAVIGEA-484 @NAVIGEA-665
  Scenario: VARIATION-7-- Check suitability checks.
    Check the message in "Control new loan based investments:" and "Control financial aptitude: " are shown.
    
    Sum Assets=234.926.487
    Sum Debts=141.000.000
    LoanTotalAssets = 141.000.000/234.926.487*100=60.01%
    
    so LoanTotalAssets =60% 
    
    Add investment amount 140.000.000 NOK
    
    Liquidit asset > 20% of the value 
    
    cash account=52.71% so >15% and >150.000 NOK
    
    PortfolioInvestments >= NetAssets => show feedback
    Portfolio investments=107.319.824
    NetAssets = SumAssets - SumDebts
    NetAssets=234.926.487- 141.000.000=93.926.487
    so PortfolioInvestments > NetAssets

    Given I am logged in with organization "TetralogSystemsQA" and CrmKey "QaTestData13"
    When I navigate to the "Advisory" module
    And I select the "Analysis" page
    And I enter a "140000000" in Withdrawal or additional field
    And In the Basic advisory setup area, I select the following checkboxes:
      | name                               | state |
      | Internal passive - IntPASS         | true  |
      | Internal active - IntACT           | true  |
      | External - External_SEB HIEH YIELD | true  |
      | External - External Underspecified | true  |
    And I select the "Result" page
    And I select the "Documentation" page
    Then the financial plausibility check should shown as
      | plausibility                        | message                                                               |
      | Control total debts:                | Loans are in excess of 60% of net assets.                             |
      | Control cash accounts:              | OK                                                                    |
      | Control liquid assets:              | OK                                                                    |
      | Control new loan based investments: | The recommendation contains new additional investments.               |
      | Control financial aptitude:         | The recommendation contains new additional investments.               |
      | Office:                             | The Investment Document has to be contersigned by the office manager. |
