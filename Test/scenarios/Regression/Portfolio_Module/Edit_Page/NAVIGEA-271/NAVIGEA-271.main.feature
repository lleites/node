Feature: Portfolio Overview - General: No warning when leaving PO area with unsaved changes

  @active @Navigea-271 @Navigea-271-v1 @Portfolio_module @Edit_page
  Scenario: NAVIGEA-271-v1 -Edit external portfolio.Go to Advisory screen without save.Check that the warning message is shown
    Given I am logged in with organization "TetralogSystemsQA" and CrmKey "SeleniumTestPortfolio_Default"
  	When I navigate to the "Portfolio" module
    And I click on the edit portfolio button in "External portfolio" portfolio
    And I enter the value "Test" on the portfolio name
    And I navigate to the "Advisory" module
    Then The message that ask me: "If you leave this page, the changes you have made and didn't save yet will be lost. Do you really want to save the changes?" should be shown
    And I click on the popup no button
    
  @active @Navigea-271 @Navigea-271-v2 @Portfolio_module @Edit_page
  Scenario: NAVIGEA-271-v2 -New external portfolio.Go to Advisory screen without save.Check that the warning message is shown
   Given I am logged in with organization "TetralogSystemsQA" and CrmKey "SeleniumTestPortfolio_Default"
  	When I navigate to the "Portfolio" module
    And I click on the new external portfolio button
    And I enter the value "Test" on the portfolio name
    And I navigate to the "Advisory" module
    Then The message that ask me: "If you leave this page, the changes you have made and didn't save yet will be lost. Do you really want to save the changes?" should be shown
    And I click on the popup no button
    
  @active @Navigea-271 @Navigea-271-v3 @Portfolio_module @Edit_page
  Scenario: NAVIGEA-271-v3 -Delete a deposit in external portfolio.Go to Advisory screen without save.Check that the warning message is shown
    Given I am logged in with organization "TetralogSystemsQA" and CrmKey "SeleniumTestPortfolio_Default"
  	When I navigate to the "Portfolio" module
    And I click on the edit portfolio button in "External portfolio" portfolio
    And I add in the external portfolio the following securities:
      | ISIN         | Name                | Share |
      | SE0000433203 | # SEB Östeuropafond | 100   |
    And I save the portfolio
    And I delete the "# SEB Östeuropafond" deposit
    And The message that ask me: "You are going to remove position 'SE0000433203', Are you sure?" should be shown 
    And I click on the save button in the message
    And I navigate to the "Advisory" module
    Then The message that ask me: "If you leave this page, the changes you have made and didn't save yet will be lost. Do you really want to save the changes?" should be shown
    And I click on the popup no button
    
  @active @Navigea-271 @Navigea-271-v4 @Portfolio_module @Edit_page
  Scenario: NAVIGEA-271-v3 -Check that the warning message is NOT shown if no changes.
    Given I am logged in with organization "TetralogSystemsQA" and CrmKey "SeleniumTestPortfolio_Default"
  	When I navigate to the "Portfolio" module
    And I click on the edit portfolio button in "External portfolio" portfolio
    And I navigate to the "Advisory" module
    