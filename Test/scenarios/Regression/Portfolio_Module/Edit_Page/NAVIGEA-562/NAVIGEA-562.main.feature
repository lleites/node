Feature: NAVIGEA-562: CLONE - System froze when creating new ext after deleting old one without saving

 @active @NAVIGEA-562 @Portfolio_module @Overview_page
  Scenario: @NAVIGEA-562--check that when creating new external portfolio after deleting old one without saving the app is not frozen
    Given I am logged in with organization "TetralogSystemsQA" and CrmKey "SeleniumTestPortfolio_Default"
    When I navigate to the "Portfolio" module
    And I click on the new external portfolio button
    And I enter the value "Test Ext. Portfolio 1" on the portfolio name
    And I add "50" shares of position name "# SEB Europafond Småbolag (518)" and ISIN "SE0000433252"
    And I save the portfolio
    And I navigate back to the portfolio overview screen
    And I delete the portfolio "Test Ext. Portfolio 1"
    And I see a popup with a delete button
    And I click on the popup delete button
    And I click on the popup ok button
    And I click on the new external portfolio button
    And I click on the popup save and leave button
    Then I should be in Edit page