Feature: "Nominal/No. Shares" has many decimal digits shown after the comma after "Apply to portfolio" upon search results

  @active @NAVIGEA-312 @NAVIGEA-312-v1 @Portfolio_module @Edit_page
  Scenario: NAVIGEA-312-v1-Create a new external portfolio.Check the "Nominal/No. Shares" values.
    Given I am logged in with organization "TetralogSystemsQA" and CrmKey "SeleniumTestPortfolio_Default"
  	When I navigate to the "Portfolio" module
    And I click on the new external portfolio button
    And I enter the value "Test Ext. Porfolio 1" on the portfolio name
    And I add in the external portfolio the following securities:
      | ISIN         | Name                            | Value   |
      | SE0000433252 | # SEB Europafond Småbolag (518) | 2659    |
      | SE0000433203 | # SEB Östeuropafond             | 3567    |
      | SE0000429789 | Carnegie Sverigefond            | 3456.50 |
      | NO0008002086 | DNB Norden (I)                  | 5678    |
    Then I should see the editable portfolio as
      | order | name                            | securityExposure                | volatilityRiskClass | complexityRiskClass | liquidityRiskClass | datafeedStatus | dontSell | dontBuy | price | currency | numberOfShares | value    |
      | 1     | Client Account                  | Money Market NOK                | L                   | L                   | L                  | VALID          | false    | true    | n/a   | NOK      | 0.000          | 0.00     |
      | 2     | # SEB Europafond Småbolag (518) | UCITS Funds - Developed Markets | H                   | L                   | L                  | VALID          | false    | true    | 37.46 | SEK      | 82.798         | 2,659.00 |
      | 3     | # SEB Östeuropafond             | UCITS Funds - Frontier Markets  | H                   | L                   | L                  | VALID          | false    | true    | 18.45 | SEK      | 225.515        | 3,567.00 |
      | 4     | Carnegie Sverigefond            | UCITS Funds - Sweden            | H                   | L                   | L                  | VALID          | false    | true    | 62.00 | SEK      | 65.030         | 3,456.50 |
      | 5     | DNB Norden (I)                  | UCITS Funds - Developed Markets | H                   | L                   | L                  | VALID          | false    | true    | 46.89 | NOK      | 121.092        | 5,678.00 |

  @active @NAVIGEA-312 @NAVIGEA-312-v2 @Portfolio_module @Edit_page
  Scenario: NAVIGEA-312-v2- Add more share for a security in external portfolio.Check the "Nominal/No. Shares" values.
    Given I am logged in with organization "TetralogSystemsQA" and CrmKey "SeleniumTestPortfolio_Default"
  	When I navigate to the "Portfolio" module
    And I click on the edit portfolio button in "External portfolio" portfolio
    And I change in the external portfolio the following securities:
      | ISIN         | Name             | Share |
      | DK0015939359 | Bankinvest Asien | 7     |
    And I save the portfolio
    And I navigate back to the portfolio overview screen
    Then The external portfolio "External portfolio" should have the following positions :
      | order | name                        | securityExposure               | volatilityRiskClass | complexityRiskClass | liquidityRiskClass | dontSell | dontBuy | price  | currency | numberOfShares | value |    
      | 1     | Bankinvest Asien            | UCITS Funds - Emerging Markets | H                   | L                   | L                  | -        | ✓       | 426.00 | DKK      | 7              | 2,951 |
      | 2     | Bankinvest Basis Aktier Udb | UCITS Funds - International    | H                   | L                   | L                  | -        | ✓       | 129.00 | DKK      | 8              | 1,110 |
      | 3     | Storebrand Barnespar        | UCITS Funds - International    | H                   | L                   | L                  | -        | ✓       | 48.81  | NOK      | 28             | 1,366 |
