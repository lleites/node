Feature: Wrong table row index when pagination activated

  @active @NAVIGEA-322 @NAVIGEA-322-v1 @Portfolio_module @Edit_page
  Scenario: NAVIGEA-322-v1- Check the table row in Edit page in the last page.
    Given I am logged in with organization "TetralogSystemsQA" and CrmKey "SeleniumTestPortfolio_Default"
    When I navigate to the "Portfolio" module
    And I click on the new external portfolio button
    And I search all the securities
    And I go to the "last" page in the search result pagination bar
    Then The search result should include the following securities:
      | order | nameAndIsin                                        | securityClassAndExposure                      | volatilityRiskClass | complexityRiskClass | liquidityRiskClass | datafeedIcon   | price  | currency |
      | 171   | LU0053685615 @ JPMF Emerging Markets Equity Fund A | UCITS Funds @ UCITS Funds - Emerging Markets  | H                   | L                   | L                  | VALID          | 146.00 | USD      |
      | 172   | LU0051759099 @ JPMF Eastern Europe Equity Fund A   | UCITS Funds @ UCITS Funds - Emerging Markets  | H                   | L                   | L                  | VALID          | 145.00 | EUR      |
      | 173   | LU0051755006 @ JPMF - JF China Fund A              | UCITS Funds @ UCITS Funds - Emerging Markets  | H                   | L                   | L                  | VALID          | 144.00 | USD      |
      | 174   | LU0058908533 @ JPMF - JF India Fund A              | UCITS Funds @ UCITS Funds - Emerging Markets  | H                   | L                   | L                  | VALID          | 150.00 | USD      |

  @active @NAVIGEA-322 @NAVIGEA-322-v2 @Portfolio_module @Edit_page
  Scenario: NAVIGEA-322-v2- Check the table row in Edit page in the first page.
    Given I am logged in with organization "TetralogSystemsQA" and CrmKey "SeleniumTestPortfolio_Default"
    When I navigate to the "Portfolio" module
    And I click on the new external portfolio button
    And I search all the securities
    Then The search result should include the following securities:
      | order | nameAndIsin                                     | securityClassAndExposure          | volatilityRiskClass | complexityRiskClass | liquidityRiskClass | datafeedIcon | price    | currency |
      | 1     | SE0000744195 @ Stockholmsbørsen: OMXS All Share | Index @ Index - Unspecified       | H                   | L                   | L                  | VALID        | 1,010.00 | SEK      |
      | 3     | NO0000000021 @ Oslo Børs Hovedindeksen          | Index @ Index - Unspecified       | H                   | L                   | L                  | VALID        | 1,020.00 | NOK      |
      | 5     | NO0007035335 @ Oslo Børs Fondsindeks            | Index @ Index - Unspecified       | L                   | L                   | L                  | VALID        | 1,000.00 | NOK      |
      | 8     | Bank Account DKK @ Client Account               | Cash @ Money Market International | M                   | L                   | L                  | VALID        | n/a      | DKK      |
      | 10    | Bank Account EUR @ Client Account               | Cash @ Money Market International | M                   | L                   | L                  | VALID        | n/a      | EUR      |

  @active @NAVIGEA-322 @NAVIGEA-322-v3 @Portfolio_module @Edit_page
  Scenario: NAVIGEA-322-v3- Check the table row in Edit page in the page 10.
    Given I am logged in with organization "TetralogSystemsQA" and CrmKey "SeleniumTestPortfolio_Default"
    When I navigate to the "Portfolio" module
    And I click on the new external portfolio button
    And I search all the securities
    And I go to the "10" page in the search result pagination bar
    Then The search result should include the following securities:
      | order | nameAndIsin                                        | securityClassAndExposure                      | volatilityRiskClass | complexityRiskClass | liquidityRiskClass | datafeedIcon | price   | currency |
      | 91    | TS000UFDMMLM @ UCITS Funds - Developed Markets MLM | UCITS Funds @ UCITS Funds - Developed Markets | M                   | L                   | M                  | VALID        | 119.00  | NOK      |
      | 100   | TS000UFFIHLM @ UCITS Funds - Fixed Income HLM      | UCITS Funds @ UCITS Funds - Fixed Income      | H                   | L                   | M                  | VALID        | 114.00  | NOK      |
