Feature: NAVIGEA-471: PO - Edit page - Check fractions not allowed for certain instruments

  @active @NAVIGEA-471-v1 @Portfolio_module @Edit_page @Fractions
  Scenario: @NAVIGEA-471-v1--Check that a visual clue is shown when trying to add a number of shares with decimals for an instrument that do not allows fractions
    Given I am logged in with organization "TetralogSystemsQA" and CrmKey "SeleniumTestPortfolio_Default"
    When I navigate to the "Portfolio" module
    And I click on the new external portfolio button
    And I enter the value "Test Fractions" on the portfolio name
    And I search for instrument "SE0003428333"
    And I enter "150.56" for "number of shares" for search result "SE0003428333"
    Then I should see a visual clue next to the "Nominal" entry field for position "Nordic Secondary AB" and ISIN "SE0003428333"
	And when I click on the visual clue in position name "Nordic Secondary AB" and ISIN "SE0003428333" I should see the following message: "Fractions are not allowed for assets categorized as 'Alternative Investments'. Please use integers for number of shares only!"

  @active @NAVIGEA-471-v2 @Portfolio_module @Edit_page @Fractions
  Scenario: @NAVIGEA-471-v2--Check that a visual clue is shown when trying to modify the number of shares with decimals for an instrument that do not allows fractions
    Given I am logged in with organization "TetralogSystemsQA" and CrmKey "SeleniumTestPortfolio_Default"
    When I navigate to the "Portfolio" module
    And I click on the new external portfolio button
    And I enter the value "Test Fractions" on the portfolio name
    And I add "200" shares of position name "Nordic Secondary AB" and ISIN "SE0003428333"
    And I change in the external portfolio the following securities:
      | ISIN         | Name                | Share |
      | SE0003428333 | Nordic Secondary AB | 355.5 |
    Then I should see in the portfolio table a visual clue for position "Nordic Secondary AB" and ISIN "SE0003428333"
	And when I click on the visual clue in portfolio table position name "Nordic Secondary AB" and ISIN "SE0003428333" I should see the following message: "Fractions are not allowed for assets categorized as 'Alternative Investments'. Please use integers for number of shares only!"