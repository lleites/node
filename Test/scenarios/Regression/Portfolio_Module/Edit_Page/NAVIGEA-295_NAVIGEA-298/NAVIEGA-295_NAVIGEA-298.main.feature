Feature: Navigea-295-- Portfolio Overview - Edit page: Back to overview is blocked by mandatory portfolio name
   Navigea-298--PO - General - Portfolio name should not be madatory

  @active @Navigea-295 @Portfolio_module @Edit_page
  Scenario: NAVIGEA-295-v1 - New portfolio. Check that back to overview page is not blocked by an empty portfolio name
    Given I am logged in with organization "TetralogSystemsQA" and CrmKey "SeleniumTestPortfolio_Default"
    When I navigate to the "Portfolio" module
    And I click on the new external portfolio button
    And I save the portfolio
    And I navigate back to the portfolio overview screen
    Then I see the overview portfolio summary table

  @active @NAVIGEA-298 @NAVIGEA-298-v1 @Portfolio_module @Edit_page
  Scenario: NAVIGEA-298-v1--Create a new external portfolio with an empty porfolio name
    Given I am logged in with organization "TetralogSystemsQA" and CrmKey "SeleniumTestPortfolio_Default"
    When I navigate to the "Portfolio" module
    And I click on the new external portfolio button
    And I add in the external portfolio the following securities:
      | ISIN         | Name                            | Share |
      | SE0000433252 | # SEB Europafond Småbolag (518) | 50    |
    And I save the portfolio
    And I navigate back to the portfolio overview screen
    Then The app should have a portfolio with name "" in the portfolio overview screen
    Then The external portfolio "" should have the following positions :
      | order | name                            | securityExposure                | volatilityRiskClass | complexityRiskClass | liquidityRiskClass | dontSell | dontBuy | price | currency | numberOfShares | value |
      | 1     | Client Account                  | Money Market NOK                | L                   | L                   | L                  | -        | ✓       | n/a   | NOK      | 0              | 0     |
      | 2     | # SEB Europafond Småbolag (518) | UCITS Funds - Developed Markets | H                   | L                   | L                  | -        | ✓       | 37.46 | SEK      | 50             | 1,605 |

  @active @NAVIGEA-298 @NAVIGEA-298-v2 @Portfolio_module @Edit_page
  Scenario: NAVIGEA-298-v2 --Change a deposit name to a empty name in external portfolio.
    Given I am logged in with organization "TetralogSystemsQA" and CrmKey "SeleniumTestPortfolio_Default"
    When I navigate to the "Portfolio" module
    And I click on the new external portfolio button
    And I enter the value "external portfolio 2" on the portfolio name
    And I add in the external portfolio the following securities:
      | ISIN         | Name                            | Share |
      | SE0000433252 | # SEB Europafond Småbolag (518) | 50    |
    And I save the portfolio
    And I navigate back to the portfolio overview screen
    And I click on the edit portfolio button in "external portfolio 2" portfolio
    And I add in the external portfolio the following securities:
      | ISIN         | Name                 | Share |
      | NO0008000981 | Storebrand Barnespar | 50    |
    And I enter the value "" on the portfolio name
    And I save the portfolio
    And I navigate back to the portfolio overview screen
    Then The external portfolio "" should have the following positions :
      | order | name                            | securityExposure                | volatilityRiskClass | complexityRiskClass | liquidityRiskClass | dontSell | dontBuy | price | currency | numberOfShares | value |
      | 1     | Client Account                  | Money Market NOK                | L                   | L                   | L                  | -        | ✓       | n/a   | NOK      | 0              | 0     |
      | 2     | # SEB Europafond Småbolag (518) | UCITS Funds - Developed Markets | H                   | L                   | L                  | -        | ✓       | 37.46 | SEK      | 50             | 1,605 |
      | 3     | Storebrand Barnespar            | UCITS Funds - International     | H                   | L                   | L                  | -        | ✓       | 48.81 | NOK      | 50             | 2,440 |
