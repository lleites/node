Feature: PO - Edit - Navigea-263-256 --Substitution: Don't block dropdown, when product is "not categorisized"

  @Navigea-263 @Portfolio_module @Edit_page @Stanbdby
  Scenario: Navigea-263 : The security exposure dropdown must be enabled if the value is 'Not categorized' -- substitution search
    Given I am logged in with organization "TetralogSystemsQA" and CrmKey "SeleniumTestPortfolio_Default"
    When I navigate to the "Portfolio" module
    And I edit the portfolio number 1
    And I click substitution icon for security "Bankinvest Basis Aktier Udb"
    Then I should see the following prepopulated search options:
      | securityClass | securityExposure | currency | riskVolatility | riskComplexity | riskLiquidity |
      | UCITS fund    | Not categorized  | DKK      | High           | Low            | Low           |
    And The "Security exposure" search criteria dropdown should be "enabled"

  @Navigea-256 @Portfolio_module @Edit_page @Stanbdby
  Scenario: Navigea-256 : Verify that if the user clicks in two different substitutes, the second one updates the sec. exposure
    Given I am logged in with organization "TetralogSystemsQA" and CrmKey "QaTestData10"
    When I navigate to the "Portfolio" module
    And I edit the portfolio number 1
    And I click substitution icon for security "Equities Norway HLL"
    Then I should see the following prepopulated search options:
      | securityClass | securityExposure | currency | riskVolatility | riskComplexity | riskLiquidity |
      | Listed Shares | Equities Norway  | NOK      | High           | Low            | Low           |
    #And I should see the following text "Specification for "Equities Norway HLL", "TS00000ENHLL", Value 200,000.00 NOK"
    When I click substitution icon for security "Bankinvest Basis Aktier Udb"
    Then I should see the following prepopulated search options:
      | securityClass | securityExposure | currency | riskVolatility | riskComplexity | riskLiquidity |
      | Cash          | Money Market NOK | NOK      | Low            | Low            | Low           |
	#And I should see the following text "Specification for "Cash NOK LLL", "TS0000NOKLLL", Value 400,000.00 NOK"