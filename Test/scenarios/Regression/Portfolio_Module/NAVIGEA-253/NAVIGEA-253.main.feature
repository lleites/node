Feature: Validation not working for number of shares starting with number but containing other characters

  @active @Navigea-253 @Portfolio_module @Edit_page
  Scenario: Navigea-253: Check that dont buy/dont sell attributes are persisted
  	Given I am logged in with organization "TetralogSystemsQA" and CrmKey "SeleniumTestPortfolio_Default"
  	When I navigate to the "Portfolio" module
  	And I edit the portfolio number 1
  	And I "check" the column "Don't sell" for the position name "Bankinvest Asien"
  	And I "check" the column "Don't buy" for the position name "Bankinvest Basis Aktier Udb"
  	And I "check" the column "Don't sell" for the position name "Storebrand Barnespar"
  	And I save the portfolio number 0
  	And I navigate back to the portfolio overview screen
  	Then I should see external portfolio 1 as
  		""" 
  		{
			rowData : [
				{
					'@class' : 'com.tetralog.webtest.table.row.impl.NavigeaPortfolioRow',
					rowIndex : 1,
					order : 1,
					name : 'Bankinvest Asien',
					nameAndIsinTooltip : {
						isin : 'DK0015939359',
						name : 'Bankinvest Asien'
					},
					securityExposure : 'UCITS Funds - Emerging Markets',
					volatilityRiskClass : 'H',
					complexityRiskClass : 'L',
					liquidityRiskClass : 'L',
					datafeedStatus : 'VALID',
					dontSell : '✓',
					dontBuy : '✓',
					price : '426.00',
					currency : 'DKK',
					numberOfShares : '15',
					value : '6,324'
				}, {
					'@class' : 'com.tetralog.webtest.table.row.impl.NavigeaPortfolioRow',
					rowIndex : 2,
					order : 2,
					name : 'Bankinvest Basis Aktier Udb',
					nameAndIsinTooltip : {
						isin : 'DK0015773873',
						name : 'Bankinvest Basis Aktier Udb'
					},
					securityExposure : 'UCITS Funds - International',
					volatilityRiskClass : 'H',
					complexityRiskClass : 'L',
					liquidityRiskClass : 'L',
					datafeedStatus : 'VALID',
					dontSell : '-',
					dontBuy : '✓',
					price : '129.00',
					currency : 'DKK',
					numberOfShares : '8',
					value : '1,110'
				}, {
					'@class' : 'com.tetralog.webtest.table.row.impl.NavigeaPortfolioRow',
					rowIndex : 3,
					order : 3,
					name : 'Storebrand Barnespar',
					nameAndIsinTooltip : {
						isin : 'NO0008000981',
						name : 'Storebrand Barnespar'
					},
					securityExposure : 'UCITS Funds - International',
					volatilityRiskClass : 'H',
					complexityRiskClass : 'L',
					liquidityRiskClass : 'L',
					datafeedStatus : 'VALID',
					dontSell : '✓',
					dontBuy : '✓',
					price : '48.81',
					currency : 'NOK',
					numberOfShares : '28',
					value : '1,366'
				}
			]
		}
		"""
