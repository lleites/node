Feature: NAVIGEA-339-- PO - AV General: Summary Tables and Summary Rows: Don't show decimals for values
   NAVIGEA-288-- BA SA: CONSTRAINTS: Basic don't sell / don't buy constraints

  @active @NAVIGEA-339-V3 @NAVIGEA-288-V1 @Advisory_module @Analysis-page
  Scenario: NAVIGEA-339-V3_@NAVIGEA-288-V1 --Analysis page: Check the portfolios that are shown in Analysis page.
    Given I am logged in with organization "TetralogSystemsQA" and CrmKey "SeleniumTestPortfolio_Default"
    When I navigate to the "Advisory" module
    And I select the "Analysis" page
    And I expand the details of the "internal" portfolio number 1
    Then The app should show the following securities in "internal" portfolio 1 in Analysis page
      | order | name                                           | securityExposure            | volatilityRiskClass | complexityRiskClass | liquidityRiskClass | risk  | performance | datafeedStatus | dontSell | dontBuy | price  | currency | numberOfShares | weight | value   |
      | 1     | AB Global Growth Trends A USD Acc              | UCITS Funds - International | H                   | L                   | L                  | 14.17 | 0.70        | VALID          |     -    |     -   | 33.55  | USD      | 120            | 9.86   | 22,493  |
      | 2     | Aberdeen Global - Asian Smaller Companies Fund | UCITS Funds - International | H                   | L                   | L                  | 36.44 | 10.31       | VALID          |     -    |     -   | 162.00 | USD      | 120            | 47.63  | 108,613 |
    And The "internal" portfolio 1 total value should be "131,107" and "57.50" % in Analysis page
    And I expand the details of the "internal" portfolio number 2
    And The app should show the following securities in "internal" portfolio 2 in Analysis page
      | order | name                                           | securityExposure               | volatilityRiskClass | complexityRiskClass | liquidityRiskClass | risk  | performance | datafeedStatus | dontSell | dontBuy | price  | currency | numberOfShares | weight | value  |
      | 1     | Aberdeen Global - Asian Smaller Companies Fund | UCITS Funds - International    | H                   | L                   | L                  | 36.44 | 10.31       | VALID          |    -     |    -    | 162.00 | USD      | 86             | 34.41  | 78,473 |
      | 2     | Aberdeen Global - Emerging Markets Equity Fund | UCITS Funds - Emerging Markets | H                   | L                   | L                  | 17.62 | 4.55        | VALID          |    -     |    -    | 157.00 | USD      | 11             | 4.23   | 9,648  |
    And The "internal" portfolio 2 total value should be "88,122" and "38.64" % in Analysis page
    And I expand the details of the "external" portfolio number 1
    And The app should show the following securities in "external" portfolio 1 in Analysis page
      | order | name                        | securityExposure               | volatilityRiskClass | complexityRiskClass | liquidityRiskClass | risk  | performance | datafeedStatus | dontSell | dontBuy | price  | currency | numberOfShares | weight | value |
      | 1     | Bankinvest Asien            | UCITS Funds - Emerging Markets | H                   | L                   | L                  | 10.46 | 4.22        | VALID          |    -     |    ✓    | 426.00 | DKK      | 15             | 2.77   | 6,324 |
      | 2     | Bankinvest Basis Aktier Udb | UCITS Funds - International    | H                   | L                   | L                  | 13.59 | 7.20        | VALID          |    -     |    ✓    | 129.00 | DKK      | 8              | 0.49   | 1,110 |
      | 3     | Storebrand Barnespar        | UCITS Funds - International    | H                   | L                   | L                  | 11.73 | 7.61        | VALID          |     -    |    ✓    | 48.81  | NOK      | 28             | 0.60   | 1,366 |
    And The "external" portfolio 1 total value should be "8,803" and "3.86" % in Analysis page
    And The Portfolio summary should have the following values in Analysis page:
      | Portfolio summary | Weight % | Value NOK |
      | Internal          | 96.14    | 219,229   |
      | External          | 3.86     | 8,803     |
      | Total             | 100.00   | 228,032   |

  @active @NAVIGEA-339-V4 @NAVIGEA-288-V2 @Portfolio_module
  Scenario: NAVIGEA-339-V4_NAVIGEA-288-V2--Portfolio overview: Check the portfolios that are shown in in Portfolio Overview page.
    Given I am logged in with organization "TetralogSystemsQA" and CrmKey "SeleniumTestPortfolio_Default"
    When I navigate to the "Portfolio" module
    And I expand the details of the internal portfolio number 1
    And I should see internal portfolio 1 as Table
      | order | name                                           | securityExposure            | volatilityRiskClass | complexityRiskClass | liquidityRiskClass | risk  | performance | datafeedStatus | dontSell | dontBuy | price  | currency | numberOfShares | value   |
      | 1     | AB Global Growth Trends A USD Acc              | UCITS Funds - International | H                   | L                   | L                  | 14.17 | 0.70        | VALID          |     -    |    -    | 33.55  | USD      | 120            | 22,493  |
      | 2     | Aberdeen Global - Asian Smaller Companies Fund | UCITS Funds - International | H                   | L                   | L                  | 36.44 | 10.31       | VALID          |     -    |    -    | 162.00 | USD      | 120            | 108,613 |
    And The Internal portfolio 1 total value should be "131,107" in Portfolio Overview page
    And I should see internal portfolio 2 as Table
      | order | name                                           | securityExposure               | volatilityRiskClass | complexityRiskClass | liquidityRiskClass | risk  | performance | datafeedStatus | dontSell | dontBuy | price  | currency | numberOfShares | value  |
      | 1     | Aberdeen Global - Asian Smaller Companies Fund | UCITS Funds - International    | H                   | L                   | L                  | 36.44 | 10.31       | VALID          |     -    |    -    | 162.00 | USD      | 86             | 78,473 |
      | 2     | Aberdeen Global - Emerging Markets Equity Fund | UCITS Funds - Emerging Markets | H                   | L                   | L                  | 17.62 | 4.55        | VALID          |     -    |     -   | 157.00 | USD      | 11             | 9,648  |
    And The Internal portfolio 2 total value should be "88,122" in Portfolio Overview page
    And I should see external portfolio 1 as Table
      | order | name                        | securityExposure               | volatilityRiskClass | complexityRiskClass | liquidityRiskClass | risk  | performance | datafeedStatus | dontSell | dontBuy | price  | currency | numberOfShares | value |
      | 1     | Bankinvest Asien            | UCITS Funds - Emerging Markets | H                   | L                   | L                  | 10.46 | 4.22        | VALID          |    -     |    ✓    | 426.00 | DKK      | 15             | 6,324 |
      | 2     | Bankinvest Basis Aktier Udb | UCITS Funds - International    | H                   | L                   | L                  | 13.59 | 7.20        | VALID          |    -     |    ✓    | 129.00 | DKK      | 8              | 1,110 |
      | 3     | Storebrand Barnespar        | UCITS Funds - International    | H                   | L                   | L                  | 11.73 | 7.61        | VALID          |    -     |    ✓    | 48.81  | NOK      | 28             | 1,366 |
    And The External portfolio 1 total value should be "8,803" in Portfolio Overview page
    And The Portfolio summary should have the following values in Portfolio Overview page:
      | Portfolio summary | Value NOK |
      | Internal          | 219,229   |
      | External          | 8,803     |
      | Total             | 228,032   |

  @active @NAVIGEA-339-V5 @NAVIGEA-288-V3 @Advisory_module @Parameters-page
  Scenario: NAVIGEA-339-V5_@NAVIGEA-288-V3--Parameters page: Check the portfolios that are shown  in Portfolio Overview page.
   Given I am logged in with organization "TetralogSystemsQA" and CrmKey "SeleniumTestPortfolio_Default"
    And I navigate to the "Advisory" module
    And In the Basic advisory setup area, I select the following checkboxes:
      | name                                    | state |
      | Internal active - Internal portfolio 1  | true  |
      | Internal passive - Internal portfolio 2 | true  |
      | External - External portfolio           | true  |
    And I select the "Setup" page
    And I click on the "Show Parameters" button
    Then I should see the parameters table for the "internal" portfolio 1 as
      | order | name                                           | securityExposure            | volatilityRiskClass | complexityRiskClass | liquidityRiskClass | risk  | performance | datafeedStatus | dontSell | minSellPercentage | minSellValue | dontBuy | maxBuyPercentage | maxBuyValue | currentWeight | value   |
      | 1     | AB Global Growth Trends A USD Acc              | UCITS Funds - International | H                   | L                   | L                  | 14.17 | 0.70        | VALID          | false    | 0.00              |              | false   | 100.00           |             | 9.86          | 22,493  |
      | 2     | Aberdeen Global - Asian Smaller Companies Fund | UCITS Funds - International | H                   | L                   | L                  | 36.44 | 10.31       | VALID          | false    | 0.00              |              | false   | 100.00           |             | 47.63         | 108,613 |
    And The "internal" portfolio 1 total value should be "131,107" and "57.50" % in Parameters page
    And I should see the parameters table for the "internal" portfolio 2 as
      | order | name                                           | securityExposure               | volatilityRiskClass | complexityRiskClass | liquidityRiskClass | risk  | performance | datafeedStatus | dontSell | minSellPercentage | minSellValue | dontBuy | maxBuyPercentage | maxBuyValue | currentWeight | value  |
      | 1     | Aberdeen Global - Asian Smaller Companies Fund | UCITS Funds - International    | H                   | L                   | L                  | 36.44 | 10.31       | VALID          | false    | 0.00              |              | false   | 100.00           |             | 34.41         | 78,473 |
      | 2     | Aberdeen Global - Emerging Markets Equity Fund | UCITS Funds - Emerging Markets | H                   | L                   | L                  | 17.62 | 4.55        | VALID          | false    | 0.00              |              | false   | 100.00           |             | 4.23          | 9,648  |
    And The "internal" portfolio 2 total value should be "88,122" and "38.64" % in Parameters page
    And I should see the parameters table for the "external" portfolio 1 as
      | order | name                        | securityExposure               | volatilityRiskClass | complexityRiskClass | liquidityRiskClass | risk  | performance | datafeedStatus | dontSell | minSellPercentage | minSellValue | dontBuy | maxBuyPercentage | maxBuyValue | currentWeight | value |
      | 1     | Bankinvest Asien            | UCITS Funds - Emerging Markets | H                   | L                   | L                  | 10.46 | 4.22        | VALID          | false    | 0.00              |              | true    | 100.00           |             | 2.77          | 6,324 |
      | 2     | Bankinvest Basis Aktier Udb | UCITS Funds - International    | H                   | L                   | L                  | 13.59 | 7.20        | VALID          | false    | 0.00              |              | true    | 100.00           |             | 0.49          | 1,110 |
      | 3     | Storebrand Barnespar        | UCITS Funds - International    | H                   | L                   | L                  | 11.73 | 7.61        | VALID          | false    | 0.00              |              | true    | 100.00           |             | 0.60          | 1,366 |
    And The "external" portfolio 1 total value should be "8,803" and "3.86" % in Parameters page
