Feature: NAVIGEA-327-- AV - Results page: Transaction Table shows wrong portfolios
   		 NAVIGEA-339-- PO - AV General: Summary Tables and Summary Rows: Don't show decimals for values
  	(in this feature only the AV-Result page)
  	 	 NAVIGEA-570:GENERAL: Rename Portfolio Types (names to be displayed)
         NAVIGEA-575: AV - Results: Transaction table - the values that are shown in Value NOK in Transaction page are not correct
         NAVIGEA-586: AV - Results: Results Table - The Recommended "weight%" and "Value NOK" values in Result summary are not correct

  @dev @NAVIGEA-327 @NAVIGEA-570 @NAVIGEA-575 @NAVIGEA-339-V1 @NAVIGEA-586 @Advisory_module @Result-page
  Scenario: NAVIGEA-327_NAVIGEA-339-V1_@NAVIGEA-586--Results page: Check the portfolios that are shown in Transaction page.
    Given I am logged in with organization "TetralogSystemsQA" and CrmKey "SeleniumTestPortfolio_Default"
    When I navigate to the "Advisory" module
    And I select the "Analysis" page
    And In the Basic advisory setup area, I select the following checkboxes:
      | name                                    | state |
      | Internal active - Internal portfolio 1  | true  |
      | Internal passive - Internal portfolio 2 | true  |
      | External - External portfolio           | true  |
    And I select the "Result" page
    Then The app should show the "internal" portfolio 1 table in Results page as
      | oder | name                                           | securityExposure               | volatilityRiskClass | complexityRiskClass | liquidityRiskClass | risk  | performance | currentShare | currentWeight% | currentValueNOK | recommendedShare | recommendedWeight% | recommendedValueNOK |
      | 1    | BankInvest New Emerging Markets Equities       | UCITS Funds - Emerging Markets | H                   | L                   | L                  | 10.12 | 5.45        | 0            | 0.00           | 0               | 20               | 10.70              | 24,391              |
      | 2    | East Capital Rysslandfonden                    | UCITS Funds - Emerging Markets | H                   | L                   | L                  | 9.61  | 4.23        | 0            | 0.00           | 0               | 120              | 8.77               | 20,000              |
      | 3    | GAM Star China Equity USD Acc                  | UCITS Funds - Emerging Markets | H                   | L                   | L                  | 5.36  | 4.03        | 0            | 0.00           | 0               | 963              | 59.12              | 134,821             |
      | 4    | AB Global Growth Trends A USD Acc              | UCITS Funds - International    | H                   | L                   | L                  | 14.17 | 0.70        | 120          | 9.86           | 22,493          | 226              | 18.63              | 42,493              |
      | 5    | Aberdeen Global - Asian Smaller Companies Fund | UCITS Funds - International    | H                   | L                   | L                  | 36.44 | 10.31       | 120          | 47.63          | 108,613         | 0                | 0.00               | 0                   |
    And The "internal" portfolio 1 total value should be "221,707"and "97.23" % in Results page
    And The app should show the "Internal portfolio 2" table in "Results" page as
      | oder | name                                           | securityExposure               | volatilityRiskClass | complexityRiskClass | liquidityRiskClass | risk  | performance | currentShare | currentWeight% | currentValueNOK | recommendedShare | recommendedWeight% | recommendedValueNOK |
      | 1    | Aberdeen Global - Asian Smaller Companies Fund | UCITS Funds - International    | H                   | L                   | L                  | 36.44 | 10.31       | 86           | 34.41          | 78,473          | 0                | 0.00               | 0                   |
      | 2    | Aberdeen Global - Emerging Markets Equity Fund | UCITS Funds - Emerging Markets | H                   | L                   | L                  | 17.62 | 4.55        | 11           | 4.23           | 9,648           | 0                | 0.00               | 0                   |
    And The "Internal portfolio 2" total value should be "0"and "0.00" % in Results page
    And The app should show the "External portfolio" table in "Results" page as
      | oder | name                        | securityExposure               | volatilityRiskClass | complexityRiskClass | liquidityRiskClass | risk  | performance | currentShare | currentWeight% | currentValueNOK | recommendedShare | recommendedWeight% | recommendedValueNOK |
      | 1    | Bankinvest Asien            | UCITS Funds - Emerging Markets | H                   | L                   | L                  | 10.46 | 4.22        | 15           | 2.77           | 6,324           | 15               | 2.77               | 6,324               |
      | 2    | Bankinvest Basis Aktier Udb | UCITS Funds - International    | H                   | L                   | L                  | 13.59 | 7.20        | 8            | 0.49           | 1,110           | 0                | 0.00               | 0                   |
      | 3    | Storebrand Barnespar        | UCITS Funds - International    | H                   | L                   | L                  | 11.73 | 7.61        | 28           | 0.60           | 1,366           | 0                | 0.00               | 0                   |
    And The "External portfolio" total value should be "6,325" and "2.77" % in Results page
    And The app should show "Internal active" portfolio type in "Internal portfolio 1" internal portfolio in "Results" page
    And The app should show "Internal passive" portfolio type in "Internal portfolio 2" internal portfolio in "Results" page
    And The app should show "External" portfolio type in "External portfolio" external portfolio in "Results" page
    Then I should see the Results summary table expanded in the following way
      | Summary                | Weight% | Value NOK |
      | Internal portfolios    |         |           |
      | Current                | 96.14   | 219,229   |
      | Additional investments | 0.00    | 0         |
      | Withdrawals            | 0.00    | 0         |
      | Recommended            | 97.23   | 221,707   |
      | External portfolios    |         |           |
      | Current                | 3.86    | 8,803     |
      | Recommended            | 2.77    | 6,325     |
      | Current portfolios     | 100.00  | 228,032   |
      | Additional investments | 0.00    | 0         |
      | Withdrawals            | 0.00    | 0         |
      | Recommended portfolios | 100.00  | 228,032   |
      | Balance                | 0.00    | 0         |

  @dev @NAVIGEA-327  @NAVIGEA-570 @NAVIGEA-575 @NAVIGEA-339-V2 @Advisory_module @Transaction-page
  Scenario: NAVIGEA-327_NAVIGEA-339-V2- -Transaction page: Check the portfolios that are shown in Transaction page.
    Given I am logged in with organization "TetralogSystemsQA" and CrmKey "SeleniumTestPortfolio_Default"
    When I navigate to the "Advisory" module
    And I select the "Analysis" page
    And In the Basic advisory setup area, I select the following checkboxes:
      | name                                    | state |
      | Internal active - Internal portfolio 1  | true  |
      | Internal passive - Internal portfolio 2 | true  |
      | External - External portfolio           | true  |
    And I select the "Result" page
    And I click on the "Show transactions" button in the Result page
    Then I should see the following values in the Buy recommendation table for the internal portfolio "Internal portfolio 1" in Transaction page.
      | order | name                                     | securityExposure               | volatilityRiskClass | complexityRiskClass | liquidityRiskClass | shareCurrent | shareRecommended | buySell(modify) | price  | currency | limitLower | limitUpper | valueBuySell |
      | 1    | BankInvest New Emerging Markets Equities | UCITS Funds - Emerging Markets | H                   | L                   | L                  | 0            | 20               | 20.141          | 164.00 | EUR      |            |            | 24,391       |
      | 2    | East Capital Rysslandfonden              | UCITS Funds - Emerging Markets | H                   | L                   | L                  | 0            | 120              | 120.253         | 194.00 | SEK      |            |            | 19,999       |
      | 3    | GAM Star China Equity USD Acc            | UCITS Funds - Emerging Markets | H                   | L                   | L                  | 0            | 963              | 963.013         | 140.00 | NOK      |            |            | 134,821      |
      | 4    | AB Global Growth Trends A USD Acc        | UCITS Funds - International    | H                   | L                   | L                  | 120          | 226              | 106.696         | 33.55  | USD      |            |            | 19,999       |
    And I should see the following values in the Sell recommendation table for the internal portfolio "Internal portfolio 1" in Transaction page.
      | order | name                                           | securityExposure            | volatilityRiskClass | complexityRiskClass | liquidityRiskClass | shareCurrent | shareRecommended | buySell(modify) | price  | currency | limitLower | limitUpper | valueBuySell |
      | 5    | Aberdeen Global - Asian Smaller Companies Fund | UCITS Funds - International | H                   | L                   | L                  | 120          | 0                | 120.000         | 162.00 | USD      |            |            | 108,613      |
#    And I should see an empty table in the Buy recommendation for the internal portfolio "Internal Portfolio 2" in Transaction page.
    # none
    And I should see the following values in the Sell recommendation table for the internal portfolio "Internal portfolio 2" in Transaction page.
      | order | name                                           | securityExposure               | volatilityRiskClass | complexityRiskClass | liquidityRiskClass | shareCurrent | shareRecommended | buySell(modify) | price  | currency | limitLower | limitUpper | valueBuySell |
      | 1    | Aberdeen Global - Asian Smaller Companies Fund | UCITS Funds - International    | H                   | L                   | L                  | 86           | 0                | 86.700          | 162.00 | USD      |            |            | 78,473       |
      | 2    | Aberdeen Global - Emerging Markets Equity Fund | UCITS Funds - Emerging Markets | H                   | L                   | L                  | 11           | 0                | 11.000          | 157.00 | USD      |            |            | 9,648        |
#    And I should see an empty table in the Buy recommendation table for the external portfolio "External Portfolio" in Transaction page.
    #none
    And I should see the following values in the Sell recommendation table for the external portfolio "External Portfolio" in Transaction page.
      | oder | name                        | securityExposure            | volatilityRiskClass | complexityRiskClass | liquidityRiskClass | shareCurrent | shareRecommended | buySell(modify) | price  | currency | limitLower | limitUpper | valueBuySell |
      | 1    | Bankinvest Basis Aktier Udb | UCITS Funds - International | H                   | L                   | L                  | 8            | 0                | 8.700           | 129.00 | DKK      |            |            | 1,110        |
      | 2    | Storebrand Barnespar        | UCITS Funds - International | H                   | L                   | L                  | 28           | 0                | 28.000          | 48.81  | NOK      |            |            | 1,366        |
    And The app should show "Internal active" portfolio type in "Internal portfolio 1" internal portfolio in "Transaction" page
    And The app should show "Internal passive" portfolio type in "Internal portfolio 2" internal portfolio in "Transaction" page
    And The app should show "External" portfolio type in "External portfolio" external portfolio in "Transaction" page
    Then I should see the Transactions summary table expanded in the following way
      | Summary                | Value NOK |
      | Internal portfolios    |           |
      | Additional investments | 0         |
      | Withdrawals            | 0         |
      | Buy recommendations    | 199,213   |
      | Sell recommendations   | 196,736   |
      | External portfolios    |           |
      | Sell recommendations   | 2,478     |
      | Additional investments | 0         |
      | Withdrawals            | 0         |
      | Buy recommendations    | 199,213   |
      | Sell recommendations   | 196,736   |
      | Balance                | 0         |
