Feature: NAVIGEA-769-- AV - Results page: Security class - zero values should not be shown
  	 	 NAVIGEA-809-- AV - Results page: Security exposure - zero values should not be shown

        
  @active @NAVIGEA-769 @Advisory_module @Result-page @Security_exposure
  Scenario: NAVIGEA-769-- AV - Results page: Security class - values in zero should not be shown
    Given I am logged in with organization "TetralogSystemsQA" and CrmKey "QaTestData01"
    When I navigate to the "Advisory" module
    And In the Basic advisory setup area, I select the following checkboxes:
      | name                           | state |
      |  Internal active - Active PTF  | true  |
    And I select the "Result" page
    And I select the "Portfolio structure" option in the Select Analysis dropdown
    And I select the "Security class" in Show structure by dropdown
    Then The Portfolio structure table should not contain rows with zero in all columns 
    
  @active @NAVIGEA-809 @Advisory_module @Result-page @Security_exposure
  Scenario: NAVIGEA-809-- AV - Results page: Security exposure - zero values should not be shown
    Given I am logged in with organization "TetralogSystemsQA" and CrmKey "QaTestData01"
    When I navigate to the "Advisory" module
    And In the Basic advisory setup area, I select the following checkboxes:
      | name                           | state |
      |  Internal active - Active PTF  | true  |
    And I select the "Result" page
    And I select the "Portfolio structure" option in the Select Analysis dropdown
    And I select the "Security exposure" in Show structure by dropdown
    Then The Portfolio structure table should not contain rows with zero in all columns 