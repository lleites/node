Feature: When setting "Min %" in the "Setup" page, "MIn Value NOK" entry field gets updated to wrong value

  @dev @Navigea-313 @Navigea-313-v1 @Advisory_module @Parameters_page
  Scenario: Navigea-313-v1: Enter a "Min%" and "Max%".Check the Min Value NOK and Max Value NOK values.
    Given I am logged in with organization "TetralogSystemsQA" and CrmKey "QaTestData01"
    And I navigate to the "Advisory" module
    And I select the "Analysis" page
    And In the Basic advisory setup area, I select the following checkboxes:
      | name                         | state |
      | Internal active - Active PTF | true  |
    And I select the "Setup" page
    And I click on the "Show Parameters" button
    And I enter the following values for the internal portfolio "Active PTF "
      | oder | name                                   | minSellPercentage | minSellValue | maxBuyPercentage | maxBuyValue |
      | 1    | Dexia Invest Lange Danske Obligationer | 10                |              | 20               |             |
      | 2    | SEB Asset Selection SEK (990)          | 30                |              | 100              |             |
    Then I should see the table "Active PTF" in Parameters page as
      | order | name                                   | securityExposure           | volatilityRiskClass | complexityRiskClass | liquidityRiskClass | datafeedStatus | dontSell | minSellPercentage | minSellValue | dontBuy | maxBuyPercentage | maxBuyValue | currentWeight | value   |
      | 1     | Dexia Invest Lange Danske Obligationer | UCITS Funds - Fixed Income | H                   | L                   | L                  | VALID          | false    | 10                | 40,504.19    | false   | 20               | 81,008.37   | 51.32         | 207,863 |
      | 2     | SEB Asset Selection SEK (990)          | UCITS Funds - Alternatives | H                   | M                   | L                  | VALID          | false    | 30                | 121,512.56   | false   | 100              | 405,041.85  | 48.68         | 197,178 |

  @dev @Navigea-313 @Navigea-313-v2 @Advisory_module @Parameters_page
  Scenario: Navigea-313-v2: Enter a "Min Value NOK" and "Max Value NOK".Check the Min% and Max% values.
    Given I am logged in with organization "TetralogSystemsQA" and CrmKey "QaTestData01"
    And I navigate to the "Advisory" module
    And I select the "Analysis" page
    And In the Basic advisory setup area, I select the following checkboxes:
      | name                         | state |
      | Internal active - Active PTF | true  |
    And I select the "Setup" page
    And I click on the "Show Parameters" button
    And I enter the following values for the internal portfolio "Active PTF"
      | oder | name                                   | minSellPercentage | minSellValue | maxBuyPercentage | maxBuyValue |
      | 1    | Dexia Invest Lange Danske Obligationer |                   | 20,000.00    |                  | 202,000.00  |
      | 2    | SEB Asset Selection SEK (990)          |                   | 40,000.00    |                  | 405,042.00  |
    Then I should see the table "Active PTF" in Parameters page as
      | order | name                                   | securityExposure           | volatilityRiskClass | complexityRiskClass | liquidityRiskClass | datafeedStatus | dontSell | minSellPercentage | minSellValue | dontBuy | maxBuyPercentage | maxBuyValue | currentWeight | value   |
      | 1     | Dexia Invest Lange Danske Obligationer | UCITS Funds - Fixed Income | H                   | L                   | L                  | VALID          | false    | 4.94              | 20,000.00    | false   | 49.87            | 202,000.00  | 51.32         | 207,863 |
      | 2     | SEB Asset Selection SEK (990)          | UCITS Funds - Alternatives | H                   | M                   | L                  | VALID          | false    | 9.88              | 40,000.00    | false   | 100.00           | 405,042.00  | 48.68         | 197,178 |
