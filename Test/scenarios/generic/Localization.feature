Feature: Localization

	@fix
	Scenario: MessageResource handling vs. Language preference works (Language: de)
		Given Fake MessageResources for language "de" are uploaded
			  # Note: xX ... Xx around message resources to avoid accidental matches
			  """
			  portfolio.summary = xX Übersicht Xx
			  portfolio.summary.internal = xX Summe Intern Xx
			  portfolio.summary.external = xX Summe Extern Xx
			  portfolio.summary.total = xX Alles in allem Xx
			  """
		  And Language preference is set to "de"
		 When I am logged in the application
    	  And I navigate to the "Portfolio" module
		 Then I want to see the text "xX Übersicht Xx" as heading of the summary table
		  And I want to see the text "xX Summe Intern Xx" as display label for the sum of internal values
		  And I want to see the text "xX Summe Extern Xx" as display label for the sum of external values
		  And I want to see the text "xX Alles in allem Xx" as display label for the sum of total values
		  
	@fix
	Scenario: MessageResource handling vs. Language preference works (Language: en)
		Given Fake MessageResources for language "en" are uploaded
			  # Note: xX ... Xx around message resources to avoid accidental matches
			  """
			  portfolio.summary = xX Overview Xx
			  portfolio.summary.internal = xX Sum of internal Xx
			  portfolio.summary.external = xX Sum of external Xx
			  portfolio.summary.total = xX Wow, you're that rich: Xx
			  """
		  And Language preference is set to "en"
		 When I am logged in the application
    	  And I navigate to the "Portfolio" module
		 Then I want to see the text "xX Overview Xx" as heading of the summary table
		  And I want to see the text "xX Sum of internal Xx" as display label for the sum of internal values
		  And I want to see the text "xX Sum of external Xx" as display label for the sum of external values
		  And I want to see the text "xX Wow, you're that rich: Xx" as display label for the sum of total values
		  