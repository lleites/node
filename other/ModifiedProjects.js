var fs = require('fs');


function findModifiedProjects(lines) {
    for (var i = 0; i < lines.length; i++) {
        var splittedLines = lines[i].split("/") ;
        var index = splittedLines.indexOf("trunk");
         if (index==-1) {
            continue;    
         }
         
        var projectName = splittedLines[ index + 1 ]; 
        
        if (projectName === undefined) {
            continue;
        }
        
        if (splittedLines[ index + 2 ].search(projectName) != -1){
            projectName += ":" + splittedLines[ index + 2 ]
        }
            
        
        if (modifiedProjects.hasOwnProperty(projectName)){
            //modifiedProjects[projectName]=modifiedProjects[projectName]+1;
            modifiedProjects[projectName].push(lines[i]);
        }else {
            //modifiedProjects[projectName]=1;
            modifiedProjects[projectName]=[lines[i]];
        }
        
    }
}

console.log("Running..")
var data = fs.readFileSync('lines.txt', 'utf8');
var lines = data.split("\n");
var modifiedProjects = {};


findModifiedProjects(lines);
console.log(modifiedProjects);
fs.writeFileSync('result.txt', JSON.stringify(modifiedProjects), 'utf8');


