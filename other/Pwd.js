var sys = require('sys')

var exec = require('child_process').exec;

var child;

var execResult;

function resultCallback (error, stdout, stderr) {

    sys.print('stdout: ' + stdout);
    execResult=stdout;
    
    sys.print('stderr: ' + stderr);
    
    if (error !== null) {
    
    console.log('exec error: ' + error);
    
    }

    console.log(execResult);
} 

// executes `pwd`

child = exec("pwd", resultCallback );
