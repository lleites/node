#!/usr/bin/env node

var fs = require('fs');
var util = require('util');
var program = require('commander');

program.version('1.0.0')
    .option('-d, --dir [command]', 'Directory containing the features to report.', './Test/scenarios')
    .parse(process.argv);

var FeaturesTool = {};

FeaturesTool.walk = function(dir) {
    var results = [];
    var list = fs.readdirSync(dir);
    list.forEach(function(file) {
        file = dir + '/' + file;
        var stat = fs.statSync(file);
        if (stat && stat.isDirectory()) {
            results = results.concat(FeaturesTool.walk(file));
        }
        else {
            results.push(file);
        }
    });
    return results;
};

FeaturesTool.buildResult = function(scenariosDir, resultPath) {

    var files = FeaturesTool.walk(scenariosDir);
    //util.log("Files:" + JSON.stringify(files));
    util.log("Found: " + files.length + " files");
    files = files.filter(function(file) {
        return (file.search('.feature') !== -1);
    });
    util.log("Found: " + files.length + " feature files");

    var results = [];
    for (var fi = 0; fi < files.length; fi++) {

        var file = files[fi];
        util.log("Parsing File:" + file);
        var lines = fs.readFileSync(file, 'UTF8').split('\n');
        for (var i = 0; i < lines.length; i++) {
            var line = lines[i].trim();
            if (line[0] == '@') {
                var atValue = line.split(' ')[0];
                //util.log(atValue);
                results.push({
                    'at': atValue,
                    'data': {
                        'scenario': lines[++i].trim(),
                        'file': file,
                        'feature': lines[0]
                    }
                });

            }
        }
    }

    //util.log(JSON.stringify(results, undefined, 5));

    var recap = {};
    for (var ri = 0; ri < results.length; ri++) {
        var result = results[ri];
        if (recap.hasOwnProperty(result.at)) {
            recap[result.at].push(result.data);
        }
        else {
            recap[result.at] = [result.data];
        }
    }

    //util.log(JSON.stringify(recap, undefined, 5));
    fs.writeFileSync(resultPath, JSON.stringify(recap, undefined, 5), 'utf8');
    util.log('Done! Result: ' + resultFile);
};
var now = new Date();
now = now.getFullYear() + '-' + (now.getMonth() + 1) + '-' + now.getDate();
var resultFile = __dirname + '/results/Features-' + now + '.json';
FeaturesTool.buildResult(program.dir, resultFile );
